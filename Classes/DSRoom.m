// 
//  DSRoom.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "DSRoom.h"

#import "DSDevice.h"
#import "DSScene.h"
#import "DSMeter.h"

@implementation DSRoom 

@synthesize dirty;

@dynamic lastCalledScene;
@dynamic lastCalledShadeScene;
@dynamic lastCalledBlueScene;
@dynamic lastCalledAudioScene;
@dynamic lastCalledVideoScene;
@dynamic name;
@dynamic identifier;
@dynamic meterID;
@dynamic devices;
@dynamic scenes;
@dynamic sortOrder;
@dynamic dss;
@dynamic meter;
@dynamic group1present;
@dynamic group2present;
@dynamic group3present;
@dynamic group4present;
@dynamic group5present;


- (id) init {
	self = [super init];
	
	if (self) {
		allObserver = [NSMutableArray new];
	}
	
	return self;
}

- (DSScene *) getScene: (NSNumber*)sceneNo withGroup: (NSNumber*)group {
	if (!sceneNo) {
		return nil;
	}
    for (DSScene *scene in self.scenes) {
        if ([scene.sceneNo isEqual:sceneNo] && [scene.group isEqual:group]) {
            return scene;
        }
    }
    return nil;
}


- (void) addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context {
	if (!allObserver) {
		allObserver = [NSMutableArray new];
	}
	[super addObserver:observer forKeyPath:keyPath options:options context:context];
	
	NSMutableDictionary *d = [NSMutableDictionary new];
	[d setObject:observer forKey:keyPath];
	[allObserver addObject:d];
}

- (void) removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath {
	for (NSDictionary *dict in allObserver) {
		for (NSString *myKeypath in [dict allKeys]) {
			id theObserver = [dict objectForKey:myKeypath];
			
			if (observer == theObserver && [keyPath isEqualToString:myKeypath]) {
				[allObserver removeObject:dict];
                @try {
                    [super removeObserver:observer forKeyPath:keyPath];
                }
                @catch (NSException *exception) {
                    //just in case
                }
				return;
			}
		}
	}
	
    @try {
        [super removeObserver:observer forKeyPath:keyPath];
    }
    @catch (NSException *exception) {
        //just in case
    }
}

- (void)prepareForDeletion {
	while ([allObserver count] > 0) {
		NSDictionary *dict = [allObserver objectAtIndex:0];
		NSString *keypath = [[dict allKeys] objectAtIndex:0];
		id observer = [dict objectForKey:keypath];
		
		//if ([observer respondsToSelector:@selector(releaseObservation)]) {
		[self removeObserver:observer forKeyPath:keypath];
			//[observer releaseObservation];
		//}
	}

	[super prepareForDeletion];
}

- (void)willTurnIntoFault {
	for (NSDictionary *dict in allObserver) {
		NSString *keypath = [[dict allKeys] objectAtIndex:0];
		id observer = [dict objectForKey:keypath];
		
		//if ([observer respondsToSelector:@selector(releaseObservation)]) {
		[self removeObserver:observer forKeyPath:keypath];
		//[observer releaseObservation];
		//}
	}
	
	[super willTurnIntoFault];
}

@end
