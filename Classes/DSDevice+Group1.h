//
//  DSDevice+Group1.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "DSDevice.h"

#define kDeviceOutputModeDisabled 0
#define kDeviceOutputModeSwitched 16
#define kDeviceOutputModeDimmed 22
#define kDeviceOutputModeSwitched_2_Step 34
#define kDeviceOutputModeSwitchedSinglePolarity 35
#define kDeviceOutputModeSwitched_3_Step 38
#define kDeviceOutputModeSwitchedRelay 39
#define kDeviceOutputModeSwitchedDoublePolarity 43
#define kDeviceOutputModeDimmed_0_10V 49
#define kDeviceOutputModeDimmed_1_10V 51

#define kDeviceGtinGE_KM200 @"4290046000010"
#define kDeviceGtinGE_TKM210 @"4290046000027"
#define kDeviceGtinGE_SDM200 @"4290046000034"
#define kDeviceGtinGE_SDS200 @"7640156790221"
#define kDeviceGtinGE_SDS201 @"7640156790238"
#define kDeviceGtinGE_SDS220 @"7640156790214"
#define kDeviceGtinGE_TKM220 @"4290046000201"
#define kDeviceGtinGE_TKM230 @"4290046000218"
#define kDeviceGtinGE_KL200 @"4290046000195"
#define kDeviceGtinGE_UMV200 @"7640156790603"
#define kDeviceGtinSW_KL200 @"4290046000959"
#define kDeviceGtinSW_ZWS200_J @"4290046000935"
#define kDeviceGtinSW_ZWS200_F @"4290046000942"
#define kDeviceGtinSW_ZWS200_EF @"7640156790481"
#define kDeviceGtinGE_UMR200 @"7640156790597"
#define kDeviceGtinSW_KL213 @"7640156791679"
#define kDeviceGtinSW_KL214 @"7640156791686"
#define kDeviceGtinGE_SDM201 @"7640156791655"
#define kDeviceGtinGE_SDM202 @"7640156791662"

#define kDevideOemEanGE_SDS220HalterEdition @"7640156790436"

@interface DSDevice (Group1)

-(NSArray*)availableOutputModes;
-(NSString*)outputModeName;

-(BOOL)isOutputModeReadonly;

+(NSString*)nameForOutputMode:(NSInteger)outputMode;

-(BOOL)isOutputModeDimmable;

@end
