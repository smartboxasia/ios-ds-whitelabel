//
//  SB_iPhoneAppDelegate.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class SB_iPhoneViewController;

@interface SB_iPhoneAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    SB_iPhoneViewController *viewController;
	NSArray *screenModes;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet SB_iPhoneViewController *viewController;

-(BOOL)isTestVersion;

@end

