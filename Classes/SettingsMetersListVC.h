//
//  SettingsMetersListVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class CustomLabel;

@interface SettingsMetersListVC : GAITrackedViewController {
	IBOutlet UIScrollView *scrollView;
	NSMutableArray *childControllers;
    IBOutlet CustomLabel *titleLabel;
}

@property(nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (void) buildScrollView;
- (IBAction) actionBack;

@end
