//
//  SettingsDevicesListVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDevicesListVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "SettingsDevicesListItemVC.h"
#import "CustomLabel.h"


@implementation SettingsDevicesListVC

@synthesize scrollview;
@synthesize background;

- (id) initWithRoom: (DSRoom*) theRoom{
	if(self = [super init]) {
        currentRoom = theRoom;
    }
	return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    titleLabel.text = NSLocalizedString(@"key_devices_title", nil);

    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

    scrollviewItems = [[NSMutableArray alloc] initWithCapacity:0];
    [self buildScrollview];
    
	[[ModelLocator instance] addObserver:self forKeyPath:k_DATA_DEVICELIST_HAS_CHANGED options:0 context:nil];

    self.screenName = @"Settings Devices";
}

- (void)buildScrollview{
    int y = 0;
    int i = 0;
    
    if ([currentRoom.devices count] > 0) {
        NSSortDescriptor *sorterDevices_group = [[NSSortDescriptor alloc] initWithKey:@"group" ascending:YES];
        NSSortDescriptor *sorterDevices_name = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        
        NSArray *sortedDevices = [currentRoom.devices sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sorterDevices_group, sorterDevices_name,nil]];
        [sorterDevices_group release];
        [sorterDevices_name release];
        
        for (DSDevice* device in sortedDevices) {
            SettingsDevicesListItemVC *item = [[SettingsDevicesListItemVC alloc] initWithDevice:device andDarkBg:i%2==1];
            item.view.frame = CGRectMake(-1, y, item.view.frame.size.width, item.view.frame.size.height);
            [self.scrollview addSubview:item.view];
            y += item.view.frame.size.height;
            i++;
        }
    }
	
	self.scrollview.contentSize = CGSizeMake(275, y);
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_DATA_DEVICELIST_HAS_CHANGED]) {
		[self buildScrollview];
	}
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [titleLabel release];
    [super dealloc];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_DEVICELIST_HAS_CHANGED];
    }
    @catch (NSException *exception) {
        //just in case
    }

}


@end
