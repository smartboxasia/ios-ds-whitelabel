//
//  MainVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "MainVC.h"
#import "SplashViewVC.h"
#import "ModelLocator.h"
#import "InfoBarVC.h"
#import "OverviewRoomListVC.h"
#import "FavoriteBarVC.h"
#import "RoomSceneListVC.h"
#import "InfoBarSmallVC.h"
#import "RoomDeviceListVC.h"
#import "DataController.h"
#import "SettingsVC.h"
#import "EnergieAmpelVC.h"
#import "NSTimer+blocks.h"
#import "StartupServerSelectionVC.h"
#import "JustifyVC.h"
#import "JustifyShadeVC.h"
#import "DSDevice.h"
#import "LoginWaitingVC.h"
#import "StartupController.h"
#import "WelcomeLocalVC.h"
#import "WelcomeDefaultVC.h"
#import "GAITrackedViewController.h"
#import "GAIHelper.h"

@implementation MainVC

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    GSLog(@"log");

    [super viewDidLoad];

    sHeight = [UIScreen mainScreen].bounds.size.height;
    sWidth  = [UIScreen mainScreen].bounds.size.width;

    [self.view setFrame:CGRectMake(0, 0, sWidth, sHeight)];

	shouldShowWaitingView = NO;
	alertUpdater = nil;

	[[ModelLocator instance] setValue:self forKey:k_VIEW_MAIN];

	if ([[[DataController instance] fetchAllDss] count] > 0) {
		DSServer *dss = [[DataController instance] getLastDssInCoreData];

		if (dss) {
			[[StartupController instance] performDssConnectWithServer:dss];
		}
		SplashViewVC *vc = [SplashViewVC new];
		[self.view addSubview:vc.view];
		[self addChildViewController:vc];
	}
	else {
		WelcomeDefaultVC *welcomeVc = [[WelcomeDefaultVC alloc] init];
		[self.view addSubview:welcomeVc.view];
		[self addChildViewController:welcomeVc];
	}

}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    GSLog(@"log");

    [[NSNotificationCenter defaultCenter] addObserverForName:k_DATA_LOGIN_STARTED object:nil queue:nil usingBlock:^(NSNotification *arg1) {
		shouldShowWaitingView = YES;
		[NSTimer scheduledTimerWithTimeInterval:0 repeats:NO usingBlock:^(NSTimer *timer) {
			if (!shouldShowWaitingView) {
				return;
			}
			if (vcLoginWaiting && [vcLoginWaiting.view superview]) {
				return;
			}
			LoginWaitingVC *waiting = [LoginWaitingVC new];

            // //iphone 5 support.
            // if ([UIScreen mainScreen].scale == 2.f && sHeight == 568.0f) {
            //     [waiting.view setFrame:CGRectMake(0, 0, 320, 568)];
            // }
            [waiting.view setFrame:CGRectMake(0, 0, sWidth, sHeight)];

			waiting.view.alpha = 0;
			[self.view addSubview:waiting.view];
			vcLoginWaiting = waiting;
			[self.view bringSubviewToFront:waiting.view];

			[UIView animateWithDuration:0.4 animations:^{
				waiting.view.alpha = 1;
			}];
		}];
	}];

	[[NSNotificationCenter defaultCenter] addObserverForName:k_DATA_LOGIN_FAILED object:nil queue:nil usingBlock:^(NSNotification *arg1) {
		shouldShowWaitingView = NO;
	}];

	[[NSNotificationCenter defaultCenter] addObserverForName:k_DATA_LOGIN_SUCCESSFUL object:nil queue:nil usingBlock:^(NSNotification *arg1) {
		shouldShowWaitingView = NO;
	}];

	[[ModelLocator instance] addObserver:self forKeyPath:k_DATA_STRUCTURE_HAS_CHANGED options:0 context:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideBallGame:) name:@"BallGameDidDismiss" object:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    GSLog(@"log");

    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:k_DATA_LOGIN_STARTED object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:k_DATA_LOGIN_FAILED object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:k_DATA_LOGIN_SUCCESSFUL object:nil];
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_STRUCTURE_HAS_CHANGED];
    }
    @catch (NSException *exception) {
        //just in case
    }

}


#pragma mark -
#pragma mark Observer

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    GSLog(@"log");

	if ([keyPath isEqualToString:k_DATA_STRUCTURE_HAS_CHANGED]) {
//		GSLog(@"Changed: %@", [[ModelLocator instance] valueForKey:k_DATA_STRUCTURE_HAS_CHANGED]);


		if (!alertUpdater && [[[ModelLocator instance] valueForKey:k_DATA_STRUCTURE_HAS_CHANGED] isEqualToString:@"1"]) {
//			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"key_updating", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//			if (![[[ModelLocator instance] valueForKey:k_DATA_DEMOMODE_ACTIVE] boolValue]) {
//				[alert show];
//			}
//
//
//			UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
//
//			// Adjust the indicator so it is up a few pixels from the bottom of the alert
//			indicator.center = CGPointMake(alert.bounds.size.width / 2, alert.bounds.size.height - 50);
//			[indicator startAnimating];
//			[alert addSubview:indicator];
//			[indicator release];
//
//
//			alertUpdater = alert;
			// re-arrange view

			[self refreshOverview];
		}
		else if (alertUpdater && [[[ModelLocator instance] valueForKey:k_DATA_STRUCTURE_HAS_CHANGED] isEqualToString:@"0"]) {
//			[alertUpdater dismissWithClickedButtonIndex:0 animated:YES];
//			alertUpdater = nil;
		}


	}

}

#pragma mark -
#pragma mark UI manipulation

- (void) removeSplashAndAddOverview {
    GSLog(@"log");

	UIViewController *splashVC = [[ModelLocator instance] valueForKey:k_VIEW_SPLASH];
	UIViewController *welcomeVC = [[ModelLocator instance] valueForKey:k_VIEW_WELCOME];

	if (splashVC && [splashVC isKindOfClass:[SplashViewVC class]] && splashVC.view.superview == self.view) {
		[splashVC.view removeFromSuperview];
		[self addOverview];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_SPLASH];
		[splashVC release];
	}
	else if (welcomeVC && welcomeVC.view.superview == self.view) {
		[welcomeVC.view removeFromSuperview];
		[welcomeVC removeFromParentViewController];
		[self addOverview];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_WELCOME];
		[welcomeVC release];
	}
}

- (void) addOverview {
    GSLog(@"log");

	EnergieAmpelVC *energieAmpel = [EnergieAmpelVC new];

    // energieAmpel.view.frame = CGRectMake(0, 0, energieAmpel.view.frame.size.width, energieAmpel.view.frame.size.height);
    energieAmpel.view.frame = CGRectMake(0, 0, ceilf(sWidth*0.7), energieAmpel.view.frame.size.height);

	[energieAmpel showStandBy];
	[energieAmpel startWabern];
	[self.view addSubview:energieAmpel.view];
	[energieAmpel storeInML];

	if (![[ModelLocator instance] valueForKey:k_DSS_AVAILABLE]) {
		[energieAmpel showStandBy];
	}

	if ([[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] && [[[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] isEqualToString:@"0"]) {
		[energieAmpel showStandBy];
	}

	// [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(timerTick:) userInfo:energieAmpel repeats:YES];

	InfoBarVC *infoVc = [[InfoBarVC alloc] init];
    // infoVc.view.frame = CGRectMake(0, 0, infoVc.view.frame.size.width, infoVc.view.frame.size.height);
    infoVc.view.frame = CGRectMake(0, 0, sWidth, infoVc.view.frame.size.height);

	[self.view addSubview:infoVc.view];
	[infoVc storeInML];

	OverviewRoomListVC *overviewVc = [[OverviewRoomListVC alloc] init];
    // overviewVc.view.frame = CGRectMake(0, 0, overviewVc.view.frame.size.width, overviewVc.view.frame.size.height);
    // overviewVc.view.frame = CGRectMake(0, 0, sWidth, overviewVc.view.frame.size.height);
    overviewVc.view.frame = CGRectMake(0, 0, sWidth, sHeight-97);
	[self.view addSubview:overviewVc.view];

	[overviewVc storeInML];

	FavoriteBarVC *favoriteVc = [[FavoriteBarVC alloc] init];

    //iphone 5 support.
    // favoriteVc.view.frame = CGRectMake(favoriteVc.view.frame.origin.x, sHeight-favoriteVc.view.frame.size.height, favoriteVc.view.frame.size.width, favoriteVc.view.frame.size.height);
	favoriteVc.view.frame = CGRectMake(favoriteVc.view.frame.origin.x, sHeight-favoriteVc.view.frame.size.height, sWidth, favoriteVc.view.frame.size.height);
  
	[self.view addSubview:favoriteVc.view];

	SettingsVC *settings = [SettingsVC new];
    // settings.view.frame = CGRectMake(0, sHeight-42, settings.view.frame.size.width, settings.view.frame.size.height);
    settings.view.frame = CGRectMake(0, sHeight-42, sWidth, settings.view.frame.size.height);
  
	[self.view addSubview:settings.view];
	[self.view bringSubviewToFront:energieAmpel.view];
	[self.view bringSubviewToFront:infoVc.view];
	[self.view bringSubviewToFront:favoriteVc.view];
	[self.view bringSubviewToFront:settings.view];
}

- (void) showServerList {
    GSLog(@"log");

	if (![[ModelLocator instance] valueForKey:k_VIEW_SERVERLIST]) {
		StartupServerSelectionVC *serverSelection = [StartupServerSelectionVC new];
        serverSelection.view.frame = CGRectMake((self.view.frame.size.width/2)-serverSelection.view.frame.size.width/2, sHeight-270, serverSelection.view.frame.size.width, serverSelection.view.frame.size.height);
		serverSelection.view.alpha = 0;
		serverSelection.view.hidden = YES;
		[self.view addSubview:serverSelection.view];
	}

	StartupServerSelectionVC *selectionVC = [[ModelLocator instance] valueForKey:k_VIEW_SERVERLIST];

	if (!selectionVC.view.superview) {
		[self.view addSubview:selectionVC.view];
	}

	if (selectionVC.view.hidden || selectionVC.view.alpha == 0) {
		[[StartupController instance] startDssSearch];
		[self.view bringSubviewToFront:selectionVC.view];
		selectionVC.view.frame = CGRectMake((self.view.frame.size.width/2)-selectionVC.view.frame.size.width/2, 210, selectionVC.view.frame.size.width, selectionVC.view.frame.size.height);


		selectionVC.view.hidden = NO;

		[UIView animateWithDuration:0.4 animations:^{
			selectionVC.view.alpha = 1;
		}];
	}
	else {
		[[StartupController instance] stopDssSearch];
		[UIView animateWithDuration:0.4 animations:^{
			selectionVC.view.alpha = 0;
		} completion:^(BOOL finished) {
			selectionVC.view.hidden = YES;
		}];
	}
}

- (void) timerTick: (NSTimer*)timer {
    GSLog(@"log");

	//int r = rand() % 100;
	//GSLog(@"rand: %f", (float)r/100);

	//[[ModelLocator instance] setValue:[NSNumber numberWithInt:r] forKey:k_INFO_TOTAL_CONSUMPTION];
}

- (void) refreshOverview {
    GSLog(@"log");

	if ([[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST] && [((OverviewRoomListVC*)[[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST]).view superview]) {

		OverviewRoomListVC *overviewVc = [[OverviewRoomListVC alloc] init];
        // overviewVc.view.frame = CGRectMake(0, 0, overviewVc.view.frame.size.width, overviewVc.view.frame.size.height);
        overviewVc.view.frame = CGRectMake(0, 0, sWidth, sHeight-97);

		[self.view addSubview:overviewVc.view];

		[self.view bringSubviewToFront:((OverviewRoomListVC*)[[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST]).view];
		[self.view bringSubviewToFront:((EnergieAmpelVC*)[[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]).view];
		[self.view bringSubviewToFront:((InfoBarVC*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR]).view];
		[self.view bringSubviewToFront:((FavoriteBarVC*)[[ModelLocator instance] valueForKey:k_VIEW_FAVORITEBAR]).view];
		[self.view bringSubviewToFront:((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).view];

		[UIView animateWithDuration:0.3 animations:^{
			((OverviewRoomListVC*)[[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST]).view.alpha = 0;

		} completion:^(BOOL finished) {
			[((OverviewRoomListVC*)[[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST]).view removeFromSuperview];
			[((OverviewRoomListVC*)[[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST]) release];

			[overviewVc storeInML];
            // [alertUpdater dismissWithClickedButtonIndex:0 animated:YES];
            // alertUpdater = nil;
		}];
	}
}

- (void) arrangeViewOrder {
    GSLog(@"log");

	[self.view bringSubviewToFront: ((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_FAVORITEBAR]).view];
	[self.view bringSubviewToFront: ((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).view];
}


- (void) showSettings {
    GSLog(@"log");

	StartupServerSelectionVC *selectionVC = [[ModelLocator instance] valueForKey:k_VIEW_SERVERLIST];

	if (selectionVC && !selectionVC.view.hidden) {
		[self showServerList];
	}
	SettingsVC *settings = [[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];

	[UIView animateWithDuration:0.4 animations:^{
		settings.view.frame = CGRectMake(0, -8, settings.view.frame.size.width, settings.view.frame.size.height);
	}];

}

- (void) hideSettings {
    GSLog(@"log");

	SettingsVC *settings = [[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[UIView animateWithDuration:0.4 animations:^{
		settings.view.frame = CGRectMake(0, sHeight-42, settings.view.frame.size.width, settings.view.frame.size.height);
	}];
}

- (void) showAdjustmentScreen: (id) entity {
    GSLog(@"log");

	JustifyVC *justify = nil;

	if ([entity isKindOfClass:[DSDevice class]]) {
		justify = [[JustifyVC alloc] initWithDevice:(DSDevice*)entity];
	}
	else if ([entity isKindOfClass:[DSScene class]]) {
		justify = [[JustifyVC alloc] initWithScene:(DSScene*)entity];
	}

	if (!justify) {
		return;
	}

    GSLog(@"justify width: %f, height %f", justify.view.frame.size.width, justify.view.frame.size.height);
	justify.view.frame = CGRectMake(0, self.view.frame.size.height, justify.view.frame.size.width, justify.view.frame.size.height);

	[self.view addSubview:justify.view];
	[self.view bringSubviewToFront:((EnergieAmpelVC*)[[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]).view];
	[self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR]).view];
	[self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL]).view];
	[self arrangeViewOrder];

	[UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
		justify.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
	} completion:^(BOOL finished) {

	}];

}

- (void) showShadeAdjustmentScreen: (id) entity {
    GSLog(@"log");

	JustifyShadeVC *justify = nil;

	if ([entity isKindOfClass:[DSDevice class]]) {
		justify = [[JustifyShadeVC alloc] initWithDevice:(DSDevice*)entity];
	}
	else if ([entity isKindOfClass:[DSScene class]]) {
		justify = [[JustifyShadeVC alloc] initWithScene:(DSScene*)entity];
	}

	if (!justify) {
		return;
	}
	justify.view.frame = CGRectMake(0, self.view.frame.size.height, justify.view.frame.size.width, justify.view.frame.size.height);
	[self.view addSubview:justify.view];

	[self.view bringSubviewToFront:((EnergieAmpelVC*)[[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]).view];
	[self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR]).view];
	[self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL]).view];
	[self arrangeViewOrder];

	[UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
		justify.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
	} completion:^(BOOL finished) {

	}];

}

#pragma mark -
#pragma mark Animations

- (void) animateFromOverviewToRoomScenes: (DSRoom*) room {
    GSLog(@"log");

	InfoBarVC *infobar = [[ModelLocator instance] valueForKey:k_VIEW_INFOBAR];
	OverviewRoomListVC *roomlist = [[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST];

	// offset berechnen:
	int offset = 0;

	NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sorted = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [orderDescriptor release];
    [nameDescriptor release];

	int index = (int)[sorted indexOfObject:room];

	CGPoint point = ((OverviewRoomListVC*)[[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST]).scrollView.contentOffset;
    // GSLog(@"point: %f,%f", point.x, point.y);

	if (index >= 0) {
		offset = (index * 52) - point.y;
	}

	[[ModelLocator instance] setValue:[NSNumber numberWithInt:offset] forKey:k_VIEW_ROOM_SCENELIST_OFFSET];

	RoomSceneListVC *scenes = [[RoomSceneListVC alloc] initWithRoom:room andOffset:offset withIndex:0];
    // scenes.view.frame = CGRectMake(320, 50, scenes.view.frame.size.width, scenes.view.frame.size.height);
    // scenes.view.frame = CGRectMake(sWidth, 50, sWidth, scenes.view.frame.size.height);
    scenes.view.frame = CGRectMake(sWidth, 50, sWidth, sHeight-97);

	[self.view addSubview:scenes.view];

	InfoBarSmallVC *infobarSmall = [[InfoBarSmallVC alloc] initWithRoom:room];

	// [NSThread sleepForTimeInterval:2];
	// infobarSmall.view.frame = CGRectMake(320, 0, 320, 51);
	infobarSmall.view.frame = CGRectMake(sWidth, 0, sWidth, 51);

	[self.view addSubview:infobarSmall.view];

    infobarSmall.labelTitle.text = NSLocalizedString(@"key_ACTIVITIES", @"");

    // while ([infobarSmall retainCount] > 1) {
    //     [infobarSmall release];
    // }

	[self.view bringSubviewToFront:((EnergieAmpelVC*)[[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]).view];
	[self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR]).view];
	[self.view bringSubviewToFront:infobarSmall.view];

	[self arrangeViewOrder];

    // TODO: build animation

	[UIView animateWithDuration:0.5 animations:^{
        // scenes.view.frame = CGRectMake(0, 0, scenes.view.frame.size.width, scenes.view.frame.size.height);
        // scenes.view.frame = CGRectMake(0, 0, sWidth, scenes.view.frame.size.height);
        scenes.view.frame = CGRectMake(0, 0, sWidth, sHeight-97);

        // roomlist.view.frame = CGRectMake(-320, -50, roomlist.view.frame.size.width, roomlist.view.frame.size.height);
        // roomlist.view.frame = CGRectMake(-sWidth, -50, sWidth, roomlist.view.frame.size.height);
        roomlist.view.frame = CGRectMake(-sWidth, -50, sWidth, sHeight-97);

        // infobarSmall.view.frame = CGRectMake(0, -50, infobarSmall.view.frame.size.width, infobarSmall.view.frame.size.height);
        infobarSmall.view.frame = CGRectMake(0, -50, sWidth, infobarSmall.view.frame.size.height);

        // infobar.view.frame = CGRectMake(-320, -50, infobar.view.frame.size.width, infobar.view.frame.size.height);
        infobar.view.frame = CGRectMake(-sWidth, -50, sWidth, infobar.view.frame.size.height);

        /*infobarSmall.view.frame = CGRectMake(0, infobarSmall.view.frame.origin.y, infobarSmall.view.frame.size.width, infobarSmall.view.frame.size.height);
        scenes.view.frame = CGRectMake(0, infobarSmall.view.frame.size.height, scenes.view.frame.size.width, scenes.view.frame.size.height);
        */

        [scenes animateStartup];
	} completion:^(BOOL finished) {

		[infobar.view removeFromSuperview];
		[roomlist.view removeFromSuperview];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_INFOBAR];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_OVERVIEW_ROOMLIST];
		[infobar prepareDealloc];
		[infobar release];

		[roomlist prepareDealloc];
		[roomlist release];

		[infobarSmall storeInML];
		[scenes storeInML];
	}];
}

- (void) animateBackFromRoomScenes: (DSRoom*) room {
    GSLog(@"log");

	InfoBarSmallVC *infobarSmall = [[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL];
	RoomSceneListVC *scenes = [[ModelLocator instance] valueForKey:k_VIEW_ROOM_SCENELIST];

	InfoBarVC *infobar = [[InfoBarVC alloc] init];
	// infobar.view.frame = CGRectMake(-320, -50, infobar.view.frame.size.width, infobar.view.frame.size.height);
    infobar.view.frame = CGRectMake(-sWidth, -50, sWidth, infobar.view.frame.size.height);
	[self.view addSubview:infobar.view];

	// calculate offset for scrollview content
	NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sorted = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [orderDescriptor release];
    [nameDescriptor release];

	int index = (int)[sorted indexOfObject:room];

	int offset = 0;

	if (index >= 0) {
		offset = (index * 52) - [[[ModelLocator instance] valueForKey:k_VIEW_ROOM_SCENELIST_OFFSET] intValue];
	}

    OverviewRoomListVC *roomlist = [[OverviewRoomListVC alloc] initWithOffset:offset];
    // roomlist.view.frame = CGRectMake(-320, -50, roomlist.view.frame.size.width, roomlist.view.frame.size.height);
    // roomlist.view.frame = CGRectMake(-sWidth, -50, sWidth, roomlist.view.frame.size.height);
    roomlist.view.frame = CGRectMake(-sWidth, -50, sWidth, sHeight-97);
    [self.view addSubview:roomlist.view];

    [self.view bringSubviewToFront:((EnergieAmpelVC*)[[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]).view];
    [self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL]).view];
    [self.view bringSubviewToFront:infobar.view];
    [self arrangeViewOrder];

	// TODO: build animation

	[UIView animateWithDuration:0.5 animations:^{
        // infobar.view.frame = CGRectMake(0, 0, infobar.view.frame.size.width, infobar.view.frame.size.height);
        infobar.view.frame = CGRectMake(0, 0, sWidth, infobar.view.frame.size.height);

        // infobarSmall.view.frame = CGRectMake(320, 0, infobarSmall.view.frame.size.width, infobarSmall.view.frame.size.height);
        infobarSmall.view.frame = CGRectMake(sWidth, 0, sWidth, infobarSmall.view.frame.size.height);

        // roomlist.view.frame = CGRectMake(0, 0, roomlist.view.frame.size.width, roomlist.view.frame.size.height);
        // roomlist.view.frame = CGRectMake(0, 0, sWidth, roomlist.view.frame.size.height);
        roomlist.view.frame = CGRectMake(0, 0, sWidth, sHeight-97);

        // scenes.view.frame = CGRectMake(320, 50, scenes.view.frame.size.width, scenes.view.frame.size.height);
        // scenes.view.frame = CGRectMake(sWidth, 50, sWidth, scenes.view.frame.size.height);
        scenes.view.frame = CGRectMake(sWidth, 50, sWidth, sHeight-97);

	} completion:^(BOOL finished) {
        [infobarSmall.view removeFromSuperview];
        [scenes.view removeFromSuperview];
        [[ModelLocator instance] setValue:nil forKey:k_VIEW_INFOBAR_SMALL];
        [[ModelLocator instance] setValue:nil forKey:k_VIEW_ROOM_SCENELIST];
        // GSLog(@"rt count: %i", [infobarSmall retainCount]);
        [infobarSmall prepareDealloc];
        [infobarSmall release];
        // [infobarSmall release];
        //	GSLog(@"rt count: %i", [infobarSmall retainCount]);
        [scenes release];
        [infobar storeInML];
        [roomlist storeInML];
	}];
}

- (void) animateFromOverviewToRoomDevices: (DSRoom*) room {
    GSLog(@"log");

	InfoBarVC *infobar = [[ModelLocator instance] valueForKey:k_VIEW_INFOBAR];
	OverviewRoomListVC *roomlist = [[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST];

    // offset berechnen:
    int offset = 0;
    NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sorted = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [orderDescriptor release];
    [nameDescriptor release];

	int index = (int)[sorted indexOfObject:room];

	CGPoint point = ((OverviewRoomListVC*)[[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST]).scrollView.contentOffset;
    // GSLog(@"point: %f,%f", point.x, point.y);

	if (index >= 0) {
		offset = (index * 52) - point.y;
	}

	RoomDeviceListVC *devices = [[RoomDeviceListVC alloc] initWithRoom:room andOffset:offset withIndex:0];
	devices.cameFromOverview = YES;

	// devices.view.frame = CGRectMake(320, 50, devices.view.frame.size.width, devices.view.frame.size.height);
    // devices.view.frame = CGRectMake(sWidth, 50, sWidth, devices.view.frame.size.height);
    devices.view.frame = CGRectMake(sWidth, 50, sWidth, sHeight-97);
	[self.view addSubview:devices.view];

	InfoBarSmallVC *infobarSmall = [[InfoBarSmallVC alloc] initWithRoom:room];
	// infobarSmall.view.frame = CGRectMake(320, 0, 320, 51);
	infobarSmall.view.frame = CGRectMake(sWidth, 0, sWidth, 51);
	[self.view addSubview:infobarSmall.view];
	infobarSmall.labelTitle.text = NSLocalizedString(@"key_DEVICES", @"");

	[self.view bringSubviewToFront:((EnergieAmpelVC*)[[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]).view];
	[self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR]).view];
	[self.view bringSubviewToFront:infobarSmall.view];
	[self arrangeViewOrder];

	// TODO: build animation

	[UIView animateWithDuration:0.5 animations:^{
        // devices.view.frame = CGRectMake(0, 0, devices.view.frame.size.width, devices.view.frame.size.height);
        // devices.view.frame = CGRectMake(0, 0, sWidth, devices.view.frame.size.height);
        devices.view.frame = CGRectMake(0, 0, sWidth, sHeight-97);

        // roomlist.view.frame = CGRectMake(-320, -50, roomlist.view.frame.size.width, roomlist.view.frame.size.height);
        // roomlist.view.frame = CGRectMake(-sWidth, -50, sWidth, roomlist.view.frame.size.height);
        roomlist.view.frame = CGRectMake(-sWidth, -50, sWidth, sHeight-97);

        // infobarSmall.view.frame = CGRectMake(0, -50, infobarSmall.view.frame.size.width, infobarSmall.view.frame.size.height);
        infobarSmall.view.frame = CGRectMake(0, -50, sWidth, infobarSmall.view.frame.size.height);

        // infobar.view.frame = CGRectMake(-320, -50, infobar.view.frame.size.width, infobar.view.frame.size.height);
        infobar.view.frame = CGRectMake(-sWidth, -50, sWidth, infobar.view.frame.size.height);

        /*infobarSmall.view.frame = CGRectMake(0, infobarSmall.view.frame.origin.y, infobarSmall.view.frame.size.width, infobarSmall.view.frame.size.height);
        scenes.view.frame = CGRectMake(0, infobarSmall.view.frame.size.height, scenes.view.frame.size.width, scenes.view.frame.size.height);
        */
        [devices animateStartup];
	} completion:^(BOOL finished) {

		[infobar.view removeFromSuperview];
		[roomlist.view removeFromSuperview];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_INFOBAR];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_OVERVIEW_ROOMLIST];
		[infobar prepareDealloc];
		[infobar release];
		[roomlist release];

		[infobarSmall storeInML];
		[devices storeInML];
	}];
}

- (void) animateBackFromRoomDevices: (DSRoom*) room {
    GSLog(@"log");

	InfoBarSmallVC *infobarSmall = [[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL];
	RoomDeviceListVC *devices = [[ModelLocator instance] valueForKey:k_VIEW_ROOM_DEVICELIST];

	InfoBarVC *infobar = [[InfoBarVC alloc] init];

	// infobar.view.frame = CGRectMake(-320, -50, infobar.view.frame.size.width, infobar.view.frame.size.height);
	infobar.view.frame = CGRectMake(-sWidth, -50, sWidth, infobar.view.frame.size.height);

    [self.view addSubview:infobar.view];

	// calculate offset for scrollview content
	NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sorted = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [orderDescriptor release];
    [nameDescriptor release];

	int index = (int)[sorted indexOfObject:room];
	int offset = 0;

	if (index >= 0) {
		offset = (index * 52) - devices.offset;
	}

	OverviewRoomListVC *roomlist = [[OverviewRoomListVC alloc] initWithOffset:offset];
    // roomlist.view.frame = CGRectMake(-320, -50, roomlist.view.frame.size.width, roomlist.view.frame.size.height);
    // roomlist.view.frame = CGRectMake(-sWidth, -50, sWidth, roomlist.view.frame.size.height);
    roomlist.view.frame = CGRectMake(-sWidth, -50, sWidth, sHeight-97);
	[self.view addSubview:roomlist.view];

	[self.view bringSubviewToFront:((EnergieAmpelVC*)[[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]).view];
	[self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL]).view];
	[self.view bringSubviewToFront:infobar.view];
	[self arrangeViewOrder];

	// TODO: build animation

	[UIView animateWithDuration:0.5 animations:^{
        // infobar.view.frame = CGRectMake(0, 0, infobar.view.frame.size.width, infobar.view.frame.size.height);
        infobar.view.frame = CGRectMake(0, 0, sWidth, infobar.view.frame.size.height);

        // infobarSmall.view.frame = CGRectMake(320, 0, infobarSmall.view.frame.size.width, infobarSmall.view.frame.size.height);
        infobarSmall.view.frame = CGRectMake(sWidth, 0, sWidth, infobarSmall.view.frame.size.height);

        // roomlist.view.frame = CGRectMake(0, 0, roomlist.view.frame.size.width, roomlist.view.frame.size.height);
        // roomlist.view.frame = CGRectMake(0, 0, sWidth, roomlist.view.frame.size.height);
        roomlist.view.frame = CGRectMake(0, 0, sWidth, sHeight-97);

        // devices.view.frame = CGRectMake(320, 50, devices.view.frame.size.width, devices.view.frame.size.height);
        // devices.view.frame = CGRectMake(sWidth, 50, sWidth, devices.view.frame.size.height);
        devices.view.frame = CGRectMake(sWidth, 50, sWidth, sHeight-97);
	} completion:^(BOOL finished) {
		[infobarSmall.view removeFromSuperview];
		[devices.view removeFromSuperview];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_INFOBAR_SMALL];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_ROOM_DEVICELIST];
		[infobarSmall prepareDealloc];
		[infobarSmall release];
		[devices release];

		[infobar storeInML];
		[roomlist storeInML];
	}];
}

- (void) animateFromScenesToRoomDevices: (DSRoom*) room withIndex:(int)index {
    GSLog(@"log");

	InfoBarSmallVC *infobar_old = [[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL];
	RoomSceneListVC *scenes = [[ModelLocator instance] valueForKey:k_VIEW_ROOM_SCENELIST];
	RoomDeviceListVC *devices = [[RoomDeviceListVC alloc] initWithRoom:room andOffset:-10 withIndex:index];

	devices.cameFromOverview = NO;
    GSLog(@"ROOM DEVICE LIST WIDTH: %f, height :%f", sWidth, devices.view.frame.size.height);

	// devices.view.frame = CGRectMake(320, 0, devices.view.frame.size.width, devices.view.frame.size.height);
    // devices.view.frame = CGRectMake(sWidth, 0, sWidth, devices.view.frame.size.height);
    devices.view.frame = CGRectMake(sWidth, 0, sWidth, sHeight-97);
	[self.view addSubview:devices.view];

	InfoBarSmallVC *infobar_new = [[InfoBarSmallVC alloc] initWithRoom:room];
	// infobar_new.view.frame = CGRectMake(320, -50, 320, 51);
    infobar_new.view.frame = CGRectMake(sWidth, -50, sWidth, 51);
	[self.view addSubview:infobar_new.view];
	infobar_new.labelTitle.text = NSLocalizedString(@"key_DEVICES", @"");

	[self.view bringSubviewToFront:((EnergieAmpelVC*)[[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]).view];
	[self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL]).view];
	[self.view bringSubviewToFront:infobar_new.view];
	[self arrangeViewOrder];

	// TODO: build animation

	[UIView animateWithDuration:0.5 animations:^{
        // devices.view.frame = CGRectMake(0, 0, devices.view.frame.size.width, devices.view.frame.size.height);
        // devices.view.frame = CGRectMake(0, 0, sWidth, devices.view.frame.size.height);
        devices.view.frame = CGRectMake(0, 0, sWidth, sHeight-97);

        // scenes.view.frame = CGRectMake(-320, 0, scenes.view.frame.size.width, scenes.view.frame.size.height);
        // scenes.view.frame = CGRectMake(-sWidth, 0, sWidth, scenes.view.frame.size.height);
        scenes.view.frame = CGRectMake(-sWidth, 0, sWidth, sHeight-97);

        // infobar_new.view.frame = CGRectMake(0, -50, infobar_new.view.frame.size.width, infobar_new.view.frame.size.height);
        infobar_new.view.frame = CGRectMake(0, -50, sWidth, infobar_new.view.frame.size.height);

        // infobar_old.view.frame = CGRectMake(-320, -50, infobar_old.view.frame.size.width, infobar_old.view.frame.size.height);
        infobar_old.view.frame = CGRectMake(-sWidth, -50, sWidth, infobar_old.view.frame.size.height);

        /*infobarSmall.view.frame = CGRectMake(0, infobarSmall.view.frame.origin.y, infobarSmall.view.frame.size.width, infobarSmall.view.frame.size.height);
        scenes.view.frame = CGRectMake(0, infobarSmall.view.frame.size.height, scenes.view.frame.size.width, scenes.view.frame.size.height);
        */
        [devices animateStartup];
	} completion:^(BOOL finished) {

		[infobar_old.view removeFromSuperview];
		[scenes.view removeFromSuperview];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_INFOBAR_SMALL];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_ROOM_SCENELIST];
		[infobar_old prepareDealloc];
		[infobar_old release];
		[scenes release];

		[infobar_new storeInML];
		[devices storeInML];
	}];
}

- (void) animateBackFromRoomDevicesToScenes: (DSRoom*) room withIndex:(int)index {
    GSLog(@"log");

	InfoBarSmallVC *infobar_old = [[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL];
	RoomDeviceListVC *devices = [[ModelLocator instance] valueForKey:k_VIEW_ROOM_DEVICELIST];
	InfoBarSmallVC *infobar_new = [[InfoBarSmallVC alloc] initWithRoom:room];

	// infobar_new.view.frame = CGRectMake(-320, -50, infobar_new.view.frame.size.width, infobar_new.view.frame.size.height);
    infobar_new.view.frame = CGRectMake(-sWidth, -50, sWidth, infobar_new.view.frame.size.height);
	[self.view addSubview:infobar_new.view];
	infobar_new.labelTitle.text = NSLocalizedString(@"key_ACTIVITIES", @"");

	RoomSceneListVC *scenes = [[RoomSceneListVC alloc] initWithRoom:room andOffset:0 withIndex:index];
	// scenes.view.frame = CGRectMake(-320, 0, scenes.view.frame.size.width, scenes.view.frame.size.height);
    // scenes.view.frame = CGRectMake(-sWidth, 0, sWidth, scenes.view.frame.size.height);
    scenes.view.frame = CGRectMake(-sWidth, 0, sWidth, sHeight-97);

	[self.view addSubview:scenes.view];

	[self.view bringSubviewToFront:((EnergieAmpelVC*)[[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]).view];
	[self.view bringSubviewToFront:((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_INFOBAR_SMALL]).view];
	[self.view bringSubviewToFront:infobar_new.view];
	[self arrangeViewOrder];

	//TODO: build animation

	[UIView animateWithDuration:0.5 animations:^{
        // infobar_new.view.frame = CGRectMake(0, -50, infobar_new.view.frame.size.width, infobar_new.view.frame.size.height);
        infobar_new.view.frame = CGRectMake(0, -50, sWidth, infobar_new.view.frame.size.height);

        // infobar_old.view.frame = CGRectMake(320, -50, infobar_old.view.frame.size.width, infobar_old.view.frame.size.height);
        infobar_old.view.frame = CGRectMake(sWidth, -50, sWidth, infobar_old.view.frame.size.height);

        // scenes.view.frame = CGRectMake(0, 0, scenes.view.frame.size.width, scenes.view.frame.size.height);
        // scenes.view.frame = CGRectMake(0, 0, sWidth, scenes.view.frame.size.height);
        scenes.view.frame = CGRectMake(0, 0, sWidth, sHeight-97);

        // devices.view.frame = CGRectMake(320, 0, devices.view.frame.size.width, devices.view.frame.size.height);
        // devices.view.frame = CGRectMake(sWidth, 0, sWidth, devices.view.frame.size.height);
        devices.view.frame = CGRectMake(sWidth, 0, sWidth, sHeight-97);

	} completion:^(BOOL finished) {
		[infobar_old.view removeFromSuperview];
		[devices.view removeFromSuperview];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_INFOBAR_SMALL];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_ROOM_DEVICELIST];
		[infobar_old prepareDealloc];
		[infobar_old release];
		[devices release];

		[infobar_new storeInML];
		[scenes storeInML];
		scenes.offset = [[[ModelLocator instance] valueForKey:k_VIEW_ROOM_SCENELIST_OFFSET] intValue];
	}];
}



- (void)didReceiveMemoryWarning {
    GSLog(@"log");

    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    GSLog(@"log");

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)dealloc {
    GSLog(@"log");

    [super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) didHideBallGame:(NSNotification *)notification {
    GSLog(@"log");

    RoomDeviceListVC *devices = (RoomDeviceListVC *)[[ModelLocator instance] valueForKey:k_VIEW_ROOM_DEVICELIST];
    RoomSceneListVC *scenes = (RoomSceneListVC *)[[ModelLocator instance] valueForKey:k_VIEW_ROOM_SCENELIST];
    OverviewRoomListVC *overview = (OverviewRoomListVC*)[[ModelLocator instance] valueForKey:k_VIEW_OVERVIEW_ROOMLIST];

    if ( devices.view.superview ) {
        [GAIHelper trackViewControllerWithName:devices.screenName];
    }
    else if (scenes.view.superview) {
        [GAIHelper trackViewControllerWithName:scenes.screenName];
    }
    else if (overview.view.superview) {
        [GAIHelper trackViewControllerWithName:overview.screenName];
    }

}
@end
