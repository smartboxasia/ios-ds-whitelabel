//
//  SettingsNetworkNewConnectionVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"
#import "GAI.h"

@interface SettingsNetworkNewConnectionVC : GAITrackedViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;

@property (nonatomic, retain) IBOutlet UISegmentedControl *segmentTabBar;
@property (nonatomic, retain) IBOutlet UIView *viewTabCloud;
@property (nonatomic, retain) IBOutlet UIView *viewTabManual;

@property (nonatomic, retain) IBOutlet UITextField *textfieldName;
@property (nonatomic, retain) IBOutlet UITextField *textfieldEmail;
@property (nonatomic, retain) IBOutlet UITextField *textfieldPassword;
@property (nonatomic, retain) IBOutlet UITextField *textfieldIp;
@property (nonatomic, retain) IBOutlet UITextField *textfieldPort;

@property (nonatomic, retain) IBOutlet UITextField *urlChooserField;

@property (retain, nonatomic) IBOutlet CustomLabel *labelTitle;
@property (retain, nonatomic) IBOutlet CustomLabel *labelUseYour;
@property (retain, nonatomic) IBOutlet CustomLabel *labelPleaseNote;
@property (retain, nonatomic) IBOutlet CustomLabel *labelEmail;
@property (retain, nonatomic) IBOutlet CustomLabel *labelPassword;
@property (retain, nonatomic) IBOutlet CustomLabel *labelConnectByEntering;
@property (retain, nonatomic) IBOutlet CustomLabel *labelManualName;

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicatorConnecting;

@property (nonatomic, retain) IBOutlet UIButton *buttonConnectCloud;
@property (nonatomic, retain) IBOutlet UIButton *buttonConnectLocal;

- (IBAction) actionBack: (id)sender;
- (IBAction) actionConnect: (id)sender;
- (IBAction) actionTabChanged: (id)sender;

@end
