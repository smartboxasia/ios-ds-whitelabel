//
//  SettingsInfoBarVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsInfoBarVC.h"
#import "SettingsVC.h"
#import "SettingsInfoBarItemVC.h"
#import "ModelLocator.h"
#import "CustomLabel.h"

@implementation SettingsInfoBarVC

@synthesize scrollview;
@synthesize background;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    titleLabel.text = NSLocalizedString(@"key_infopanel_title", nil);
    
    // iphone 5 support.
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

	items = [NSMutableArray new];
	
	[self buildScrollview];
	
	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_INFOBAR_LIST];
    
    self.screenName = @"Settings Info";
}

- (void) buildScrollview {
	for (SettingsInfoBarItemVC *item in items) {
		[item.view removeFromSuperview];
		[item release];
	}
	[items removeAllObjects];
	
	NSInteger yOffset = 0;
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	NSInteger indexVerbrauch = 0;
	NSInteger indexWetter = 1;
	NSInteger indexName = 2;
	
	if ([defaults objectForKey:k_VIEW_LABEL_STROMVERBRAUCH]) {
		indexVerbrauch = [[defaults objectForKey:k_VIEW_LABEL_STROMVERBRAUCH] integerValue];
	}
	
	if ([defaults objectForKey:k_VIEW_LABEL_WETTER]) {
		indexWetter = [[defaults objectForKey:k_VIEW_LABEL_WETTER] integerValue];
	}
	
	if ([defaults objectForKey:k_VIEW_LABEL_DSS_NAME]) {
		indexName = [[defaults objectForKey:k_VIEW_LABEL_DSS_NAME] integerValue];
	}
	
	for (NSInteger i=0; i<3; i++) {
		NSString *name = nil;
		
		if (indexVerbrauch == i) {
			name = k_VIEW_LABEL_STROMVERBRAUCH;
		}
		else if (indexWetter == i) {
			name = k_VIEW_LABEL_WETTER;
		}
		else if (indexName == i) {
			name = k_VIEW_LABEL_DSS_NAME;
		}
		else {
			name = @"";
		}
		
		SettingsInfoBarItemVC *item = [[SettingsInfoBarItemVC alloc] initWithName:name andOffset:yOffset andMaximum:3];
		[items addObject:item];
		item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
		[self.scrollview addSubview:item.view];
		
		yOffset += item.view.frame.size.height;
	}
}

- (void) magneticPointHasChanged: (int) from toIndex: (int) to {
	if (to >= [items count] || to < 0 || from < 0 || from >= [items count]) {
		return;
	}
	SettingsInfoBarItemVC *itemHasMoved = (SettingsInfoBarItemVC*)[items objectAtIndex:from];
	SettingsInfoBarItemVC *itemToBeMoved = (SettingsInfoBarItemVC*)[items objectAtIndex:to];
	if (to>from) {
		[itemToBeMoved decrementMagneticPoint];
	}
	else {
		[itemToBeMoved incrementMagneticPoint];
	}
	
	[items removeObject:itemHasMoved];
	[items insertObject:itemHasMoved atIndex:to];
}

- (void) arrangeItems {
	for (int i = 0; i < [items count]; i++) {
		SettingsInfoBarItemVC *item = [items objectAtIndex:i];
		[item gotoPosition:i];
	}
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults synchronize];
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [titleLabel release];
    [super dealloc];
}


@end
