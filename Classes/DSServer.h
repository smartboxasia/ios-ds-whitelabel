//
//  DSServer.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <CoreData/CoreData.h>

#define DSS_CONNECTION_TYPE_MANUAL @"DSS_CONNECTION_TYPE_MANUAL"
#define DSS_CONNECTION_TYPE_CLOUD @"DSS_CONNECTION_TYPE_CLOUD"

@class DSMeter;
@class DSRoom;
@class DSScene;

@interface DSServer : NSManagedObject {
	NSMutableArray *allObserver;
}

@property (nonatomic, retain) NSString *macAddress;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *ip;
@property (nonatomic, retain) NSString *version;
@property (nonatomic, retain) NSString *appToken;
@property (nonatomic, retain) NSString *cloudEmail;
@property (nonatomic, retain) NSString *cloudUrl;
@property (nonatomic, retain) NSString *cloudPassword;
@property (nonatomic, retain) NSString *connectionName;
@property (nonatomic, retain) NSString *connectionType;
@property (nonatomic, retain) NSNumber *port;
@property (nonatomic, retain) NSDate *lastConnectionTS;
@property (nonatomic, retain) NSSet *meters;
@property (nonatomic, retain) NSSet *rooms;
@property (nonatomic, retain) NSSet *scenes;
@property (nonatomic, assign) BOOL detectedByBonjour;

@property (nonatomic, assign) BOOL connected;

+ (NSInteger) getPortFromUrl: (NSString *) urlString;

- (BOOL) isRemoteDss;
- (DSRoom *) getRoom: (NSString*) identifier;
- (DSMeter *) getMeter: (NSString*) identifier;
- (DSScene *) getScene: (NSNumber*) sceneNo inRoom: (DSRoom*) room withGroup:(NSNumber*)group;
- (DSScene *) getHighLevelEvent: (NSNumber*) withId;
@end

@interface DSServer (CoreDataGeneratedAccessors)
- (void)addMetersObject:(DSMeter *)value;
- (void)removeMetersObject:(DSMeter *)value;
- (void)addMeters:(NSSet *)value;
- (void)removeMeters:(NSSet *)value;

- (void)addRoomsObject:(DSRoom *)value;
- (void)removeRoomsObject:(DSRoom *)value;
- (void)addRooms:(NSSet *)value;
- (void)removeRooms:(NSSet *)value;

- (void)addScenesObject:(DSScene *)value;
- (void)removeScenesObject:(DSScene *)value;
- (void)addScenes:(NSSet *)value;
- (void)removeScenes:(NSSet *)value;

- (void)addScenes:(NSSet *)value;
- (void)removeScenes:(NSSet *)value;

@end