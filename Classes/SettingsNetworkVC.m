//
//  SettingsNetworkVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsNetworkVC.h"
#import "SettingsNetworkItemVC.h"
#import "ModelLocator.h"
#import "SettingsVC.h"
#import "StartupController.h"
#import "DataController.h"
#import "DSServer.h"
#import "SettingsNetworkNewConnectionVC.h"

@implementation SettingsNetworkVC

@synthesize scrollview;
@synthesize background;

@synthesize viewHeadManualConnections, viewHeadCloudConnections;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = NSLocalizedString(@"key_settings_connection_title", nil);
    self.digitalStromLabel.text = NSLocalizedString(@"key_settings_connection_type_cloud_label", nil);
    self.manualLabel.text = NSLocalizedString(@"key_settings_connection_type_manual_label", nil);
    
	// iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

	serverItems = [NSMutableArray new];
    
    self.screenName = @"Settings Servers";
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self buildScrollview];
	[[ModelLocator instance] addObserver:self forKeyPath:@"serverlist" options:0 context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(autoDetectedDssListChanged) name:kNotificationDssListChanged object:Nil];
    
    //reload servers when screen appears
    [self actionReloadServers];

}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:@"serverlist"];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationDssListChanged object:Nil];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

- (void) autoDetectedDssListChanged {
	[self buildScrollview];
}

- (void) buildScrollview {
	
	for (SettingsNetworkItemVC *vc in serverItems) {
		if (vc.view.superview) {
			[vc.view removeFromSuperview];
		}
	}
	
	[serverItems removeAllObjects];
	
	NSInteger yOffset = 0;
	NSInteger counter = 0;
//    
//    //add known servers
//    NSArray *knownServers = [[DataController instance] allocAllKnownDss];
//	
//    if (knownServers) {
//        for (int i = 0; i<[knownServers count] ; i++) {
//            if (![[ModelLocator instance].serverlist containsObject:[knownServers objectAtIndex:i]]){
//                [[ModelLocator instance].serverlist addObject:[knownServers objectAtIndex:i]];
//            }
//        }
//        [ModelLocator instance].serverlist = [ModelLocator instance].serverlist;
//    }
    
	NSArray *dssList = [[DataController instance] fetchAllDss];
	NSMutableArray *manualDssList = [NSMutableArray new];
	NSMutableArray *cloudDssList = [NSMutableArray new];
	
	for (NSString *serverIp in [ModelLocator instance].localServerAddresses) {
		
		bool dssFound = NO;
		DSServer *theDss = nil;
		
		for (DSServer *dss in dssList) {
			if ([serverIp isEqualToString:dss.ip]) {
				theDss = dss;
				dssFound = YES;
				break;
			}
		}
		
		if (!dssFound) {
			DSServer *newDss = [[DataController instance] createDsServerEntity];
			newDss.ip = serverIp;
			newDss.connectionType = DSS_CONNECTION_TYPE_MANUAL;
			newDss.connectionName = serverIp;
			newDss.detectedByBonjour = YES;
			newDss.name = serverIp;
			theDss = newDss;
			[[DataController instance] saveContext];
		}
		
		[manualDssList addObject:theDss];
	}
	
	for (DSServer *dss in dssList) {
		if (![manualDssList containsObject:dss] && [dss.connectionType isEqual:DSS_CONNECTION_TYPE_MANUAL]) {
			[manualDssList addObject:dss];
		}
	}
	
	for (DSServer *dss in dssList) {
		if ([dss.connectionType isEqual:DSS_CONNECTION_TYPE_CLOUD]) {
			[cloudDssList addObject:dss];
		}
	}
	
	/*
	 *	local connections
	 */
	
	
	self.viewHeadManualConnections.frame = CGRectMake(0, yOffset, self.viewHeadManualConnections.frame.size.width, self.viewHeadManualConnections.frame.size.height);
	yOffset += self.viewHeadManualConnections.frame.size.height;
	[self.viewHeadManualConnections updateConstraints];
	
	for (DSServer *dss in manualDssList) {
		SettingsNetworkItemVC *item = [[SettingsNetworkItemVC alloc] initWithServer:dss andDarkBgFlag:counter%2==1];
		item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
		
		yOffset += item.view.frame.size.height;
		
		[self.scrollview addSubview:item.view];
		[serverItems addObject:item];
		counter++;
	}

	if ([manualDssList count] == 0) {
		/*
		 *	display "none" entry
		 */
		
		SettingsNetworkItemVC *item = [[SettingsNetworkItemVC alloc] initWithServerName: NSLocalizedString(@"key_settings_connections_none", @"none") andDarkBgFlag:NO];
		item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
		
		yOffset += item.view.frame.size.height;
		[serverItems addObject:item];
		[self.scrollview addSubview:item.view];
	}
	
	/*
	 *	cloud connections
	 */
	
	self.viewHeadCloudConnections.frame = CGRectMake(0, yOffset, self.viewHeadCloudConnections.frame.size.width, self.viewHeadCloudConnections.frame.size.height);
	yOffset += self.viewHeadCloudConnections.frame.size.height;
	[self.viewHeadCloudConnections updateConstraints];
	
	counter = 0;
	
    for (DSServer *dss in cloudDssList) {
		SettingsNetworkItemVC *item = [[SettingsNetworkItemVC alloc] initWithServer:dss andDarkBgFlag:counter%2==1];
		item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
		
		yOffset += item.view.frame.size.height;
		
		[self.scrollview addSubview:item.view];
		[serverItems addObject:item];
		counter++;
	}
	
	if ([cloudDssList count] == 0) {
		/*
		 *	display "none" entry
		 */
		
		SettingsNetworkItemVC *item = [[SettingsNetworkItemVC alloc] initWithServerName: NSLocalizedString(@"key_settings_connections_none", @"none") andDarkBgFlag:NO];
		item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
		
		yOffset += item.view.frame.size.height;
		[serverItems addObject:item];
		[self.scrollview addSubview:item.view];
	}
	
	self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, yOffset);
	[self.scrollview updateConstraints];
}


- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"serverlist"]) {
		[self buildScrollview];
	}
}

- (void) activateReloadButton {
	((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonReload.hidden = NO;
	
	[UIView animateWithDuration:0.3 animations:^{
		((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonReload.alpha = 1;
	}];
	
	[((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonReload addTarget:self action:@selector(actionReloadServers) forControlEvents:UIControlEventTouchUpInside];
}

- (void) deactivateReloadButton: (BOOL) animated {
	if (animated) {
		[UIView animateWithDuration:0.3 animations:^{
			((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonReload.alpha = 0;
		} completion:^(BOOL finished) {
			((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonReload.hidden = YES;
		}];
	}
	
	[((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonReload removeTarget:self action:@selector(actionReloadServers) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	
	//	stop bonjour service
	[[StartupController instance] stopDssSearch];
	
	//	delete all dss, which were detected by bonjour
	NSArray *allDss = [[DataController instance] fetchAllAutoDetectedDss];
	
	for (DSServer *dsServer in allDss) {
		[[DataController instance] deleteDsServer:dsServer];
	}
	
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
}

- (IBAction) actionAddServer{
    
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[[SettingsNetworkNewConnectionVC alloc] init]];
	
    //[self displayCredentialsAlertView:NO];
    
}

#pragma  mark - alert view

- (void) displayCredentialsAlertView:(BOOL)wrongPass {
    NSString *title;
    if(!wrongPass){
        title = NSLocalizedString(@"key_credentials_title", nil);
    }
    else{
        title = NSLocalizedString(@"key_credentials_wrong_pass_title", nil);
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"key_cancel",nil)
                                              otherButtonTitles:NSLocalizedString(@"key_OK",nil), nil];
    
    alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    
    alertView.tag = 100;
    [alertView show];
    [alertView release];
}


- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    //login alert view
    if(alertView.tag == 100){
        if(buttonIndex == 1){
            UITextField *userNameField = [alertView textFieldAtIndex:0];
            UITextField *passwordField = [alertView textFieldAtIndex:1];
            if([[StartupController instance]performDigitalstromLoginWith:userNameField.text andPassword:passwordField.text]){
            }
            else{
                [self displayCredentialsAlertView:YES];
            }
        }
        else {
            NSLog(@"Manual input: cancelled alertview");
        }
    }
}


- (void) actionReloadServers {
/*	[[ModelLocator instance].serverlist removeAllObjects];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	
	NSString *port = [defaults valueForKey:@"dss_port"];
	
	if (port && [port length] > 0) {
		[[ModelLocator instance] setValue:port forKey:k_DSS_PORT];
	}
	else {
		[[ModelLocator instance] setValue:@"8080" forKey:k_DSS_PORT];
		port = @"8080";
	}
	
	NSString *ip = [defaults valueForKey:@"dss_ip"];
	
	[[ModelLocator instance].serverlist removeAllObjects];
	
	if (ip) {
		if ([ip length] > 0) {
			if (![[ModelLocator instance].serverlist containsObject:[NSString stringWithFormat:@"%@:%@", ip, port]]) {
				[[ModelLocator instance].serverlist addObject:[NSString stringWithFormat:@"%@:%@", ip, port]];
				NSMutableDictionary *dns = [[ModelLocator instance] valueForKey:k_DATA_DNS_DICT];
				[dns setValue:[NSString stringWithFormat:@"%@:%@", ip, port] forKey:[NSString stringWithFormat:@"%@:%@", ip, port]];
			}
			
		}
	}
	
	
	[ModelLocator instance].serverlist = [ModelLocator instance].serverlist;*/
	[[StartupController instance] startDssSearch];
}




- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [_titleLabel release];
    [_digitalStromLabel release];
    [_manualLabel release];
    [super dealloc];
}


@end
