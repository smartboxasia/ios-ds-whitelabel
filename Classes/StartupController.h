//
//  StartupController.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DSServer;

@interface StartupController : NSObject <NSNetServiceBrowserDelegate, NSNetServiceDelegate>{
	NSNetServiceBrowser *aNetServiceBrowser;
	NSNetService* currentResolve;
	
	BOOL tokenInvalid;
    BOOL loginShowed;
}

+ (StartupController*) instance;

- (void) startDssSearch;
- (void) stopDssSearch;

- (void) prepareStructure;
- (void) loadStructure;
- (void) displayCredentialsAlertView;
- (void) continueTokenAuthenticationWithUser: (NSString*) username andPassword: (NSString*) password;
- (BOOL) performDigitalstromLoginWith: (NSString*) username andPassword: (NSString*) password;

- (void) performDssConnectWithServer: (DSServer*) dsServer;
- (void) performDssConnect: (NSString*) serverUrl;

- (DSServer*) createDssForToken: (NSString*) token andRelayLink: (NSString*) relayLink;

@end
