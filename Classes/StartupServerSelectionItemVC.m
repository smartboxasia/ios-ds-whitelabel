//
//  StartupServerSelectionItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "StartupServerSelectionItemVC.h"
#import "ModelLocator.h"
#import "FacilityController.h"
#import "DataController.h"
#import "StartupController.h"
#import "MainVC.h"
#import "DSServer.h"


@implementation StartupServerSelectionItemVC

@synthesize imageBackground, labelName, imageSelectionState;

- (id) initWithDss: (DSServer*) _dss {
	if (self = [super init]) {
		dsServer = _dss;
		[dsServer retain];
	}
	
	return self;
}

- (void) viewDidLoad {
	[super viewDidLoad];
	
	if (dsServer.connectionName) {
		self.labelName.text = dsServer.connectionName;
	}
	else {
        if(dsServer.cloudEmail)
            self.labelName.text = dsServer.cloudEmail;
        else
            self.labelName.text = dsServer.name;
	}
	
}

- (void) viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];

	if (dsServer.connected) {
		[self setActive];
	}
	else {
		[self setInactive];
	}
    
	//[[ModelLocator instance] addObserver:self forKeyPath:k_DSS_IP options:0 context:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateConnectionState) name:kNotificationConnectedToDss object:nil];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //[[ModelLocator instance] removeObserver:self forKeyPath:k_DSS_IP];
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationConnectedToDss object:nil];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

- (void) updateConnectionState {
	if (dsServer.connected) {
		[self setActive];
	}
	else {
		[self setInactive];
	}
}


- (IBAction) actionButtonPressed {
	
	[[StartupController instance] performDssConnectWithServer:dsServer];	
}

- (void) setActive {
	self.imageSelectionState.image = [UIImage imageNamed:@"Svr_Ausw_RadioBtn_aktiv.png"];
}

- (void) setInactive {
	self.imageSelectionState.image = [UIImage imageNamed:@"Svr_Ausw_RadioBtn_inaktiv.png"];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void) prepareDealloc {
	[dsServer release];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

    [super dealloc];
}


@end
