//
//  ColorDotsView.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

#define COBIPAGECONTROL_HEIGHT 20
#define DOT_WIDTH 8
#define DOT_SPACING 12

@interface ColorDotsView : UIView{
    int numberOfPages;
    int currentDot;
    UIColor* color1;
    UIColor* color2;
    UIColor* color3;
    UIColor* color4;
    UIColor* color5;
}

@property (assign) int numberOfPages;
@property (assign) int currentDot;
@property (nonatomic, retain) UIColor* color1;
@property (nonatomic, retain) UIColor* color2;
@property (nonatomic, retain) UIColor* color3;
@property (nonatomic, retain) UIColor* color4;
@property (nonatomic, retain) UIColor* color5;

@end
