//
//  SettingsDeviceDetailPrivacyFacebookVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceDetailPrivacyFacebookVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"

@interface SettingsDeviceDetailPrivacyFacebookVC ()

@end

@implementation SettingsDeviceDetailPrivacyFacebookVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC *)[[ModelLocator instance] valueForKey: k_VIEW_SETTINGS];
	[settings goBack];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
