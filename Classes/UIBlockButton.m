//
//  UIBlockButton.m
//  touch-n-living
//
//  Created by Mathias on 18.02.11.
//  Copyright 2011 Granny&Smith Technikagentur GmbH & Co. KG. All rights reserved.
//

#import "UIBlockButton.h"


@implementation UIBlockButton

-(void) handleControlEvent:(UIControlEvents)event withBlock:(ActionBlock) action{
	_actionBlock = Block_copy(action);
	[self addTarget:self action:@selector(callActionBlock:) forControlEvents:event];
}

- (void) removeControlEvent:(UIControlEvents)event {
	[self removeTarget:self action:@selector(callActionBlock:) forControlEvents:event];
	if (_actionBlock) {
		Block_release(_actionBlock);
	}
}

-(void) callActionBlock:(id)sender{
	_actionBlock();
}

-(void) dealloc{
	Block_release(_actionBlock);
	[super dealloc];
}


@end
