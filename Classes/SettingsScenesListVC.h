//
//  SettingsScenesListVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorDotsView.h"
#import "GAI.h"

@class DSRoom;

@interface SettingsScenesListVC : GAITrackedViewController {
	UIScrollView *scrollview;
	NSMutableArray *childControllers;
    DSRoom *currentRoom;
    NSMutableArray *availableColors;
    int currentIndex;
    BOOL animateItems;
    BOOL isGlobalActivities;
    BOOL isLookingForFavorite;
    
    IBOutlet UILabel *titleLabel;
}

@property(nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property(nonatomic, retain) IBOutlet UISegmentedControl *segment;
@property (nonatomic, retain) IBOutlet ColorDotsView *colorDotsView;
@property (nonatomic, assign) BOOL showAll;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (id) initWithRoom: (DSRoom*) theRoom isLookingForFavorite:(BOOL)favorite withIndex:(int)index;
- (id) initWithName: (NSString*) theName isLookingForFavorite:(BOOL)favorite;
- (void) buildScrollView;
- (IBAction) actionBack;
//- (IBAction) actionAdd;
//- (IBAction) actionReloadScenes;
- (IBAction) changeSegment;

- (DSRoom*) currentRoomshowing;
- (int) currentIndex;

- (void) handleLeftSwipeGesture: (UISwipeGestureRecognizer *) recognizer;
- (void) handleRightSwipeGesture: (UISwipeGestureRecognizer *) recognizer;
//- (void) activateReloadButton;
//- (void) deactivateReloadButton: (BOOL) animated;

@end
