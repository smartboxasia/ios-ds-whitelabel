//
//  SettingsFavoriteBarItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsFavoriteBarItemVC.h"
#import "DSScene.h"
#import "SettingsFavoriteBarVC.h"
#import "ModelLocator.h"
#import "DataController.h"

@implementation SettingsFavoriteBarItemVC

@synthesize labelName, imageDragMe, labelIndex, buttonRemove;

- (id) initWithScene: (DSScene*) _scene andOffset:(float) _offset andMaximum:(NSInteger) _max {
	self = [super init];
	
	if (self) {
		scene = _scene;
		editmodeOn = NO;
		offset = _offset;
		maximum = _max;
	}
	
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.labelName.text = scene.name;
	
	magneticY = (int)floor(offset/self.view.frame.size.height);
	
	self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
}

- (void) toggleEditmode {
	if (!editmodeOn) {
		editmodeOn = YES;
		self.imageDragMe.hidden = NO;
		
		[UIView animateWithDuration:0.2 animations:^{
			//self.imageDragMe.alpha = 1;
			self.imageDragMe.frame = CGRectMake(240, self.imageDragMe.frame.origin.y, self.imageDragMe.frame.size.width, self.imageDragMe.frame.size.height);
			self.labelIndex.alpha = 0;
			self.buttonRemove.frame = CGRectMake(7, self.buttonRemove.frame.origin.y, self.buttonRemove.frame.size.width, self.buttonRemove.frame.size.height);
		}];
	}
	else {
		editmodeOn = NO;
		
		[UIView animateWithDuration:0.2 animations:^{
			self.imageDragMe.frame = CGRectMake(self.view.frame.size.width, self.imageDragMe.frame.origin.y, self.imageDragMe.frame.size.width, self.imageDragMe.frame.size.height);
			self.labelIndex.alpha = 1;
			self.buttonRemove.frame = CGRectMake(self.buttonRemove.frame.size.width*(-1), self.buttonRemove.frame.origin.y, self.buttonRemove.frame.size.width, self.buttonRemove.frame.size.height);
		} completion:^(BOOL finished) {
			self.imageDragMe.hidden = YES;
			
		}];
		
		
	}
	
}

- (IBAction) actionRemoveSceneFromFavs {
	if (scene) {
		scene.favourit = [NSNumber numberWithBool:NO];
		[[DataController instance] saveContext];
	}
	
	[[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_FAVORITELIST_HAS_CHANGED];
	[[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_SCENELIST_HAS_CHANGED];
}


- (void) gotoPosition: (NSInteger) _pos {
	magneticY = (int)_pos;
	scene.sortOrder = [NSNumber numberWithInteger:magneticY];
	
	[UIView beginAnimations:@"" context:nil];
	[UIView setAnimationDuration:0.2];
	
	self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	
	[UIView commitAnimations];
	
	self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
	
}

- (void) incrementMagneticPoint {
	magneticY ++;
	scene.sortOrder = [NSNumber numberWithInteger:magneticY];
	
	[UIView beginAnimations:@"" context:nil];
	[UIView setAnimationDuration:0.2];
	
	self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	
	[UIView commitAnimations];
	
	self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
	
	/*[UIView animateWithDuration:0.2 animations:^{
		self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	}];*/
}

- (void) decrementMagneticPoint {
	magneticY --;
	scene.sortOrder = [NSNumber numberWithInteger:magneticY];
	
	[UIView beginAnimations:@"" context:nil];
	[UIView setAnimationDuration:0.2];
	
	self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	
	[UIView commitAnimations];
	
	self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
	/*[UIView animateWithDuration:0.2 animations:^{
		self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	}];*/
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	if (editmodeOn) {
//		GSLog(@"began");
		CGPoint beginCenter = self.view.frame.origin;
		
		UITouch *touch = [touches anyObject];
		CGPoint touchPoint = [touch locationInView:self.view.superview];
		
		deltaY = touchPoint.y - beginCenter.y;
//		GSLog(@"delta was: %i", deltaY);
		[self.view.superview bringSubviewToFront:self.view];
	}
	
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	if (editmodeOn) {
		UITouch *touch = [touches anyObject];
		CGPoint touchPoint = [touch locationInView:self.view.superview];
		
		// Set the correct center when touched 
		
		
		self.view.frame = CGRectMake(self.view.frame.origin.x, touchPoint.y-deltaY, self.view.frame.size.width, self.view.frame.size.height);
		
		if (self.view.frame.origin.y - magneticY < self.view.frame.size.height || self.view.frame.origin.y - magneticY > self.view.frame.size.height) {
//			GSLog(@"old magn: %i", magneticY);
			int old = magneticY;
			magneticY = (int)floor(self.view.frame.origin.y/self.view.frame.size.height);
			
			if (magneticY < 0) {
				magneticY = 0;
			}
			
			if (magneticY >= maximum) {
				magneticY = (int)maximum-1;
			}
			
			if (old-magneticY == 1 || old-magneticY == -1) {
				self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
				scene.sortOrder = [NSNumber numberWithInteger:magneticY];

				[(SettingsFavoriteBarVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_FAVORITES_LIST] magneticPointHasChanged:old toIndex:magneticY];
			}
//			GSLog(@"new magn: %i", magneticY);
		}
	}
	
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	if (editmodeOn) {
		[UIView animateWithDuration:0.2 animations:^{
			self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
		}];
		[(SettingsFavoriteBarVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_FAVORITES_LIST] arrangeItems];
	}
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	if (editmodeOn) {
		[UIView animateWithDuration:0.2 animations:^{
			self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
		}];
		[(SettingsFavoriteBarVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_FAVORITES_LIST] arrangeItems];
	}
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
