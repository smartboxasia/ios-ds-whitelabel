//
//  SettingsFavoriteBarVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsFavoriteBarVC.h"
#import "DataController.h"
#import "SettingsFavoriteBarItemVC.h"
#import "DSScene.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "UIBlockButton.h"
#import "SettingsScenesListByRoomVC.h"
#import "CustomLabel.h"
#import "SettingsInfoBarItemVC.h"

@implementation SettingsFavoriteBarVC

@synthesize scrollview;
@synthesize background;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    titleLabel.text = NSLocalizedString(@"key_favorites_title", nil);

   // iphone 5 support
   CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
   if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
       background.frame = CGRectMake(0, 0, 320, 513);
       [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
   }

	editmodeOn = NO;
	items =[NSMutableArray new];

	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_FAVORITES_LIST];

	// Bearbeiten Button einblenden
	UIBlockButton *buttonBearbeiten = (UIBlockButton*)((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonBearbeiten;
	buttonBearbeiten.hidden = NO;

	[UIView animateWithDuration:0.2 animations:^{
		buttonBearbeiten.alpha = 1;
	}];

	// Action für Bearbeiten-Button hinzufügen
	[buttonBearbeiten handleControlEvent:UIControlEventTouchUpInside withBlock:^{
		[self toggleEditmode];
	}];

    self.screenName = @"Settings Favorites";
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self buildScrollview];
	[[ModelLocator instance] addObserver:self forKeyPath:k_DATA_FAVORITELIST_HAS_CHANGED options:0 context:nil];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
            [[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_FAVORITELIST_HAS_CHANGED];
    }
    @catch (NSException *exception) {
        //just in case
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_DATA_FAVORITELIST_HAS_CHANGED]) {
		[self buildScrollview];
	}
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    UIBlockButton *buttonBearbeiten = (UIBlockButton*)((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonBearbeiten;

    buttonBearbeiten.hidden = NO;

	[UIView animateWithDuration:0.2 animations:^{
		buttonBearbeiten.alpha = 1;
	}];
}

- (void) buildScrollview {
    editmodeOn = YES;
    [self toggleEditmode];

	for (SettingsFavoriteBarItemVC *item in items) {
		[item.view removeFromSuperview];
	}
	[items removeAllObjects];

	NSInteger yOffset = 0;

	NSInteger favcount = 0;

	NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSArray *sortedScenes = [[DataController instance].scenes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
    [sorter release];

	for (DSScene *scene in sortedScenes) {
		if (![scene.favourit boolValue]) {
			continue;
		}
		favcount++;
	}

	for (DSScene *scene in sortedScenes) {
		if (![scene.favourit boolValue]) {
			continue;
		}
		SettingsFavoriteBarItemVC *item = [[SettingsFavoriteBarItemVC alloc] initWithScene:scene andOffset:yOffset andMaximum:favcount];
		[items addObject:item];
		item.view.frame = CGRectMake(-1, yOffset, item.view.frame.size.width, item.view.frame.size.height);
		[self.scrollview addSubview:item.view];

		yOffset += item.view.frame.size.height;
	}

	self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, yOffset);

}


- (void) magneticPointHasChanged: (int) from toIndex: (int) to {
	if (to >= [items count] || to < 0 || from < 0 || from >= [items count]) {
		return;
	}
	SettingsFavoriteBarItemVC *itemHasMoved = (SettingsFavoriteBarItemVC*)[items objectAtIndex:from];
	SettingsFavoriteBarItemVC *itemToBeMoved = (SettingsFavoriteBarItemVC*)[items objectAtIndex:to];
	if (to>from) {
		[itemToBeMoved decrementMagneticPoint];
	}
	else {
		[itemToBeMoved incrementMagneticPoint];
	}

	[items removeObject:itemHasMoved];
	[items insertObject:itemHasMoved atIndex:to];
}

- (void) arrangeItems {
	for (int i = 0; i < [items count]; i++) {
		SettingsFavoriteBarItemVC *item = [items objectAtIndex:i];
		[item gotoPosition:i];
	}

	[[DataController instance] saveContext];

	[[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_SCENELIST_HAS_CHANGED];
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
	//[settings showSubGroup:[SettingsFavoriteBarSceneListVC new]];

	UIBlockButton *buttonBearbeiten = (UIBlockButton*)((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonBearbeiten;
	buttonBearbeiten.selected = NO;
	[buttonBearbeiten removeControlEvent:UIControlEventTouchUpInside];

	[UIView animateWithDuration:0.2 animations:^{
		buttonBearbeiten.alpha = 0;
	} completion:^(BOOL finished) {
		buttonBearbeiten.hidden = YES;
	}];

	[[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_FAVORITELIST_HAS_CHANGED];
}

- (IBAction) actionAddActivity {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[[SettingsScenesListByRoomVC alloc] initWithFavorite:YES]];

	UIBlockButton *buttonBearbeiten = (UIBlockButton*)((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonBearbeiten;
	[UIView animateWithDuration:0.2 animations:^{
		buttonBearbeiten.alpha = 0;
	} completion:^(BOOL finished) {
		buttonBearbeiten.hidden = YES;
	}];
}

- (void) toggleEditmode {
	if (!editmodeOn) {
		editmodeOn = YES;

	}
	else {
		editmodeOn = NO;
	}

	UIBlockButton *buttonBearbeiten = (UIBlockButton*)((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonBearbeiten;
	buttonBearbeiten.selected = editmodeOn;

	self.scrollview.scrollEnabled = !editmodeOn;

	for (SettingsFavoriteBarItemVC *item in items) {
		[item toggleEditmode];
	}

}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

	for (SettingsFavoriteBarItemVC *item in items) {
		[item.view removeFromSuperview];
		[item release];
	}
	[items removeAllObjects];
	[items release];

    [titleLabel release];
    [super dealloc];
}


@end
