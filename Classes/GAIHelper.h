//
//  GAIHelper.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DSScene;

@interface GAIHelper : NSObject

+(void)configure:(BOOL)isTestVersion;
+(void)allowTracking:(BOOL)allowTracking;

+(void)sendUIActionWithCategory:(NSString*)category andLabel:(NSString*)label;
+(void)sendSceneCall:(DSScene*)scene;
+(void)trackViewControllerWithName:(NSString *)name;

@end
