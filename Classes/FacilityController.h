//
//  untitled.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <CoreLocation/CoreLocation.h>
//#import <MapKit/MapKit.h>
#import "DSRoom.h"


@interface FacilityController : NSObject {
//	CLLocationManager *locationManager;
//	NSString *currentLocationInfo;
	NSTimer *timerConsumption;
	NSTimer *timerRoomConsumption;
	
	NSMutableDictionary *dictRoomConsumptionTimer;
	
//	CLLocationCoordinate2D location;
	BOOL consumptionPollingRunning;
	BOOL consumptionBlockRunning;
}

//@property (nonatomic , retain) CLLocationManager *locationManager;
//@property (retain) NSString *currentLocationInfo;

+ (FacilityController*) instance;

//- (void) startUpdatingLocation;

- (void) loadWeatherInformation;
- (void) startConsumptionPolling;
- (void) endConsumptionPolling;
- (void) timerConsumptionSelector: (NSTimer *) theTimer;

- (void) startRoomConsumptionPolling: (DSRoom*) room;
- (void) endRoomConsumptionPolling: (DSRoom*) room;
- (void) timerRoomConsumptionSelector: (NSTimer *) theTimer;
- (void) loadAndApplyRoomConsumption: (DSRoom*) room;

//- (void)setPowerForDSDevice: (BOOL) powerOn withDeviceId:(NSString *)dsid;

- (void) displayWeatherCondition: (int) condition;

@end
