//
//  DataController.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DSDevice.h"
#import "DSRoom.h"
#import "DSMeter.h"
#import "DSScene.h"
#import "DigitalstromJSONService.h"

#define k_SERVICE_SCENE_MAX @"max"
#define k_SERVICE_SCENE_MIN @"min"
#define k_SERVICE_SCENE_STOP @"stop"
#define k_SERVICE_SCENE_DEC @"dec"
#define k_SERVICE_SCENE_INC @"inc"
#define k_SERVICE_SCENE_STANDBY @"standby"
#define k_SERVICE_SCENE_STIMMUNG1 @"stimmung1"

@class DSServer;

@interface DataController : NSObject {
	NSManagedObjectModel *managedObjectModel;
	NSPersistentStoreCoordinator *persistentStoreCoordinator;
	NSFetchedResultsController *fetchedResultsController;
	NSManagedObjectContext *objectContext;
	
	DigitalstromJSONService *service;
	
	NSMutableArray *rooms;
	NSMutableArray *newRooms;
	NSMutableArray *scenes;
	NSMutableArray *meters;
	
	DSServer *currentDss;
	
	NSMutableDictionary *demoStructure;
	
	// entweder 0 (Update läuft nicht, Update nicht abgeschlossen), 1 (Update läuft), 2 (Update läuft nicht, Update ist abgeschlossen)
	int devicesScenesUpdateState;
	
	// derzeitiger Update-Stand im Intervall 0 - devicesScenesUpdateMaxValue
	int devicesScenesUpdateStand;
	
	// der maximale Update-Stand 
	int devicesScenesUpdateMaxValue;
	
	BOOL useDemo;
    
    BOOL rotationEnabled;
	
	BOOL meterConsumptionPollingRunning;
	BOOL meterConsumptionBlockRunning;
	
	BOOL initServerStructRunning;
    
    BOOL _scenePollingBlockRunning;
    BOOL _scenePollingShouldRun;
}

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain) NSManagedObjectContext *objectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

@property (retain, nonatomic) NSMutableArray *rooms;
@property (retain, nonatomic) NSMutableArray *scenes;
@property (retain, nonatomic) NSMutableArray *meters;
@property (retain, nonatomic) NSNumber *dssLatitude;
@property (retain, nonatomic) NSNumber *dssLongitude;

@property (assign) BOOL rotationEnabled;

+ (DataController*) instance;
- (NSString*) getDSSVersion;
- (DSServer*) getCurrentDSS;
- (void) setCurrentDSS: (DSServer*) dsServer;
- (NSString*) getApartmentName;
- (void) getDSSLocation;

#pragma mark consumption timer
- (void) startRoomConsumptionPolling: (DSRoom*) room;
- (void) endRoomConsumptionPolling: (DSRoom*) room;

- (void) startMeterConsumptionPolling;
- (BOOL) endMeterConsumptionPolling;
- (void) meterConsumptionExecutor;


#pragma mark Consumption Methods
- (NSNumber*) getTotalConsumption;
- (NSNumber*) getDeviceConsumption: (DSDevice*) device;
- (NSNumber*) getRoomConsumption: (DSRoom*) room;
- (void) loadMeterConsumptionInfo: (NSTimer*)timer;
- (NSInteger) demoSceneConsumption: (NSInteger) sceneNo;

#pragma mark scene updater functions

- (void) startScenePolling: (DSRoom*)room;
- (void) endScenePolling;
- (void) scenePollingExecutor: (DSRoom*)room;
- (void) updateLastCalledScenesWithArray:(NSArray*)lastCalledArray forRoom:(DSRoom*)room;

#pragma mark Structure Methods
- (void) initStructure;
- (BOOL) initServerStructure;

- (BOOL) updateRoomAndDevicesWithDict: (NSMutableDictionary*)roomDict;
- (void) updateReachableGroupsWithNonDsDeviceDict: (NSMutableDictionary*)roomDict;
- (void) updateMetersWithArray: (NSArray*) metersArray;
- (BOOL) updateScenesWithDict: (NSDictionary*) scenesDict;
- (BOOL) updateHighLevelEventsWithArray: (NSArray*) eventArray;

- (DSDevice*) getExistingDeviceInCoreData:(NSString *)identifier withPowerOn:(NSNumber*)powerOn;
- (NSArray*) getRoomsInCoreData;
- (DSDevice *) createDevice: (NSDictionary*)deviceDict withGroup:(NSDictionary*)groupDict withRoom:(DSRoom*)room;
- (int) getProductID: (int)productIDBaseTen;
- (BOOL) updateDatabase;

//- (void) deleteAllMetersFromCoreData;

#pragma mark Device Methods
- (BOOL) setDevicePower: (DSDevice*)device withBool:(BOOL) powerOn withGroup:(int)group;
- (BOOL) setDeviceValue: (DSDevice*)device withValue: (NSNumber*) value;
- (BOOL) setShadeAngle: (DSDevice*)device withValue: (NSNumber*) value;
- (BOOL) setDeviceName: (DSDevice*)device withString: (NSString*) name;
- (BOOL) moveDevice: (DSDevice*)device toRoom: (DSRoom*) room;
- (BOOL) setOutputModeForDevice: (DSDevice*)device toMode: (int) newMode;
- (void) setDeviceProperties: (NSMutableDictionary*) deviceDict;
- (BOOL) updateValueOfDevice: (DSDevice*)device;
- (void) updateConsumptionValuesOfDevice: (DSDevice*)device;

#pragma mark Room Methods
- (BOOL) setRoomName: (NSString*) name withRoom: (DSRoom*) room;
- (BOOL) setRoomPower: (DSRoom*)room withBool:(BOOL) powerOn;

#pragma mark Scene Methods
- (BOOL) createScene: (NSString*)sceneName withRoom:(DSRoom*)room withGroup:(NSNumber*)group favouriteScene:(BOOL)favouriteFlag;
- (BOOL) createScene: (NSNumber*)sceneNo withName:(NSString*)sceneName withRoom:(DSRoom*)room withGroup:(NSNumber*)group favouriteScene:(BOOL)favouriteFlag;
- (void) setSceneFavouriteFlag: (DSScene*) scene withFavourite:(BOOL)favouriteFlag;
- (void) loadScenesFromCoreData;
- (DSScene*) createDSScene: (NSNumber*)sceneNo withName:(NSString*)sceneName withRoom:(DSRoom*)room withGroup:(NSNumber*)group favouriteScene:(BOOL)favouriteFlag showOnList:(BOOL)listFlag;
- (BOOL) setSceneName: (DSScene*)scene withName:(NSString*)sceneName withRoom:(DSRoom*)room;
- (BOOL) deleteScene: (DSScene*)scene withRoom:(DSRoom*)room;
- (BOOL) saveScene:(DSScene*) scene;

- (void) loadApartmentScenes;
- (void) loadDemoScenes;
- (BOOL) turnSceneOn: (DSScene*)scene;
- (BOOL) undoScene: (DSScene *) scene;
- (BOOL) callStandardScene: (NSString*)standardScene withRoom:(DSRoom*)room withGroup:(int)group;
- (BOOL) callStandardScene: (NSString*)standardScene withDevice:(DSDevice*)device withGroup:(int)group;
- (BOOL) callStandbyScene:(DSRoom*)room;

#pragma mark dSS Methods
- (DSServer*) createDsServerEntity;
- (DSServer*) getDssForIpAddressInCoreData: (NSString*)ipAddress;
- (DSServer*) getDssForMacAddressInCoreData: (NSString*)macAddress;
- (DSServer*) getLastDssInCoreData;
- (NSArray*) fetchAllDss;
- (NSArray*) fetchAllManualDss;
- (NSArray*) fetchAllAutoDetectedDss;
- (NSArray*) allocAllKnownDss;
- (DSMeter*) createDemoMeter: (NSString*) meterName withId: (NSString*) identifier;
- (void) markDsServersAsDisconnected;
- (void) deleteDsServer: (DSServer*) dsServer;

#pragma mark cloud service calls
-(void) cloudServiceDeleteMaxPowerForDevice:(DSDevice *)device;
-(void) cloudServiceDeleteSocketTypeForDevice:(DSDevice *)device;
- (void) cloudServiceSetMaxPower:(NSNumber*) maxPower forDevice: (DSDevice*) device;
- (void) cloudServiceSetSocket:(NSString*) socket forDevice: (DSDevice*) device;
- (void) cloudServiceGetAllTags: (DSDevice*) device;
- (void) cloudServiceGetDeviceDetailsForDevice: (DSDevice*) device;
- (void) cloudServiceCheckIlluminantForDevice: (DSDevice*) device andEan: (NSString*) ean;
- (void) cloudServiceTransferDimmingCurveForDevice: (DSDevice*) device andGtin: (NSString*) gtin;

- (void) cloudServiceGetArticleDataForDevice:(DSDevice*) device;

#pragma mark Core-Data Methods
- (BOOL) saveContext;
- (NSManagedObjectModel *)managedObjectModel;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSString *)applicationDocumentsDirectory;
- (NSManagedObjectContext *) managedObjectContext;
- (NSManagedObjectContext *) switchManagedObjectContext;

@end
