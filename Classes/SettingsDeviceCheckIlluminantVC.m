//
//  SettingsDeviceCheckIlluminantOverlayVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceCheckIlluminantVC.h"
#import "ModelLocator.h"
#import "SettingsDeviceCameraCheckOverlayVC.h"
#import "DSDevice.h"
#import "DataController.h"
#import "GAIHelper.h"


@interface SettingsDeviceCheckIlluminantVC ()
{
    AVCaptureSession *_session;
    AVCaptureDevice *_cameraDevice;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
}
@end

@implementation SettingsDeviceCheckIlluminantVC

@synthesize imageBackground;
@synthesize labelTop;
@synthesize buttonBack;

- (id) initWithActionType: (NSString*) _actionType andDevice: (DSDevice*) _device {
    self = [super init];
    if (self) {
		actionType = _actionType;
		device = _device;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    if ([actionType isEqualToString: kActionTypeIlluminantTransfer])
    {
        self.labelTop.text = NSLocalizedString(@"key_settings_device_scan_overlay_right_button_transfer_dimming_curve", nil);
    }else
    {
        self.labelTop.text = NSLocalizedString(@"key_illuminant_check", nil);
    }
    
    [self.buttonBack setTitle:NSLocalizedString(@"key_exit", nil) forState:UIControlStateNormal];
	
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
	if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        // self.imageBackground.frame = CGRectMake(0, 0, 320, 513);
		[self.imageBackground setImage:[UIImage imageNamed:@"settings_devices_scanmask-568h"]];
		self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 568);
    }
    
    _session = [[AVCaptureSession alloc] init];
    _cameraDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_cameraDevice error:&error];
    if (_input) {
        [_session addInput:_input];
    } else {
        NSLog(@"Error: %@", error);
    }
    
    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];
    
    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
    
    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame = self.view.bounds;
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:_prevLayer];

    [self.view bringSubviewToFront:self.imageBackground];
    [self.view bringSubviewToFront:self.labelTop];
    [self.view bringSubviewToFront:self.buttonBack];

    [self startScanning];
    
    self.additionalHintView.hidden = YES;
    self.additionalHintView.alpha = 0;
    
    self.screenName = @"Settings Device Check Illuminant";
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([actionType isEqualToString: kActionTypeIlluminantTransfer])
    {
        self.additionalHintView.layer.cornerRadius = 5.0;
        self.additionalHintLabel.text = NSLocalizedString(@"key_transfer_dimming_curve_hint",nil);
        [self.additionalHintLabel setFont:[UIFont fontWithName:@"PFBeauSansPro-Regular" size:14.0]];

        self.additionalHintView.alpha = 0;
        self.additionalHintView.hidden = NO;
        [self.view bringSubviewToFront:self.additionalHintView];
        
        __block UIView *weakHintView = self.additionalHintView;
        [UIView animateWithDuration:0.2 animations:^{
            weakHintView.alpha = 1;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.2 animations:^{
                weakHintView.alpha = 0;
            } completion:^(BOOL finished) {
                weakHintView.hidden = YES;
            }];
        });
    }
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    NSString *detectionString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *metadata in metadataObjects)
    {
        for (NSString *type in barCodeTypes)
        {
            if ([metadata.type isEqualToString:type])
            {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = barCodeObject.bounds;
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];

                [GAIHelper sendUIActionWithCategory:@"barcode_scanned" andLabel:nil];
                [self respondToActionType:actionType withScannedEan:detectionString];
                [self stopScanning];
                break;
            }
        }
    }
}

-(void) startScanning
{
    [_session startRunning];
}

-(void) stopScanning
{
    [_session stopRunning];
}

- (void) respondToActionType: (NSString*) _actionType withScannedEan: (NSString*) scannedEan {
	//scannedEan = @"4290046001093";
	/*
	 *	registering for closing event
	 *	restart scanner lib after closing
	 */
	[[NSNotificationCenter defaultCenter] addObserverForName:kNotificationOverlayClosed object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self startScanning];
		[[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationOverlayClosed object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationOverlayCloseAndExit object:nil];
	}];
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationOverlayCloseAndExit object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
		[[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationOverlayCloseAndExit object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationOverlayClosed object:nil];
        [self actionCancel:nil];
	}];
    
	
	/*
	 *	add overlay to view stack
	 */
	SettingsDeviceCameraCheckOverlayVC *overlayVc = [[SettingsDeviceCameraCheckOverlayVC alloc] initWithDevice:device andActionType:_actionType];
	overlayVc.ean = scannedEan;
	overlayVc.view.frame = CGRectMake(self.view.frame.size.width/2-overlayVc.view.frame.size.width/2, 100, overlayVc.view.frame.size.width, overlayVc.view.frame.size.height);
	overlayVc.view.alpha = 0;
	
	[self.view addSubview:overlayVc.view];
	
	/*
	 *	display overlay view
	 */
	[UIView animateWithDuration:0.4 animations:^{
		overlayVc.view.alpha = 1;
	}];
	
	/*
	 *	call json service for illuminant check
	 */
	[[DataController instance] cloudServiceCheckIlluminantForDevice:device andEan:scannedEan];
}



- (IBAction) actionCancel:(id)sender {
	UIViewController *mainVc = [[ModelLocator instance] valueForKey:k_VIEW_MAIN];
	
	[UIView transitionFromView:self.view
                        toView:mainVc.view
                      duration:0.6
                       options: UIViewAnimationOptionTransitionFlipFromRight
                    completion:^(BOOL finished) {
						if (finished) {
							[self release];
						}
					}
	 ];
}

- (void) dealloc {
    [_additionalHintLabel release];
    [_additionalHintView release];
	[super dealloc];
	NSLog(@"camera dealloc");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
