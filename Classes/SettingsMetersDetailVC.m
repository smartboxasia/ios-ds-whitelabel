//
//  SettingsMetersDetailVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsMetersDetailVC.h"
#import "DataController.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "CustomLabel.h"


@implementation SettingsMetersDetailVC

@synthesize background;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

- (id) initWithMeter: (DSMeter*) theMeter {
	if(self = [super init]) {
		meter = theMeter;
	}
	
	return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    labelIdTitle.text = NSLocalizedString(@"key_ID", nil);
    labelReadingTitle.text = NSLocalizedString(@"key_settings_device_meter_reading", nil);
    labelConsumptionTitle.text = NSLocalizedString(@"key_settings_device_consumption", nil);
    
	// iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

	int index = (int)[[DataController instance].meters indexOfObject:meter];
	labelHeadIndex.text = [NSString stringWithFormat:@"5.%i", index+1];
	labelSN.text = meter.identifier;
    
    self.screenName = @"Settings Meters Details";
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    textHeadName.text = meter.name;
    
    if (meter.meteringEnabled) {
        labelPowerConsumption.text = [NSString stringWithFormat:@"%i W", [meter.powerConsumption intValue]];
        labelEnergyMeterValue.text = [NSString stringWithFormat:@"%i Wh", [meter.energyMeterValue intValue]];
    }
    else{
        labelPowerConsumption.text = @"--";
        labelEnergyMeterValue.text = @"--";
    }
    
    [meter addObserver:self forKeyPath:@"name" options:0 context:nil];
	[meter addObserver:self forKeyPath:@"powerConsumption" options:0 context:nil];
	[meter addObserver:self forKeyPath:@"energyMeterValue" options:0 context:nil];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [meter removeObserver:self forKeyPath:@"name"];
	[meter removeObserver:self forKeyPath:@"powerConsumption"];
	[meter removeObserver:self forKeyPath:@"energyMeterValue"];    
}

#pragma mark -
#pragma mark Observer method

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"name"]) {
		textHeadName.text = meter.name;
	}
	
	if ([keyPath isEqualToString:@"powerConsumption"] && meter.meteringEnabled) {
		labelPowerConsumption.text = [NSString stringWithFormat:@"%i W", [meter.powerConsumption intValue]];
        
	}
	
	if ([keyPath isEqualToString:@"energyMeterValue"] && meter.meteringEnabled) {
		labelEnergyMeterValue.text = [NSString stringWithFormat:@"%i Wh", [meter.energyMeterValue intValue]];
	}
}

#pragma mark -
#pragma mark IB Outlets

- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
}

- (IBAction) actionEdit {
	if ([textHeadName isFirstResponder]) {
		[textHeadName resignFirstResponder];
	}
	else {
		[textHeadName becomeFirstResponder];
	}
}


- (void) textFieldDidEndEditing:(UITextField *)textField {
	[[DataController instance] setMeterName:textField.text withMeter:meter];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	//[getConsumptionInfoTimer invalidate];
	//getConsumptionInfoTimer = nil;
	
    [labelIdTitle release];
    [labelReadingTitle release];
    [labelConsumptionTitle release];
    [super dealloc];
}


@end
