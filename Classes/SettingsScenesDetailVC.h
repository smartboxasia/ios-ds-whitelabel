//
//  SettingsScenesDetailVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomSwitch.h"
#import "GAI.h"

@class DSScene;
@class DSRoom;
@class DSDevice;
@class CustomLabel;

@interface SettingsScenesDetailVC : GAITrackedViewController <UIPickerViewDelegate, UIPickerViewDataSource> {
	UILabel *labelHeadName;
	UITextField *textSceneName;
	UILabel *labelRoomName;
	UIView *viewPicker;
	UIButton *buttonRoomLabel;
	UIPickerView *pickerRoom;
	UICustomSwitch *switchFavorit;
    UICustomSwitch *switchList;
    UILabel *labelShowOnList;
	DSScene *scene;
	NSInteger sceneIndex;
	DSRoom *selectedRoomFromPicker;
    
    IBOutlet CustomLabel *labelNameTitle;
    IBOutlet CustomLabel *labelRoomTitle;
    IBOutlet CustomLabel *labelFavoriteTitle;
}

@property(nonatomic, retain) IBOutlet UILabel *labelHeadName;
@property(nonatomic, retain) IBOutlet UITextField *textSceneName;
@property(nonatomic, retain) IBOutlet UILabel *labelRoomName;
@property(nonatomic, retain) IBOutlet UIView *viewPicker;
@property(nonatomic, retain) IBOutlet UIButton *buttonRoomLabel;
@property(nonatomic, retain) IBOutlet UIButton *buttonEdit;
@property(nonatomic, retain) IBOutlet UIPickerView *pickerRoom;
@property(nonatomic, retain) IBOutlet UICustomSwitch *switchFavorit;
@property(nonatomic, retain) IBOutlet UICustomSwitch *switchList;
@property(nonatomic, retain) IBOutlet UILabel *labelShowOnList;
@property(nonatomic, retain) IBOutlet UILabel *labelEditActivity;
@property(nonatomic, retain) IBOutlet UIButton *buttonEditActivity;
@property(nonatomic, retain) IBOutlet UIImageView *imageToHide;
@property(nonatomic, retain) IBOutlet UIImageView *imageToHide2;
@property(nonatomic, retain) IBOutlet UIButton *buttonCallActivity;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (id) initWithScene: (DSScene*) _scene andIndex: (NSInteger) _index;

- (IBAction) actionGoToSceneEdit;
- (IBAction) actionCallScene;
- (IBAction) actionBack;
- (IBAction) actionEnableRoomEditing;
- (IBAction) actionShowPicker;
- (IBAction) actionHidePicker;
- (IBAction) actionHidePickerWithoutSave;
- (IBAction) actionChangeFavoriteSwitch: (id) sender;
- (IBAction) actionChangeListSwitch: (id) sender;
- (void) fillHeaderText;

@end
