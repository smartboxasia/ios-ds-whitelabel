//
//  SettingsDevicesListVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSRoom;
@class CustomLabel;


@interface SettingsDevicesListVC : GAITrackedViewController {
	UIScrollView *scrollview;
	NSMutableArray *scrollviewItems;
    DSRoom *currentRoom;
    IBOutlet CustomLabel *titleLabel;
}
- (id) initWithRoom: (DSRoom*) theRoom;

@property(nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (IBAction) actionBack;

- (void) buildScrollview;

@end
