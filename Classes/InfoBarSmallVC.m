//
//  InfoBarSmall.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "InfoBarSmallVC.h"
#import "NSDate+Helper.h"
#import "DSRoom.h"
#import "DSMeter.h"
#import "ModelLocator.h"
#import "FacilityController.h"

@implementation InfoBarSmallVC

@synthesize labelConsumption, labelTitle, labelTime;

- (id) initWithRoom: (DSRoom*) theRoom {
	if (self = [super init]) {
		room = theRoom;
	}
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	[self updateDateTime];
	datetimeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateDateTime) userInfo:nil repeats:YES];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]) {
        NSNumber *consumption = [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION];
        if([consumption integerValue] > -1){
            self.labelConsumption.text = [NSString stringWithFormat:@"%@ W", [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]];
        }
        else{
            self.labelConsumption.text = @"-- W";
        }
	}
	else {
		self.labelConsumption.text = @"-- W";
	}

    [[ModelLocator instance] addObserver:self forKeyPath:k_INFO_TOTAL_CONSUMPTION options:0 context:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
            [[ModelLocator instance] removeObserver:self forKeyPath:k_INFO_TOTAL_CONSUMPTION];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

- (void) storeInML {
	[[ModelLocator instance] setValue:self forKey:k_VIEW_INFOBAR_SMALL];
}

#pragma mark -
#pragma mark NSTimer target selector

- (void) updateDateTime {
	NSDate *date = [NSDate date];
	
	self.labelTime.text = [date stringWithFormat:@"HH:mm"];
	
}

#pragma mark -
#pragma mark Observer responder

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"powerConsumption"]) {
		self.labelConsumption.text = [NSString stringWithFormat:@"%@ W", room.meter.powerConsumption];
	}
	else if([keyPath isEqualToString:k_INFO_TOTAL_CONSUMPTION]) {
		if ([[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]) {
			NSNumber *consumption = [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION];
            if([consumption integerValue] > -1){
                self.labelConsumption.text = [NSString stringWithFormat:@"%@ W", [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]];
            }
            else{
                self.labelConsumption.text = @"-- W";
            }
		}
		else {
			self.labelConsumption.text = @"-- W";
		}
		/*if (room && room.meter) {
			self.labelConsumption.text = [NSString stringWithFormat:@"%@ W", room.meter.powerConsumption];
		}*/
	}
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) prepareDealloc {
	[datetimeTimer invalidate];
	datetimeTimer = nil;
	
	//if (room && room.meter) {
	//	[room.meter removeObserver:self forKeyPath:@"powerConsumption"];
	//}
	room = nil;
}

- (void)dealloc {
    //moved from prepareDealloc
	
	[super dealloc];
}


@end
