//
//  SettingsDeviceCheckIlluminantOverlayVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GAI.h"

#define kActionTypeIlluminantCheck @"illuminantCheck"
#define kActionTypeIlluminantTransfer @"illuminantTransfer"

@class DSDevice;

@interface SettingsDeviceCheckIlluminantVC : GAITrackedViewController <AVCaptureMetadataOutputObjectsDelegate> {
	NSString *actionType;
	DSDevice *device;
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UIButton *buttonBack;;
@property (nonatomic, retain) IBOutlet UILabel *labelTop;

@property (retain, nonatomic) IBOutlet UIView *additionalHintView;
@property (retain, nonatomic) IBOutlet UILabel *additionalHintLabel;

- (id) initWithActionType: (NSString*) _actionType andDevice: (DSDevice*) _device;
- (IBAction) actionCancel:(id)sender;

- (void) respondToActionType: (NSString*) actionType withScannedEan: (NSString*) scannedEan;

@end
