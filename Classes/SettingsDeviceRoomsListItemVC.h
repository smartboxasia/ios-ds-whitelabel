//
//  SettingsDeviceRoomsListItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSRoom;
@class DSDevice;

@interface SettingsDeviceRoomsListItemVC : UIViewController {
	DSRoom *_room;
    DSDevice *_device;
	float offset;
	NSInteger maximum;
	
	UIImageView *imageBackground;
	UILabel *labelName;
	UIButton *buttonStepInto;
	UIButton *buttonBig;
	
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UILabel *labelName;
@property (nonatomic, retain) IBOutlet UIButton *buttonStepInto;
@property (nonatomic, retain) IBOutlet UIButton *buttonBig;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

- (id) initWithRoom: (DSRoom*) theRoom andOffset:(float) _offset andMaximum:(NSInteger) _max andDevice: (DSDevice*)device;

- (IBAction) actionShowRoomDetail;

@end
