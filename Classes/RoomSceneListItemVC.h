//
//  RoomSceneListItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSScene;

@interface RoomSceneListItemVC : UIViewController {
	UIImageView *imageBackground;
    UIImageView *imageColorBar;
	UIButton *buttonIcon;
	UIButton *buttonLabel;
    int colorCode;
	BOOL doAnimation;
	DSScene *scene;
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UIImageView *imageColorBar;
@property (nonatomic, retain) IBOutlet UIButton *buttonIcon;
@property (nonatomic, retain) IBOutlet UIButton *buttonLabel;
@property (assign) int colorCode;
@property (assign) BOOL doAnimation;

- (id)initWithScene: (DSScene*) theScene colorCode:(int) theColor doAnimation:(BOOL) animation;
- (IBAction) actionButtonLabel: (id) sender;
- (IBAction) actionButtonIcon: (id) sender;

- (void) buildBackground;

- (void) prepareDealloc;
@end
