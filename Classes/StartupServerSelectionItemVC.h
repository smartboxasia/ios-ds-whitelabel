//
//  StartupServerSelectionItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSServer;

@interface StartupServerSelectionItemVC : UIViewController {
	UIImageView *imageBackground;
	UILabel *labelName;
	UIImageView *imageSelectionState;
	
	DSServer *dsServer;
}

@property(nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property(nonatomic, retain) IBOutlet UILabel *labelName;
@property(nonatomic, retain) IBOutlet UIImageView *imageSelectionState;

- (id) initWithDss: (DSServer*) _dss;
- (IBAction) actionButtonPressed;
- (void) setActive;
- (void) setInactive;

- (void) prepareDealloc;


@end
