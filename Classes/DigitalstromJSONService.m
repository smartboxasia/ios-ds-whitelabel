//
//  DigitalstromJSONService.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "DigitalstromJSONService.h"
#import "ModelLocator.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "DataController.h"
#import "DSServer.h"
#import "GAIHelper.h"

//#define kCloudServiceDomain @"dsservices.cloudapp.net"
//#define kCloudServiceDomain @"dsservices.aizo.com"

@implementation DigitalstromJSONService
@synthesize serverIP, dssUsername, dssPassword, dssApplicationToken;

static DigitalstromJSONService *instance = NULL;


+(DigitalstromJSONService *)instance {
	@synchronized(self) {
		if(!instance) {
			instance = [[DigitalstromJSONService alloc] init];
		} else {
			instance.serverIP = [[ModelLocator instance] valueForKey:k_DSS_IP];
		}
		
	}
	return instance;
}

-(id) init {
	if(self = [super init]) {
		parser = [[SBJSON alloc] init];
		self.serverIP = [[ModelLocator instance] valueForKey:k_DSS_IP];
	}
	return self;
}


//Returns the structure of the apartment. 
- (NSMutableDictionary*) getStructure: (BOOL) performLogin {
	if (performLogin) {
		if (![self loginUser]) {
			return nil;
		}
	}
	
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/apartment/getStructure", self.serverIP]]; 
//	GSLog(@"getStructure: %@", url);
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	[request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
//	GSLog(@"finished getStructure");
	
	
	NSError *error = request.error;
	
	if (error && [error code] == 3) {
        [request release];
		if ([self loginUser]) {
			return [self getStructure:NO];
		}
		else {
			return nil;
		}
		
	}
	else if(error) {
        GSLog(@"ERROR: %@", error);
        [request release];
		return nil;
	}

	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *structureDict = [parser objectWithString:json_string error:nil];
	
//	GSLog(@"Structure: %@ %@", structureDict, url);
	
	[request release];
	[json_string release];
	
	return [[structureDict objectForKey:@"result"]objectForKey:@"apartment"];
}

//Returns the structure of the apartment.
- (NSMutableArray*) getReachableNonDsGroups {
	
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/apartment/zones/*(ZoneID)/groups/*(group,connectedDevices)", self.serverIP]];

	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *structureDict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
    NSMutableArray *zoneArray = [NSMutableArray arrayWithArray: [[structureDict objectForKey:@"result"]objectForKey:@"zones"]];
//	NSLog("non ds device zone array: %@", [zoneArray description]);
    
    return zoneArray;
}

- (int) getZoneCount:(NSDictionary*)structureDict {
	return (int)[[structureDict objectForKey:@"zones"] count];
}

//Normalizes the json-formated-Dictionary to a standarized custom one.
- (NSMutableArray*) copyRoomProperties:(NSMutableDictionary*)strucDict {
	int zoneCount = [self getZoneCount:strucDict];
	
	NSMutableArray *roomProperties = [[NSMutableArray alloc] init];
	
	for (int i = 1; i<zoneCount; i++) {
		NSDictionary *roomDict = [[NSMutableDictionary alloc] init];
		NSString *identifier = [[[strucDict objectForKey:@"zones"] objectAtIndex:i] valueForKey:@"id"];
		NSString *name = [[[strucDict objectForKey:@"zones"] objectAtIndex:i] valueForKey:@"name"];
		NSArray *devices = [[[strucDict objectForKey:@"zones"] objectAtIndex:i] objectForKey:@"devices"];
		
		[roomDict setValue:identifier forKey:@"identifier"];
		[roomDict setValue:name forKey:@"name"];
		[roomDict setValue:devices forKey:@"devices"];
		[roomProperties addObject:roomDict];
        [roomDict release];
	}
	
	return roomProperties;
}

- (int) getDeviceCount:(NSDictionary*)roomDict {
	return (int)[[roomDict objectForKey:@"devices"] count];
}




//Returns a boolean-value if the specified device could be successsfully turned on/off.
- (BOOL) setDevicePower: (DSDevice*)device withBool:(BOOL) powerOn withGroup:(int)group {
	NSURL *url = nil;
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	if (powerOn) {
		url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/callScene?sceneNumber=14&groupID=%i&%@=%@", self.serverIP, group, idArg, device.identifier]];
	} else {
		url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/callScene?sceneNumber=13&groupID=%i&%@=%@", self.serverIP, group, idArg, device.identifier]];
	}
	
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Returns a boolean-value if the special-value (e.g. the dimm-factor) of the specified device could be successsfully set.
//The special value depends on the group the device belongs to.
- (BOOL) setDeviceValue: (DSDevice*)device withValue: (NSNumber*) value {
    NSURL *url;
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
    //since older light klemme firmware might not understand the offset parameter,
    //we use the older function setValue instead of setOutputValue. Else we use the new command
    
    if ([device.group isEqualToString:@"1"]) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/setValue?%@=%@&value=%i",
                                    self.serverIP,
                                    idArg,
                                    device.identifier,
                                    [value intValue]]];
        
    }
    else{
        int offset = 0;
        
        if([device.group isEqualToString:@"2"])
            offset = 2;
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/setOutputValue?%@=%@&value=%i&offset=%i",
                                    self.serverIP,
                                    idArg,
                                    device.identifier,
                                    [value intValue],
                                    offset]];
    }
	
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Returns a boolean-value if the shades new angle could be set
- (BOOL) setShadeAngle: (DSDevice*)device withAngle: (NSNumber*) value {
	if(![device.type isEqualToNumber:[NSNumber numberWithInt:3292]])
        return NO;
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/setOutputValue?%@=%@&value=%i&offset=4",
                                       self.serverIP,
                                       idArg,
                                       device.identifier,
                                       [value intValue]]];
	
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Returns the consumption of the apartment.
- (NSNumber*) getApartmentConsumption {
	//		/json/property/query?query=/apartment/dSMeters/*(dSID,powerConsumption)
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/apartment/getConsumption", self.serverIP]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	[request setShouldAttemptPersistentConnection:NO];
	[request startAsynchronous];
	
	NSData *response = [request responseData];
	if (!response) {
		NSLog(@"error: %@", [request error]);
        [request release];
		return nil;
	}
    
	
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSDictionary *consumptionDict = [parser objectWithString:json_string error:nil];
    
	
    NSNumber *consumption = [NSNumber numberWithFloat:[[[consumptionDict objectForKey:@"result"]objectForKey:@"consumption"] floatValue]];
   
    [json_string release];
    [request release];
	
    return consumption;	
}

//Returns the consumption of the apartment.
- (NSDictionary*) getMetersConsumption {
	//		/json/property/query?query=/apartment/dSMeters/*(dSID,powerConsumption)
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/apartment/dSMeters/*(dSID,powerConsumption,energyMeterValue,dSUID)", self.serverIP]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	[request setShouldAttemptPersistentConnection:NO];
	[request startAsynchronous];
	
	NSData *response = [request responseData];
    [request release];
	if (!response) {
		return nil;
	}
	
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSDictionary *consumptionDict = [parser objectWithString:json_string error:nil];
	[json_string release];
	NSMutableDictionary *returnDict = [NSMutableDictionary dictionaryWithCapacity:1];
	
	id ok = [consumptionDict objectForKey:@"ok"];
	if ([ok isKindOfClass:[NSNumber class]] && [(NSNumber*)ok boolValue]) {
		NSArray *dsmeters = [(NSDictionary*)[consumptionDict objectForKey:@"result"] objectForKey:@"dSMeters"];
		for (NSDictionary *meterDict in dsmeters) {
            NSString *identifier;
            if ([meterDict objectForKey:@"dSUID"]) {
                identifier = [meterDict objectForKey:@"dSUID"];
            }
            else{
                identifier = [meterDict objectForKey:@"dSID"];
            }
            
            if(identifier != nil && meterDict != nil) {
                [returnDict setObject:meterDict forKey:identifier];
            }
		}
	}
	else {
//		if ([self loginUser]) {
//			return nil;
//		}
	}
	return returnDict;	
}


//Returns the consmption of one room.
- (NSNumber*) getRoomConsumption: (DSRoom*) room {
	NSString *name = urlEncode(room.name);
	//Äußerst seltsam, dass getConsumption vom Apartment + dem Namen der Zone aufgerufen werden muss, obwohl es in der API eine gleichnamige Methode für Zone gibt
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/apartment/getConsumption?name=%@", self.serverIP, name]];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	//[request setConnectionCanBeReused:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *zoneConsumptionDict = [parser objectWithString:json_string error:nil];
	
	NSNumber *consumption = [NSNumber numberWithFloat:[[[zoneConsumptionDict objectForKey:@"result"]objectForKey:@"consumption"] floatValue]];
	
	[request release];
	[json_string release];
	
	return consumption;
}

//Returns the last called scenes of the room
- (NSArray*) getLastCalledScenesForRoom: (DSRoom*)room {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/apartment/zones/zone%@/groups/*(group,lastCalledScene)", self.serverIP, room.identifier]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	[request setShouldAttemptPersistentConnection:NO];
	[request startAsynchronous];
	
	NSData *response = [request responseData];
    [request release];
	if (!response) {
		return nil;
	}
	
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSDictionary *lastCalledScenesDict = [parser objectWithString:json_string error:nil];

	[json_string release];
	
	return [[lastCalledScenesDict objectForKey:@"result"]objectForKey:@"groups"];
}


//Returns a boolean-value if the specified device could be successsfully renamed oder not.
- (BOOL) setDeviceName: (DSDevice*)device withString: (NSString*) name {
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/setName?%@=%@&newName=%@", self.serverIP, idArg, device.identifier, urlEncode(name)]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

- (BOOL) moveDevice: (DSDevice*)device toRoom: (DSRoom*) room{
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"deviceID";
    
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/structure/zoneAddDevice?%@=%@&zone=%@", self.serverIP, idArg, device.identifier, room.identifier]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
        if ([(NSNumber*)okString boolValue]) {
            NSLog(@"Successfully moved device:%@ to zone:%@",device.identifier,room.identifier);
            return [(NSNumber*)okString boolValue];
        } else {
            NSLog(@"failed at moving device:%@ to zone:%@",device.identifier,room.identifier);
        }

	}
	return NO;


}

- (BOOL) setOutputModeForDevice: (DSDevice*)device toMode: (int) newMode{
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/setOutputMode?%@=%@&modeID=%i", self.serverIP, idArg, device.identifier, newMode]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	if ([okString isKindOfClass:[NSNumber class]]) {
        if ([(NSNumber*)okString boolValue]) {
            NSLog(@"Successfully set output mode %i on device:%@", newMode, device.identifier);
            return [(NSNumber*)okString boolValue];
        } else {
            NSLog(@"Couldn't set output mode %i on device:%@", newMode, device.identifier);
        }
        
	}
	return NO;
    
    
}


//Returns a boolean-value if the specified room could be successsfully renamed oder not.
- (BOOL) setRoomName: (NSString*) name withRoom: (DSRoom*) room {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/zone/setName?id=%@&newName=%@", self.serverIP, room.identifier, urlEncode(name)]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Returns a boolean-value if the power-state of the specified room could be successsfully turned on/off.
- (BOOL) setRoomPower: (DSRoom*)room withBool:(BOOL) powerOn {
	NSURL *url;
	
	NSString *roomName = urlEncode(room.name);
	
	if (powerOn) {
		url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/set/turnOn?self=.zone('%@')", self.serverIP, roomName]];
	} else {
		url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/set/turnOff", self.serverIP]];
	}
	
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Returns a boolean-value if the specified device could be successsfully added to the specified scene.
- (BOOL) addDeviceToScene: (DSDevice*) device withScene: (DSScene*) scene {
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/saveScene?%@=%@&sceneNumber=%@", self.serverIP, idArg, device.identifier, [NSString stringWithFormat:@"%lld", [scene.sceneNo longLongValue]]]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Returns a boolean-value if the specified scene could be successfully opened for the specified device. Must be done before some device can be added to a scene
- (BOOL) setDontCareBitOnDevice: (DSDevice*) device withScene: (DSScene*) scene withDeleteToggle:(BOOL)deleteToggle {
	NSURL *url;
	
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	if (!deleteToggle) {
		url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/setConfig?class=128&index=%@&%@=%@&value=0", self.serverIP, [NSString stringWithFormat:@"%lld", [scene.sceneNo longLongValue]+128], idArg, device.identifier]];
	} else {
		url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/setConfig?class=128&index=%@&%@=%@&value=255", self.serverIP, [NSString stringWithFormat:@"%lld", [scene.sceneNo longLongValue]+128], idArg, device.identifier]];
	}
	
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Returns a boolean-value if the specified device could be successsfully removed from the specified scene.
- (BOOL) removeDeviceFromScene: (DSDevice*) device withScene: (DSScene*) scene {
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/undoScene?%@=%@&sceneNumber=%@&groupID=1", self.serverIP, idArg, device.identifier, [NSString stringWithFormat:@"%lld", [scene.sceneNo longLongValue]]]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Returns a boolean-value if the power-state of specified scene could be successfully turned on.
- (BOOL) setScenePowerOn: (DSScene*)scene {
	NSURL *url = nil;
	
	if (!scene.room) {
		if([scene.sceneNo longLongValue] < 1024){
            url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/apartment/callScene?sceneNumber=%@&groupID=0", self.serverIP, [NSString stringWithFormat:@"%lld", [scene.sceneNo longLongValue]]]];
        }
        else{
            url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/event/raise?name=highlevelevent&parameter=id=%@", self.serverIP, [NSString stringWithFormat:@"%lld", [scene.sceneNo longLongValue]]]];
        }
	}else {
		NSString *strUrl = [NSString stringWithFormat:@"https://%@/json/zone/callScene?id=%@&sceneNumber=%lld&groupID=%i", self.serverIP, scene.room.identifier, [scene.sceneNo longLongValue], [scene.group intValue] ];
		url = [NSURL URLWithString: strUrl];
		GSLog(@"%@", url);
	}
	
//	GSLog(@"callScene request: %@", url);
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	GSLog(@"callScene response: %@", json_string);
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
//	GSLog(@"%@", json_string);
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if (okString && [okString isKindOfClass:[NSNumber class]]) {
		
		if (![(NSNumber*)okString boolValue]) {
			if ([self loginUser]) {
				[self setScenePowerOn:scene];
			}
		}
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Saves the current output of devices to the passed scene 
- (BOOL) saveScene: (DSScene*)scene {
    if(scene.group != nil && scene.group.integerValue > 0) {
        NSURL *url = nil;
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/zone/saveScene?id=%@&groupID=%@&sceneNumber=%@", self.serverIP, scene.room.identifier, scene.group, scene.sceneNo]];
            
        GSLog(@"Saving scene: %@", url);
        ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
        [request setValidatesSecureCertificate:NO];
        [request startSynchronous];
        
        NSData *response = [request responseData];
        NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
        
        [request release];
        [json_string release];
        
        id okString = [dict objectForKey:@"ok"];
        
        if (okString && [okString isKindOfClass:[NSNumber class]]) {
            
            if (![(NSNumber*)okString boolValue]) {
                if ([self loginUser]) {
                    [self saveScene:scene];
                }
            }
            return [(NSNumber*)okString boolValue];
        }
    }
	return NO;
}

//gets the outputvalue of the passed device
- (int) getOutputOfDevice: (DSDevice*)device {
	NSURL *url = nil;
    
    //offset is related to the group
    int offset = 0;
	if([device.group isEqualToString:@"1"])
        offset = 0;
    else if ([device.group isEqualToString:@"2"])
        offset = 2;
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
     url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/getOutputValue?%@=%@&offset=%i", self.serverIP, idArg, device.identifier,offset]];
    
//	GSLog(@"getting output value: %@", url);
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
    int value = -1;
	
	if (okString && [okString isKindOfClass:[NSNumber class]]) {
		
		if (![(NSNumber*)okString boolValue]) {
			if ([self loginUser]) {
				[self getOutputOfDevice:device];
			}
		}
        else{
            value = [[[dict objectForKey:@"result"]objectForKey:@"value"] intValue];
        }

	}
	return value;
}

//gets the outputvalues of the passed shade device of type 3292 (jalosie, 2 values)
- (NSDictionary*) getOutputsOfShadeDevice: (DSDevice*)device {
    //this should only be done for type 3292
	if(![device.type isEqualToNumber:[NSNumber numberWithInt:3292]])
        return nil;
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
    //get position value
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/getOutputValue?%@=%@&offset=2", self.serverIP, idArg, device.identifier]];
    
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
    int positionValue = -1;
	
	if (okString && [okString isKindOfClass:[NSNumber class]]) {
		
		if (![(NSNumber*)okString boolValue]) {
            return nil;
		}
        else{
            positionValue = [[[dict objectForKey:@"result"]objectForKey:@"value"] intValue];
        }
        
	}
    
    //get angle value
    NSURL *url2 = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/getOutputValue?%@=%@&offset=4", self.serverIP, idArg, device.identifier]];
    
	ASIFormDataRequest *request2 = [[ASIFormDataRequest alloc] initWithURL:url2];
	[request2 setValidatesSecureCertificate:NO];
	[request2 startSynchronous];
	
	NSData *response2 = [request2 responseData];
	NSString *json_string2 = [[NSString alloc] initWithData:response2 encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict2 = [parser objectWithString:json_string2 error:nil];
	
	[request2 release];
	[json_string2 release];
	
	id okString2 = [dict2 objectForKey:@"ok"];
    int angleValue = -1;
	
	if (okString2 && [okString2 isKindOfClass:[NSNumber class]]) {
		if (![(NSNumber*)okString2 boolValue]) {
            return nil;
        }
        else{
            angleValue = [[[dict2 objectForKey:@"result"]objectForKey:@"value"] intValue];
        }
        
	}
    
    NSDictionary *resultDict = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithInt:positionValue], @"position", [NSNumber numberWithInt:angleValue], @"angle", nil];
   
    GSLog(@"got shade position:%@ and angle:%@",[resultDict objectForKey:@"position"], [resultDict objectForKey:@"angle"]);


	return resultDict;
}

- (int) getDeviceConsumption: (DSDevice*) device {
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/getSensorValue?sensorIndex=2&%@=%@", self.serverIP, idArg, device.identifier]];
    
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
    int value = -1;
	
	if (okString && [okString isKindOfClass:[NSNumber class]]) {
		
		if ([(NSNumber*)okString boolValue]) {
            value = [[[dict objectForKey:@"result"]objectForKey:@"sensorValue"] intValue];
        }
        
	}
	return value;
}

- (int) getDeviceMetering: (DSDevice*) device {
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/getSensorValue?sensorIndex=4&%@=%@", self.serverIP, idArg, device.identifier]];
	
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
    int value = -1;
	
	if (okString && [okString isKindOfClass:[NSNumber class]]) {
		
		if ([(NSNumber*)okString boolValue]) {
            value = [[[dict objectForKey:@"result"]objectForKey:@"sensorValue"] intValue];
        }
        
	}
	return value;
}



//Returns a boolean-value if the power-state of specified scene could be successfully turned on.
- (BOOL) undoScene: (DSScene*)scene {
	NSURL *url = nil;
	
	if (!scene.room) {
		url = [NSURL URLWithString:[NSString stringWithFormat: @"https://%@/json/apartment/undoScene?sceneNumber=%lld&groupID=0", self.serverIP, [scene.sceneNo longLongValue]]];
		NSLog(@"undo scene: %@", url);
	} else {
		NSString *strUrl = [NSString stringWithFormat: @"https://%@/json/zone/undoScene?id=%@&sceneNumber=%lld&groupID=%i", self.serverIP, scene.room.identifier, [scene.sceneNo longLongValue], [scene.group intValue]];
		url = [NSURL URLWithString: strUrl];
		GSLog(@"%@", url);
	}
	
//	GSLog(@"callScene request: %@", url);
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	GSLog(@"callScene response: %@", json_string);
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
//	GSLog(@"%@", json_string);
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		if (![(NSNumber*)okString boolValue]) {
			if ([self loginUser]) {
				[self setScenePowerOn:scene];
			}
		}
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//Returns a boolean-value if the specified scene could be successsfully renamed oder not.
- (BOOL) setSceneName: (NSString*) name withRoom: (DSRoom*) room withScene:(DSScene*)scene {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/zone/sceneSetName?id=%@&groupID=%i&sceneNumber=%lld&newName=%@", self.serverIP,  room.identifier, [scene.group intValue], [scene.sceneNo longLongValue], urlEncode(name)]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}

//- (NSMutableDictionary*) getScenes {
//	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/apartment/zones/*(ZoneID)/groups/group1(group,lastCalledScene)/scenes/*(scene,name)", self.serverIP]]; 
//	
//	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
//	[request setValidatesSecureCertificate:NO];
//	// [request setShouldAttemptPersistentConnection:NO];
//	[request startSynchronous];
//	
//	NSError *error = request.error;
//	
//	if (error && [error code] == 3) {
//		if ([self loginUser]) {
//			return [self getScenes];
//		}
//		else {
//			return nil;
//		}
//	}
//	else if(error) {
//		return nil;
//	}
//
//	
//	NSData *response = [request responseData];
//	
//	if (!response) {
//		return nil;
//	}
//	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
//	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
//	
//	
//	
//	NSArray *rooms = [[dict objectForKey:@"result"]objectForKey:@"zones"];
//	
//	
//	NSMutableDictionary *roomScenes = [[NSMutableDictionary alloc] init];
//	
//	for (int i = 0; i<[rooms count]; i++) {
//		
//		NSDictionary *groups = [rooms objectAtIndex:i];
//		
//		NSMutableDictionary *groupsScenes = [[NSMutableDictionary alloc] init];
//		for (int j = 0; j<[[groups objectForKey:@"group1"] count]; j++) {
//			
//			NSMutableDictionary *group = [[groups objectForKey:@"group1"] objectAtIndex:j];
//			
//			if ([group valueForKey:@"group"] != nil) {
//				
//				NSMutableArray *scenes = [[NSMutableArray alloc] init];
//				
//				for (int x = 0; x<[[group objectForKey:@"scenes"] count]; x++) {
//					
//					NSString *sceneName = [[[group objectForKey:@"scenes"] objectAtIndex:x] valueForKey:@"name"];
//					NSString *sceneID = [[[group objectForKey:@"scenes"] objectAtIndex:x] valueForKey:@"scene"];
//					
//					BOOL nameFail = NO;
//					if (sceneName == nil || [sceneName isEqualToString:@""]) {
//						nameFail = YES;
//					} else if ([sceneName length] == 7) {
//						
//						NSRange range = [sceneName rangeOfString:@"Scene"];
//						if (range.location != NSNotFound){
//							NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
//							if ([f numberFromString:[NSString stringWithFormat:@"%c", [sceneName characterAtIndex:5]]] != nil) {
//								if ([f numberFromString:[NSString stringWithFormat:@"%c", [sceneName characterAtIndex:6]]] != nil) {
//									nameFail = YES;
//								}
//							}
//						}
//					}
//					
//					if ([sceneID intValue] > 33 && !nameFail) {
//						NSMutableArray *scene = [[NSMutableArray alloc] initWithObjects:sceneID, sceneName, nil];
//						[scenes addObject:scene];
//					}
//					
//					if ([sceneID intValue]==67) {
//						NSMutableArray *scene = [[NSMutableArray alloc] initWithObjects:sceneID, NSLocalizedString(@"key_default_scene_name_0", @""), nil];
//						[scenes addObject:scene];
//					}
//					
//					if (/*[sceneID intValue] == 0 || */[sceneID intValue] == 5 || [sceneID intValue] == 17 || [sceneID intValue] == 18 || [sceneID intValue] == 19) {
//						NSString *sName = @"";
//						NSRange range = [sceneName rangeOfString:@"T0_S"];
//						
//						if (range.location != NSNotFound) {
//							
//							switch ([sceneID intValue]) {
//								/*case 0:
//									sName = [NSString stringWithFormat:@"Standby"]; 
//									break;*/
//								case 5:
//									sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_scene_name_5", @"")]; 
//									break;
//								case 17:
//									sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_scene_name_17", @"")]; 
//									break;
//								case 18:
//									sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_scene_name_18", @"")]; 
//									break;
//								case 19:
//									sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_scene_name_19", @"")]; 
//									break;
//								default:
//									break;
//							}
//							
//							
//						} else {
//							sName = sceneName;
//						}
//						
//						
//						NSMutableArray *scene = [[NSMutableArray alloc] initWithObjects:sceneID, sName, nil];
//						[scenes addObject:scene];
//					}
//					
//				}
//				
//				
//					NSMutableArray *scene = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:67], NSLocalizedString(@"key_default_scene_name_0", @""), nil];
//					[scenes addObject:scene];
//				
//				
//				NSString *lastCalledScene = [group objectForKey:@"lastCalledScene"];
//				
//				[groupsScenes setObject:scenes forKey:[NSString stringWithFormat:@"%@",[group valueForKey:@"group"]]];
//				[groupsScenes setObject:lastCalledScene forKey:@"lastCalledScene"];
//			}
//		}
//		
//		if ([groups count]>0) {
//			[roomScenes setObject:groupsScenes forKey:[NSString stringWithFormat:@"%@", [groups objectForKey:@"ZoneID"]]];
//		}
//	}
//	
//	[request release];
//	[json_string release];
//	
//	return roomScenes;
//	
//}


- (NSMutableDictionary*) allocScenes {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/apartment/zones/*(ZoneID)/groups/*(group,lastCalledScene)/scenes/*(scene,name)", self.serverIP]]; 
	
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSError *error = request.error;
	
	if (error && [error code] == 3) {
		if ([self loginUser]) {
			return [self allocScenes];
		}
		else {
			return nil;
		}
	}
	else if(error) {
		return nil;
	}
    
	
	NSData *response = [request responseData];
	
	if (!response) {
		return nil;
	}
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	
	
	NSArray *rooms = [[dict objectForKey:@"result"]objectForKey:@"zones"];
	
	
	NSMutableDictionary *roomScenes = [[NSMutableDictionary alloc] init];
	
	for (int i = 0; i<[rooms count]; i++) {
		
		NSDictionary *groups = [rooms objectAtIndex:i];
		
		NSMutableDictionary *groupsScenes = [[NSMutableDictionary alloc] init];
		for (int j = 0; j<[[groups objectForKey:@"groups"] count]; j++) {
			
			NSMutableDictionary *group = [[groups objectForKey:@"groups"] objectAtIndex:j];
			
            if ([group valueForKey:@"group"] != nil) {
				
				NSMutableArray *scenes = [[NSMutableArray alloc] init];
				
				for (int x = 0; x<[[group objectForKey:@"scenes"] count]; x++) {
					
					NSString *sceneName = [[[group objectForKey:@"scenes"] objectAtIndex:x] valueForKey:@"name"];
					NSString *sceneID = [[[group objectForKey:@"scenes"] objectAtIndex:x] valueForKey:@"scene"];
					
//					
//					if ([sceneID intValue] == 0 || [sceneID intValue] == 5 || [sceneID intValue] == 17 || [sceneID intValue] == 18 || [sceneID intValue] == 19) {
						NSString *sName = @"";
						NSRange range = [sceneName rangeOfString:@"T0_S"];
						
						if (range.location != NSNotFound || [sceneName isEqualToString:@""]) {
							
							switch ([sceneID intValue]) {
                                case 0:
                                    if(j == 1 || j == 3 || j == 4 || j == 5)
                                        sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_scene_name_0", @"")];
                                    else if(j == 2)
                                        sName = [NSString stringWithString:NSLocalizedString(@"key_default_shade_scene_name_0", @"")];									
                                    break;

								case 5:
                                    if(j == 1 || j == 3 || j == 4 || j == 5)
                                        sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_scene_name_5", @"")];
                                    else if(j == 2)
                                        sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_shade_scene_name_5", @"")];
									break;
								case 17:
                                    if(j == 1 || j == 3 || j == 4 || j == 5)
                                        sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_scene_name_17", @"")];
                                    else if(j == 2)
                                        sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_shade_scene_name_17", @"")];
                                    break;
                                case 18:
                                    if(j == 1 || j == 3 || j == 4 || j == 5)
                                        sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_scene_name_18", @"")];
                                    else if(j == 2)
                                        sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_shade_scene_name_18", @"")];
                                    break;
                                case 19:
                                    if(j == 1 || j == 3 || j == 4 || j == 5)
                                        sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_scene_name_19", @"")]; 
                                    else if(j == 2)
                                        sName = [NSString stringWithFormat:NSLocalizedString(@"key_default_shade_scene_name_19", @"")]; 
                                    break;
                                default:
                                    break;
							}
							
							
						} else {
							sName = sceneName;
						}
						
						
						NSMutableArray *scene = [[NSMutableArray alloc] initWithObjects:sceneID, sName, nil];
						[scenes addObject:scene];
                        [scene release];
//					}
					
				}				
				
				NSString *lastCalledScene = [group objectForKey:@"lastCalledScene"];
				[groupsScenes setObject:scenes forKey:[NSString stringWithFormat:@"%@",[group valueForKey:@"group"]]];
                if (j == 1) {
                    [groupsScenes setObject:lastCalledScene forKey:@"lastCalledScene"];
                } else if (j == 2){
                    [groupsScenes setObject:lastCalledScene forKey:@"lastCalledShadeScene"];
                } else if (j == 3){
                    [groupsScenes setObject:lastCalledScene forKey:@"lastCalledBlueScene"];
                } else if (j == 4){
                    [groupsScenes setObject:lastCalledScene forKey:@"lastCalledAudioScene"];
                } else if (j == 5){
                    [groupsScenes setObject:lastCalledScene forKey:@"lastCalledVideoScene"];
                }
                
				
			}
		}
		
		if ([groups count]>0) {
			[roomScenes setObject:groupsScenes forKey:[NSString stringWithFormat:@"%@", [groups objectForKey:@"ZoneID"]]];
		}
        [groupsScenes release];
	}
	
	[request release];
	[json_string release];
	
	return roomScenes;
	
}



- (BOOL) loginUser {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/system/loginApplication?loginToken=%@", self.serverIP, self.dssApplicationToken]]; 
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	//[request setTimeOutSeconds:20];
	[request setValidatesSecureCertificate:NO];
	//[request setAuthenticationScheme:kCFHTTPAuthenticationSchemeDigest];
	//[request setUsername:self.dssUsername];
	//[request setPassword:self.dssPassword];
//	GSLog(@"starting login");
	NSLog(@"login url: %@",[url description]);
	dispatch_async(dispatch_get_main_queue(), ^{
		[[NSNotificationCenter defaultCenter] postNotificationName:k_DATA_LOGIN_STARTED object:self];
	});
	
	[request startSynchronous];
    if([request error]){
        GSLog(@"login error %@", [request error]);
    }

	
	NSData *response = [request responseData];
	
	if ([request error]) {
		dispatch_sync(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:k_DATA_LOGIN_FAILED object:self];
		});
		[request release];
		return NO;
	}
	
	if (response) {
		GSLog(@"json response login: %@", [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
		
		NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
		NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
		
		[json_string release];
		
		id okString = [dict objectForKey:@"ok"];
		
		if (okString && [okString isKindOfClass:[NSNumber class]]) {
			if ([(NSNumber*)okString boolValue]) {
				dispatch_sync(dispatch_get_main_queue(), ^{
					[[NSNotificationCenter defaultCenter] postNotificationName:k_DATA_LOGIN_SUCCESSFUL object:self];
					[[ModelLocator instance] setValue:@"1" forKey:k_DSS_AVAILABLE];
					
					/*
					 *	mark all other dsServers in database as "not connected"
					 */
					[[DataController instance] markDsServersAsDisconnected];
					
					
					/*
					 *	make sure current dss is connected
					 */
					[[DataController instance] getCurrentDSS].connected = YES;
					
					[[DataController instance] saveContext];
					
					/*
					 *	send notification about connection state
					 */
					[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationConnectedToDss object:nil];
				});
				[request release];
				return YES;
			}
			else {
				//	application token no longer valid
                if(self.dssApplicationToken){
                    dispatch_sync(dispatch_get_main_queue(), ^{
						
                        [[NSNotificationCenter defaultCenter] postNotificationName:k_DATA_APPLICATION_TOKEN_NOT_VALID object:self];
                    });
                }
                else{
                    //retry in 1 sec
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2000000000), dispatch_get_main_queue(), ^{
//                        if([self loginUser]);
//                    });

                }
				
				dispatch_sync(dispatch_get_main_queue(), ^{
					if ([[DataController instance] getCurrentDSS]) {
						[[DataController instance] getCurrentDSS].connected = NO;
					}
				});
				
				[request release];
				return NO;
			}
		}
	}
	
	[request release];
	
	dispatch_sync(dispatch_get_main_queue(), ^{
		[[NSNotificationCenter defaultCenter] postNotificationName:k_DATA_LOGIN_FAILED object:self];
	});
	return NO;
}


- (BOOL)deviceInScene: (DSDevice*) device withSceneNumber:(int)sceneNumber {
	
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	//1. Aufruf auf die Apartment-Szene
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/getConfig?class=128&%@=%@&index=%@", self.serverIP, idArg, device.identifier, [NSString stringWithFormat:@"%i", sceneNumber]]];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
//	GSLog(@"DeviceInSceneDict: %@", dict);
	
	[request release];
	[json_string release];
	
	if ([[[dict objectForKey:@"result"] objectForKey:@"value"] intValue]==255) {
		return NO;
	} else if ([[[dict objectForKey:@"result"] objectForKey:@"value"] intValue]==0) {
		return YES;
	} else {
		return NO;
	}
}


- (int) getSceneDeviceStatus: (DSDevice*)device withSceneNumber:(int)sceneNumber {
    
    NSString *idArg;
    if (device.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"dsid";
    
	//2.- Aufruf auf die Raum Szene
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/getConfig?class=128&%@=%@&index=%@", self.serverIP, idArg, device.identifier, [NSString stringWithFormat:@"%i", sceneNumber]]];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
//	GSLog(@"getSceneDeviceStatus: %@", dict);
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		if ([(NSNumber*)okString boolValue]) {
			return [[[dict objectForKey:@"result"] objectForKey:@"value"] intValue];
		} else {
			return -1;
		}
		
	}
	return -1;
}

- (NSMutableDictionary*) getDSSLocation{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/config/geodata(latitude,longitude)", self.serverIP]];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSDictionary *dict = [parser objectWithString:json_string error:nil];
    NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]initWithCapacity:2];
    
    if (dict && [dict isKindOfClass:[NSDictionary class]]) {
        if([[[dict objectForKey:@"result"] objectForKey:@"geodata"] count] > 0){
            NSNumber *latitude = [NSNumber numberWithFloat:[[[[[dict objectForKey:@"result"] objectForKey:@"geodata"] objectAtIndex:0]objectForKey:@"latitude"] floatValue]];
            NSNumber *longitude = [NSNumber numberWithFloat:[[[[[dict objectForKey:@"result"] objectForKey:@"geodata"] objectAtIndex:0]objectForKey:@"longitude"] floatValue]];
            
            [resultDict setValue:latitude forKey:@"latitude"];
            [resultDict setValue:longitude forKey:@"longitude"];
        }
    }
	
	[request release];
	[json_string release];
	
	return resultDict;
}


- (NSString*) getDSSVersion {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/system/version(distroVersion)", self.serverIP]];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
    [request release];
    
    if (response) {
        NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
        [json_string release];
        id okString = [dict objectForKey:@"ok"];
		
		if ([okString isKindOfClass:[NSNumber class]]) {
			if ([(NSNumber*)okString boolValue]) {
                NSString *result = [[[[dict objectForKey:@"result"]objectForKey:@"version"] objectAtIndex:0] objectForKey:@"distroVersion"];
                return result;
            }
        }
    }
    
    return nil;
}

- (NSString*) getApartmentName {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/apartment/getName", self.serverIP]]; 
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	
	[request release];
	
	if (response) {
		NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
		NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
		
		[json_string release];
		
		id okString = [dict objectForKey:@"ok"];
		
		if ([okString isKindOfClass:[NSNumber class]]) {
			if ([(NSNumber*)okString boolValue]) {
				return [[dict objectForKey:@"result"] objectForKey:@"name"];
			} else {
				return @"dSS";
			}
		}
	}
	
	return @"dSS";
}

- (NSMutableArray*) allocDSMs {
//	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/apartment/dSMeters/*(dSID,powerConsumption,energyMeterValue,apiVersion,name,dSUID)", self.serverIP]];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/apartment/getCircuits", self.serverIP]];
    
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSError *error = request.error;
	
	if (error && [error code] == 3) {
		if ([self loginUser]) {
            [request release];
			return [self allocDSMs];
		}
		else {
            [request release];
			return nil;
		}
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
//	GSLog(@"json_string: %@", dict);
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		if ([(NSNumber*)okString boolValue]) {
            NSArray *arg = [[NSArray alloc] initWithArray:[[dict objectForKey:@"result"] objectForKey:@"circuits"]];
			NSMutableArray *dsms = [self allocDSMProperties:arg];
            [arg release];
			return dsms;
		} else {
			return nil;
		}
	} return nil;
}

- (NSMutableArray*) allocDSMProperties: (NSArray*)dsMeters {
	NSMutableArray *a = [[NSMutableArray alloc] init];
	
//	GSLog(@"Class: %@", [[dsMeters objectAtIndex:0] class]);
	
	for (NSDictionary *dsmDict in dsMeters) {
        //#8306 Filtering only dsm11(16), dsm12(17), VDC(33) -> Ignoring VDSM(32)
        NSInteger busMemberType = [[dsmDict objectForKey:@"busMemberType"] integerValue];
        if(!(busMemberType == 16 || busMemberType == 17 || busMemberType == 33)) {
            continue;
        }
        
        NSString *dsid;
        
        if ([dsmDict objectForKey:@"dSUID"]) {
            dsid = [dsmDict objectForKey:@"dSUID"];
        }else{
            dsid = [dsmDict objectForKey:@"dsid"];
        }
        
        //if the "hasMetering" isn't present, it cannot be a vdsm, so it has metering per default
        NSNumber *meteringEnabled = @1;
        if ([dsmDict objectForKey:@"hasMetering"]) {
            meteringEnabled = [dsmDict objectForKey:@"hasMetering"];
        }
        
//		NSNumber *powerConsumption = [NSNumber numberWithInt:[[dsmDict objectForKey:@"powerConsumption"] intValue]];
//		NSNumber *energyMeterValue = [NSNumber numberWithInt:[[dsmDict objectForKey:@"energyMeterValue"] intValue]];
		NSNumber *apiVersion = [NSNumber numberWithInt:[[dsmDict objectForKey:@"apiVersion"] intValue]];
		NSString *name = [dsmDict objectForKey:@"name"];
		
		NSMutableDictionary *d = [[NSMutableDictionary alloc] init];							  
		[d setObject:dsid forKey:@"dsid"];
//		[d setObject:powerConsumption forKey:@"powerConsumption"];
//		[d setObject:energyMeterValue forKey:@"energyMeterValue"];
		[d setObject:apiVersion forKey:@"apiVersion"];
		[d setObject:name forKey:@"name"];
        [d setObject:meteringEnabled forKey:@"hasMetering"];
		
		[a addObject:d];
	}
	return a;
}

- (NSArray*) getDSMConsumptionInfo:(DSMeter*)meter {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/apartment/dSMeters/%@(powerConsumption,energyMeterValue)", self.serverIP, meter.identifier]]; 
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
//	GSLog(@"json_string: %@", dict);
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		if ([(NSNumber*)okString boolValue]) {
			
			return [[dict objectForKey:@"result"] objectForKey:[NSString stringWithFormat:@"%@", meter.identifier]];
		} else {
			return nil;
		}
	} return nil;
}

- (BOOL) setMeterName: (NSString*) name withMeter: (DSMeter*) meter {
    
    NSString *idArg;
    if (meter.isUsingDsuid)
        idArg = @"dsuid";
    else
        idArg = @"id";
    
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/circuit/setName?%@=%@&newName=%@", self.serverIP, idArg, meter.identifier, urlEncode(name)]];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]]) {
		return [(NSNumber*)okString boolValue];
	}
	return NO;
}


- (BOOL) callStandardScene: (NSString*)standardScene withRoom:(DSRoom*)room withGroup:(int)group {
	
	int sceneNo = 0;
	BOOL isStandardScene = YES;
	
	if ([standardScene isEqualToString:k_SERVICE_SCENE_MAX]) {
		sceneNo = 14;
	} else if ([standardScene isEqualToString:k_SERVICE_SCENE_MIN]) {
		sceneNo = 13;
	} else if ([standardScene isEqualToString:k_SERVICE_SCENE_DEC]) {
		sceneNo = 11;
	} else if ([standardScene isEqualToString:k_SERVICE_SCENE_INC]) {
		sceneNo = 12;
	} else if ([standardScene isEqualToString:k_SERVICE_SCENE_STOP]) {
		sceneNo = 15;
	}
//	else if ([standardScene isEqualToString:k_SERVICE_SCENE_STIMMUNG1]) {
//		sceneNo = 5;
//	}
    else if ([standardScene isEqualToString:k_SERVICE_SCENE_STANDBY]) {
		sceneNo = 0;
	}
    else {
		isStandardScene = NO;
	}
	
	
	if (isStandardScene) {
		NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/zone/callScene?id=%@&sceneNumber=%i&groupID=%i", self.serverIP, room.identifier, sceneNo, group ]];
		NSLog(@"callScene url: %@", url);
		ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
		[request setValidatesSecureCertificate:NO];
		// [request setShouldAttemptPersistentConnection:NO];
		[request startSynchronous];
		
		NSData *response = [request responseData];
		NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
		NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
		
		[request release];
		[json_string release];
		
//		GSLog(@"%@", json_string);
		
		id okString = [dict objectForKey:@"ok"];
		
		if ([okString isKindOfClass:[NSNumber class]]) {
			if (![(NSNumber*)okString boolValue]) {
				if ([self loginUser]) {
					[self callStandardScene:standardScene withRoom:room withGroup:group];
				}
			}
			
            if ([standardScene isEqualToString:k_SERVICE_SCENE_STOP]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(group == 2)
                        room.lastCalledShadeScene = [NSString stringWithFormat:@"%i", sceneNo];
                });
            }
            
			return [(NSNumber*)okString boolValue];
		} 
		return NO;
	} else {
		return NO;
	}
}

- (BOOL) callStandardScene: (NSString*)standardScene withDevice:(DSDevice*)device withGroup:(int)group  {
	
	int sceneNo = 0;
	BOOL isStandardScene = YES;
	
	if ([standardScene isEqualToString:k_SERVICE_SCENE_MAX]) {
		sceneNo = 14;
	} else if ([standardScene isEqualToString:k_SERVICE_SCENE_MIN]) {
		sceneNo = 13;
	} else if ([standardScene isEqualToString:k_SERVICE_SCENE_DEC]) {
		sceneNo = 11;
	} else if ([standardScene isEqualToString:k_SERVICE_SCENE_INC]) {
		sceneNo = 12;
	} else if ([standardScene isEqualToString:k_SERVICE_SCENE_STOP]) {
		sceneNo = 15;
	} else if ([standardScene isEqualToString:k_SERVICE_SCENE_STANDBY]) {
		sceneNo = 0;
	} else {
		isStandardScene = NO;
	}
	
	
	if (isStandardScene) {
        
        NSString *idArg;
        if (device.isUsingDsuid)
            idArg = @"dsuid";
        else
            idArg = @"dsid";
        
		NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/device/callScene?sceneNumber=%@&groupID=%i&%@=%@", self.serverIP, [NSString stringWithFormat:@"%i", sceneNo], group, idArg, device.identifier]];
//		NSLog(@"callScene request: %@", url);
		
		ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
		[request setValidatesSecureCertificate:NO];
		// [request setShouldAttemptPersistentConnection:NO];
		[request startSynchronous];
		
		NSData *response = [request responseData];
		NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
		NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
		
//		GSLog(@"StandardScene: %@", json_string);
		
		[request release];
		[json_string release];
		
		id okString = [dict objectForKey:@"ok"];
		
		if ([okString isKindOfClass:[NSNumber class]]) {
			return [(NSNumber*)okString boolValue];
		} return NO;
	} else {
		return NO;
	}
	
	
}


- (NSString *) getMacAddressOfDss {
	if (![self loginUser]) {
		return nil;
	}
	
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/system/host/interfaces/eth0(mac,ip)", self.serverIP]]; 
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request setValidatesSecureCertificate:NO];
	// [request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	
	[request release];
	
	if (response) {
		NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
		NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
		
		[json_string release];
		
		id okString = [dict objectForKey:@"ok"];
		
		if ([okString isKindOfClass:[NSNumber class]]) {
			if ([(NSNumber*)okString boolValue]) {
				NSArray *interfaces = [[dict objectForKey:@"result"] objectForKey:@"eth0"];
				
				if (interfaces && [interfaces count] > 0) {
					NSDictionary *interfaceDict = (NSDictionary*)[interfaces objectAtIndex:0];
					
					if (interfaceDict) {
						return [interfaceDict valueForKey:@"mac"];
					}
				}
				
				return nil;
			} else {
				return nil;
			}
		}
	}
	
	return nil;
}


- (NSString*) getApplicationToken: (NSString*) ipAddress {
    
	NSString *urlStr = [NSString stringWithFormat:@"https://%@/json/system/requestApplicationToken?applicationName=%@", ipAddress, urlEncode([NSString stringWithFormat:@"%@: %@", [[UIDevice currentDevice] name], [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey]])];

	NSURL *url = [NSURL URLWithString: urlStr];
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	request.useCookiePersistence = NO;
	[request setValidatesSecureCertificate:NO];
	[request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]] && [(NSNumber*)okString boolValue]) {
		NSDictionary *resultDict = [dict objectForKey:@"result"];
		
		if (resultDict && [resultDict isKindOfClass:[NSDictionary class]]) {
			NSString *appToken = [resultDict objectForKey:@"applicationToken"];
			
			if (appToken && [appToken isKindOfClass:[NSString class]]) {
				//	apptoken is there
				return appToken;
			}
		}
		return nil;
	}
	return nil;
}

- (NSString*) performLogin: (NSString*) username withPassword: (NSString*) password withIp: (NSString*) ipAddress {
	NSString *urlStr = [NSString stringWithFormat:@"https://%@/json/system/login?user=%@&password=%@", ipAddress, urlEncode(username), urlEncode(password)];
	NSURL *url = [NSURL URLWithString: urlStr];
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	request.useCookiePersistence = NO;
	[request setValidatesSecureCertificate:NO];
	[request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error (%i) from perform login: %@", [request responseStatusCode], [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]] && [(NSNumber*)okString boolValue]) {
		
		NSDictionary *resultDict = [dict objectForKey:@"result"];
		
		if (resultDict && [resultDict isKindOfClass:[NSDictionary class]]) {
			NSString *token = [resultDict objectForKey:@"token"];
			
			if (token && [token isKindOfClass:[NSString class]]) {
				//	apptoken is there
				return token;
			}
		}
		return nil;
	} else {
		return nil;
	}
	
}

- (BOOL) performDsLogin: (NSString*) username withPassword: (NSString*) password andCloudUrl: (NSString*)cloudUrl {
	
	NSDictionary *dict = [self checkDsLogin:username withPassword:password andCloudUrl:cloudUrl];
	
	if (dict) {
		[[ModelLocator instance] setValue:[dict valueForKey:@"RelayLink"] forKey:k_DSS_IP];
		[DigitalstromJSONService instance].dssApplicationToken = [dict valueForKey:@"Token"];
		[[ModelLocator instance] setValue:[dict valueForKey:@"Token"] forKey:k_DSS_APPLICATION_TOKEN];
        
        // Allow tracking after sign in via a cloud account
        [GAIHelper allowTracking:true];
		
		return YES;
	}
	else {
		return NO;
	}
	
	
}

- (NSDictionary*) checkDsLogin: (NSString*) username withPassword: (NSString*) password andCloudUrl: (NSString*)cloudUrl {
    //Get the UUID of the phone
    NSString *phoneUUID = [[NSUserDefaults standardUserDefaults] objectForKey:kApplicationUUIDKey];

	NSString *name = [[UIDevice currentDevice] name];
    NSBundle *bundle = [NSBundle mainBundle];
    // Attempt to find a name for this application
	NSString *appName = [bundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];
	if (!appName) {
		appName = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
	}
    if(!appName){
        appName = @"digitalSTROM";
    }
    
	NSString *urlStr = [NSString stringWithFormat: @"%@public/accessmanagement/V1_0/RemoteConnectivity/GetRelayLinkAndToken?user=%@&password=%@&appName=%@&mobileAppUuid=%@&mobileName=%@", cloudUrl, urlEncode(username), urlEncode(password), urlEncode(appName),phoneUUID, urlEncode(name)];

    NSLog(@"loggin in with %@",urlStr);

	NSURL *url = [NSURL URLWithString: urlStr];
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
    [request setRequestMethod:@"POST"];
	request.useCookiePersistence = NO;
	[request setValidatesSecureCertificate:NO];
	[request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error (%i) from digitalstrom.com login: %@", [request responseStatusCode], [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	NSNumber *okString = [dict objectForKey:@"ReturnCode"];
	NSLog(@"%@",[dict description]);
	if ([okString integerValue] == 0) {
		
		NSDictionary *resultDict = [dict objectForKey:@"Response"];
		
        NSLog(@"Retrieved relay link and app token");
        
		if (resultDict && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSString *retrievedRelayLink = [resultDict objectForKey:@"RelayLink"];
            NSString *retrievedAppToken = [resultDict objectForKey:@"Token"];
            
            retrievedRelayLink = [retrievedRelayLink stringByReplacingOccurrencesOfString:@"https://" withString:@""];
            NSLog(@"got link and token: %@ --- %@", retrievedRelayLink, retrievedAppToken);
            //retrievedRelayLink = [NSString stringWithFormat:@"%@:8080",retrievedRelayLink];
            
            NSMutableDictionary *dict = [NSMutableDictionary new];
            [dict setValue:retrievedAppToken forKey:@"Token"];
			[dict setValue:retrievedRelayLink forKey:@"RelayLink"];
			
            return dict;
		}
		return nil;
	}
	else {
		return nil;
	}
	
}


- (BOOL) performEnableToken: (NSString*) appToken withIp: (NSString*) ipAddress andLoginToken: (NSString*) token {
	NSString *urlStr = [NSString stringWithFormat:@"https://%@/json/system/enableToken?applicationToken=%@&token=%@", ipAddress, appToken, token];
	NSURL *url = [NSURL URLWithString: urlStr];

	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	request.useCookiePersistence = NO;
	[request setValidatesSecureCertificate:NO];
	[request setShouldAttemptPersistentConnection:NO];
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error(%i) from enable token: %@", [request responseStatusCode],  [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	id okString = [dict objectForKey:@"ok"];
	
	if ([okString isKindOfClass:[NSNumber class]] && [(NSNumber*)okString boolValue]) {		
		return YES;
	} else {
		return NO;
	}
	
}

- (NSMutableArray*) getHighLevelEvents {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/json/property/query?query=/usr/events/*(name,id)", self.serverIP]]; 
    
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
    [request setValidatesSecureCertificate:NO];
    // [request setShouldAttemptPersistentConnection:NO];
    [request startSynchronous];
    
    NSError *error = request.error;
    
    if (error && [error code] == 3) {
        if ([self loginUser]) {
            return [self getHighLevelEvents];
        }
        else {
            return nil;
        }
    }
    else if(error) {
        return nil;
    }
    
    NSData *response = [request responseData];
	
    if (!response) {
        return nil;
    }
    
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
    
    NSMutableArray *events = [[dict objectForKey:@"result"]objectForKey:@"events"];
    NSMutableArray *allScenes = [[NSMutableArray alloc] init];
    
    //	NSMutableDictionary *roomScenes = [[NSMutableDictionary alloc] init];
    //	
    for (int i = 0; i<[events count]; i++) {
        
        //		NSDictionary *groups = [rooms objectAtIndex:i];
        //		
        //		NSMutableDictionary *groupsScenes = [[NSMutableDictionary alloc] init];
        //		for (int j = 0; j<[[groups objectForKey:@"groups"] count]; j++) {
        //			
        //			NSMutableDictionary *group = [[groups objectForKey:@"groups"] objectAtIndex:j];
        //			
        //            if ([group valueForKey:@"group"] != nil) {
        //				
        //				
        
        //				for (int x = 0; x<[[group objectForKey:@"scenes"] count]; x++) {
        NSString *tempId = [[events objectAtIndex:i] valueForKey:@"id"];
        NSString *sceneName = [[events objectAtIndex:i] valueForKey:@"name"];
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        NSNumber *sceneId = [f numberFromString:tempId];
        
        NSMutableArray *scene = [[NSMutableArray alloc] initWithObjects:sceneId, sceneName, nil];
        [allScenes addObject:scene];
        [scene release];
        
    }
    
    //    
    //    if ([groups count]>0) {
    //        [roomScenes setObject:groupsScenes forKey:[NSString stringWithFormat:@"%@", [groups objectForKey:@"ZoneID"]]];
    //    }
    //    [groupsScenes release];
	
    [request release];
    [json_string release];
    
    return allScenes;
    
}


NSString * urlEncode(NSString *string) {
    static NSString * const kAFLegalCharactersToBeEscaped = @"?!@#$^&%*+=,:;'\"`<>()[]{}/\\|~ ";
    
	return [(NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)string, NULL, (CFStringRef)kAFLegalCharactersToBeEscaped, CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)) autorelease];
}

#pragma mark -
#pragma mark Cloud Service calls

- (NSDictionary*) cloudServiceDeleteTags:(NSArray *)tags withAuthentication: (NSString*) sessionToken andDeviceId: (NSString*) deviceId {
    [self checkIfCloudUrlIsSet];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@public/service/v1_0/Tagging/DeleteTags?appToken=%@&objectId=%@&objectType=DSDevice", [[DataController instance] getCurrentDSS].cloudUrl, sessionToken, deviceId];
    NSURL *url = [NSURL URLWithString: urlStr];
    
    
    NSMutableArray *jsonTags = [[NSMutableArray alloc] init];
    for(NSString *tag in tags)
    {
        [jsonTags addObject: @{@"IrdiPR":@"",
                               @"PropertyShortName":tag}];
    }
    NSError *error;
    NSDictionary *jsonBody = @{@"TagsToDelete": jsonTags};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonBody
                                    options:NSJSONWritingPrettyPrinted
                                      error:&error];

    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    request.requestMethod = @"POST";
    [request setPostBody:[NSMutableData dataWithData: jsonData]];
    [request startSynchronous];
    
    if ([request error]) {
        NSLog(@"error(%i) cloudservice deleteTags: %@", [request responseStatusCode],  [request error]);
    }
    
    NSData *response = [request responseData];
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
    
    [request release];
    [json_string release];
    
    return dict;

}


- (NSDictionary*) cloudServiceSetTagMaxPower: (NSString*) tagValue withAuthentication: (NSString*) sessionToken andDeviceId: (NSString*) deviceId {
    
    [self checkIfCloudUrlIsSet];
    
	//	https://dsservices.aizo.com/service/v1_0/Tagging/SetTag?appToken=d9b2d3d0d4ae98ffb3263413002f3800bd34a555150818ba325b60b1f63ba646&objectId=<DSID bei DSDevice oder GTIN bei OEMArticle> &irdiPR=&PropertyShortName=MAX_POWER&irdiVA=&valueShortName=WATT&value=10&languageCode=XX-xx&objectType=<DSDevice oder OEMArticle>
	
	NSString *urlStr = [NSString stringWithFormat:@"%@public/service/v1_0/Tagging/SetTag?appToken=%@&objectId=%@&objectType=DSDevice&irdiPR=&PropertyShortName=MAX_POWER&irdiVA=&valueShortName=WATT&value=%@&languageCode=XX-xx", [[DataController instance] getCurrentDSS].cloudUrl, sessionToken, deviceId, tagValue];
	NSLog(@"calling url: %@", urlStr);
	NSURL *url = [NSURL URLWithString: urlStr];
    
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request addRequestHeader:@"Content-Type" value:@"application/json"];
	request.requestMethod = @"POST";
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error(%i) cloudservice setTag (max power): %@", [request responseStatusCode],  [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	return dict;
}

- (NSDictionary*) cloudServiceSetTagSocket: (NSString*) tagValue withAuthentication: (NSString*) sessionToken andDeviceId: (NSString*) deviceId {
	//	https://dsservices.aizo.com/service/v1_0/Tagging/SetTag?appToken=d9b2d3d0d4ae98ffb3263413002f3800bd34a555150818ba325b60b1f63ba646&objectId=<DSID bei DSDevice oder GTIN bei OEMArticle> &irdiPR=&PropertyShortName=MAX_POWER&irdiVA=&valueShortName=WATT&value=10&languageCode=XX-xx&objectType=<DSDevice oder OEMArticle>
    
    [self checkIfCloudUrlIsSet];
    
	NSString *urlStr = [NSString stringWithFormat:@"%@public/service/v1_0/Tagging/SetTag?appToken=%@&objectId=%@&objectType=DSDevice&irdiPR=&PropertyShortName=SOCKET_TYPE&irdiVA=&valueShortName=%@&value=&languageCode=XX-xx", [[DataController instance] getCurrentDSS].cloudUrl, sessionToken, deviceId, tagValue];
	NSLog(@"calling url: %@", urlStr);
	NSURL *url = [NSURL URLWithString: urlStr];
	
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	request.requestMethod = @"POST";
	[request addRequestHeader:@"Content-Type" value:@"application/json"];
	
	//request.useCookiePersistence = NO;
	//[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error(%i) cloudservice setTag (socket): %@", [request responseStatusCode],  [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	return dict;
}

- (NSDictionary*) cloudServiceGetAllTagsWithAuthentication: (NSString*) sessionToken andObjectId: (NSString*) objectId andType: (NSString*) objectType {
    
    [self checkIfCloudUrlIsSet];
    
	NSString *urlStr = [NSString stringWithFormat:@"%@public/service/v1_0/Tagging/GetAllTags?appToken=%@&objectId=%@&languageCode=XX-xx&includeMetadata=false&objectType=%@", [[DataController instance] getCurrentDSS].cloudUrl, sessionToken, objectId, objectType];
	NSLog(@"calling url: %@", urlStr);
	NSURL *url = [NSURL URLWithString: urlStr];
	
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request addRequestHeader:@"Content-Type" value:@"application/json"];
	//request.useCookiePersistence = NO;
	//[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error(%i) cloudservice getAllTags: %@", [request responseStatusCode],  [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	return dict;
}

- (NSDictionary*) cloudServiceGetAllIlluminantsForDevice: (NSString*) deviceId withAuthentication: (NSString*) sessionToken {
    
    [self checkIfCloudUrlIsSet];
    
	NSString *urlStr = [NSString stringWithFormat:@"%@public/MaterialMaster/v1_0/IlluminantService/GetIlluminants?token=%@&languageCode=XX_xx&dSid=%@", [[DataController instance] getCurrentDSS].cloudUrl, sessionToken, deviceId];
	NSLog(@"calling url: %@", urlStr);
	NSURL *url = [NSURL URLWithString: urlStr];
	
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request addRequestHeader:@"Content-Type" value:@"application/json"];
	//request.useCookiePersistence = NO;
	//[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error(%i) cloudservice getAllTags: %@", [request responseStatusCode],  [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	return dict;
}


- (NSDictionary*) cloudServiceGetDeviceDetailsForDeviceWithGtin: (NSString*) gtin{
    
    [self checkIfCloudUrlIsSet];

	NSString *urlStr = [NSString stringWithFormat:@"%@public/MaterialMaster/v1_0/ArticleDataService/GetBasicArticleData?ean=%@&languageCode=de&countryCode=de", [[DataController instance] getCurrentDSS].cloudUrl, gtin];
	NSURL *url = [NSURL URLWithString: urlStr];
    
	NSLog(@"get cloud device details: %@", urlStr);
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request addRequestHeader:@"Content-Type" value:@"application/json"];
	//request.useCookiePersistence = NO;
	//[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error(%i) cloudservice getDeviceDetails: %@", [request responseStatusCode],  [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	NSLog(@"response GetDeviceDetails: %@", json_string);
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	return dict;
}


- (NSDictionary*) cloudServiceCheckIlluminantForDss: (NSString*) sessionToken andDeviceId: (NSString*) deviceId andEan: (NSString*) ean {
	/*
	 deviceId = @"3504175fe00000000000fd68";
	 macAddress = @"a8:99:5c:ff:02:ee";
	 ean = @"4260150056647";
	 
	 
	 //	ReturnCode 0 - OK
	 deviceId = @"3504175fe00000000000f56b";
	 macAddress = @"aa:bb:cc:00:0a:b6";
	 ean = @"4008321975409";
	 
	 
	 //	ReturnCode 1 - UNKNOWN_MAC_ADDRESS
	 deviceId = @"3504175fe00000000000f56b";
	 macAddress = @"aa:aa:bb:cc:dd:ee";
	 ean = @"4008321975409";
	 
	 //	ReturnCode 2 - UNKNOWN_DSID
	 deviceId = @"3504175fe000000000000000";
	 macAddress = @"aa:bb:cc:00:0a:b6";
	 ean = @"4008321975409";
	 
	 //	ReturnCode 3 - EAN_NOT_FOUND
	 deviceId = @"3504175fe00000000000f56b";
	 macAddress = @"aa:bb:cc:00:0a:b6";
	 ean = @"0000000000111";
	 
	 //	ReturnCode 4 - EAN_NO_MATCH
	 deviceId = @"3504175fe00000000000f56b";
	 macAddress = @"aa:bb:cc:00:0a:b6";
	 ean = @"0000000000222";
	 
	 //	ReturnCode 5 - EAN_NOT_DIMMABLE
	 deviceId = @"3504175fe00000000000f56b";
	 macAddress = @"aa:bb:cc:00:0a:b6";
	 ean = @"0000000000333";
	 
	 //	ReturnCode 6 - DSID_NOT_DIMMABLE
	 deviceId = @"3504175fe000000000000111";
	 macAddress = @"aa:bb:cc:00:0a:b6";
	 ean = @"0000000000333";
	 
	 //	ReturnCode 99 - UNKNOWN_EXCEPTION
	 deviceId = @"3504175fe000000000000111";
	 macAddress = @"aa:bb:cc:00:0a:b6";
	 ean = @"0000000000444";
	 */
	
	//ean = @"4260201931329";
	//deviceId = @"3504175fe000000000018b55";
    
    [self checkIfCloudUrlIsSet];
	
	NSString *urlStr = [NSString stringWithFormat:@"%@public/MaterialMaster/v1_0/IlluminantService/CheckIlluminant?token=%@&dSid=%@&gtin=%@", [[DataController instance] getCurrentDSS].cloudUrl, sessionToken, deviceId, ean];
	NSLog(@"calling url: %@", urlStr);
	NSURL *url = [NSURL URLWithString: urlStr];
	
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
	[request addRequestHeader:@"Content-Type" value:@"application/json"];
	//request.useCookiePersistence = NO;
	//[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error(%i) cloudservice checkIlluminant: %@", [request responseStatusCode],  [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
	
	[request release];
	[json_string release];
	
	return dict;
}


- (NSDictionary*) cloudServiceTransferDimmingCurve: (NSString*) dimmingCurveId andDeviceId: (NSString*) deviceId andIlluminantId: (NSString*) illuminantId withAuthentication: (NSString*) sessionToken {
	
	//deviceId = @"3504175fe00000000000fd68";
	//macAddress = @"a8:99:5c:ff:02:ee";
	
    [self checkIfCloudUrlIsSet];
    
	NSString *urlStr = [NSString stringWithFormat:@"%@public/dss/v1_1/DimWizard/TransferDimmingCurve?token=%@&dsid=%@&IlluminantID=%@&DimmingCurveID=%@", [[DataController instance] getCurrentDSS].cloudUrl, sessionToken, deviceId, illuminantId, dimmingCurveId];
	NSURL *url = [NSURL URLWithString: urlStr];
    NSLog(@"Transfer dimming curve url: %@",urlStr);
	
	//ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
	[request addRequestHeader:@"Content-Type" value:@"application/json"];
	[request setRequestMethod:@"POST"];
	/*[request setPostValue:macAddress forKey:@"macAddress"];
	 [request setPostValue:deviceId forKey:@"dSid"];
	 [request setPostValue:ean forKey:@"ean"];
	 [request setPostValue:dimmingCurve forKey:@"DimmingCurveId"];*/
	//request.useCookiePersistence = NO;
	//[request setValidatesSecureCertificate:NO];
	[request startSynchronous];
	
	if ([request error]) {
		NSLog(@"error(%i) cloudservice transferDimmingCurve: %@", [request responseStatusCode],  [request error]);
	}
	
	NSData *response = [request responseData];
	NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	
	NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
    NSLog(@"transfer dimming response: %@", [dict description]);
	
	[request release];
	[json_string release];
	
	return dict;
}

- (NSDictionary*) cloudServiceGetArticleDataForGtin:(NSString*) gtin {
    
    NSString* cloudUrl = keyProductionCloudUrl;
    if(![[DataController instance] getCurrentDSS].cloudUrl) {
        cloudUrl = [[DataController instance] getCurrentDSS].cloudUrl;
    }
    
    NSString *urlStr = [NSString stringWithFormat:@"%@public/MasterDataManagement/Article/v1_0/ArticleData/GetBasicArticleData?ean=%@&languageCode=%@&countryCode=%@", cloudUrl, gtin, @"XX", @"xx"];
    NSURL *url = [NSURL URLWithString: urlStr];
    NSLog(@"GetBasicArticleData url: %@",urlStr);

    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    
    [request startSynchronous];
    
    if ([request error]) {
        NSLog(@"error(%i) cloudservice GetBasicArticleData: %@", [request responseStatusCode],  [request error]);
    }
    
    NSData *response = [request responseData];
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dict = [parser objectWithString:json_string error:nil];
    NSLog(@"GetBasicArticleData response: %@", [dict description]);
    
    [request release];
    [json_string release];
    
    return dict;
}


//Older versions didn't have a cloud url set, so we make sure they have now. 
- (void) checkIfCloudUrlIsSet{
    if([[DataController instance] getCurrentDSS].cloudUrl == nil){
        [[[DataController instance] getCurrentDSS] setCloudUrl:keyProductionCloudUrl];
    }
}

@end
