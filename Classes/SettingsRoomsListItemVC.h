//
//  SettingsRoomsListItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSRoom;

@interface SettingsRoomsListItemVC : UIViewController {
	DSRoom *room;
	NSString *nonRoomName;
	BOOL editmodeOn;
    BOOL goingToScenes;
    BOOL goingToDevices;
    BOOL isLookingForFavorite;
	float offset;
	NSInteger maximum;
	
	UIImageView *imageBackground;
	UILabel *labelName;
	UIButton *buttonStepInto;
	UIImageView *imageDragMe;
	UIButton *buttonBig;
	
	@private int deltaY;
	@private int magneticY;
	
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UILabel *labelName;
@property (nonatomic, retain) IBOutlet UIButton *buttonStepInto;
@property (nonatomic, retain) IBOutlet UIImageView *imageDragMe;
@property (nonatomic, retain) IBOutlet UIButton *buttonBig;

- (id) initWithRoom: (DSRoom*) theRoom andOffset:(float) _offset andMaximum:(NSInteger) _max gointToScenes:(BOOL)toScenes isLookingForFavorite:(BOOL)favorite gointToDevices:(BOOL)toDevices;
- (id) initWithName: (NSString*) theNonRoomName isLookingForFavorite:(BOOL)favorite;

- (void) toggleEditmode;
- (void) gotoPosition: (NSInteger) _pos;
- (void) incrementMagneticPoint;
- (void) decrementMagneticPoint;

- (IBAction) actionShowRoomDetail;

@end
