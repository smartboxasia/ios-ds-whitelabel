//
//  SettingsScenesDetailVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsScenesDetailVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DSScene.h"
#import "DataController.h"
#import "DSRoom.h"
#import "CustomLabel.h"
#import "CustomButton.h"
#import "SettingsScenesListVC.h"
#import "SettingsConfigSceneVC.h"
#import "GAIHelper.h"

@implementation SettingsScenesDetailVC

@synthesize labelHeadName, textSceneName, labelRoomName, viewPicker, buttonRoomLabel, pickerRoom, switchFavorit, switchList, buttonEdit,labelShowOnList,labelEditActivity,buttonEditActivity,imageToHide,imageToHide2,buttonCallActivity,background;

- (id) initWithScene: (DSScene*) _scene andIndex: (NSInteger) _index {
	if (self = [super init]) {
		scene = _scene;
		sceneIndex = _index;
	}
	return self;
}

- (void) viewDidLoad {
    
    labelNameTitle.text = NSLocalizedString(@"key_name", nil);
    labelRoomTitle.text = NSLocalizedString(@"key_room", nil);
    labelFavoriteTitle.text = NSLocalizedString(@"key_favorite", nil);
    labelShowOnList.text = NSLocalizedString(@"key_activities_label", nil);
    labelEditActivity.text = NSLocalizedString(@"key_edit_activity", nil);
    [self.buttonCallActivity setTitle:NSLocalizedString(@"key_call_activity", nil) forState:UIControlStateNormal];
    
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }
    
    self.screenName = @"Settings Activities Edit Details";
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
	if (scene && sceneIndex >= 0) {
		[self fillHeaderText];
	}
	
	if (scene) {
		[scene addObserver:self forKeyPath:@"name" options:0 context:nil];
        
		if (scene.room) {
			self.labelRoomName.text = scene.room.name;
		}
        else {
            self.labelRoomName.text = @"N/A";
        }
        
        if ([scene.sceneNo longLongValue] > 1023){
            self.buttonEdit.hidden = YES;
        }
	}
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if(scene)
        [scene removeObserver:self forKeyPath:@"name"];
    
}


- (void) fillHeaderText {
	self.labelHeadName.text = [NSString stringWithFormat:@"3. %@", scene.name];
	self.textSceneName.text = scene.name;
    self.switchFavorit.on = [scene.favourit boolValue];
    
    //not applicable for off scene and non-room scenes
    if ([scene.sceneNo longLongValue] == 0 || !scene.room) {
        self.switchList.hidden = YES;
        self.labelShowOnList.hidden = YES;
        self.labelEditActivity.hidden = YES;
        self.buttonEditActivity.hidden = YES;
        self.imageToHide.hidden = YES;
        self.imageToHide2.hidden = YES;
        //move call activity button up
        [self.buttonCallActivity setFrame:CGRectMake(self.buttonCallActivity.frame.origin.x,
                                                     234,
                                                     self.buttonCallActivity.frame.size.width,
                                                     self.buttonCallActivity.frame.size.height)];
        
    }
    else{
        self.switchList.on = [scene.mainList boolValue];
    }
    
    //fix custom orange switches
    self.switchFavorit.frame = CGRectMake(192, 184, 94, 27);
    self.switchList.frame = CGRectMake(192, 229, 94, 27);


}

#pragma mark -
#pragma mark Observer method

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"name"]) {
		[self fillHeaderText];		
	}
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	
    SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
		

}


- (IBAction) actionEnableRoomEditing {
	self.textSceneName.enabled = YES;
	[self.textSceneName becomeFirstResponder];
	
	if (self.viewPicker.frame.origin.y < self.view.frame.size.height) {
		[UIView animateWithDuration:0.4 animations:^{
			self.viewPicker.frame = CGRectMake(0, self.view.frame.size.height+13, self.viewPicker.frame.size.width, self.viewPicker.frame.size.height);
		}];
	}
	
}

- (IBAction) actionShowPicker {
	if (scene) {
		return;
	}
	[self.textSceneName resignFirstResponder];
	
	[UIView animateWithDuration:0.4 animations:^{
		self.viewPicker.frame = CGRectMake(0, self.view.frame.size.height-self.viewPicker.frame.size.height+13, self.viewPicker.frame.size.width, self.viewPicker.frame.size.height);
	}];
}

- (IBAction) actionHidePicker {
	selectedRoomFromPicker = ((DSRoom*)[[DataController instance].rooms objectAtIndex:[self.pickerRoom selectedRowInComponent:0]]);
	self.labelRoomName.text = selectedRoomFromPicker.name;

	[UIView animateWithDuration:0.4 animations:^{
		self.viewPicker.frame = CGRectMake(0, self.view.frame.size.height+13, self.viewPicker.frame.size.width, self.viewPicker.frame.size.height);
	}];
	
	if (!scene) {
		if (selectedRoomFromPicker && [self.textSceneName.text length] > 0) {
            if(![[DataController instance] createScene:self.textSceneName.text withRoom:selectedRoomFromPicker withGroup:NULL favouriteScene:NO]) {
				dispatch_async(dispatch_get_main_queue(), ^{
					UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"") message:NSLocalizedString(@"key_error_saving_activity", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"") otherButtonTitles:nil];
					[alert show];
                    [alert release];
				});
			}
			else {
				[(SettingsScenesListVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_SCENES_LIST] buildScrollView];
				[self actionBack];
			}
		}
	}
}

- (IBAction) actionHidePickerWithoutSave {
	[UIView animateWithDuration:0.4 animations:^{
		self.viewPicker.frame = CGRectMake(0, self.view.frame.size.height+13, self.viewPicker.frame.size.width, self.viewPicker.frame.size.height);
	}];
}

- (IBAction) actionChangeFavoriteSwitch: (id) sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        scene.favourit = [NSNumber numberWithBool:((UISwitch*)sender).on];
        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_SCENELIST_HAS_CHANGED];
        [[DataController instance] saveContext];
    });
}

- (IBAction) actionChangeListSwitch: (id) sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(((UISwitch*)sender).on)
            scene.mainList = [NSNumber numberWithInt:1];
        else
            scene.mainList = [NSNumber numberWithInt:0];
        
        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_SCENELIST_HAS_CHANGED];
        [[DataController instance] saveContext];
    });
}

- (IBAction) actionGoToSceneEdit{
    SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[[SettingsConfigSceneVC alloc] initWithScene:scene]];
}

- (IBAction) actionCallScene{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [GAIHelper sendSceneCall:scene];
        
        [[DataController instance] turnSceneOn:scene];
    });
}
#pragma mark -
#pragma mark UITextFieldDelegate methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		if (scene) {
			if (![[DataController instance] setSceneName:scene withName:self.textSceneName.text withRoom:scene.room]) {
				dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"") message:NSLocalizedString(@"key_could_not_submit_name", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"") otherButtonTitles:nil];
					[alert show];
                    [alert release];
					
					self.textSceneName.text = scene.room.name;
					[self fillHeaderText];
				});
			}
		}
		else {
			if (selectedRoomFromPicker && [self.textSceneName.text length] > 0) {
                if(![[DataController instance] createScene:self.textSceneName.text withRoom:selectedRoomFromPicker withGroup:NULL favouriteScene:NO]) {
					dispatch_async(dispatch_get_main_queue(), ^{
						UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"") message:NSLocalizedString(@"key_error_saving_activity", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"") otherButtonTitles:nil];
						[alert show];
                        [alert release];
					});
				}
				else {
					[(SettingsScenesListVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_SCENES_LIST] buildScrollView];
					[self actionBack];
				}
            }
			
		}
	});
	
	self.textSceneName.enabled = NO;
	[self.textSceneName resignFirstResponder];
	
	return YES;
}

#pragma mark -
#pragma mark PickerView delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	selectedRoomFromPicker = ((DSRoom*)[[DataController instance].rooms objectAtIndex:row]);
	self.labelRoomName.text = selectedRoomFromPicker.name;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return [[DataController instance].rooms count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return ((DSRoom*)[[DataController instance].rooms objectAtIndex:row]).name;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

    [labelNameTitle release];
    [labelRoomTitle release];
    [labelFavoriteTitle release];
    [super dealloc];
}


@end
