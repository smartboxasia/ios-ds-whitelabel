//
//  DSMeter.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <CoreData/CoreData.h>

@class DSServer;

@interface DSMeter :  NSManagedObject  
{
	NSMutableArray *allObserver;
	BOOL dirty;
}

@property (nonatomic, assign) BOOL dirty;

@property (nonatomic, assign) BOOL * meteringEnabled;

@property (nonatomic, retain) NSNumber * powerConsumption;
@property (nonatomic, retain) NSNumber * energyMeterValue;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * apiVersion;
@property (nonatomic, retain) DSServer * dss;

- (BOOL) isUsingDsuid;

@end



