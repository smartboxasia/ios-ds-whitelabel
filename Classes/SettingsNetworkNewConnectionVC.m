//
//  SettingsNetworkNewConnectionVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsNetworkNewConnectionVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "DSServer.h"
#import "StartupController.h"
#import "SB_iPhoneAppDelegate.h"

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

@interface SettingsNetworkNewConnectionVC (){
    UIPickerView *myPicker;
    NSArray *urlArray;
    NSString *cloudUrlToUse;
}

@end

@implementation SettingsNetworkNewConnectionVC

@synthesize imageBackground;
@synthesize segmentTabBar, viewTabCloud, viewTabManual;
@synthesize textfieldName, textfieldEmail, textfieldPort, textfieldIp, textfieldPassword;
@synthesize buttonConnectCloud, buttonConnectLocal;
@synthesize indicatorConnecting;
@synthesize urlChooserField;

- (id)init {
	self = [super init];
	if (self) {
		
	}
	
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
    self.labelTitle.text = NSLocalizedString(@"key_settings_connection_label_new", nil);
    self.labelUseYour.text = NSLocalizedString(@"key_settings_new_connection_info1", nil);
    self.labelPleaseNote.text = NSLocalizedString(@"key_settings_new_connection_info2", nil);
    self.labelEmail.text = NSLocalizedString(@"key_email", nil);
    self.labelPassword.text = NSLocalizedString(@"key_password", nil);
    self.textfieldEmail.placeholder = NSLocalizedString(@"key_email_hint", nil);
    self.textfieldPassword.placeholder = NSLocalizedString(@"key_password_hint", nil);

    self.labelManualName.text = NSLocalizedString(@"key_name", nil);
    self.labelConnectByEntering.text = NSLocalizedString(@"key_settings_new_connection_info3", nil);
    self.textfieldName.placeholder = NSLocalizedString(@"key_name_hint", nil);
    
    [self.buttonConnectCloud setTitle:NSLocalizedString(@"key_connect", nil) forState:UIControlStateNormal];
    [self.buttonConnectLocal setTitle:NSLocalizedString(@"key_connect", nil) forState:UIControlStateNormal];
    
	// iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        self.imageBackground.frame = CGRectMake(0, 0, 320, 513);
        [self.imageBackground setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }
	
	self.indicatorConnecting.alpha = 0;
	
	UIFont *font = [UIFont fontWithName:@"PFBeauSansPro-Bold" size:12.0];
	NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
														   forKey:NSFontAttributeName];
	[self.segmentTabBar setTitleTextAttributes:attributes
									forState:UIControlStateNormal];
    [self.segmentTabBar setTitle:NSLocalizedString(@"key_my_digitalstrom", nil) forSegmentAtIndex:0];
    [self.segmentTabBar setTitle:NSLocalizedString(@"key_settings_connection_type_manual", nil) forSegmentAtIndex:1];
	
	[self.segmentTabBar setContentOffset:CGSizeMake(0, 3) forSegmentAtIndex:0];
	[self.segmentTabBar setContentOffset:CGSizeMake(0, 3) forSegmentAtIndex:1];
    
    //create array with urls for url chooser
    urlArray = [[NSArray alloc] initWithObjects:keyDevCloudUrl, keyAlphaCloudUrl, keyBetaCloudUrl, keyMesseCloudUrl, keyProductionCloudUrl, nil];
    
    if([(SB_iPhoneAppDelegate*)[UIApplication sharedApplication].delegate isTestVersion]) {
        //testing scenario
        self.labelPleaseNote.hidden = YES;
        self.urlChooserField.hidden = NO;
        
        //setup picker for url selector
        myPicker = [[[UIPickerView alloc] init] autorelease];
        myPicker.dataSource = self;
        myPicker.delegate = self;
        myPicker.backgroundColor = [UIColor whiteColor];
        
        //set input and hide indicator
        urlChooserField.inputView = myPicker;
        cloudUrlToUse = keyDevCloudUrl;
        urlChooserField.text = @"Development Feed";
        
        if(IS_OS_7_OR_LATER){
            [[self urlChooserField] setTintColor:[UIColor clearColor]];
        }
    }
    else {
        //production scenario
        self.labelPleaseNote.hidden = NO;
        self.urlChooserField.hidden = YES;
        cloudUrlToUse = keyProductionCloudUrl;
    }
    
    self.screenName = @"Credentials Dialog";
}

#pragma mark -
#pragma mark IB Actions

- (IBAction) actionBack: (id)sender {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
}

- (IBAction) actionConnect: (id)sender {

	/*
	 *	do the validation first
	 */
	
	NSString *emailStr = self.textfieldEmail.text;
	NSString *passwordStr = self.textfieldPassword.text;
	
	NSString *nameStr = self.textfieldName.text;
	NSString *ipStr = self.textfieldIp.text;
	NSString *portStr = self.textfieldPort.text;
	
	if (self.segmentTabBar.selectedSegmentIndex == 0) {
		//	cloud connection selected
		
		if ([[emailStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0 && [[passwordStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0) {
			//	use cloud connection
			
			self.indicatorConnecting.hidden = NO;
			
			[UIView animateWithDuration:0.3 animations:^{
				self.buttonConnectCloud.alpha = 0;
				self.indicatorConnecting.alpha = 1;
				
			} completion:^(BOOL finished) {
				
			}];
			
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
				BOOL result = [[DigitalstromJSONService instance] performDsLogin:emailStr withPassword:passwordStr andCloudUrl:cloudUrlToUse];
				
				if (result) {
					dispatch_async(dispatch_get_main_queue(), ^{
						DSServer *dss = [[StartupController instance] createDssForToken:[[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN] andRelayLink:[[ModelLocator instance] valueForKey:k_DSS_IP]];
						dss.cloudEmail = emailStr;
						dss.cloudPassword = passwordStr;
						dss.connectionType = DSS_CONNECTION_TYPE_CLOUD;
						dss.name = NSLocalizedString(@"key_settings_network_new_connection_connecting", @"connecting");
                        dss.cloudUrl = cloudUrlToUse;
						
						/*
						 *	mark all other dsServers in database as "not connected"
						 */
						[[DataController instance] markDsServersAsDisconnected];
												
						/*
						 *	make sure current dss is connected
						 */
						dss.connected = YES;
						
						[[DataController instance] saveContext];
						
						SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
						[settings goBack];
						
						/*
						 *	send notification about connection state
						 */
						[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationConnectedToDss object:nil];
						
						/*
						 *	establish connection to dss
						 */
						[[StartupController instance] performDssConnectWithServer:dss];
					});
				}
				else {
					dispatch_async(dispatch_get_main_queue(), ^{
						[UIView animateWithDuration:0.3 animations:^{
							self.buttonConnectCloud.alpha = 1;
							self.indicatorConnecting.alpha = 0;
							
						} completion:^(BOOL finished) {
							
						}];
						
						UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"error") message:NSLocalizedString(@"key_credentials_wrong_pass_title", @"error") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"OK") otherButtonTitles: nil];
						
						[alert show];
					});
				}
			});
			
			
		}
		else {
			//	neccessary fields have to be filled out
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"Error") message:NSLocalizedString(@"key_settings_connection_error_create_connection", @"Error") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
		}
	}
	else {
		//	manual connection selected
		if([[nameStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0 && [[ipStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0 && [[portStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0) {
			//	use manual connection
			
			if ([portStr integerValue] > 0 && [portStr integerValue] < 65536) {
				//	create instance
				
				DSServer *dss = [[DataController instance] createDsServerEntity];
				dss.connectionType = DSS_CONNECTION_TYPE_MANUAL;
				dss.ip = ipStr;
				dss.name = nameStr;
				dss.connectionName = nameStr;
				dss.port = [NSNumber numberWithInteger:[portStr integerValue]];
				
				[[DataController instance] saveContext];
				
				/*
				 *	going back in interface
				 */
				SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
				[settings goBack];
				
				/*
				 *	mark all other dsServers in database as "not connected"
				 */
				[[DataController instance] markDsServersAsDisconnected];
				
				/*
				 *	perform connection
				 */
				[[StartupController instance] performDssConnectWithServer:dss];
			}
			else {
				//	invalid port
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"Error") message:NSLocalizedString(@"key_settings_connection_error_invalid_port", @"Error") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[alert show];
			}
		}
		else {
			//	neccessary fields have to be filled out
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"Error") message:NSLocalizedString(@"key_settings_connection_error_create_connection", @"Error") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
		}
	}
}

- (IBAction) actionTabChanged: (id)sender {
	[self.textfieldEmail resignFirstResponder];
	[self.textfieldIp resignFirstResponder];
	[self.textfieldName resignFirstResponder];
	[self.textfieldPassword resignFirstResponder];
	[self.textfieldPort resignFirstResponder];
	
	NSInteger selectedIndex = ((UISegmentedControl*)sender).selectedSegmentIndex;
	
	if (selectedIndex == 0) {
		self.viewTabCloud.alpha = 0;
		self.viewTabCloud.hidden = NO;
		
		[UIView animateWithDuration:0.3 animations:^{
			self.viewTabCloud.alpha = 1;
			self.viewTabManual.alpha = 0;
		} completion:^(BOOL finished) {
			self.viewTabManual.hidden = YES;
		}];
		
	}
	else {
		self.viewTabManual.alpha = 0;
		self.viewTabManual.hidden = NO;
		
		[UIView animateWithDuration:0.3 animations:^{
			self.viewTabCloud.alpha = 0;
			self.viewTabManual.alpha = 1;
		} completion:^(BOOL finished) {
			self.viewTabCloud.hidden = YES;
		}];
	}
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.textfieldEmail) {
		[self.textfieldPassword becomeFirstResponder];
	}
	else if (textField == self.textfieldPassword) {
		[self.textfieldPassword resignFirstResponder];
	}
	else if (textField == self.textfieldName) {
		[self.textfieldIp becomeFirstResponder];
	}
	else if (textField == self.textfieldIp) {
		[self.textfieldPort becomeFirstResponder];
	}
	else if (textField == self.urlChooserField) {
		[self.urlChooserField becomeFirstResponder];
	}
	
	return YES;
}


#pragma mark - picker delegates

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [urlArray count];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return  1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    switch (row) {
        case 0:
            return @"Development Feed";
            break;
        case 1:
            return @"Alpha Feed";
            break;
        case 2:
            return @"Beta Feed";
            break;
        case 3:
            return @"Messe Feed";
            break;
        case 4:
            return @"Production Feed";
            break;
        default:
            return @"Default";
            break;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    switch (row) {
        case 0:
            [urlChooserField setText: @"Development Feed"];
            break;
        case 1:
            [urlChooserField setText: @"Alpha Feed"];
            break;
        case 2:
            [urlChooserField setText: @"Beta Feed"];
            break;
        case 3:
            [urlChooserField setText: @"Messe Feed"];
            break;
        case 4:
            [urlChooserField setText: @"Production Feed"];
            break;
        default:
            [urlChooserField setText: @"Default"];
            break;
    }
    cloudUrlToUse = [urlArray objectAtIndex:row];
    [urlChooserField resignFirstResponder];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_labelTitle release];
    [_labelUseYour release];
    [_labelPleaseNote release];
    [_labelEmail release];
    [_labelPassword release];
    [_labelConnectByEntering release];
    [_labelManualName release];
    [super dealloc];
}
@end
