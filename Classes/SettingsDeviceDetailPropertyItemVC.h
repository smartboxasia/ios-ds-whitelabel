//
//  SettingsDeviceDetailPropertyItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsDeviceDetailPropertyItemVC : UIViewController {
	NSString *value;
	NSString *propertyName;
	bool oddIndex;
}

@property (nonatomic, retain) IBOutlet UILabel *labelPropertyName;
@property (nonatomic, retain) IBOutlet UILabel *labelPropertyValue;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;

- (id) initWithProperty: (NSString*) _propertyName oddIndex: (BOOL) _oddIndex andValue: (NSString*) _value;
- (void) updateValue: (NSString*) _value;
- (BOOL) isProperty: (NSString*) _propertyName;

@end
