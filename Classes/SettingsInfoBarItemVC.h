//
//  SettingsInfoBarItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SettingsInfoBarItemVC : UIViewController {
	NSString *propertyName;
	float offset;
	NSInteger maximum;
	
	UILabel *labelName;
	UILabel *labelIndex;
	UIImageView *imageDragMe;
	
	
@private int deltaY;
@private int magneticY;
}

@property (nonatomic, retain) IBOutlet UILabel *labelName;
@property (nonatomic, retain) IBOutlet UILabel *labelIndex;
@property (nonatomic, retain) IBOutlet UIImageView *imageDragMe;

- (id) initWithName: (NSString*) _name andOffset:(float) _offset andMaximum:(NSInteger) _max;

- (void) gotoPosition: (NSInteger) _pos;
- (void) incrementMagneticPoint;
- (void) decrementMagneticPoint;

@end
