//
//  DigitalstromJSONService.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJSON.h"
#import "DSDevice.h"
#import "DSRoom.h"
#import "DSScene.h"
#import "DSMeter.h"

//cloud urls
#define keyProductionCloudUrl @"https://dsservices.aizo.com/" //Production
#define keyAlphaCloudUrl @"https://testdsservices.aizo.com/" //alpha
#define keyBetaCloudUrl @"https://fieldtestdsservices.aizo.com/" //beta
#define keyMesseCloudUrl @"https://messedsservices.aizo.com/" //Messe
#define keyDevCloudUrl @"https://devdsservices.aizo.com/" //Development

@interface DigitalstromJSONService : NSObject {
	SBJSON *parser;
	NSString *serverIP;
	NSString *dssUsername;
	NSString *dssPassword;
	NSString *dssApplicationToken;
}

@property (retain, nonatomic) NSString *serverIP;
@property (nonatomic, retain) NSString *dssUsername;
@property (nonatomic, retain) NSString *dssPassword;
@property (nonatomic, retain) NSString *dssApplicationToken;


+(DigitalstromJSONService *)instance;

- (NSMutableDictionary*) getStructure: (BOOL) performLogin;
- (NSMutableArray*) getReachableNonDsGroups;
- (BOOL) setDevicePower: (DSDevice*)device withBool:(BOOL) powerOn withGroup:(int)group;
- (BOOL) setDeviceValue: (DSDevice*)device withValue: (NSNumber*) value;
- (BOOL) setShadeAngle: (DSDevice*)device withAngle: (NSNumber*) value;

- (NSNumber*) getApartmentConsumption;
- (NSDictionary*) getMetersConsumption;
- (NSArray*) getDSMConsumptionInfo:(DSMeter*)meter;

- (NSNumber*) getRoomConsumption: (DSRoom*) room;

- (NSArray*) getLastCalledScenesForRoom: (DSRoom*)room;

- (BOOL) setDeviceName: (DSDevice*)device withString: (NSString*) name;
- (BOOL) moveDevice: (DSDevice*)device toRoom: (DSRoom*) room;
- (BOOL) setOutputModeForDevice: (DSDevice*)device toMode: (int) newMode;
- (BOOL) setRoomName: (NSString*) name withRoom: (DSRoom*) room;
- (BOOL) setRoomPower: (DSRoom*)room withBool:(BOOL) powerOn;
- (BOOL) addDeviceToScene: (DSDevice*) device withScene: (DSScene*) scene;
- (BOOL) removeDeviceFromScene: (DSDevice*) device withScene: (DSScene*) scene;
- (BOOL) setScenePowerOn: (DSScene*)scene;
- (BOOL) undoScene: (DSScene*)scene;
- (BOOL) saveScene: (DSScene*)scene;
- (int) getZoneCount:(NSDictionary*)structureDict;
- (int) getOutputOfDevice: (DSDevice*)device;
- (NSDictionary*) getOutputsOfShadeDevice: (DSDevice*)device;
- (int) getDeviceConsumption: (DSDevice*) device;
- (int) getDeviceMetering: (DSDevice*) device;


- (int) getZoneCount:(NSDictionary*)structureDict;
- (NSMutableArray*) copyRoomProperties:(NSMutableDictionary*)strucDict;
- (int) getDeviceCount:(NSDictionary*)roomDict;
- (BOOL) setDontCareBitOnDevice: (DSDevice*) device withScene: (DSScene*) scene withDeleteToggle:(BOOL)deleteToogle;
- (BOOL) setSceneName: (NSString*) name withRoom: (DSRoom*) room withScene:(DSScene*)scene;
- (BOOL)deviceInScene: (DSDevice*) device withSceneNumber:(int)sceneNumber;
- (int) getSceneDeviceStatus: (DSDevice*)device withSceneNumber:(int)sceneNumber;
- (NSMutableDictionary*) allocScenes;


- (BOOL) loginUser;
- (NSString*) getDSSVersion;
- (NSString*) getApartmentName;
- (NSMutableDictionary*) getDSSLocation;
- (NSMutableArray*) allocDSMs;
- (NSMutableArray*) allocDSMProperties: (NSArray*)dsMeters;
- (BOOL) setMeterName: (NSString*) name withMeter: (DSMeter*) meter;
- (BOOL) callStandardScene: (NSString*)standardScene withRoom:(DSRoom*)room withGroup:(int)group;
- (BOOL) callStandardScene: (NSString*)standardScene withDevice:(DSDevice*)device withGroup:(int)group;

- (NSString *) getMacAddressOfDss;

- (NSString *) getApplicationToken: (NSString*) ipAddress;
- (NSString *) performLogin: (NSString*) username withPassword: (NSString*) password withIp: (NSString*) ipAddress;

- (NSDictionary*) checkDsLogin: (NSString*) username withPassword: (NSString*) password andCloudUrl: (NSString*)cloudUrl;
- (BOOL) performDsLogin: (NSString*) username withPassword: (NSString*) password andCloudUrl: (NSString*)cloudUrl;
- (BOOL) performEnableToken: (NSString*) appToken withIp: (NSString*) ipAddress andLoginToken: (NSString*) token;

- (NSMutableArray*) getHighLevelEvents;

- (NSDictionary*) cloudServiceDeleteTags:(NSArray *)tags withAuthentication: (NSString*) sessionToken andDeviceId: (NSString*) deviceId;
- (NSDictionary*) cloudServiceSetTagMaxPower: (NSString*) tagValue withAuthentication: (NSString*) sessionToken andDeviceId: (NSString*) deviceId;
- (NSDictionary*) cloudServiceSetTagSocket: (NSString*) tagValue withAuthentication: (NSString*) sessionToken andDeviceId: (NSString*) deviceId;
- (NSDictionary*) cloudServiceGetAllIlluminantsForDevice: (NSString*) deviceId withAuthentication: (NSString*) sessionToken;
- (NSDictionary*) cloudServiceGetAllTagsWithAuthentication: (NSString*) sessionToken andObjectId: (NSString*) objectId andType: (NSString*) objectType;
- (NSDictionary*) cloudServiceGetDeviceDetailsForDeviceWithGtin: (NSString*) gtin;
- (NSDictionary*) cloudServiceCheckIlluminantForDss: (NSString*) sessionToken andDeviceId: (NSString*) deviceId andEan: (NSString*) ean;
- (NSDictionary*) cloudServiceTransferDimmingCurve: (NSString*) dimmingCurveId andDeviceId: (NSString*) deviceId andIlluminantId: (NSString*) illuminantId withAuthentication: (NSString*) sessionToken;
- (NSDictionary*) cloudServiceGetArticleDataForGtin:(NSString*) gtin;
- (void) checkIfCloudUrlIsSet;
@end
