//
//  RoomDeviceListItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "RoomDeviceListItemVC.h"
#import "DSDevice.h"
#import "DataController.h"
#import "ModelLocator.h"
#import "MainVC.h"
#import "GAIHelper.h"

@implementation RoomDeviceListItemVC

@synthesize buttonIcon, buttonLabel, imageBackground, imageColorBar, colorCode, doAnimation;

- (id)initWithDevice: (DSDevice*) theDevice colorCode:(int)theColorCode doAnimation:(BOOL)animation {
	if (self = [super init]) {
		device = theDevice;
        colorCode = theColorCode;
        doAnimation = animation;
	}
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
	
	if (device) {
		[self.buttonLabel setTitle:device.name forState:UIControlStateNormal];
	}
    
    switch (colorCode) {
        case 1:
            //yellow klemme devices
            self.imageColorBar.image = [UIImage imageNamed:@"RS_Geraete_Farbcode_Licht.png"];
            [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"lightbulb_wide.png"] forState:UIControlStateNormal];
            break;
        case 2:
            //gray klemme devices
            self.imageColorBar.image = [UIImage imageNamed:@"RS_Geraete_Farbcode_Besch.png"];
            [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"shade_wide.png"] forState:UIControlStateNormal];
            break;
        case 3:
            //blue scenes
            self.imageColorBar.image = [UIImage imageNamed:@"RS_device_blue.png"];
            [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"blue_on_wide.png"] forState:UIControlStateNormal];
            break;
            
        case 4:
            //blue scenes
            self.imageColorBar.image = [UIImage imageNamed:@"RS_device_cyan.png"];
            [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"music_wide.png"] forState:UIControlStateNormal];
            break;
        case 5:
            //blue scenes
            self.imageColorBar.image = [UIImage imageNamed:@"RS_device_magenta.png"];
            [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"television_wide.png"] forState:UIControlStateNormal];
            break;

        default:
            break;
    }
    
    //animate color bar with fade effect
    if(doAnimation){
        self.imageColorBar.alpha = 0.0;
        self.buttonLabel.alpha = 0.0;
        self.buttonIcon.alpha = 0.0;
        self.imageBackground.alpha = 0.0;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        self.imageColorBar.alpha = 1.0;
        self.buttonLabel.alpha = 1.0;
        self.buttonIcon.alpha = 1.0;
        self.imageBackground.alpha = 1.0;
        [UIView commitAnimations];
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
	[self drawBackground];
	[device addObserver:self forKeyPath:@"powerOn" options:0 context:nil];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [device removeObserver:self forKeyPath:@"powerOn" context:nil];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"powerOn"]) {
		[self drawBackground];
	}
}

- (void) drawBackground {
	if ([device.powerOn boolValue]) {
		self.imageBackground.highlighted = YES;
		//self.imageBackground.image = [UIImage imageNamed:@"RS_blankButton_aktiv.png"];
	}
	else {
		self.imageBackground.highlighted = NO;
		//self.imageBackground.image = [UIImage imageNamed:@"RS_blankButton.png"];
	}
}

- (IBAction) actionButtonIcon: (id) sender {
    if([device.group isEqualToString:@"2"]){
        [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] showShadeAdjustmentScreen:device];
    }
    else{
        [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] showAdjustmentScreen:device];
    }

}

- (IBAction) actionButtonLabel: (id) sender {
	//self.imageBackground.highlighted = NO;
	// toggle power of device
	if (device) {
        [GAIHelper sendUIActionWithCategory:@"scene_called" andLabel:@"device"];
        
		switch (colorCode) {
            case 1:
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    [[DataController instance] setDevicePower:device withBool:![device.powerOn boolValue] withGroup:colorCode];
                });		
                break;
            case 2:
                [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] showShadeAdjustmentScreen:device];
                break;
            case 3:
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    [[DataController instance] setDevicePower:device withBool:![device.powerOn boolValue] withGroup:colorCode];
                });
            case 4:
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    [[DataController instance] setDevicePower:device withBool:![device.powerOn boolValue] withGroup:colorCode];
                });
            case 5:
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    [[DataController instance] setDevicePower:device withBool:![device.powerOn boolValue] withGroup:colorCode];
                });
                break;
            default:
                break;
        }
    }
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
