//
//  SettingsGreyConfigItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsGreyConfigItemVC.h"
#import "DSDevice.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"

@implementation SettingsGreyConfigItemVC

@synthesize imageBackground,
positionSlider,
angleSlider,
loadingWheel,
labelName,
labelPositionValue,
labelAngleValue,
imageAngle1,
imageAngle2,
labelAngleName,
labelPositionName,
imagePosition1,
imagePosition2;

- (id) initWithDevice: (DSDevice*) theDevice andDarkBg: (BOOL) isDark {
	if(self = [super init]) {
		device = theDevice;
		hasDarkBackground = isDark;
        NSLog(@"device type is %i",[theDevice.type intValue]);
        if([theDevice.type isEqualToNumber:[NSNumber numberWithInt:3292]])
            hasAngleControl = YES;
        else
            hasAngleControl = NO;
	}
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	if(device) {
		self.labelName.text = device.name;
        self.labelAngleName.text = NSLocalizedString(@"key_angle", nil);
        self.labelPositionName.text = NSLocalizedString(@"key_position", nil);
		
		if(hasDarkBackground) {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_hell_doppelt-02.png"];		
		}
		else {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_hell_doppelt.png"];
		}
        
        //make orange sliders
        [self.angleSlider setMinimumTrackImage:[[UIImage imageNamed:@"orangeslider.png"] stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self.positionSlider setMinimumTrackImage:[[UIImage imageNamed:@"orangeslider.png"] stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0] forState:UIControlStateNormal];
        
        //if no angle control is available, hide it
        if(!hasAngleControl){
            self.angleSlider.hidden = YES;
            self.labelAngleName.hidden = YES;
            self.labelAngleValue.hidden = YES;
            self.imageAngle1.hidden = YES;
            self.imageAngle2.hidden = YES;
            //reduce height because of hidden controls
            [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width,82)];
        }

        //we start out by showing the loading animation
        [self toggleLoadingAnimation:YES];
        
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //add observer for the dimmFactor (position)
    [device addObserver:self forKeyPath:@"dimmFactor" options:0 context:nil];
    
    //and for angle control if needed
    if(hasAngleControl)
        [device addObserver:self forKeyPath:@"angleFactor" options:0 context:nil];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //remove observers
    if(hasAngleControl)
        [device removeObserver:self forKeyPath:@"angleFactor"];
    
    [device removeObserver:self forKeyPath:@"dimmFactor"];
}


- (void) toggleLoadingAnimation: (BOOL) turnOn {
    if (turnOn){
        self.loadingWheel.hidden = NO;
        [self.loadingWheel startAnimating];
        
        self.angleSlider.hidden = YES;
        self.labelAngleName.hidden = YES;
        self.labelAngleValue.hidden = YES;
        self.positionSlider.hidden = YES;
        self.labelPositionName.hidden = YES;
        self.labelPositionValue.hidden = YES;
        self.imageAngle1.hidden = YES;
        self.imageAngle2.hidden = YES;
        self.imagePosition1.hidden = YES;
        self.imagePosition2.hidden = YES;
    }
    else{
        self.loadingWheel.hidden = YES;
        [self.loadingWheel stopAnimating];
        
        if(hasAngleControl){
            self.angleSlider.hidden = NO;
            self.labelAngleName.hidden = NO;
            self.labelAngleValue.hidden = NO;
            self.imageAngle1.hidden = NO;
            self.imageAngle2.hidden = NO;
        }
        self.positionSlider.hidden = NO;
        self.labelPositionName.hidden = NO;
        self.labelPositionValue.hidden = NO;
        self.imagePosition1.hidden = NO;
        self.imagePosition2.hidden = NO;
        
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"dimmFactor"]) {
        [self toggleLoadingAnimation:NO];
        //inversed because of visual logic (except for markise)
        float newValue = [device.dimmFactor floatValue];
        if(![device.type isEqualToNumber:[NSNumber numberWithInt:3282]])
            newValue = 1- newValue;
                
        self.positionSlider.value = newValue;
        labelPositionValue.text = [NSString stringWithFormat:@"%.0f%%",newValue*100];
	}
    else if ([keyPath isEqualToString:@"angleFactor"]) {
        [self toggleLoadingAnimation:NO];
        //inversed because of visual logic
        float newValue = [device.angleFactor floatValue];
        newValue = 1 - newValue;
        self.angleSlider.value = newValue;
        labelAngleValue.text = [NSString stringWithFormat:@"%.0f%%",newValue*100];
	}
}

- (IBAction) positionSliderChanged: (id) sender{
     labelPositionValue.text = [NSString stringWithFormat:@"%.0f%%",((UISlider*)sender).value*100];
}

- (IBAction) positionSliderTouchUpInside: (id) sender{
    if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            float newValue = ((UISlider*)sender).value;
            //dont re-inverse the value for markisen
            if(![device.type isEqualToNumber:[NSNumber numberWithInt:3282]])
                newValue = 1-newValue;
            
			[[DataController instance] setDeviceValue:device withValue:[NSNumber numberWithFloat:newValue]];
		});
	}
}

- (IBAction) angleSliderChanged: (id) sender{
     labelAngleValue.text = [NSString stringWithFormat:@"%.0f%%",((UISlider*)sender).value*100];
}

- (IBAction) angleSliderTouchUpInside: (id) sender{
    if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            float newValue = ((UISlider*)sender).value;
            newValue = 1-newValue;
			[[DataController instance] setShadeAngle:device withValue:[NSNumber numberWithFloat:newValue]];
		});
	}
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
