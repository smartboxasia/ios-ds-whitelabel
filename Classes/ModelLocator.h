//
//  ModelLocator.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define k_VIEW_MAIN @"view_main"
#define k_VIEW_SETTINGS @"view_settings"
#define k_VIEW_SETTINGS_SCENES_LIST @"view_settings_scenes_list"
#define k_VIEW_SETTINGS_SCENES_BY_ROOM_LIST @"view_settings_scenes_by_room_list"
#define k_VIEW_SETTINGS_DEVICES_BY_ROOM_LIST @"view_settings_devices_by_room_list"
#define k_VIEW_SETTINGS_CHOOSE_ROOM_DEVICE_LIST @"view_settings_choose_room_device_list"
#define k_VIEW_SETTINGS_CHOOSE_MODE_DEVICE_LIST @"view_settings_choose_mode_device_list"
#define k_VIEW_SETTINGS_DEVICE_CHOOSE_SOCKET_LIST @"view_settings_device_choose_socket_list"
#define k_VIEW_SETTINGS_CONFIGURE_SCENE @"view_settings_configure_scene"
#define k_VIEW_SETTINGS_ROOMS_LIST @"view_settings_rooms_list"
#define k_VIEW_SETTINGS_METERS_LIST @"view_settings_meters_list"
#define k_VIEW_SETTINGS_FAVORITES_LIST @"view_settings_favorites_list"
#define k_VIEW_SETTINGS_INFOBAR_LIST @"view_settings_infobar_list"

#define k_VIEW_SPLASH @"view_splash"
#define k_VIEW_WELCOME @"view_welcome"
#define k_VIEW_SERVERLIST @"view_serverlist"
#define k_VIEW_INFOBAR @"view_infobar"
#define k_VIEW_INFOBAR_SMALL @"view_infobar_small"
#define k_VIEW_FAVORITEBAR @"view_favoritebar"
#define k_VIEW_OVERVIEW_ROOMLIST @"view_overview_roomlist"
#define k_VIEW_ROOM_SCENELIST @"view_room_scenelist"
#define k_VIEW_ROOM_DEVICELIST @"view_room_devicelist"
#define k_VIEW_ROOM_SCENELIST_OFFSET @"view_room_scenelist_offset"

#define k_VIEW_ENERGIE_AMPEL @"view_energieampel"

#define k_DSS_IP @"dss_ip"
#define k_DSS_PORT @"dss_port"
#define k_DSS_USERNAME @"dss_username"
#define k_DSS_PASSWORD @"dss_password"
#define k_DSS_VERSION @"dss_version"
#define k_DSS_NAME @"dss_name"
#define k_DSS_JSON_STRING @"dss_json_string"
#define k_DSS_AVAILABLE @"dss_available"
#define k_DSS_APPLICATION_TOKEN @"dss_application_token"

#define k_STANDARD_OUTSIDE_TEMP @"standard_outside_temp"
#define k_STANDARD_CONDITION_TEMP @"standard_condition_temp"
#define k_STANDARD_CITY @"standard_city"

#define k_INFO_TOTAL_CONSUMPTION @"info_total_consumption"
#define k_INFO_PREFIX_ROOM_CONSUMPTION @"info_room_consumption_"

#define k_DATA_STRUCTURE_HAS_CHANGED @"data_structure_changed"
#define k_DATA_ROOMLIST_HAS_CHANGED @"data_roomlist_changed"
#define k_DATA_DEVICELIST_HAS_CHANGED @"data_devicelist_changed"
#define k_DATA_SCENELIST_HAS_CHANGED @"data_scenelist_changed"
#define k_DATA_FAVORITELIST_HAS_CHANGED @"data_favoritelist_changed"
#define k_DATA_INFOLIST_HAS_CHANGED @"data_infolist_changed"
#define k_DATA_DEMOMODE_ACTIVE @"data_demomode_active"
#define k_DATA_ENERGY_MAX_VALUE @"data_energy_max_value"
#define k_DATA_APP_VERSION @"data_app_version"

#define k_DATA_DNS_DICT @"data_dns_dict"

#define k_DATA_APARTMENT_LAST_CALLED_SCENE @"data_apartment_lastcalledscene"

#define k_DATA_LOGIN_STARTED @"data_login_started"
#define k_DATA_LOGIN_SUCCESSFUL @"data_login_successful"
#define k_DATA_LOGIN_FAILED @"data_login_failed"
#define k_DATA_APPLICATION_TOKEN_NOT_VALID @"data_app_token_not_valid"

#define k_VIEW_LABEL_STROMVERBRAUCH NSLocalizedString(@"key_powerconsumption", @"")
#define k_VIEW_LABEL_WETTER NSLocalizedString(@"key_weather", @"")
#define k_VIEW_LABEL_DSS_NAME NSLocalizedString(@"key_dss_name", @"")

#define kNotificationSettingsDevicesEntered @"kNotificationSettingsDevicesEntered"
#define kNotificationSettingsDevicesLeft @"kNotificationSettingsDevicesLeft"
#define kNotificationSettingsDevicesIlluminantCheckStarted @"kNotificationSettingsDevicesIlluminantCheckStarted"

#define kApplicationUUIDKey @"digitalstrom_uuid_key"


//	0 - OK
#define kNotificationSettingsDevicesIlluminantCheck0OK @"kNotificationSettingsDevicesIlluminantCheckOK"

//	1 - UNKNOWN_MAC_ADDRESS
#define kNotificationSettingsDevicesIlluminantCheck1UnkownMacAddress @"kNotificationSettingsDevicesIlluminantCheckUnkownMacAddress"

//	2 - UNKNOWN_DSID
#define kNotificationSettingsDevicesIlluminantCheck2UnkownDsId @"kNotificationSettingsDevicesIlluminantCheckUnkownDsId"

//	3 - EAN_NOT_FOUND
#define kNotificationSettingsDevicesIlluminantCheck3EanNotFound @"kNotificationSettingsDevicesIlluminantCheckEanNotFound"

//	4 - EAN_NO_MATCH
#define kNotificationSettingsDevicesIlluminantCheck4EanNoMatch @"kNotificationSettingsDevicesIlluminantCheckEanNoMatch"

//	5 - EAN_NOT_DIMMABLE
#define kNotificationSettingsDevicesIlluminantCheck5EanNotDimmable @"kNotificationSettingsDevicesIlluminantCheckEanNotDimmable"

//	6 - DSID_NOT_DIMMABLE
#define kNotificationSettingsDevicesIlluminantCheck6DsidNotDimmable @"kNotificationSettingsDevicesIlluminantCheckDsidNotDimmable"

//	7 - NOT_ENOUGH_POWER
#define kNotificationSettingsDevicesIlluminantCheck7NotEnoughPower @"kNotificationSettingsDevicesIlluminantCheck7NotEnoughPower"

//	- - SOCKET TYPE NOT MATCHING
#define kNotificationSettingsDevicesIlluminantCheckSocketTypeNotMatching @"kNotificationSettingsDevicesIlluminantCheckSocketTypeNotMatching"

//	- - SOCKET TYPE AND MAX POWER NOT MATCHING
#define kNotificationSettingsDevicesIlluminantCheckSocketTypeAndMaxPowerNotMatching @"kNotificationSettingsDevicesIlluminantCheckSocketTypeAndMaxPowerNotMatching"

//	- - ILLUMINANT DIMMING STATE
#define kNotificationSettingsDevicesIlluminantCheckIlluminantDimmingState @"kNotificationSettingsDevicesIlluminantCheckIlluminantDimmingState"

//	99 - UNKNOWN_EXCEPTION
#define kNotificationSettingsDevicesIlluminantCheck99UnknownException @"kNotificationSettingsDevicesIlluminantCheckUnknownException"


#define kNotificationSettingsDevicesTransferDimmingCurveTransferStarted @"kNotificationSettingsDevicesTransferDimmingCurveTransferStarted"
#define kNotificationSettingsDevicesTransferDimmingCurveTransferDone @"kNotificationSettingsDevicesTransferDimmingCurveTransferDone"
#define kNotificationSettingsDevicesTransferDimmingCurveTransferFailed @"kNotificationSettingsDevicesTransferDimmingCurveTransferFailed"


#define kNotificationConnectedToDss @"kNotificationConnectedToDss"
#define kNotificationDssListChanged @"kNotificationDssListChanged"

@interface ModelLocator : NSObject {
	NSMutableDictionary *_observedKeys;
	//NSMutableArray *serverlist;
	UIViewController *appController;
	NSMutableSet *observers;
	
}

@property (nonatomic, retain) UIViewController *appController;
//@property (nonatomic, retain) NSMutableArray *serverlist;
@property (nonatomic, retain) NSMutableArray *localServerAddresses;

+ (ModelLocator *)instance;

- (void) loadDefaults;
- (id)valueForKey:(NSString *)key;

- (void)setValue:(id)value forKey:(NSString *)key;

- (void) traceObjects;

@end
