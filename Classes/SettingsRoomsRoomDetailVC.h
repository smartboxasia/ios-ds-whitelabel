//
//  SettingsRoomsRoomDetailVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSRoom;
@class CustomLabel;

@interface SettingsRoomsRoomDetailVC : GAITrackedViewController <UITextFieldDelegate>{
	DSRoom *room;
	UILabel *labelHeadName;
	UITextField *textRoomName;
	UILabel *labelCircuitName;
    IBOutlet CustomLabel *labelNameTitle;
    IBOutlet CustomLabel *labelMeterTitle;
}

@property (nonatomic, retain) IBOutlet UILabel *labelHeadName;
@property (nonatomic, retain) IBOutlet UITextField *textRoomName;
@property (nonatomic, retain) IBOutlet UILabel *labelCircuitName;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (id) initWithRoom: (DSRoom*) theRoom;

- (IBAction) actionBack;
- (IBAction) actionEnableRoomEditing;



@end
