//
//  SettingsGreyConfigItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomSwitch.h"

@class DSDevice;

@interface SettingsGreyConfigItemVC : UIViewController {
	DSDevice *device;
	BOOL hasDarkBackground;
    BOOL hasAngleControl;
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UISlider *positionSlider;
@property (nonatomic, retain) IBOutlet UISlider *angleSlider;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *loadingWheel;
@property (nonatomic, retain) IBOutlet UILabel *labelName;
@property (nonatomic, retain) IBOutlet UILabel *labelPositionValue;
@property (nonatomic, retain) IBOutlet UILabel *labelAngleValue;
@property (nonatomic, retain) IBOutlet UIImageView *imageAngle1;
@property (nonatomic, retain) IBOutlet UIImageView *imageAngle2;
@property (nonatomic, retain) IBOutlet UILabel *labelAngleName;
@property (nonatomic, retain) IBOutlet UILabel *labelPositionName;
@property (nonatomic, retain) IBOutlet UIImageView *imagePosition1;
@property (nonatomic, retain) IBOutlet UIImageView *imagePosition2;

- (id) initWithDevice: (DSDevice*) theDevice andDarkBg: (BOOL) isDark;
- (void) toggleLoadingAnimation: (BOOL) turnOn;
- (IBAction) positionSliderChanged: (id) sender;
- (IBAction) positionSliderTouchUpInside: (id) sender;
- (IBAction) angleSliderChanged: (id) sender;
- (IBAction) angleSliderTouchUpInside: (id) sender;

@end
