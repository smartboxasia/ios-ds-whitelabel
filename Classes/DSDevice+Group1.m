//
//  DSDevice+Group1.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "DSDevice+Group1.h"

@implementation DSDevice (Group1)


-(NSUInteger)extractLastByte {
    unsigned result = -1;
    @try {
        NSScanner *scanner = [NSScanner scannerWithString:self.old_dsid];
        [scanner setScanLocation:self.old_dsid.length-8];
        [scanner scanHexInt:&result];
    } @catch(NSException *scanException) {
        // Be careful, there could be anything within identifier.
    }
    return result;
}

-(NSArray*)availableOutputModes {
    if([self.gtin isEqualToString:kDeviceGtinGE_SDS200])
    {
        NSUInteger result = [self extractLastByte];
        if (result % 2 == 0)
        {
            return @[@kDeviceOutputModeDisabled,
                     @kDeviceOutputModeSwitched,
                     @kDeviceOutputModeDimmed];
        }else
        {
            return @[];
        }
    }
    else if([self.gtin isEqualToString:kDeviceGtinGE_UMR200]) {
        NSUInteger result = [self extractLastByte];
        if (result % 4 == 2)
        {
            return @[@kDeviceOutputModeDisabled,
                     @kDeviceOutputModeSwitchedSinglePolarity,
                     @kDeviceOutputModeSwitchedDoublePolarity,
                     @kDeviceOutputModeSwitched_2_Step,
                     @kDeviceOutputModeSwitched_3_Step];
        }
        else if(result % 4 == 3)
        {
            return @[@kDeviceOutputModeDisabled,
                     @kDeviceOutputModeSwitchedSinglePolarity];
        }
        
        return @[];
        
    }else if([self.gtin isEqualToString:kDeviceGtinGE_KM200] ||
       [self.gtin isEqualToString:kDeviceGtinGE_SDM200] ||
       [self.gtin isEqualToString:kDeviceGtinGE_SDS220] ||
       [self.gtin isEqualToString:kDeviceGtinGE_SDS201] ||
       [self.gtin isEqualToString:kDeviceGtinGE_TKM210] ||
       [self.gtin isEqualToString:kDeviceGtinGE_SDM201] ||
       [self.gtin isEqualToString:kDeviceGtinGE_SDM202]) {
        return @[@kDeviceOutputModeDisabled,
                 @kDeviceOutputModeSwitched,
                 @kDeviceOutputModeDimmed];
        
    } else if([self.gtin isEqualToString:kDeviceGtinGE_KL200]) {
        return @[@kDeviceOutputModeDisabled,
                 @kDeviceOutputModeSwitchedSinglePolarity];
        
    } else if([self.gtin isEqualToString:kDeviceGtinGE_UMV200]) {
        return @[@kDeviceOutputModeDisabled,
                 @kDeviceOutputModeSwitched,
                 @kDeviceOutputModeDimmed_0_10V,
                 @kDeviceOutputModeDimmed_1_10V];
        
    } else if([self.gtin isEqualToString:kDeviceGtinSW_KL200] ||
              [self.gtin isEqualToString:kDeviceGtinSW_ZWS200_J] ||
              [self.gtin isEqualToString:kDeviceGtinSW_ZWS200_F] ||
              [self.gtin isEqualToString:kDeviceGtinSW_ZWS200_EF] ||
              [self.gtin isEqualToString:kDeviceGtinSW_KL213] ||
              [self.gtin isEqualToString:kDeviceGtinSW_KL214]) {
        return @[@kDeviceOutputModeDisabled,
                 @kDeviceOutputModeSwitchedRelay];
    }
    
    //#9372 Hack for SDS220 Halter Edition
    if([self.oemEanNumber isEqualToString:kDevideOemEanGE_SDS220HalterEdition] ||
       [self.oemEanNumber isEqualToString:kDeviceGtinGE_SDS220]) {
        return @[@kDeviceOutputModeDisabled,
                  @kDeviceOutputModeSwitched,
                  @kDeviceOutputModeDimmed];
    }

    return @[];
}

+(NSString*)nameForOutputMode:(NSInteger)outputMode {
    if(outputMode==kDeviceOutputModeDisabled){
        return NSLocalizedString(@"key_outputmode_disabled", nil);
    }
    else if(outputMode==kDeviceOutputModeSwitched ||
            outputMode==kDeviceOutputModeSwitchedSinglePolarity ||
            outputMode==kDeviceOutputModeSwitchedRelay){
        return NSLocalizedString(@"key_outputmode_switched", nil);
    }
    else if(outputMode==kDeviceOutputModeDimmed){
        return NSLocalizedString(@"key_outputmode_dimmed", nil);
    }
    else if(outputMode==kDeviceOutputModeDimmed_0_10V){
        return NSLocalizedString(@"key_outputmode_dimmed_0_10V", nil);
    }
    else if(outputMode==kDeviceOutputModeDimmed_1_10V){
        return NSLocalizedString(@"key_outputmode_dimmed_1_10V", nil);
    }
    else if(outputMode==kDeviceOutputModeSwitchedDoublePolarity) {
        return NSLocalizedString(@"key_outputmode_combined_switched", nil);
    }
    else if(outputMode==kDeviceOutputModeSwitched_2_Step) {
        return NSLocalizedString(@"key_outputmode_combined_switched_2step", nil);
    }
    else if(outputMode==kDeviceOutputModeSwitched_3_Step) {
        return NSLocalizedString(@"key_outputmode_combined_switched_3step", nil);
    }
    
    return @"-";
}

-(NSString*)outputModeName {
    if([self.gtin isEqualToString:kDeviceGtinGE_UMR200] &&
       [self.outputMode integerValue]==kDeviceOutputModeSwitchedSinglePolarity) {
        
        return NSLocalizedString(@"key_outputmode_seperatly_switched", nil);
    }
    
    return [DSDevice nameForOutputMode:[self.outputMode integerValue]];
}

-(BOOL)isOutputModeDimmable {
    NSInteger outputMode = [self.outputMode integerValue];
    if(outputMode == kDeviceOutputModeDimmed ||
       outputMode == kDeviceOutputModeDimmed_0_10V ||
       outputMode == kDeviceOutputModeDimmed_1_10V) {
        return YES;
    }
    
    return NO;
}
-(BOOL)isOutputModeReadonly {
    if([self.gtin isEqualToString:kDeviceGtinGE_UMR200]) {
        return YES;
    }
    
    return NO;
}

@end
