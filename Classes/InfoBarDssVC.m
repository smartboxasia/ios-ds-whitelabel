//
//  InfoBarDssVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "InfoBarDssVC.h"
#import "DigitalstromJSONService.h"
#import "ModelLocator.h"


@implementation InfoBarDssVC

@synthesize labelName, labelVersion;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self applyLabels];
	[[ModelLocator instance] addObserver:self forKeyPath:k_DSS_VERSION options:0 context:nil];
	[[ModelLocator instance] addObserver:self forKeyPath:k_DSS_NAME options:0 context:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DSS_VERSION];
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DSS_NAME];
    }
    @catch (NSException *exception) {
        //just in case
    }

}


- (void) applyLabels {
	self.labelName.text = [[ModelLocator instance] valueForKey:k_DSS_NAME];
	self.labelVersion.text = [[ModelLocator instance] valueForKey:k_DSS_VERSION];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_DSS_NAME] || [keyPath isEqualToString:k_DSS_VERSION]) {
		[self applyLabels];
	}
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) prepareDealloc {

}

- (void)dealloc {
	
    [super dealloc];
}


@end
