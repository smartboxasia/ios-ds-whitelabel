//
//  SettingsScenesDeviceDetailVC.h
//  DS iPhone
//
//  Created by rolf on 24.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSDevice.h"
#import "DSScene.h"
#import "CustomTextField.h"
#import "UICustomSwitch.h"
#import "DetachedDevice.h"

@interface SettingsScenesDeviceDetailVC : UIViewController {
	DSDevice *device;
	DSScene *scene;
	
	NSMutableDictionary *deviceProperties;
	
	IBOutlet CustomTextField *deviceNameTextField;
	IBOutlet UILabel *internID;
	IBOutlet UICustomSwitch *powerSwitch;
	IBOutlet UISlider *dimmSlider;
	IBOutlet UILabel *dimmLabel;
	IBOutlet UILabel *roomLabel;
}

- (id)initWithDevice:(DSDevice *)d withScene:(DSScene*)s;
- (IBAction) actionBack;
- (IBAction) powerSwitchChanged: (id) sender;
- (IBAction) sliderChanged: (id) sender;
- (IBAction) sliderTouchUpInside: (id) sender;

@end
