//
//  SettingsVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface SettingsVC : GAITrackedViewController {
	UILabel *labelHeader;
	UIButton *buttonHeaderLogo;
	UIButton *buttonBearbeiten;
	UIButton *buttonReload;
	UIViewController *viewControllerSceneReload;
	
	UIViewController *currentSubController;
	
	NSMutableArray *viewStack;
    
    CGFloat sHeight;
    CGFloat sWidth;
}

@property(nonatomic, retain) IBOutlet UILabel *labelHeader;
@property(nonatomic, retain) IBOutlet UIButton *buttonHeaderLogo;
@property(nonatomic, retain) IBOutlet UIButton *buttonBearbeiten;
@property(nonatomic, retain) IBOutlet UIButton *buttonReload;

- (IBAction) actionToggleSettings: (id) sender;
- (IBAction) actionLogo;
- (void) showSubGroup: (UIViewController*) groupComponent;
- (void) goBack;
- (void) goBackTwice;
- (void) showController: (UIViewController*) controller;
- (void) displaySceneReload;
- (void) hideSceneReload;
- (void) showWithoutIntelligence;

@end
