//
//  SettingsDeviceChooseModeVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceChooseModeVC.h"
#import "SettingsDeviceModeListItemVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "CustomLabel.h"
#import "DSDevice+Group1.h"

@implementation SettingsDeviceChooseModeVC

@synthesize scrollview;
@synthesize background;
@synthesize titelLabel;

- (id) initWithCurrentDevice: (DSDevice*)device{
    if (self = [super init]) {
        _currentDevice = device;
	}
    
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_CHOOSE_MODE_DEVICE_LIST];

    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }
    
    titelLabel.text = NSLocalizedString(@"key_choose_mode", nil);
	items = [NSMutableArray new];
	
	[self buildScrollView];
    
    self.screenName = @"Settings Devices Output Mode";
} 

- (NSInteger)addItemForOutputMode:(NSInteger)outputMode atOffset:(NSInteger)yOffset {
    SettingsDeviceModeListItemVC *item = [[SettingsDeviceModeListItemVC alloc] initWithMode:outputMode andDevice:_currentDevice];
    item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
    if ([_currentDevice.outputMode intValue] == outputMode) {
        [item.buttonStepInto.imageView setImage: [UIImage imageNamed:@"Ausw_btn_speichern"]];
    }
    else{
        [item.buttonStepInto.imageView setImage: nil];
    }
    [self.scrollview addSubview:item.view];
    yOffset += item.view.frame.size.height;
    [items addObject:item];
    
    return yOffset;
}

- (void)buildScrollView {
	for (UIViewController *item in items) {
		[item.view removeFromSuperview];
		[item release];
	}
	[items removeAllObjects];
    
    NSInteger yOffset = 0;
    
    NSArray* availableOutputModes = _currentDevice.availableOutputModes;
    
    for(NSNumber* outputMode in availableOutputModes) {
        yOffset = [self addItemForOutputMode:[outputMode integerValue] atOffset:yOffset];
    }

	self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, yOffset);
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
	
	[[ModelLocator instance] setValue:nil forKey:k_VIEW_SETTINGS_CHOOSE_MODE_DEVICE_LIST];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
	
	for (UIViewController *child in items) {
		[child.view removeFromSuperview];
		[child release];
	}
	[items removeAllObjects];
	
    [super dealloc];
}


@end
