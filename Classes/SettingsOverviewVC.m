//
//  SettingsOverviewVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsOverviewVC.h"
#import "ModelLocator.h"
#import "SettingsVC.h"
#import "SettingsRoomsListVC.h"
#import "SettingsDevicesListVC.h"
#import "SettingsScenesListByRoomVC.h"
#import "SettingsDevicesListByRoomVC.h"
#import "SettingsScenesListVC.h"
#import "SettingsFavoriteBarVC.h"
#import "CustomLabel.h"
#import "DataController.h"
#import "SettingsInfoBarVC.h"
#import "SettingsMetersListVC.h"
#import "SettingsNetworkVC.h"
#import "ModelLocator.h"
#import "SB_iPhoneAppDelegate.h"
#import "SB_iPhoneAppDelegate.h"

@implementation SettingsOverviewVC

@synthesize labelVersion;
@synthesize background;

- (void) viewDidLoad {
    [super viewDidLoad];
    
    titleLabel.text = NSLocalizedString(@"key_overview", nil);
    label1.text = NSLocalizedString(@"key_settings_connection_title", nil);
    label2.text = NSLocalizedString(@"key_rooms_title", nil);
    label3.text = NSLocalizedString(@"key_activities_title", nil);
    label4.text = NSLocalizedString(@"key_devices_title", nil);
    label5.text = NSLocalizedString(@"key_meters_title", nil);
    label6.text = NSLocalizedString(@"key_favorites_title", nil);
    label7.text = NSLocalizedString(@"key_infopanel_title", nil);
    
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && fabs(screenHeight - 568.0f) < FLT_EPSILON) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

    [self setVersionString];
}

-(void) setVersionString
{
    self.labelVersion.text = [NSString stringWithFormat:@"v%@", [self appVersionDisplayString]];
    self.labelVersion.textAlignment = NSTextAlignmentRight;

//    if ([UIScreen mainScreen].bounds.size.height <= 567)
//    {
//        self.labelVersion.text = [NSString stringWithFormat:@"v%@", [self appVersionDisplayString]];
//        self.labelVersion.textAlignment = NSTextAlignmentRight;
//        
//    }else
//    {
//        self.labelVersion.text = [NSString stringWithFormat:@"%@, Version %@",[self appName], [self appVersionDisplayString]];
//        self.labelVersion.textAlignment = NSTextAlignmentCenter;
//    }
}

-(NSString *) appName
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [infoDictionary objectForKey:@"CFBundleDisplayName"];
}

-(NSString *) appVersionDisplayString
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    if([(SB_iPhoneAppDelegate*)[UIApplication sharedApplication].delegate isTestVersion]) {
        return minorVersion;
    } else {
        return majorVersion;
    }
}


#pragma mark -
#pragma mark Button Actions

- (IBAction) buttonAction1: (id) sender {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[SettingsNetworkVC new]];
}

- (IBAction) buttonAction2: (id) sender {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[SettingsRoomsListVC new]];
}

- (IBAction) buttonAction3: (id) sender {
    SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[[SettingsScenesListByRoomVC alloc]initWithFavorite:NO]];
}

- (IBAction) buttonAction4: (id) sender {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[SettingsDevicesListByRoomVC new]];
}

- (IBAction) buttonAction5: (id) sender {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[SettingsMetersListVC new]];
}

- (IBAction) buttonAction6: (id) sender {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[SettingsFavoriteBarVC new]];
}

- (IBAction) buttonAction7: (id) sender {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[SettingsInfoBarVC new]];
}

- (IBAction) buttonAction8: (id) sender {

}

- (IBAction) buttonAction9: (id) sender {

}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	//[[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_APP_VERSION];
	
    [titleLabel release];
    [label1 release];
    [label2 release];
    [label3 release];
    [label4 release];
    [label5 release];
    [label6 release];
    [label7 release];
    [super dealloc];
}


@end
