//
//  InfoBarTemperatureVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface InfoBarTemperatureVC : UIViewController {
	UILabel *labelTemperature;
	IBOutlet UIImageView *conditionImageView;
}

@property (nonatomic, retain) IBOutlet UILabel *labelTemperature;
@property (nonatomic, retain) IBOutlet UILabel *labelCity;

@end
