//
//  SettingsScenesDetailAddDeviceVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsScenesDetailAddDeviceVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "SettingsDevicesListItemVC.h"
#import "CustomLabel.h"
#import "CustomButton.h"
#import "DigitalstromJSONService.h"


@implementation SettingsScenesDetailAddDeviceVC

@synthesize scrollview;
@synthesize background;

- (id) initWithScene: (DSScene*) _scene {
	if (self = [super init]) {
		scene = _scene;
	}
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

	//iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

	int i=0;
	int y=0;
	
	for (DSRoom* room in [DataController instance].rooms) {
		
		if ([room.devices count] > 0) {
			BOOL roomNameDrawed = NO;
			
			for (DSDevice* device in room.devices) {
				if ([self containsDevice:device inSet:scene.devices]) {
					continue;
				}
				
				if (!roomNameDrawed) {
					// draw rooms name
					UIImageView *imageBgRoom = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Einst_ListenTeiler.png"]];
					imageBgRoom.frame = CGRectMake(0, y, 275, 20);
					[self.scrollview addSubview:imageBgRoom];
                    [imageBgRoom release];
					
					CustomLabel *labelRoom = [CustomLabel new];
					labelRoom.frame = CGRectMake(15, y+2, 240, 20);
					labelRoom.text = room.name;
					labelRoom.backgroundColor = [UIColor clearColor];
					labelRoom.font = [labelRoom.font fontWithSize:12];
					[self.scrollview addSubview:labelRoom];
                    [labelRoom release];
					y+=20;
					roomNameDrawed = YES;
				}
				
				//SettingsDevicesListItemVC *item = [[SettingsDevicesListItemVC alloc] initWithDevice:device andDarkBg:i%2==1];
				//item.view.frame = CGRectMake(0, y, item.view.frame.size.width, item.view.frame.size.height);
				UIView *itemView = [self allocViewForDeviceItem:device withYOffset:y];
				[self.scrollview addSubview: itemView];
				i++;
				y += itemView.frame.size.height;
			}
			
			
			
		}		
	}
	
	self.scrollview.contentSize = CGSizeMake(275, y);
}

- (BOOL) containsDevice: (DSDevice*) device inSet: (NSSet*) set {
	for (DSDevice *d in set) {
		if ([d.identifier isEqualToString:device.identifier]) {
			return YES;
		}
	}
	
	return NO;
}

- (UIView*) allocViewForDeviceItem: (DSDevice*) device withYOffset:(float) yOffset {
	UIView *view = [UIView new];
	view.frame = CGRectMake(0, yOffset, 275, 45);
	
	NSString *strImageName;
	
	if (((int)yOffset%2) == 1) {
		strImageName = @"Einst_Liste_hell.png";
	}
	else {
		strImageName = @"Einst_Liste_dunkel.png";
	}
	
	UIImageView *imageBG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:strImageName]];
	imageBG.frame = CGRectMake(0, 0, 275, 45);
	[view addSubview:imageBG];
    [imageBG release];
	
	
	CustomLabel *label = [[CustomLabel alloc] initWithFrame:CGRectMake(15, 12, 237, 21)];
	label.text = device.name;
	label.textAlignment = NSTextAlignmentLeft;
	label.lineBreakMode = NSLineBreakByTruncatingTail;
	label.backgroundColor = [UIColor clearColor];
	[view addSubview:label];
    [label release];
	
	UIImageView *imageArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Einst_Icon_Pfeil.png"]];
	imageArrow.frame = CGRectMake(245, 11, 23, 23);
	[view addSubview:imageArrow];
    [imageArrow release];
	
	CustomButton *button = [CustomButton buttonWithType:UIButtonTypeCustom];
	button.frame = CGRectMake(0, 0, 275, 45);
	[button handleControlEvent:UIControlEventTouchUpInside withBlock:^{
		GSLog(@"device: %@", device.name);
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			if (![[DataController instance] addDeviceToScene:device withScene:scene]) {
				dispatch_async(dispatch_get_main_queue(), ^{
					UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"") message:NSLocalizedString(@"key_error_add_device_to_activity", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"") otherButtonTitles:nil];
					[alert show];
                    [alert release];
				});
			}			
		});
		[self actionBack];
	}];
	[view addSubview:button];
	
	return view;
}

- (void) activateReloadButton {
	((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonReload.hidden = NO;
	
	[UIView animateWithDuration:0.3 animations:^{
		((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonReload.alpha = 1;
	}];
	
	[((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonReload addTarget:self action:@selector(actionReloadScenes) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
	
	[self activateReloadButton];	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
