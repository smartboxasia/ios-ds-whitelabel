//
//  SettingsFavoriteBarSceneListVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class CustomLabel;

@interface SettingsFavoriteBarSceneListVC : GAITrackedViewController {
	UIScrollView *scrollview;
	NSMutableArray *childControllers;
    IBOutlet CustomLabel *titleLabel;
}

@property(nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (void) buildScrollView;
- (IBAction) actionBack;

@end
