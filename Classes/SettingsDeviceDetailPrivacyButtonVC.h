//
//  SettingsDeviceDetailPrivacyButtonVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface SettingsDeviceDetailPrivacyButtonVC : UIViewController

- (IBAction) actionButtonClicked:(id)sender;

@end
