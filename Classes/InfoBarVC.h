//
//  InfoBarVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface InfoBarVC : UIViewController <UIScrollViewDelegate> {

	UILabel *labelTemp;
	UILabel *labelWeekday;
	UILabel *labelDayAndMonth;
	UILabel *labelTime;
	
	UIScrollView *scrollView;
	UIPageControl *pageControl;
	
	NSTimer *datetimeTimer;
	
	NSMutableArray *items;
}

@property (nonatomic, retain) IBOutlet UILabel *labelTemp;
@property (nonatomic, retain) IBOutlet UILabel *labelWeekday;
@property (nonatomic, retain) IBOutlet UILabel *labelDayAndMonth;
@property (nonatomic, retain) IBOutlet UILabel *labelTime;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;


- (void) prepareDealloc;
- (void) storeInML;
- (void) updateDateTime;
- (void) buildScrollView;

@end
