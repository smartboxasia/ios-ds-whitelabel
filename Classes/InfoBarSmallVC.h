//
//  InfoBarSmall.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSRoom;

@interface InfoBarSmallVC : UIViewController {
	NSTimer *datetimeTimer;
	
	UILabel *labelConsumption;
	UILabel *labelTitle;
	UILabel *labelTime;
	
	DSRoom *room;
}

@property (nonatomic, retain) IBOutlet UILabel *labelConsumption;
@property (nonatomic, retain) IBOutlet UILabel *labelTitle;
@property (nonatomic, retain) IBOutlet UILabel *labelTime;

- (id) initWithRoom: (DSRoom*) theRoom;

- (void) updateDateTime;
- (void) storeInML;
- (void) prepareDealloc;

@end
