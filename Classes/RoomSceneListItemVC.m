//
//  RoomSceneListItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "RoomSceneListItemVC.h"
#import "DSScene.h"
#import "DataController.h"
#import "MainVC.h"
#import "ModelLocator.h"
#import "GAIHelper.h"
#import "DSServer.h"


@implementation RoomSceneListItemVC

@synthesize imageBackground,imageColorBar, buttonLabel, buttonIcon, colorCode, doAnimation;

- (id)initWithScene: (DSScene*) theScene colorCode:(int)theColorCode doAnimation:(BOOL)animation {
	if (self = [super init]) {
		scene = theScene;
        colorCode = theColorCode;
        doAnimation = animation;
	}
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
	
	if (scene) {
		[self.buttonLabel setTitle:scene.name forState:UIControlStateNormal];
	}
	
    switch (colorCode) {
        case 1:
            //yellow scenes
            self.imageColorBar.image = [UIImage imageNamed:@"RS_Geraete_Farbcode_Licht.png"];
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"lightbulb_off_wide.png"] forState:UIControlStateNormal];
            else
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"lightbulb_wide.png"] forState:UIControlStateNormal];
            break;
        case 2:
            //gray scenes
            self.imageColorBar.image = [UIImage imageNamed:@"RS_Geraete_Farbcode_Besch.png"];
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"shade_deact_full_wide.png"] forState:UIControlStateNormal];
            else if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:5]])
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"shade_open_wide.png"] forState:UIControlStateNormal];
            else
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"shade_wide.png"] forState:UIControlStateNormal];
            break;
        case 3:
            //blue scenes
            self.imageColorBar.image = [UIImage imageNamed:@"RS_device_blue.png"];
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"blue_off_wide.png"] forState:UIControlStateNormal];
            else
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"blue_on_wide.png"] forState:UIControlStateNormal];
            break;
        case 4:
            //blue scenes
            self.imageColorBar.image = [UIImage imageNamed:@"RS_device_cyan.png"];
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"music_off_wide.png"] forState:UIControlStateNormal];
            else
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"music_wide.png"] forState:UIControlStateNormal];
            break;
        case 5:
            //blue scenes
            self.imageColorBar.image = [UIImage imageNamed:@"RS_device_magenta.png"];
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"television_off_wide.png"] forState:UIControlStateNormal];
            else
                [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"television_wide.png"] forState:UIControlStateNormal];
            break;

        default:
            //default
            self.imageColorBar.hidden = YES;
            [self.buttonIcon setBackgroundImage:[UIImage imageNamed:@"RS_Konifi_Icon.png"] forState:UIControlStateNormal];

            break;
    }
   
    //animate color bar with fade effect
    if(doAnimation){
        self.imageColorBar.alpha = 0.0;
        self.buttonLabel.alpha = 0.0;
        self.buttonIcon.alpha = 0.0;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        self.imageColorBar.alpha = 1.0;
        self.buttonLabel.alpha = 1.0;
        self.buttonIcon.alpha = 1.0;
        [UIView commitAnimations];
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self buildBackground];
    
    if (scene && scene.room) {
        if (colorCode == 1) {
            [scene.room addObserver:self forKeyPath:@"lastCalledScene" options:0 context:nil];
        }
        else if(colorCode == 2){
            [scene.room addObserver:self forKeyPath:@"lastCalledShadeScene" options:0 context:nil];
        }
        else if(colorCode == 3){
            [scene.room addObserver:self forKeyPath:@"lastCalledBlueScene" options:0 context:nil];
        }
        else if(colorCode == 4){
            [scene.room addObserver:self forKeyPath:@"lastCalledAudioScene" options:0 context:nil];
        }
        else if(colorCode == 5){
            [scene.room addObserver:self forKeyPath:@"lastCalledVideoScene" options:0 context:nil];
        }
        
	}
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        if (colorCode == 1) {
            [scene.room removeObserver:self forKeyPath:@"lastCalledScene" ];
        }
        else if(colorCode == 2){
            [scene.room removeObserver:self forKeyPath:@"lastCalledShadeScene"];
        }
        else if(colorCode == 3){
            [scene.room removeObserver:self forKeyPath:@"lastCalledBlueScene"];
        }
        else if(colorCode == 4){
            [scene.room removeObserver:self forKeyPath:@"lastCalledAudioScene"];
        }
        else if(colorCode == 5){
            [scene.room removeObserver:self forKeyPath:@"lastCalledVideoScene"];
        }
        
    }
    @catch (NSException *exception) {
        //just in case
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//	NSLog(@"scene: %@ // room: %@", scene, scene.room);
	if ([keyPath isEqualToString:@"lastCalledScene"] || [keyPath isEqualToString:@"lastCalledShadeScene"] || [keyPath isEqualToString:@"lastCalledBlueScene"] || [keyPath isEqualToString:@"lastCalledAudioScene"] || [keyPath isEqualToString:@"lastCalledVideoScene"]) {
		[self buildBackground];
	}
	
}

- (void) buildBackground {
	if (!scene || (scene && !scene.sceneNo)|| (scene && !scene.group) || (scene && !scene.room) || (scene && ![scene isKindOfClass:[DSScene class]]) || (scene && scene.room && ![scene.room isKindOfClass:[DSRoom class]])) {
        self.imageBackground.highlighted = NO;
		return;
	}
	
	if ((scene.sceneNo && scene.room.lastCalledScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:1]]) {
        self.imageBackground.highlighted = YES;
	}
    else if ((scene.sceneNo && scene.room.lastCalledShadeScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledShadeScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:2]]) {
        self.imageBackground.highlighted = YES;
	}
    else if ((scene.sceneNo && scene.room.lastCalledBlueScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledBlueScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:3]]) {
        self.imageBackground.highlighted = YES;
	}
    else if ((scene.sceneNo && scene.room.lastCalledAudioScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledAudioScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:4]]) {
        self.imageBackground.highlighted = YES;
	}
    else if ((scene.sceneNo && scene.room.lastCalledVideoScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledVideoScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:5]]) {
        self.imageBackground.highlighted = YES;
	}

	else {
        self.imageBackground.highlighted = NO;
	}
}


- (IBAction) actionButtonIcon: (id) sender {
        if([scene.group isEqualToNumber:[NSNumber numberWithInt:1]]){
            //don't open adjustmenscreen for scene 0 on lights
            if(![scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]]){
                [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] showAdjustmentScreen:scene];
            }
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:2]]){
            [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] showShadeAdjustmentScreen:scene];
        }
        //skip for other groups
}

- (IBAction) actionButtonLabel: (id) sender {
	if (scene && ![scene isFault]) {
		if (scene.room) {
            
            [GAIHelper sendUIActionWithCategory:@"scene_called" andLabel:@"room"];
            
			if ([scene.room.lastCalledScene intValue] == [scene.sceneNo longLongValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:1]] ) {
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    DSScene* defaultTurnOffScene = [[[DataController instance] getCurrentDSS] getScene:[NSNumber numberWithInt:0] inRoom:scene.room withGroup:scene.group];
                    [[DataController instance] turnSceneOn:defaultTurnOffScene];
				});
			}
            else if ([scene.room.lastCalledShadeScene intValue] == [scene.sceneNo longLongValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:2]] ) {
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    [[DataController instance] callStandardScene:k_SERVICE_SCENE_STOP withRoom:scene.room withGroup:2];
                    
				});
			}
            else if ([scene.room.lastCalledBlueScene intValue] == [scene.sceneNo longLongValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:3]] ) {
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    DSScene* defaultTurnOffScene = [[[DataController instance] getCurrentDSS] getScene:[NSNumber numberWithInt:0] inRoom:scene.room withGroup:scene.group];
                    [[DataController instance] turnSceneOn:defaultTurnOffScene];
                    
				});
			}
            else if ([scene.room.lastCalledAudioScene intValue] == [scene.sceneNo longLongValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:4]] ) {
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    DSScene* defaultTurnOffScene = [[[DataController instance] getCurrentDSS] getScene:[NSNumber numberWithInt:0] inRoom:scene.room withGroup:scene.group];
                    [[DataController instance] turnSceneOn:defaultTurnOffScene];
                    
				});
			}

            else if ([scene.room.lastCalledVideoScene intValue] == [scene.sceneNo longLongValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:5]] ) {
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    DSScene* defaultTurnOffScene = [[[DataController instance] getCurrentDSS] getScene:[NSNumber numberWithInt:0] inRoom:scene.room withGroup:scene.group];
                    [[DataController instance] turnSceneOn:defaultTurnOffScene];
                    
				});
			}

            
			else {
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
					[[DataController instance] turnSceneOn:scene];
				});
			}
			
			return;

		}
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] turnSceneOn:scene];
		});		
	}	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) prepareDealloc {
	//[scene.room removeObserver:self forKeyPath:@"lastCalledScene"];
}

- (void)dealloc {
    [super dealloc];
}


@end
