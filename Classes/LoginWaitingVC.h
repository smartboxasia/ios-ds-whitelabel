//
//  LoginWaitingVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"


@interface LoginWaitingVC : UIViewController {
	UILabel *labelMessage;
	UIButton *buttonRepeat;
	UIActivityIndicatorView *activityIndicator;
	NSMutableArray *observers;
}

@property (nonatomic , retain) IBOutlet UILabel *labelMessage;
@property (nonatomic , retain) IBOutlet UIButton *buttonRepeat;
@property (nonatomic , retain) IBOutlet UIActivityIndicatorView *activityIndicator;


- (IBAction) actionRetry;
- (IBAction) actionClose;

@end
