//
//  InfoBarVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "InfoBarVC.h"
#import "ModelLocator.h"
#import "NSDate+Helper.h"
#import "FacilityController.h"
#import "InfoBarConsumptionVC.h"
#import "InfoBarTemperatureVC.h"
#import "InfoBarDssVC.h"

@implementation InfoBarVC

@synthesize labelTemp;
@synthesize labelWeekday;
@synthesize labelDayAndMonth;
@synthesize labelTime;
@synthesize scrollView;
@synthesize pageControl;

#pragma mark -
#pragma mark tear up methods
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	items = [NSMutableArray new];
	
//	if ([[ModelLocator instance] valueForKey:k_STANDARD_OUTSIDE_TEMP]) {
//		self.labelTemp.text = [NSString stringWithFormat:@"%@°C", [[ModelLocator instance] valueForKey:k_STANDARD_OUTSIDE_TEMP]];
//	}
//	else {
//		self.labelTemp.text = @"--° C";
//	}
	
	
	[self updateDateTime];
	
	datetimeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateDateTime) userInfo:nil repeats:YES];
	
    [FacilityController instance];
	
//	[self buildScrollView];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[ModelLocator instance] valueForKey:k_STANDARD_OUTSIDE_TEMP]) {
		self.labelTemp.text = [NSString stringWithFormat:@"%@°C", [[ModelLocator instance] valueForKey:k_STANDARD_OUTSIDE_TEMP]];
	}
	else {
		self.labelTemp.text = @"--° C";
	}
    [self buildScrollView];
    [[ModelLocator instance] addObserver:self forKeyPath:k_STANDARD_OUTSIDE_TEMP options:0 context:nil];
	[[ModelLocator instance] addObserver:self forKeyPath:k_DATA_INFOLIST_HAS_CHANGED options:0 context:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_STANDARD_OUTSIDE_TEMP];
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_INFOLIST_HAS_CHANGED];
    }
    @catch (NSException *exception) {
        //just in case
    }

}


- (void) storeInML {
	[[ModelLocator instance] setValue:self forKey:k_VIEW_INFOBAR];
}

- (void) buildScrollView {
	
	for (UIViewController *item in items) {
		[item.view removeFromSuperview];
		[item release];
	}
	[items removeAllObjects];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	NSInteger indexVerbrauch = 0;
	NSInteger indexWetter = 1;
	NSInteger indexName = 2;
	
	if ([defaults objectForKey:k_VIEW_LABEL_STROMVERBRAUCH]) {
		indexVerbrauch = [[defaults objectForKey:k_VIEW_LABEL_STROMVERBRAUCH] integerValue];
	}
	
	if ([defaults objectForKey:k_VIEW_LABEL_WETTER]) {
		indexWetter = [[defaults objectForKey:k_VIEW_LABEL_WETTER] integerValue];
	}
	
	if ([defaults objectForKey:k_VIEW_LABEL_DSS_NAME]) {
		indexName = [[defaults objectForKey:k_VIEW_LABEL_DSS_NAME] integerValue];
	}
	
	NSInteger i;
	
	for (i=0; i<3; i++) {
				
		if (indexVerbrauch == i) {
			InfoBarConsumptionVC *vc = [InfoBarConsumptionVC new];
			vc.view.frame = CGRectMake(230*i, vc.view.frame.origin.y, vc.view.frame.size.width, vc.view.frame.size.height);
			[self.scrollView addSubview:vc.view];
			[items addObject:vc];
		}
		else if (indexWetter == i) {
			InfoBarTemperatureVC *vc = [InfoBarTemperatureVC new];
			vc.view.frame = CGRectMake(230*i, vc.view.frame.origin.y, vc.view.frame.size.width, vc.view.frame.size.height);
			[self.scrollView addSubview:vc.view];
			[items addObject:vc];
		}
		else if (indexName == i) {
			InfoBarDssVC *vc = [InfoBarDssVC new];
			vc.view.frame = CGRectMake(230*i, vc.view.frame.origin.y, vc.view.frame.size.width, vc.view.frame.size.height);
			[self.scrollView addSubview:vc.view];
			[items addObject:vc];
		}
	}
	
	self.scrollView.contentSize = CGSizeMake(230*i, self.scrollView.frame.size.height);
	self.scrollView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	
	self.pageControl.numberOfPages =i;
}

#pragma mark -
#pragma mark NSTimer target selector

- (void) updateDateTime {
	NSDate *date = [NSDate date];
	self.labelWeekday.text = [date stringWithFormat:@"EEEE"];
	self.labelDayAndMonth.text = [date stringWithFormat:@"dd. LLLL"];
	self.labelTime.text = [date stringWithFormat:@"HH:mm"];
}


#pragma mark -
#pragma mark Observer responder

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_STANDARD_OUTSIDE_TEMP]) {
		self.labelTemp.text = [NSString stringWithFormat:@"%@°C", [[ModelLocator instance] valueForKey:k_STANDARD_OUTSIDE_TEMP]];
	}
	else if ([keyPath isEqualToString:k_DATA_INFOLIST_HAS_CHANGED]) {
		[self buildScrollView];
	}

}

#pragma mark -
#pragma mark Scrollview delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)theScrollView {
	int index = round(theScrollView.contentOffset.x/230);
	self.pageControl.currentPage = index;
}

#pragma mark -
#pragma mark IB Actions


#pragma mark -
#pragma mark tear down methods

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) prepareDealloc {
	[datetimeTimer invalidate];
	
	for (UIViewController *child in items) {
		[child.view removeFromSuperview];
		[child prepareDealloc];
		[child release];
	}
	
	[items removeAllObjects];
	[items release];
}

- (void)dealloc {
	
    [super dealloc];
}


@end
