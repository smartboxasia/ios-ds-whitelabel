//
//  SettingsScenesDeviceDetailVC.m
//  DS iPhone
//
//  Created by rolf on 24.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SettingsScenesDeviceDetailVC.h"
#import "DSRoom.h"
#import "SettingsVC.h"
#import "DataController.h"
#import "ModelLocator.h"


@implementation SettingsScenesDeviceDetailVC

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

- (id)initWithDevice:(DSDevice *)d withScene:(DSScene*)s {
    self = [super init];
    if (self) {
		device = [[DataController instance] getDetachedDevice:d withScene:s];
		[device retain];
		scene = s;
		
		deviceProperties = [[NSMutableDictionary alloc] init];
		[deviceProperties setObject:d.powerOn forKey:@"powerOn"];
		[deviceProperties setObject:d.dimmFactor forKey:@"dimmFactor"];
		
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	deviceNameTextField.text = device.name;
	internID.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"key_ID", @""), [device.identifier substringWithRange:NSMakeRange([device.identifier length]-8, 8)]];
	powerSwitch.frame = CGRectMake(powerSwitch.frame.origin.x, powerSwitch.frame.origin.y, powerSwitch.frame.size.width, 27);
	
	powerSwitch.on = [device.powerOn boolValue];
	
	dimmLabel.text = [NSString stringWithFormat:@"%.0f%%",[device.dimmFactor floatValue]*100];
	[dimmSlider setMinimumTrackImage:[[UIImage imageNamed:@"orangeslider.png"] stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0] forState:UIControlStateNormal];
	dimmSlider.value = [device.dimmFactor floatValue];
	
	roomLabel.text = ((DSRoom*)device.room).name;
	
	
}



#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	
	NSNumber *powerOn = [NSNumber numberWithBool:powerSwitch.on];
	NSNumber *dimmFactor = [NSNumber numberWithFloat:dimmSlider.value];
	
	BOOL updated = NO;
	if ([powerOn boolValue] != [[deviceProperties objectForKey:@"powerOn"] boolValue] || [dimmFactor floatValue] != [[deviceProperties objectForKey:@"dimmFactor"] floatValue]) {
		updated = YES;
	}
	
	
	if (updated) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_note", @"") message:NSLocalizedString(@"key_reset_device_status", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"key_no", @"") otherButtonTitles:NSLocalizedString(@"key_yes", @""),nil];
		[alert show];
		[alert release];
	} else {
		[[DataController instance] saveDeviceStateInScene:device withScene:scene];
		SettingsVC *settings = (SettingsVC *)[[ModelLocator instance] valueForKey: k_VIEW_SETTINGS];
		[settings goBack];
	}

	
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	[[DataController instance] saveDeviceStateInScene:device withScene:scene];
	
	DetachedDevice *dd = [[DataController instance] getDetachedDevice:device withScene:scene];
	
	GSLog(@"Name: %@ Power: %@ %@", dd.name, dd.powerOn, dd.identifier);
	
	NSNumber *powerOn = [NSNumber numberWithBool:powerSwitch.on];
	NSNumber *dimmFactor = [NSNumber numberWithFloat:dimmSlider.value];
	
	[[DataController instance] updateDetachedDevice:dd withPowerOn:powerOn withDimmFactor:dimmFactor withScene:scene];
	
	SettingsVC *settings = (SettingsVC *)[[ModelLocator instance] valueForKey: k_VIEW_SETTINGS];
	[settings goBack];
	
	//TODO: Dimm-Factor setzen geht hier noch nicht, da zuerst der Gerätetyp eindeutig sein muss
	
	if (buttonIndex == 1) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] setDevicePower:device withBool:[[deviceProperties objectForKey:@"powerOn"] boolValue] withGroup:[device.group integerValue]];
		});
	}
}


- (IBAction) powerSwitchChanged: (id) sender {
	if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			
			GSLog(@"RetainCount %i", [device retainCount]);
			
			[[DataController instance] setDevicePower:((DSDevice*)device) withBool:((UICustomSwitch*) sender).on withGroup:[device.group integerValue]];
		});		
	}
}

- (IBAction) sliderChanged: (id) sender {
	//device.dimmFactor = [dimmerSlider.value];
	dimmLabel.text = [NSString stringWithFormat:@"%.0f%%",((UISlider*)sender).value*100];
}

- (IBAction) sliderTouchUpInside: (id) sender {
	if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] setDeviceValue:device withValue:[NSNumber numberWithFloat:((UISlider*)sender).value]];
		});		
	}
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/



- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[device release];
    [super dealloc];
}


@end
