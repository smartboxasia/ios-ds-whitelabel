//
//  FavoriteBarButtonVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSScene;

@interface FavoriteBarButtonVC : UIViewController {
	UIButton *button;
	UIImageView *imageBg;
	DSScene *scene;
	BOOL needConfirmation;
    BOOL shouldUndoScene;
}

@property (nonatomic, retain) IBOutlet UIButton *button;
@property (nonatomic, retain) IBOutlet UIImageView *imageBg;

- (id) initWithScene: (DSScene*) theScene;

- (IBAction) actionButtonUp: (id) sender;
- (IBAction) actionButtonDown: (id) sender;
- (void) executeScene;

@end
