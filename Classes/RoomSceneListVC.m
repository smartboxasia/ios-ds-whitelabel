//
//  RoomSceneListVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "RoomSceneListVC.h"
#import "ModelLocator.h"
#import "DSRoom.h"
#import "MainVC.h"
#import "DataController.h"
#import "RoomSceneListItemVC.h"
#import "ColorDotsView.h"

@interface RoomSceneListVC()
@property (nonatomic, retain) IBOutlet ColorDotsView *colorDotsView;
@end

@implementation RoomSceneListVC

@synthesize scrollView, labelTitle;
@synthesize viewRibbon, offset;
@synthesize colorDotsView, currentIndex, animateItems;

#pragma mark -
#pragma mark tear up


- (id) initWithRoom: (DSRoom*) theRoom andOffset: (float) theOffset withIndex:(int)index {
	if (self = [super init]) {
		room = theRoom;
		self.offset = theOffset;
		scrollviewItems = [[NSMutableArray alloc] initWithCapacity:0];
        availableColors= [[NSMutableArray alloc] initWithCapacity:0];


        if (theRoom.group1present)
            [availableColors addObject:[NSNumber numberWithInt:1]];
        if (theRoom.group2present)
            [availableColors addObject:[NSNumber numberWithInt:2]];
        if (theRoom.group3present)
            [availableColors addObject:[NSNumber numberWithInt:3]];
        if (theRoom.group4present)
            [availableColors addObject:[NSNumber numberWithInt:4]];
        if (theRoom.group5present)
            [availableColors addObject:[NSNumber numberWithInt:5]];

        self.currentIndex = index;
	}

	return self;
}

- (void)viewDidLoad {
    GSLog("log");

    [super viewDidLoad];

    sWidth = [UIScreen mainScreen].bounds.size.width;
    sHeight = [UIScreen mainScreen].bounds.size.height;

	if (room) {
		self.labelTitle.text = room.name;

        // self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, self.scrollView.frame.size.width, sHeight-199);
        // self.viewRibbon.frame = CGRectMake(0, 50+self.offset, self.viewRibbon.frame.size.width, self.viewRibbon.frame.size.height);

        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, sWidth, sHeight-199);
        self.viewRibbon.frame = CGRectMake(0, 50+self.offset, sWidth, self.viewRibbon.frame.size.height);
        self.viewRibbon.hidden = NO;

        colorDotsView.numberOfPages = 0;

        UIColor *tempColor;

        for (NSUInteger i=0; i<[availableColors count]; i++) {

            colorDotsView.numberOfPages++;

            if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:1]]){
                tempColor = [UIColor yellowColor];
            }
            else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:2]]){
                tempColor = [UIColor grayColor];
            }
            else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:3]]){
                tempColor = [UIColor blueColor];
            }
            else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:4]]){
                tempColor = [UIColor cyanColor];
            }
            else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:5]]){
                tempColor = [UIColor magentaColor];
            }

            if(i==0){
                colorDotsView.color1 = tempColor;
            }
            else if(i==1){
                colorDotsView.color2 = tempColor;
            }
            else if(i==2){
                colorDotsView.color3 = tempColor;
            }
            else if(i==3){
                colorDotsView.color4 = tempColor;
            }
            else if(i==4){
                colorDotsView.color5 = tempColor;
            }
        }

        colorDotsView.currentDot = currentIndex;

	}

    //set gesture recognizer for right/left swipe if more than 1 group is available
    if(colorDotsView.numberOfPages > 1){
        UISwipeGestureRecognizer *recognizer;

        recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightSwipeGesture:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
        [[self view] addGestureRecognizer:recognizer];
        [recognizer release];

        recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftSwipeGesture:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
        [[self view] addGestureRecognizer:recognizer];
        [recognizer release];
    }

	self.screenName = @"Overview Activities";
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self buildScrollview];
    [[ModelLocator instance] addObserver:self forKeyPath:k_DATA_SCENELIST_HAS_CHANGED options:0 context:nil];

    //start polling of the last called scenes
    [[DataController instance] startScenePolling:room];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_SCENELIST_HAS_CHANGED];
    }
    @catch (NSException *exception) {
        //just in case
    }

    //end polling of the last called scenes
    [[DataController instance] endScenePolling];

}


- (DSRoom*) currentRoomShowing{
    if(room)
        return room;
    else
        return nil;
}

- (void) buildScrollview {

    // empty scrollview before inserting
	for (UIViewController *item in scrollviewItems) {
		[item.view removeFromSuperview];
        // [(RoomSceneListItemVC*)item prepareDealloc];
		[item release];
	}

	[scrollviewItems removeAllObjects];

    //return if no available colors
    if ([availableColors count] == 0){
        return;
    }

    colorDotsView.currentDot = currentIndex;

    // fill scrollview with scenes
    int yOffset = 17;

    //	NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"sceneNo" ascending:YES];
    //	NSArray *sortedScenes = [room.scenes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
    //    [sorter release];

    NSArray* sceneOrder = [NSArray arrayWithObjects:[NSNumber numberWithInt:0],
                         [NSNumber numberWithInt:5],
                         [NSNumber numberWithInt:17],
                         [NSNumber numberWithInt:18],
                         [NSNumber numberWithInt:19],
                         [NSNumber numberWithInt:6],
                         [NSNumber numberWithInt:7],
                         [NSNumber numberWithInt:8],
                         [NSNumber numberWithInt:9],
                         [NSNumber numberWithInt:33],
                         [NSNumber numberWithInt:20],
                         [NSNumber numberWithInt:21],
                         [NSNumber numberWithInt:22],
                         [NSNumber numberWithInt:35],
                         [NSNumber numberWithInt:23],
                         [NSNumber numberWithInt:24],
                         [NSNumber numberWithInt:25],
                         [NSNumber numberWithInt:37],
                         [NSNumber numberWithInt:26],
                         [NSNumber numberWithInt:27],
                         [NSNumber numberWithInt:28],
                         [NSNumber numberWithInt:39],
                         [NSNumber numberWithInt:29],
                         [NSNumber numberWithInt:30],
                         [NSNumber numberWithInt:31],
                         nil];

    NSMutableArray *sortedScenes = [[NSMutableArray alloc]init];

    for(NSNumber *sceneNumber in sceneOrder) {
        NSArray *roomsWithSceneNumber = [[room.scenes allObjects]filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sceneNo = %d",[sceneNumber integerValue]]];
        if(roomsWithSceneNumber && roomsWithSceneNumber.count > 0) {
            [sortedScenes addObjectsFromArray:roomsWithSceneNumber];
        }
    }

    //add the normal scenes
    for (id item in sortedScenes) {
        DSScene *scene = (DSScene*)item;
        if([scene.group intValue] == [[availableColors objectAtIndex:self.currentIndex] intValue]
            && ![scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]]
            && [scene.mainList isEqualToNumber:[NSNumber numberWithInt:1]]) {

            RoomSceneListItemVC *vc = [[RoomSceneListItemVC alloc] initWithScene:scene colorCode:[[availableColors objectAtIndex:self.currentIndex] intValue] doAnimation:animateItems];
            vc.view.frame = CGRectMake(0, yOffset, sWidth, vc.view.frame.size.height);
            [scrollView addSubview:vc.view];
            [scrollviewItems addObject:vc];
            yOffset += 52;
        }
    }

    //add the off scene at the end
    for (id item in sortedScenes) {
        DSScene *scene = (DSScene*)item;
        if([scene.group intValue] == [[availableColors objectAtIndex:self.currentIndex] intValue] && [scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]]) {
            RoomSceneListItemVC *vc = [[RoomSceneListItemVC alloc] initWithScene:scene colorCode:[[availableColors objectAtIndex:self.currentIndex] intValue] doAnimation:animateItems];
            GSLog(@"%f", sWidth);
            vc.view.frame = CGRectMake(0, yOffset, sWidth, vc.view.frame.size.height);
            [scrollView addSubview:vc.view];
            [scrollviewItems addObject:vc];
            yOffset += 52;
        }
    }

    self.scrollView.contentSize = CGSizeMake(270, yOffset+3);
    self.scrollView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if([keyPath isEqualToString:k_DATA_SCENELIST_HAS_CHANGED]) {
		if ([[[ModelLocator instance] valueForKey:k_DATA_SCENELIST_HAS_CHANGED] boolValue]) {
			[self buildScrollview];
		}
	}
}

- (void) animateStartup {
    [UIView animateWithDuration:0.3 delay:0.25 options:0 animations:^{
        self.viewRibbon.frame = CGRectMake(0, 50, self.viewRibbon.frame.size.width, self.viewRibbon.frame.size.height);
        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94, self.scrollView.frame.size.width, sHeight-199);
    } completion:nil];
}

- (void) animateShutdown {
	NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sorted = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [orderDescriptor release];
    [nameDescriptor release];

	if ([sorted indexOfObject:room] == 0) {
		[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] animateBackFromRoomScenes:room];
	}
	else {
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 50000000), dispatch_get_main_queue(), ^{
			[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] animateBackFromRoomScenes:room];
		});
	}

    GSLog(@"sWidth %f, scrollView.width %f", sWidth, self.scrollView.frame.size.width);
    
	[UIView animateWithDuration:0.3 animations:^{
        // self.viewRibbon.frame = CGRectMake(0, 50+offset, self.viewRibbon.frame.size.width, self.viewRibbon.frame.size.height);
        // self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, sWidth, sHeight-199);
        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
        self.viewRibbon.frame = CGRectMake(0, 50+offset, sWidth, self.viewRibbon.frame.size.height);
	} completion:^(BOOL finished) {

	}];
}

- (void) storeInML {
	[[ModelLocator instance] setValue:self forKey:k_VIEW_ROOM_SCENELIST];
}

- (void) handleLeftSwipeGesture:(UISwipeGestureRecognizer *)recognizer {
    if(colorDotsView.currentDot != colorDotsView.numberOfPages-1){
        colorDotsView.currentDot++;
        self.currentIndex++;
        animateItems = YES;
        [self buildScrollview];
        animateItems = NO;
        NSLog(@"Left swipe handled");
    }
}

- (void) handleRightSwipeGesture:(UISwipeGestureRecognizer *)recognizer {
    if(colorDotsView.currentDot != 0){
        colorDotsView.currentDot--;
        self.currentIndex--;
        animateItems = YES;
        [self buildScrollview];
        animateItems = NO;
        NSLog(@"Right swipe handled");
    }
}



#pragma mark -
#pragma mark IB Outlets

- (IBAction) buttonActionBackward: (id) sender {
	[self animateShutdown];
}

- (IBAction) buttonActionForward: (id) sender {
	[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] animateFromScenesToRoomDevices:room withIndex:self.currentIndex];
}

#pragma mark -
#pragma mark tear down

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) prepareDealloc {
	//[[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_SCENELIST_HAS_CHANGED];
}

- (void)dealloc {
    [super dealloc];
}


@end
