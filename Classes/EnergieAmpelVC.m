//
//  EnergieAmpelVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "EnergieAmpelVC.h"
#import "ModelLocator.h"
#import "DataController.h"

@implementation EnergieAmpelVC

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	if (![[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] || ([[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] && [[[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] isEqualToString:@"0"])) {
		[self _showStandBy];
	}
	else if ([[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] && [[[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] isEqualToString:@"1"]) {
		[self _showValue: [self _calculateValue]];
	}
	
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[ModelLocator instance] addObserver:self forKeyPath:k_INFO_TOTAL_CONSUMPTION options:0 context:nil];
	[[ModelLocator instance] addObserver:self forKeyPath:k_DSS_AVAILABLE options:0 context:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_INFO_TOTAL_CONSUMPTION];
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DSS_AVAILABLE];
    }
    @catch (NSException *exception) {
        //just in case
    }

}


- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_INFO_TOTAL_CONSUMPTION]) {
		if ([[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]) {
			NSNumber *consumption = [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION];
            //if consumption is -1, we do not have a connection yet. 
            if([consumption integerValue] > -1){
                [self _showValue: [self _calculateValue]];
            }
            else{
                [self _showStandBy];
            }
		}
		else {
			[self _showStandBy];
		}
	}
	else if ([keyPath isEqualToString:k_DSS_AVAILABLE]) {
		if (![[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] || ([[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] && [[[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] isEqualToString:@"0"])) {
			[self _showStandBy];
		}
		else if ([[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] && [[[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] isEqualToString:@"1"]) {
			[self _showValue: [self _calculateValue]];
		}
	}
}


- (float) _calculateValue {
	NSNumber *consumption = [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION];
	
	if (!consumption) {
		consumption = [NSNumber numberWithInt:0];
	}
	
	int numberOfDevices = 0;
	for (DSRoom *room in [DataController instance].rooms) {
		numberOfDevices += [room.devices count];
	}
	
	float maxPowerPerDevice = 50.0;
	float value;
	
	if ([[ModelLocator instance] valueForKey:k_DATA_ENERGY_MAX_VALUE]) {
		float max = [[[ModelLocator instance] valueForKey:k_DATA_ENERGY_MAX_VALUE] floatValue];
		
		if (max>0) {
			value = (float)[consumption floatValue]/max;
		}
		else {
			value = (float)[consumption floatValue]/(numberOfDevices*maxPowerPerDevice);
		}
		
		
	}
	else {
		value = (float)[consumption floatValue]/(numberOfDevices*maxPowerPerDevice);
	}
	
	
	
	if (value > 1) {
		value = 1.0;
	}
	else if(value < 0) {
		value = 0;
	}
	
	if (!isnan(value)) {
		return value;
	}
	else {
		return 0.0;
	}

}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void) storeInML {
	[[ModelLocator instance] setValue:self forKey:k_VIEW_ENERGIE_AMPEL];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
    [super dealloc];
}

-(void) startWabern {
	waberTimer = [NSTimer scheduledTimerWithTimeInterval: 3 target:self selector:@selector(onTick:) userInfo:nil repeats: YES];
}

-(void) stopWabern {
	[waberTimer invalidate];
}

-(void) onTick: (NSTimer *) theTimer {
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:2.0];
	if (ampelFarben.alpha > 0.7){
		ampelFarben.alpha = 0.4;
	}else {
		ampelFarben.alpha = 1;
	}
	[UIView commitAnimations];
}

-(void) showValue: (float)_value {
	return;
}

-(void) _showValue: (float)_value {
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0];
	ampelFarben.frame = CGRectMake(0, -87*_value, ampelFarben.frame.size.width, ampelFarben.frame.size.height);
	[UIView commitAnimations];
}

-(void) showStandBy{
	return;
}
-(void) _showStandBy{
	ampelFarben.frame = CGRectMake(0, -94, ampelFarben.frame.size.width, ampelFarben.frame.size.height);
}


@end
