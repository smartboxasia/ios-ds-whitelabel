//
//  SettingsRoomsListVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsRoomsListVC.h"
#import "SettingsRoomsListItemVC.h"
#import "DataController.h"
#import "DSRoom.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "SettingsOverviewVC.h"
#import "UIBlockButton.h"
#import "CustomLabel.h"


@implementation SettingsRoomsListVC

@synthesize scrollview;
@synthesize background;

- (void)viewDidLoad {
    [super viewDidLoad];
	
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }
    
    titleLabel.text = NSLocalizedString(@"key_rooms_title", nil);
    
	editmodeOn = NO;
	items =[NSMutableArray new];
	
	[self buildScrollview];
	
	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_ROOMS_LIST];
	//[[ModelLocator instance] addObserver:self forKeyPath:k_DATA_ROOMLIST_HAS_CHANGED options:0 context:nil];
	
    [self displayEditButton];
    
    self.screenName = @"Settings Rooms";
}

//- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//	if ([keyPath isEqualToString:k_DATA_ROOMLIST_HAS_CHANGED]) {
//		[self buildScrollview];
//	}
//}

- (void) buildScrollview {
	for (UIViewController *item in items) {
		[item.view removeFromSuperview];
		[item release];
	}
	[items removeAllObjects];
	

	NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sorted = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [orderDescriptor release];
    [nameDescriptor release];
	
	NSInteger yOffset = 0;
	
	for (DSRoom* room in sorted) {
		
		SettingsRoomsListItemVC *item = [[SettingsRoomsListItemVC alloc] initWithRoom:room andOffset:yOffset andMaximum:[sorted count] gointToScenes:NO isLookingForFavorite:NO gointToDevices:NO];
		item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
		[self.scrollview addSubview:item.view];
		
		yOffset += item.view.frame.size.height;
		[items addObject:item];
	}
	
	self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, yOffset);
}

- (void) displayEditButton {
	// Bearbeiten Button einblenden
	UIBlockButton *buttonBearbeiten = (UIBlockButton*)((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonBearbeiten;
	buttonBearbeiten.hidden = NO;
	
	[UIView animateWithDuration:0.2 animations:^{
		buttonBearbeiten.alpha = 1;
	}];
	
	// Action für Bearbeiten-Button hinzufügen
	[buttonBearbeiten handleControlEvent:UIControlEventTouchUpInside withBlock:^{
		[self toggleEditmode];
	}];
}

- (void) hideEditButton {
	UIBlockButton *buttonBearbeiten = (UIBlockButton*)((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonBearbeiten;
	[buttonBearbeiten removeControlEvent:UIControlEventTouchUpInside];
	buttonBearbeiten.selected = NO;
	
	[UIView animateWithDuration:0.2 animations:^{
		buttonBearbeiten.alpha = 0;
	} completion:^(BOOL finished) {
		buttonBearbeiten.hidden = YES;
	}];
	
}

- (IBAction) actionBack {
	[self hideEditButton];
	
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
}

#pragma mark -
#pragma mark edit mode functions

- (void) toggleEditmode {
	if (!editmodeOn) {
		editmodeOn = YES;
		
	}
	else {
		editmodeOn = NO;
		[[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_ROOMLIST_HAS_CHANGED];
	}
	
	UIBlockButton *buttonBearbeiten = (UIBlockButton*)((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonBearbeiten;
	buttonBearbeiten.selected = editmodeOn;
	
	self.scrollview.scrollEnabled = !editmodeOn;
	
	for (SettingsRoomsListItemVC *item in items) {
		[item toggleEditmode];
	}
	
	
}

- (void) magneticPointHasChanged: (int) from toIndex: (int) to {
	if (to >= [items count] || to < 0 || from < 0 || from >= [items count]) {
		return;
	}
	SettingsRoomsListItemVC *itemHasMoved = (SettingsRoomsListItemVC*)[items objectAtIndex:from];
	SettingsRoomsListItemVC *itemToBeMoved = (SettingsRoomsListItemVC*)[items objectAtIndex:to];
	if (to>from) {
		[itemToBeMoved decrementMagneticPoint];
	}
	else {
		[itemToBeMoved incrementMagneticPoint];
	}
	
	[items removeObject:itemHasMoved];
	[items insertObject:itemHasMoved atIndex:to];
}

- (void) arrangeItems {
	for (int i = 0; i < [items count]; i++) {
		SettingsRoomsListItemVC *item = [items objectAtIndex:i];
		[item gotoPosition:i];
	}
	
	[[DataController instance] saveContext];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	//[[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_ROOMLIST_HAS_CHANGED];
    [titleLabel release];
    [super dealloc];
}


@end
