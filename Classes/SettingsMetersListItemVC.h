//
//  SettingsMetersListItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSMeter.h"

@interface SettingsMetersListItemVC : UIViewController {
	IBOutlet UILabel *labelName;
	IBOutlet UIImageView *imageBackground;
	IBOutlet UIButton *buttonStepInto;
	
	DSMeter *meter;
	BOOL hasDarkBackground;
	BOOL editModeOn;
	NSInteger index;
}

@property (nonatomic, retain) UIImageView *imageBackground;
@property (nonatomic, retain) UILabel *labelName;
@property (nonatomic, retain) UIButton *buttonStepInto;

- (IBAction) actionButtonClicked;
- (id) initWithMeter: (DSMeter*) theMeter withDarkBg: (BOOL) isDark andIndex: (NSInteger) _index;

@end
