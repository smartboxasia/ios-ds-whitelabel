//
//  SplashViewVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SplashViewVC.h"
#import "StartupController.h"
#import "ModelLocator.h"
#import "MainVC.h"
#import "StartupServerSelectionVC.h"
#import "NSTimer+blocks.h"
#import "DataController.h"
#import "FacilityController.h"
#import "WelcomeDefaultVC.h"
#import "DSServer.h"

@implementation SplashViewVC

@synthesize labelStatus;
@synthesize dsButton;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    labelStatus.text = NSLocalizedString(@"key_searching_server", nil);
	
//    //iphone 5 support
//    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        [self.view setFrame:CGRectMake(0, 0, 320, 568)];
//		self.imageBackground.image = [UIImage imageNamed:@"SplashBG-568h.png"];
//    }

    // fix stupid hardcode height and width
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat screenWidth  = [UIScreen mainScreen].bounds.size.width;
    [self.view setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];

	
	[[ModelLocator instance] setValue:self forKey:k_VIEW_SPLASH];
    
    //setup digitalstrom.com login button
    UIImage *buttonImage = [[UIImage imageNamed:@"greyButton.png"]resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *buttonImageHighlight = [[UIImage imageNamed:@"greyButtonHighlight.png"]resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    [dsButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [dsButton setBackgroundImage:buttonImageHighlight forState:UIControlStateHighlighted];

    //TODO:set localized button text
    [[self.dsButton titleLabel] setFont:[UIFont fontWithName:@"PFBeauSansPro-Regular" size:[self.dsButton titleLabel] .font.pointSize]];
    
    self.dsButton.alpha = 0.0;
    self.dsButton.enabled = NO;
    
//    //offset fix because of weird font issue
//    [[self.dsButton titleLabel] setFrame:CGRectMake([self.dsButton titleLabel].frame.origin.x, [self.dsButton titleLabel].frame.origin.y+2, [self.dsButton titleLabel].frame.size.width, [self.dsButton titleLabel].frame.size.height)];
	
	/*
	[NSTimer scheduledTimerWithTimeInterval:3 repeats:NO usingBlock:^(NSTimer *timer) {
        //we don't want to automatically connect to just a single server, since we want to allow for logging in to digitalstrom.com
			
			StartupServerSelectionVC *serverSelection = [StartupServerSelectionVC new];
			serverSelection.view.frame = CGRectMake((self.view.frame.size.width/2)-serverSelection.view.frame.size.width/2, self.view.frame.size.height-270, serverSelection.view.frame.size.width, serverSelection.view.frame.size.height);
			serverSelection.view.alpha = 0;
            self.dsButton.alpha = 0.0;
			[self.view addSubview:serverSelection.view];
			
			[UIView animateWithDuration:0.4 animations:^{
				serverSelection.view.alpha = 1;
                self.dsButton.alpha = 1;
                self.dsButton.enabled = YES;
			}];
			
    }];*/
	
}


- (IBAction) dsButtonClick: (id) sender{
    
    [self displayCredentialsAlertView:NO];
    
}

#pragma  mark - alert view

- (void) displayCredentialsAlertView:(BOOL)wrongPass {
    NSString *title;
    if(!wrongPass){
        title = NSLocalizedString(@"key_credentials_title", nil);
    }
    else{
        title = NSLocalizedString(@"key_credentials_wrong_pass_title", nil);
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"key_cancel",nil)
                                              otherButtonTitles:NSLocalizedString(@"key_OK",nil), nil];
    
    alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    
    alertView.tag = 100;
    [alertView show];
    [alertView release];
}


- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    //login alert view
    if(alertView.tag == 100){
        if(buttonIndex == 1){
            UITextField *userNameField = [alertView textFieldAtIndex:0];
            UITextField *passwordField = [alertView textFieldAtIndex:1];
            
            if([[StartupController instance]performDigitalstromLoginWith:userNameField.text andPassword:passwordField.text]){
                
            }
            else{
                [self displayCredentialsAlertView:YES];
            }
        }
        else {
            NSLog(@"Manual input: cancelled alertview");
        }
    }
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {	
    [super dealloc];
}


@end
