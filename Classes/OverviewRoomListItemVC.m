//
//  OverviewRoomListItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "OverviewRoomListItemVC.h"
#import "DSRoom.h"
#import "FacilityController.h"
#import "ModelLocator.h"
#import "MainVC.h"
#import "DataController.h"

@implementation OverviewRoomListItemVC

@synthesize buttonRoomLabel;

#pragma mark -
#pragma mark tear up methods

- (id) initWithRoom: (DSRoom*) theRoom {
	if (self = [super init]) {
		room = theRoom;
	}
	
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    	[room addObserver:self forKeyPath:@"name" options:0 context:nil];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.buttonRoomLabel setTitle:[room.name uppercaseString] forState:UIControlStateNormal];
	

}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];

}


- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"name"]) {
		[self.buttonRoomLabel setTitle:[room.name uppercaseString] forState:UIControlStateNormal];
	}
}

- (void) releaseObservation {

}

#pragma mark -
#pragma mark IB Actions

//- (IBAction) actionButtonRoomLabel: (id) sender {
//    //currently not used
//	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//		[[DataController instance] callStandardScene:k_SERVICE_SCENE_STIMMUNG1 withRoom:room];
//	});
//	
//	
//	/*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//		if ([room.identifier isEqualToString:@"0"]) {
//			[[FacilityController instance] setPowerForDSDevice:YES withDeviceId:@"3504175fe000000000004428"];
//		}
//		else if ([room.identifier isEqualToString:@"1"]) {
//			[[FacilityController instance] setPowerForDSDevice:YES withDeviceId:@"3504175fe00000000000444e"];
//		}
//	});*/
//}

- (IBAction) actionButtonRoomArrow: (id) sender {
	//return;
	GSLog(@"log");

	[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] animateFromOverviewToRoomScenes:room];

	//[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] animateFromOverviewToRoomDevices:room];
	/*
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		if ([room.identifier isEqualToString:@"0"]) {
			[[FacilityController instance] setPowerForDSDevice:NO withDeviceId:@"3504175fe000000000004428"];
		}
		else if ([room.identifier isEqualToString:@"1"]) {
			[[FacilityController instance] setPowerForDSDevice:NO withDeviceId:@"3504175fe00000000000444e"];
		}
	});	
	 */
}

#pragma mark -
#pragma mark tear down methods

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) prepareDealloc {
	room = nil;
}

- (void)dealloc {
    //moved from prepareDealloc
    [super dealloc];
    @try {
        [room removeObserver:self forKeyPath:@"name"];
    }
    @catch (NSException *exception) {
        //just in case
    }

}


@end
