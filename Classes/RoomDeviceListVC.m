//
//  RoomDeviceListVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "RoomDeviceListVC.h"
#import "ModelLocator.h"
#import "DSRoom.h"
#import "MainVC.h"
#import "DataController.h"
#import "RoomDeviceListItemVC.h"
#import "ColorDotsView.h"

@interface RoomDeviceListVC()
@property (nonatomic, retain) IBOutlet ColorDotsView *colorDotsView;
@end

@implementation RoomDeviceListVC

@synthesize scrollView, labelTitle;
@synthesize viewRibbon;
@synthesize cameFromOverview;
@synthesize offset;
@synthesize colorDotsView, currentIndex, animateItems;

#pragma mark -
#pragma mark tear up

- (id) initWithRoom: (DSRoom*) theRoom andOffset: (float) theOffset withIndex:(int)index{
	if (self = [super init]) {
		room = theRoom;
		self.offset = theOffset;

        availableColors= [[NSMutableArray alloc] initWithCapacity:0];

        if (room.group1present)
            [availableColors addObject:[NSNumber numberWithInt:1]];
        if (room.group2present)
            [availableColors addObject:[NSNumber numberWithInt:2]];
        if (room.group3present)
            [availableColors addObject:[NSNumber numberWithInt:3]];
        if (room.group4present)
            [availableColors addObject:[NSNumber numberWithInt:4]];
        if (room.group5present)
            [availableColors addObject:[NSNumber numberWithInt:5]];

        self.currentIndex = index;
	}

	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    GSLog(@"log");

    sHeight = [UIScreen mainScreen].bounds.size.height;
    sWidth = [UIScreen mainScreen].bounds.size.width;
    
	if (room) {
        self.labelTitle.text = room.name;
        // self.viewRibbon.frame = CGRectMake(0, 50+self.offset, self.viewRibbon.frame.size.width, self.viewRibbon.frame.size.height);
        self.viewRibbon.frame = CGRectMake(0, 50+self.offset, sWidth, self.viewRibbon.frame.size.height);

        // iphone 5 support.
        // self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, self.scrollView.frame.size.width, sHeight-199);
        // self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, sWidth, sHeight-199);
        GSLog(@"offset %f, self.offset %f", offset, self.offset);
        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, sWidth, self.scrollView.frame.size.height-199);

        self.viewRibbon.hidden = NO;
        colorDotsView.numberOfPages = 0;

        UIColor *tempColor;

        for (NSUInteger i=0; i<[availableColors count]; i++) {

            colorDotsView.numberOfPages++;

            if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:1]]){
                tempColor = [UIColor yellowColor];
            }
            else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:2]]){
                tempColor = [UIColor grayColor];
            }
            else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:3]]){
                tempColor = [UIColor blueColor];
            }
            else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:4]]){
                tempColor = [UIColor cyanColor];
            }
            else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:5]]){
                tempColor = [UIColor magentaColor];
            }

            if(i==0){
                colorDotsView.color1 = tempColor;
            }
            else if(i==1){
                colorDotsView.color2 = tempColor;
            }
            else if(i==2){
                colorDotsView.color3 = tempColor;
            }
            else if(i==3){
                colorDotsView.color4 = tempColor;
            }
            else if(i==4){
                colorDotsView.color5 = tempColor;
            }
        }

        colorDotsView.currentDot = currentIndex;

        //set gesture recognizer for right/left swipe if more than 1 group is available
        if(colorDotsView.numberOfPages > 1){
            UISwipeGestureRecognizer *recognizer;

            recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightSwipeGesture:)];
            [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
            [[self view] addGestureRecognizer:recognizer];
            [recognizer release];

            recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftSwipeGesture:)];
            [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
            [[self view] addGestureRecognizer:recognizer];
            [recognizer release];
        }

	}

    self.screenName = @"Overview Devices";
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // fill scrollview with devices
    [self buildScrollView];
    [[ModelLocator instance] addObserver:self forKeyPath:k_DATA_STRUCTURE_HAS_CHANGED options:0 context:nil];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_STRUCTURE_HAS_CHANGED];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

- (DSRoom*) currentRoomShowing {
    if(room)
        return room;
    else
        return nil;
}

- (void) buildScrollView {

    // empty scrollview before inserting
    NSArray *viewsToRemove = [scrollView subviews];
    for (UIView *v in viewsToRemove)
        [v removeFromSuperview];

    // return if no available colors
    if ([availableColors count] == 0){
        return;
    }

    colorDotsView.currentDot = currentIndex;

    // fill scrollview with devices
    int yOffset = 17;

    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortedDevices =  [room.devices sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
    [sorter release];

    for (id item in sortedDevices) {
        DSDevice *device = (DSDevice*)item;

        //do not show devices with outputmode 0
        if ([device.group isEqualToString:[NSString stringWithFormat:@"%i", [[availableColors objectAtIndex:self.currentIndex] intValue]]] && ![device.outputMode isEqualToNumber:[NSNumber numberWithInt:0]]) {

          RoomDeviceListItemVC *vc = [[RoomDeviceListItemVC alloc] initWithDevice:device colorCode:[[availableColors objectAtIndex:self.currentIndex] intValue] doAnimation:animateItems];
          // vc.view.frame = CGRectMake(0, yOffset, vc.view.frame.size.width, vc.view.frame.size.height);
          vc.view.frame = CGRectMake(0, yOffset, sWidth, vc.view.frame.size.height);
          [scrollView addSubview:vc.view];

          yOffset += 52;
        }
    }

    self.scrollView.contentSize = CGSizeMake(270, yOffset+3);
    self.scrollView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
}

- (void) animateStartup {
    GSLog(@"log");

	[UIView animateWithDuration:0.3 delay:0.25 options:0 animations:^{
		self.viewRibbon.frame = CGRectMake(0, 50, self.viewRibbon.frame.size.width, self.viewRibbon.frame.size.height);

        // iphone 5 support.
        // self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94, self.scrollView.frame.size.width, sHeight-199);
        // self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94, sWidth, sHeight-199);
        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94, sWidth, self.scrollView.frame.size.height-199);

	} completion:nil];
}

- (void) animateShutdown {
    GSLog(@"log");

	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 50000000), dispatch_get_main_queue(), ^{

        GSLog(@"cameFromOverview: %d", self.cameFromOverview);
        
        if (self.cameFromOverview) {
            [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] animateBackFromRoomDevices:room];
        }
        else {
            [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] animateBackFromRoomDevicesToScenes:room withIndex:self.currentIndex];
        }
	});

	[UIView animateWithDuration:0.3 animations:^{
		self.viewRibbon.frame = CGRectMake(0, 50+offset, self.viewRibbon.frame.size.width, self.viewRibbon.frame.size.height);

        // //iphone 5 support.
        // self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, self.scrollView.frame.size.width, sHeight-199);
        // self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, sWidth, sHeight-199);
        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, 94+offset, sWidth, self.scrollView.frame.size.height-199);
	} completion:^(BOOL finished) {

	}];
}

- (void) storeInML {
	[[ModelLocator instance] setValue:self forKey:k_VIEW_ROOM_DEVICELIST];
}

- (void) handleLeftSwipeGesture:(UISwipeGestureRecognizer *)recognizer {
    if(colorDotsView.currentDot != colorDotsView.numberOfPages-1){
        colorDotsView.currentDot++;
        self.currentIndex++;
        animateItems = YES;
        [self buildScrollView];
        animateItems = NO;
        NSLog(@"Left swipe handled");
    }
}

- (void) handleRightSwipeGesture:(UISwipeGestureRecognizer *)recognizer {
    if(colorDotsView.currentDot != 0){
        colorDotsView.currentDot--;
        self.currentIndex--;
        animateItems = YES;
        [self buildScrollView];
        animateItems = NO;
        NSLog(@"Right swipe handled");
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_DATA_STRUCTURE_HAS_CHANGED]) {
		if ([[[ModelLocator instance] valueForKey:k_DATA_STRUCTURE_HAS_CHANGED] isEqualToString:@"1"]) {
			[self buildScrollView];
		}
	}
}


#pragma mark -
#pragma mark IB Outlets

- (IBAction) buttonActionBackward: (id) sender {
	[self animateShutdown];
}

#pragma mark -
#pragma mark tear down

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}



@end
