//
//  DSServer.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "DSServer.h"
#import "DSScene.h"
#import "DSRoom.h"
#import "DSMeter.h"

@implementation DSServer

@dynamic macAddress;
@dynamic name;
@dynamic ip;
@dynamic version;
@dynamic lastConnectionTS;
@dynamic meters;
@dynamic rooms;
@dynamic scenes;
@dynamic appToken;
@dynamic cloudEmail;
@dynamic connectionType;
@dynamic port;
@dynamic connectionName;
@dynamic cloudUrl;

@synthesize cloudPassword;
@dynamic connected;
@dynamic detectedByBonjour;



- (id) init {
	self = [super init];
	
	if (self) {
		allObserver = [NSMutableArray new];
	}
	
	return self;
}

+ (NSInteger) getPortFromUrl: (NSString *) urlString {
	NSArray *comps = [urlString componentsSeparatedByString:@":"];
	
	if (comps) {
		return [[comps objectAtIndex:comps.count-1] integerValue];
	}
	
	return 8080;
}

- (BOOL) isRemoteDss {
	if (self.connectionType && [self.connectionType isEqualToString:DSS_CONNECTION_TYPE_CLOUD]) {
		return YES;
	}
	
	return NO;
}

- (DSRoom *) getRoom: (NSString*) identifier {
	for (DSRoom *room in self.rooms) {
		if ([room.identifier isEqualToString:identifier]) {
			return room;
		}
	}
	
	return nil;
}

- (DSMeter *) getMeter: (NSString*) identifier {
	for (DSMeter *meter in self.meters) {
		if ([meter.identifier isEqualToString:identifier]) {
			return meter;
		}
	}
	
	return nil;
}

- (DSScene *) getScene: (NSNumber*)sceneNo inRoom: (DSRoom*)room withGroup: (NSNumber*)group {
	if (!sceneNo) {
		return nil;
	}
	if (room) {
		for (DSScene *scene in room.scenes) {
			if ([scene.sceneNo isEqual:sceneNo] && [scene.group isEqual:group]) {
				return scene;
			}
		}
	}
	else {
		for (DSScene *scene in self.scenes) {
			if (scene.room) {
				continue;
			}
			if ([scene.sceneNo isEqual:sceneNo] && [scene.group isEqual:group]) {
				return scene;
			}
		}
	}

    return nil;
}

- (DSScene *) getHighLevelEvent: (NSNumber*) withId {
    
    for (DSScene *scene in self.scenes) {
        if (scene.room) {
            continue;
        }
        if ([scene.sceneNo isEqualToNumber:withId]) {
            return scene;
        }
    }

    return nil;
}

- (void) addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context {
	if (!allObserver) {
		allObserver = [NSMutableArray new];
	}
	[super addObserver:observer forKeyPath:keyPath options:options context:context];
	
	NSMutableDictionary *d = [NSMutableDictionary new];
	[d setObject:observer forKey:keyPath];
	[allObserver addObject:d];
}

- (void) removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath {
	for (NSDictionary *dict in allObserver) {
		for (NSString *myKeypath in [dict allKeys]) {
			id theObserver = [dict objectForKey:myKeypath];
			
			if (observer == theObserver && [keyPath isEqualToString:myKeypath]) {
				[allObserver removeObject:dict];
                @try {
                    [super removeObserver:observer forKeyPath:keyPath context:nil];
                }
                @catch (NSException *exception) {
                    //just in case
                }
				
				return;
			}
		}
	}
	
	//[super removeObserver:observer forKeyPath:keyPath context:nil];
}

- (void)prepareForDeletion {
	while ([allObserver count] > 0) {
		NSDictionary *dict = [allObserver objectAtIndex:0];
		NSString *keypath = [[dict allKeys] objectAtIndex:0];
		id observer = [dict objectForKey:keypath];
		
		//if ([observer respondsToSelector:@selector(releaseObservation)]) {
        @try {
            [self removeObserver:observer forKeyPath:keypath];
        }
        @catch (NSException *exception) {
            //just in case
        }
        //[observer releaseObservation];
		//}
	}
	
	[super prepareForDeletion];
}

- (void)willTurnIntoFault {
	for (NSDictionary *dict in allObserver) {
		NSString *keypath = [[dict allKeys] objectAtIndex:0];
		id observer = [dict objectForKey:keypath];
		
		//if ([observer respondsToSelector:@selector(releaseObservation)]) {
        @try {
            [self removeObserver:observer forKeyPath:keypath];
        }
        @catch (NSException *exception) {
            //just in case
        }
		//[observer releaseObservation];
		//}
	}
	
	[super willTurnIntoFault];
}

@end
