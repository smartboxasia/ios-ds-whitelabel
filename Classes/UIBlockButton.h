//
//  UIBlockButton.h
//  touch-n-living
//
//  Created by Mathias on 18.02.11.
//  Copyright 2011 Granny&Smith Technikagentur GmbH & Co. KG. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ActionBlock)();

@interface UIBlockButton : UIButton {
	ActionBlock _actionBlock;
}

- (void) handleControlEvent:(UIControlEvents)event withBlock:(ActionBlock) action;
- (void) removeControlEvent:(UIControlEvents)event;

@end
