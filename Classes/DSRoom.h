//
//  DSRoom.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <CoreData/CoreData.h>

#define CONNECTION_TYPE_LOCAL @"CONNECTION_TYPE_LOCAL"
#define CONNECTION_TYPE_REMOTE @"CONNECTION_TYPE_REMOTE"

@class DSDevice;
@class DSScene;
@class DSServer;
@class DSMeter;

@interface DSRoom :  NSManagedObject  
{
	NSMutableArray *allObserver;
	BOOL dirty;
}

@property (nonatomic, assign) BOOL dirty;
@property (nonatomic, retain) NSString * lastCalledShadeScene;
@property (nonatomic, retain) NSString * lastCalledScene;
@property (nonatomic, retain) NSString * lastCalledBlueScene;
@property (nonatomic, retain) NSString * lastCalledAudioScene;
@property (nonatomic, retain) NSString * lastCalledVideoScene;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * meterID;
@property (nonatomic, retain) NSSet* devices;
@property (nonatomic, retain) NSSet* scenes;
@property (nonatomic, retain) NSNumber * sortOrder;
@property (nonatomic, retain) DSServer *dss;
@property (nonatomic, retain) DSMeter *meter;

@property (nonatomic, assign) BOOL group1present;
@property (nonatomic, assign) BOOL group2present;
@property (nonatomic, assign) BOOL group3present;
@property (nonatomic, assign) BOOL group4present;
@property (nonatomic, assign) BOOL group5present;

- (DSScene *) getScene: (NSNumber*)sceneNo withGroup: (NSNumber*)group;
    
@end


@interface DSRoom (CoreDataGeneratedAccessors)
- (void)addDevicesObject:(DSDevice *)value;
- (void)removeDevicesObject:(DSDevice *)value;
- (void)addDevices:(NSSet *)value;
- (void)removeDevices:(NSSet *)value;

- (void)addScenesObject:(DSScene *)value;
- (void)removeScenesObject:(DSScene *)value;
- (void)addScenes:(NSSet *)value;
- (void)removeScenes:(NSSet *)value;

@end

