//
//  SettingsDeviceDetailVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomSwitch.h"
#import "CustomTextField.h"
#import "CustomLabel.h"
#import "DSServer.h"
#import "GAI.h"

@class DSDevice;


@interface SettingsDeviceDetailVC : GAITrackedViewController <UITextFieldDelegate> {
	DSDevice *device;
	
	IBOutlet UILabel *labelNameTitle;
	IBOutlet UILabel *stromkreisID;
	IBOutlet UILabel *internID;
	IBOutlet UILabel *dimmerValue;
    IBOutlet UILabel *consumptionValue;
    IBOutlet UILabel *meteringValue;
	IBOutlet UIActivityIndicatorView *loadingWheelValue;
    IBOutlet UIActivityIndicatorView *loadingWheelConsumption;
    IBOutlet UIActivityIndicatorView *loadingWheelMetering;
	IBOutlet UILabel *labelRaumName;
	IBOutlet CustomTextField *nameField;
    IBOutlet UIButton *refreshButton;
    
    IBOutlet UIImageView *modeImage;
    IBOutlet UILabel *modeTitleLabel;
    IBOutlet UILabel *modeNameLabel;
    IBOutlet UIButton *modeButton;
    
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
    IBOutlet CustomLabel *label4;
    IBOutlet CustomLabel *label5;
    IBOutlet CustomLabel *label6;
}

@property (nonatomic, retain) IBOutlet UILabel *labelNameTitle;
@property (nonatomic, retain) IBOutlet UIImageView *background;

@property (nonatomic, retain) IBOutlet UILabel *labelMaxPower;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicatorMaxPower;
@property (nonatomic, retain) IBOutlet UIImageView *imageBackgroundMaxPower;

@property (nonatomic, retain) IBOutlet UILabel *labelSockettype;
@property (nonatomic, retain) IBOutlet UILabel *labelSockettypeContent;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicatorSockettype;
@property (nonatomic, retain) IBOutlet UIButton *buttonSockettype;
@property (nonatomic, retain) IBOutlet UIButton *buttonMaxPower;
@property (nonatomic, retain) IBOutlet UIImageView *imageBackgroundSockettype;

@property (nonatomic, retain) IBOutlet UIImageView *imageBackgroundMode;

@property (nonatomic, retain) IBOutlet UIButton *buttonDeviceInfo;
@property (nonatomic, retain) IBOutlet UIButton *buttonServiceInfo;
@property (nonatomic, retain) IBOutlet UIButton *buttonDeviceInfo2;
@property (nonatomic, retain) IBOutlet UIButton *buttonServiceInfo2;
@property (nonatomic, retain) IBOutlet UIButton *buttonIlluminantCheck;
@property (nonatomic, retain) IBOutlet UIButton *buttonSubmitIlluminant;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) IBOutlet UIView *viewFourButtonsBottom;
@property (nonatomic, retain) IBOutlet UIView *viewTwoButtonsBottom;

@property (nonatomic, retain) IBOutlet CustomTextField *maxPowerField;


- (id) initWithDevice: (DSDevice*) theDevice;
- (void) updateDeviceValues;

- (IBAction) actionBack;
- (IBAction) nameEdit:(id)sender;
- (IBAction) refreshDevice:(id)sender;
- (IBAction) changeRoom:(id)sender;
- (IBAction) changeMode:(id)sender;
- (IBAction) maxPowerEdit:(id)sender;

@end
