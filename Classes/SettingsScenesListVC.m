//
//  SettingsScenesListVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsScenesListVC.h"
#import "SettingsVC.h"
#import "DSRoom.h"
#import "DSServer.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "SettingsScenesListItemVC.h"
#import "CustomLabel.h"
#import "SettingsScenesDetailVC.h"

@implementation SettingsScenesListVC

@synthesize scrollview,segment,colorDotsView,showAll,background;

- (id) initWithRoom: (DSRoom*) theRoom isLookingForFavorite:(BOOL)favorite withIndex:(int)index{
	if(self = [super init]) {
		isLookingForFavorite = favorite;
        currentRoom = theRoom;
        self.showAll = NO;
        
        availableColors= [[NSMutableArray alloc] initWithCapacity:0];
        
        if (theRoom.group1present)
            [availableColors addObject:[NSNumber numberWithInt:1]];
        if (theRoom.group2present)
            [availableColors addObject:[NSNumber numberWithInt:2]];
        if (theRoom.group3present)
            [availableColors addObject:[NSNumber numberWithInt:3]];
        if (theRoom.group4present)
            [availableColors addObject:[NSNumber numberWithInt:4]];
        if (theRoom.group5present)
            [availableColors addObject:[NSNumber numberWithInt:5]];
        currentIndex = index;
        animateItems = NO;
        
        self.screenName = @"Settings Activities Select Room";
    }
	return self;
    
}
- (id) initWithName: (NSString*) theName isLookingForFavorite:(BOOL)favorite{
	if(self = [super init]) {
        isLookingForFavorite = favorite;
		currentRoom = nil;
        self.showAll = YES;
        
        availableColors= [[NSMutableArray alloc] initWithCapacity:0];
        currentIndex = 0;
        animateItems = NO;
        
        if([theName isEqualToString:NSLocalizedString(@"key_global_activities", @"")]) {
           isGlobalActivities = YES;
            self.screenName = @"Settings Activities Select Global";
        }
        else {
            isGlobalActivities = NO;
            self.screenName = @"Settings Activities Select UDA";
        }
        
    }
    
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        self.view.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_SCENES_LIST];
	
    childControllers = [NSMutableArray new];

    if(currentRoom)
        titleLabel.text = [NSString stringWithFormat:@"3. %@", currentRoom.name];
    else{
        if(isGlobalActivities)
            titleLabel.text = [NSString stringWithFormat:@"3. %@", NSLocalizedString(@"key_global_activities", @"")];
        else
            titleLabel.text = [NSString stringWithFormat:@"3. %@", NSLocalizedString(@"key_high_level_events", @"")]; 
    }


    colorDotsView.numberOfPages = 0;
    UIColor *tempColor;
    
    for (NSUInteger i=0; i<[availableColors count]; i++) {
        
        colorDotsView.numberOfPages++;
        
        if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:1]]){
            tempColor = [UIColor yellowColor];
        }
        else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:2]]){
            tempColor = [UIColor grayColor];
        }
        else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:3]]){
            tempColor = [UIColor blueColor];
        }
        else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:4]]){
            tempColor = [UIColor cyanColor];
        }
        else if([[availableColors objectAtIndex:i] isEqualToNumber: [NSNumber numberWithInt:5]]){
            tempColor = [UIColor magentaColor];
        }
        
        if(i==0){
            colorDotsView.color1 = tempColor;
        }
        else if(i==1){
            colorDotsView.color2 = tempColor;
        }
        else if(i==2){
            colorDotsView.color3 = tempColor;
        }
        else if(i==3){
            colorDotsView.color4 = tempColor;
        }
        else if(i==4){
            colorDotsView.color5 = tempColor;
        }
    }
    
    colorDotsView.currentDot = currentIndex;
    
    //hide segment control if not a room
    if(!currentRoom){
        self.segment.hidden = YES;
        [self.scrollview setFrame:CGRectMake(self.scrollview.frame.origin.x
                                             , 82,
                                             self.scrollview.frame.size.width,
                                             333)];
    }
    
	[self buildScrollView];
    
    //set gesture recognizer for right/left swipe if more than 1 group is available
    if(colorDotsView.numberOfPages > 1){
        UISwipeGestureRecognizer *recognizer;
        
        recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightSwipeGesture:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
        [[self view] addGestureRecognizer:recognizer];
        [recognizer release];
        
        recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftSwipeGesture:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
        [[self view] addGestureRecognizer:recognizer];
        [recognizer release];
    }
}


- (void) buildScrollView {
    
    //if it's a real room
    if(currentRoom){
        //return if no available colors
        if ([availableColors count] == 0){
            return;
        }
        
        for (UIView *v in [self.scrollview subviews]) {
            [v removeFromSuperview];
        }
        
        [childControllers removeAllObjects];
        
        int i=0;
        int y=0;
        
//        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"sceneNo" ascending:YES];
//        NSArray *sortedScenes = [currentRoom.scenes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
//        [sorter release];
        
        NSArray* sceneOrder = [NSArray arrayWithObjects:[NSNumber numberWithInt:0],
                               [NSNumber numberWithInt:5],
                               [NSNumber numberWithInt:17],
                               [NSNumber numberWithInt:18],
                               [NSNumber numberWithInt:19],
                               [NSNumber numberWithInt:6],
                               [NSNumber numberWithInt:7],
                               [NSNumber numberWithInt:8],
                               [NSNumber numberWithInt:9],
                               [NSNumber numberWithInt:33],
                               [NSNumber numberWithInt:20],
                               [NSNumber numberWithInt:21],
                               [NSNumber numberWithInt:22],
                               [NSNumber numberWithInt:35],
                               [NSNumber numberWithInt:23],
                               [NSNumber numberWithInt:24],
                               [NSNumber numberWithInt:25],
                               [NSNumber numberWithInt:37],
                               [NSNumber numberWithInt:26],
                               [NSNumber numberWithInt:27],
                               [NSNumber numberWithInt:28],
                               [NSNumber numberWithInt:39],
                               [NSNumber numberWithInt:29],
                               [NSNumber numberWithInt:30],
                               [NSNumber numberWithInt:31],
                               nil];
        
        for (int k = 0; k<[sceneOrder count]; k++) {
            DSScene* scene = [currentRoom getScene:[sceneOrder objectAtIndex:k] withGroup:[availableColors objectAtIndex:currentIndex]];
            
            if(self.showAll || [scene.mainList boolValue]){
                    SettingsScenesListItemVC *item = [[SettingsScenesListItemVC alloc] initWithScene:scene withDarkBg:i%2==1 andIndex:i isAnimated:animateItems isLookingForFavorite:isLookingForFavorite];
                    item.view.frame = CGRectMake(0, y, item.view.frame.size.width, item.view.frame.size.height);
                    [self.scrollview addSubview:item.view];
                    i++;
                    y += item.view.frame.size.height;
                    [childControllers addObject:item];
            }
        }
        
        self.scrollview.contentSize = CGSizeMake(275, y);
    }
    //else it's global activities or user defined actions
    else{
        for (UIView *v in [self.scrollview subviews]) {
            [v removeFromSuperview];
        }
        
        [childControllers removeAllObjects];
        
        int i=0;
        int y=0;
        
        
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        NSArray *sortedScenes = [[DataController instance].scenes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
        [sorter release];
        
        if(isGlobalActivities){
            
            for (DSScene* scene in sortedScenes) {
                if (scene.room || [scene.sceneNo longLongValue] > 1023) {
                    continue;
                }
                
                SettingsScenesListItemVC *item = [[SettingsScenesListItemVC alloc] initWithScene:scene withDarkBg:i%2==1 andIndex:i isAnimated:animateItems isLookingForFavorite:isLookingForFavorite];
                item.view.frame = CGRectMake(0, y, item.view.frame.size.width, item.view.frame.size.height);
                [self.scrollview addSubview:item.view];
                i++;
                y += item.view.frame.size.height;
                [childControllers addObject:item];
            }
            
            self.scrollview.contentSize = CGSizeMake(275, y);
            
        }
        //it's user defined actions then
        else{
            
            for (DSScene* scene in sortedScenes) {
                
                if (scene.room || [scene.sceneNo longLongValue] < 1024) {
                    continue;
                }
                
                SettingsScenesListItemVC *item = [[SettingsScenesListItemVC alloc] initWithScene:scene withDarkBg:i%2==1 andIndex:i isAnimated:animateItems isLookingForFavorite:isLookingForFavorite];
                item.view.frame = CGRectMake(0, y, item.view.frame.size.width, item.view.frame.size.height);
                [self.scrollview addSubview:item.view];
                i++;
                y += item.view.frame.size.height;
                [childControllers addObject:item];
                
            }
        }
        
        self.scrollview.contentSize = CGSizeMake(275, y);
        
    }

}

- (void) handleLeftSwipeGesture:(UISwipeGestureRecognizer *)recognizer {
    if(colorDotsView.currentDot != colorDotsView.numberOfPages-1){
        colorDotsView.currentDot++;
        currentIndex++;
        animateItems = YES;
        [self buildScrollView];
        animateItems = NO;
//        NSLog(@"Left swipe handled");
    }
}

- (void) handleRightSwipeGesture:(UISwipeGestureRecognizer *)recognizer {
    if(colorDotsView.currentDot != 0){
        colorDotsView.currentDot--;
        currentIndex--;
        animateItems = YES;
        [self buildScrollView];
        animateItems = NO;
//        NSLog(@"Right swipe handled");
    }
}

- (DSRoom*) currentRoomshowing{
    if(currentRoom)
        return  currentRoom;
    else
        return nil;
}

- (int) currentIndex{
    return currentIndex;
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
	
	[[ModelLocator instance] setValue:nil forKey:k_VIEW_SETTINGS_SCENES_LIST];
}

- (IBAction) changeSegment{


    //used scenes
    if(segment.selectedSegmentIndex == 0){
        self.showAll = NO;
        [self buildScrollView];
        
	}
    //All scenes. This will also create all possible scenes for the room
    else if(segment.selectedSegmentIndex == 1){
        self.showAll = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            DSServer *currentServer = [[DataController instance] getCurrentDSS];
            //all the scenes available for modification
            NSArray *allExtraSceneNumbers = [NSArray arrayWithObjects:[NSNumber numberWithInt:33], //Preset 11
                                             [NSNumber numberWithInt:20], //12
                                             [NSNumber numberWithInt:21], //13
                                             [NSNumber numberWithInt:22], //14
                                             [NSNumber numberWithInt:35], //21
                                             [NSNumber numberWithInt:23], //22
                                             [NSNumber numberWithInt:24], //23
                                             [NSNumber numberWithInt:25], //24
                                             [NSNumber numberWithInt:37], //31
                                             [NSNumber numberWithInt:26], //32
                                             [NSNumber numberWithInt:27], //33
                                             [NSNumber numberWithInt:28], //34
                                             [NSNumber numberWithInt:39], //41
                                             [NSNumber numberWithInt:29], //42
                                             [NSNumber numberWithInt:30], //43
                                             [NSNumber numberWithInt:31], //44
                                             [NSNumber numberWithInt:6], //area 1 on
                                             [NSNumber numberWithInt:7], //2
                                             [NSNumber numberWithInt:8], //3
                                             [NSNumber numberWithInt:9], //4
                                             nil];
            
            NSArray *allExtraSceneNames = [NSArray arrayWithObjects:NSLocalizedString(@"key_scene_33", @""),
                                           NSLocalizedString(@"key_scene_20", @""),
                                           NSLocalizedString(@"key_scene_21", @""),
                                           NSLocalizedString(@"key_scene_22", @""),
                                           NSLocalizedString(@"key_scene_35", @""),
                                           NSLocalizedString(@"key_scene_23", @""),
                                           NSLocalizedString(@"key_scene_24", @""),
                                           NSLocalizedString(@"key_scene_25", @""),
                                           NSLocalizedString(@"key_scene_37", @""),
                                           NSLocalizedString(@"key_scene_26", @""),
                                           NSLocalizedString(@"key_scene_27", @""),
                                           NSLocalizedString(@"key_scene_28", @""),
                                           NSLocalizedString(@"key_scene_39", @""),
                                           NSLocalizedString(@"key_scene_29", @""),
                                           NSLocalizedString(@"key_scene_30", @""),
                                           NSLocalizedString(@"key_scene_31", @""),
                                           NSLocalizedString(@"key_scene_6", @""),
                                           NSLocalizedString(@"key_scene_7", @""),
                                           NSLocalizedString(@"key_scene_8", @""),
                                           NSLocalizedString(@"key_scene_9", @""),
                                           nil];
            
            //For each available group, check if the scenes are already there
            for (int i = 0; i < [availableColors count]; i++) {
                for (int k = 0; k < [allExtraSceneNumbers count]; k++) {
                    //if it already exists, move on
                    if([currentServer getScene: [allExtraSceneNumbers objectAtIndex:k]
                                        inRoom: currentRoom
                                     withGroup: [availableColors objectAtIndex:i]]){
                        continue;
                    }
                    //else create the scene
                    else{
                        [[DataController instance] createDSScene:[allExtraSceneNumbers objectAtIndex:k]
                                                        withName:[allExtraSceneNames objectAtIndex:k]
                                                        withRoom:currentRoom
                                                       withGroup:[availableColors objectAtIndex:i]
                                                  favouriteScene:NO
                                                      showOnList:NO];
                    }
                }
            }

            [self buildScrollView];
        });
	}
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
	
	for (UIViewController *child in childControllers) {
		[child.view removeFromSuperview];
		[child release];
	}
	[childControllers removeAllObjects];
	[segment release];
    [colorDotsView release];
    [super dealloc];
}


@end
