//
//  SettingsScenesListItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSScene;

@interface SettingsScenesListItemVC : UIViewController {
	DSScene *scene;
	BOOL hasDarkBackground;
	BOOL editModeOn;
	NSInteger index;
	
	UIImageView *imageBackground;
	UILabel *labelName;
	UIButton *buttonStepInto;
	UIButton *buttonDelete;
    UIImageView *imageGroupIcon;
    BOOL doAnimation;
    BOOL isLookingForFavorite;
	
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UIImageView *imageGroupIcon;
@property (nonatomic, retain) IBOutlet UILabel *labelName;
@property (nonatomic, retain) IBOutlet UIButton *buttonStepInto;
@property (nonatomic, retain) IBOutlet UIButton *buttonDelete;

- (id) initWithScene: (DSScene*) theScene withDarkBg: (BOOL) isDark andIndex: (NSInteger) _index isAnimated: (BOOL)animationFlag isLookingForFavorite: (BOOL)favorite;
- (IBAction) actionButtonClicked;
- (IBAction) actionDeleteScene;

@end
