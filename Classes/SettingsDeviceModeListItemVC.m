//
//  SettingsDeviceModeListItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceModeListItemVC.h"
#import "DSRoom.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "SettingsRoomsRoomDetailVC.h"
#import "DataController.h"
#import "SettingsRoomsListVC.h"
#import "SettingsScenesListVC.h"
#import "DSDevice+Group1.h"

@implementation SettingsDeviceModeListItemVC

@synthesize imageBackground, labelName, buttonStepInto, buttonBig,activityIndicator;

- (id) initWithMode: (int)modeNumber andDevice: (DSDevice*)device{
	if(self = [super init]) {
		_modeNumber = modeNumber;
        _device = device;
	}
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    activityIndicator.hidden = YES;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(_device) {
        self.labelName.text = [DSDevice nameForOutputMode:_modeNumber];

        self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_hell.png"];
	}
   
}


- (IBAction) actionSetMode {
    if(_device){
        if ([_device.outputMode intValue] == _modeNumber) {
            //choose the same, just go back
            SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
            [settings goBack];
        } else {
            activityIndicator.hidden = NO;
            [activityIndicator startAnimating];
            
            //move device by json then update the local value
            [[DataController instance] setOutputModeForDevice:_device toMode:_modeNumber];
            
            //go back when done
            SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
            [settings goBack];
            
        }
    }
    
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
    [super dealloc];
}


@end
