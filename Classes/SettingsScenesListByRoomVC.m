//
//  SettingsScenesListByRoomVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsScenesListByRoomVC.h"
#import "SettingsRoomsListItemVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "CustomLabel.h"
#import "SettingsScenesDetailVC.h"

@implementation SettingsScenesListByRoomVC

@synthesize scrollview;
@synthesize background;

- (id) initWithFavorite: (BOOL) lookingForFavorite{
    if (self = [super init]) {
		isLookingForFavorite = lookingForFavorite;
	}
    
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_SCENES_BY_ROOM_LIST];
    
    titleLabel.text = NSLocalizedString(@"key_activities_title", nil);
    
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }
	items = [NSMutableArray new];
	
    [self buildScrollView];
    
    self.screenName = @"Settings Activities Select Scene";
}

- (void)buildScrollView {
	for (UIViewController *item in items) {
		[item.view removeFromSuperview];
		[item release];
	}
	[items removeAllObjects];
    
    NSInteger yOffset = 0;
    
    SettingsRoomsListItemVC *item = [[SettingsRoomsListItemVC alloc] initWithName:NSLocalizedString(@"key_global_activities", @"") isLookingForFavorite:isLookingForFavorite];
    item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
    [self.scrollview addSubview:item.view];
    yOffset += item.view.frame.size.height;
    [items addObject:item];
    
    SettingsRoomsListItemVC *item2 = [[SettingsRoomsListItemVC alloc] initWithName: NSLocalizedString(@"key_high_level_events", @"") isLookingForFavorite:isLookingForFavorite];
    item2.view.frame = CGRectMake(0, yOffset, item2.view.frame.size.width, item2.view.frame.size.height);
    [self.scrollview addSubview:item2.view];
    yOffset += item2.view.frame.size.height;
    [items addObject:item2];
    
	NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sorted = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [orderDescriptor release];
    [nameDescriptor release];
	
	for (DSRoom* room in sorted) {
		
		SettingsRoomsListItemVC *item = [[SettingsRoomsListItemVC alloc] initWithRoom:room andOffset:yOffset andMaximum:[sorted count]gointToScenes:YES isLookingForFavorite:isLookingForFavorite gointToDevices:NO];
		item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
		[self.scrollview addSubview:item.view];
		
		yOffset += item.view.frame.size.height;
		[items addObject:item];
	}
    
	self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, yOffset);
}


#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
	
	[[ModelLocator instance] setValue:nil forKey:k_VIEW_SETTINGS_SCENES_BY_ROOM_LIST];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
	
	for (UIViewController *child in items) {
		[child.view removeFromSuperview];
		[child release];
	}
	[items removeAllObjects];
	
    [titleLabel release];
    [super dealloc];
}


@end
