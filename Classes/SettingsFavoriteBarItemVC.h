//
//  SettingsFavoriteBarItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSScene;

@interface SettingsFavoriteBarItemVC : UIViewController {
	DSScene *scene;
	bool editmodeOn;
	float offset;
	NSInteger maximum;
	
	UILabel *labelName;
	UILabel *labelIndex;
	UIImageView *imageDragMe;
	UIButton *buttonRemove;
	
	
	@private int deltaY;
	@private int magneticY;
}

@property (nonatomic, retain) IBOutlet UILabel *labelName;
@property (nonatomic, retain) IBOutlet UILabel *labelIndex;
@property (nonatomic, retain) IBOutlet UIImageView *imageDragMe;
@property (nonatomic, retain) IBOutlet UIButton *buttonRemove;

- (id) initWithScene: (DSScene*) _scene andOffset:(float) _offset andMaximum:(NSInteger) _max;

- (void) toggleEditmode;

- (void) incrementMagneticPoint;
- (void) decrementMagneticPoint;

- (IBAction) actionRemoveSceneFromFavs;

@end
