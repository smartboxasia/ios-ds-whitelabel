//
//  SettingsFavoriteBarSceneListVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsFavoriteBarSceneListVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "SettingsFavoriteBarSceneListItemVC.h"
#import "CustomLabel.h"
#import "SettingsScenesDetailVC.h"
#import "UIBlockButton.h"

@implementation SettingsFavoriteBarSceneListVC

@synthesize scrollview;
@synthesize background;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    titleLabel.text = NSLocalizedString(@"key_fav_activities_title", nil);
    
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }
    
	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_SCENES_LIST];
	childControllers = [NSMutableArray new];
	
	[self buildScrollView];
}

- (void) buildScrollView {
	
	for (UIView *v in [self.scrollview subviews]) {
		[v removeFromSuperview];
	}
	
	[childControllers removeAllObjects];
	
	int i=0;
	int y=0;
	
	NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortedScenes = [[DataController instance].scenes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
    [sorter release];
    
	for (DSScene* scene in sortedScenes) {
		if (scene.room || [scene.favourit boolValue] || [scene.sceneNo longLongValue] > 1023 ) {
			continue;
		}
		
		if (i == 0) {
			UIImageView *imageBgRoom = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Einst_ListenTeiler.png"]];
			imageBgRoom.frame = CGRectMake(0, y, 275, 20);
			[self.scrollview addSubview:imageBgRoom];
            [imageBgRoom release];
			
			CustomLabel *labelRoom = [CustomLabel new];
			labelRoom.frame = CGRectMake(15, y+2, 240, 20);
			labelRoom.text = NSLocalizedString(@"key_global_activities", @"");
			labelRoom.backgroundColor = [UIColor clearColor];
			labelRoom.font = [labelRoom.font fontWithSize:12];
			[self.scrollview addSubview:labelRoom];
            [labelRoom release];
			
			y+=20;
		}
		SettingsFavoriteBarSceneListItemVC *item = [[SettingsFavoriteBarSceneListItemVC alloc] initWithScene:scene withDarkBg:i%2==1 andIndex:i];
		item.view.frame = CGRectMake(0, y, item.view.frame.size.width, item.view.frame.size.height);
		[self.scrollview addSubview:item.view];
		i++;
		y += item.view.frame.size.height;
		[childControllers addObject:item];
	}
	
    BOOL drawHeader = YES;
    for (DSScene* scene in sortedScenes) {
//		GSLog(@"scene: %@", scene.name);
		if (scene.room || [scene.sceneNo longLongValue] < 1024 || [scene.favourit boolValue]) {
			continue;
		}
		
		if (drawHeader) {
            drawHeader = NO;
			UIImageView *imageBgRoom = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Einst_ListenTeiler.png"]];
			imageBgRoom.frame = CGRectMake(0, y, 275, 20);
			[self.scrollview addSubview:imageBgRoom];
            [imageBgRoom release];
			
			CustomLabel *labelRoom = [CustomLabel new];
			labelRoom.frame = CGRectMake(15, y+2, 240, 20);
			labelRoom.text = NSLocalizedString(@"key_high_level_events", @"");
			labelRoom.backgroundColor = [UIColor clearColor];
			labelRoom.font = [labelRoom.font fontWithSize:12];
			[self.scrollview addSubview:labelRoom];
            [labelRoom release];
			
			y+=20;
		}
		SettingsFavoriteBarSceneListItemVC *item = [[SettingsFavoriteBarSceneListItemVC alloc] initWithScene:scene withDarkBg:i%2==1 andIndex:i];
		item.view.frame = CGRectMake(0, y, item.view.frame.size.width, item.view.frame.size.height);
		[self.scrollview addSubview:item.view];
		i++;
		y += item.view.frame.size.height;
		[childControllers addObject:item];
	}

	
	for (DSRoom* room in [DataController instance].rooms) {
		if ([room.scenes count] > 0) {
			UIImageView *imageBgRoom = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Einst_ListenTeiler.png"]];
			imageBgRoom.frame = CGRectMake(0, y, 275, 20);
			[self.scrollview addSubview:imageBgRoom];
			
			CustomLabel *labelRoom = [CustomLabel new];
			labelRoom.frame = CGRectMake(15, y+2, 240, 20);
			labelRoom.text = room.name;
			labelRoom.backgroundColor = [UIColor clearColor];
			labelRoom.font = [labelRoom.font fontWithSize:12];
			[self.scrollview addSubview:labelRoom];
			
			
			y+=20;
			i=0;
			
            NSSortDescriptor *tempSorter = [[NSSortDescriptor alloc] initWithKey:@"sceneNo" ascending:YES];
			NSArray *tempScenes = [room.scenes sortedArrayUsingDescriptors:[NSArray arrayWithObject:tempSorter]];
			NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"group" ascending:YES];
			NSArray *sortedScenes = [tempScenes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
			
			for (DSScene* scene in sortedScenes) {
				if ( [scene.favourit boolValue]) {
					continue;
				}
				SettingsFavoriteBarSceneListItemVC *item = [[SettingsFavoriteBarSceneListItemVC alloc] initWithScene:scene withDarkBg:i%2==1 andIndex:i];
				item.view.frame = CGRectMake(0, y, item.view.frame.size.width, item.view.frame.size.height);
				[self.scrollview addSubview:item.view];
				i++;
				y += item.view.frame.size.height;
				[childControllers addObject:item];
			}
			
			
			
		}		
	}
	
	self.scrollview.contentSize = CGSizeMake(275, y);
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
	
	UIBlockButton *buttonBearbeiten = (UIBlockButton*)((SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS]).buttonBearbeiten;
	buttonBearbeiten.hidden = NO;
	
	[UIView animateWithDuration:0.2 animations:^{
		buttonBearbeiten.alpha = 1;
	}];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	for (UIViewController *child in childControllers) {
		[child.view removeFromSuperview];
		[child release];
	}
	[childControllers removeAllObjects];
	
    [titleLabel release];
    [super dealloc];
}


@end
