//
//  SettingsNetworkItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSServer;

@interface SettingsNetworkItemVC : UIViewController {
	BOOL hasDarkBackground;
	NSString *servername;
	DSServer *dsServer;
	
	UIButton *buttonLabel;
	UIImageView *imageSelectionState;
	UIImageView *imageBackground;
	
}

@property (nonatomic, retain) IBOutlet UIButton *buttonLabel;
@property (nonatomic, retain) IBOutlet UIButton *buttonArrow;
@property (nonatomic, retain) IBOutlet UIImageView *imageSelectionState;
@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;

- (id)initWithServer: (DSServer*) server andDarkBgFlag: (BOOL) darkBg;
- (id)initWithServerName: (NSString *) name andDarkBgFlag: (BOOL) darkBg;
- (void) setActive;
- (void) setInactive;
- (IBAction) actionConnect;
- (IBAction) actionShowDetails;

@end
