//
//  SettingsDeviceDetailPropertyItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceDetailPropertyItemVC.h"

#define kImageBackgroundDark [UIImage imageNamed:@"Einst_Liste_dunkel.png"]
#define kImageBackgroundLight [UIImage imageNamed:@"Einst_Liste_hell.png"]

@interface SettingsDeviceDetailPropertyItemVC ()

@end

@implementation SettingsDeviceDetailPropertyItemVC

@synthesize labelPropertyName, labelPropertyValue, imageBackground, activityIndicator;

- (id) initWithProperty: (NSString*) _propertyName oddIndex: (BOOL) _oddIndex andValue: (NSString*) _value {
    self = [super init];
    if (self) {
		propertyName = _propertyName;
		oddIndex = _oddIndex;
		value = _value;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	[self updateValue:value];
	
	self.labelPropertyName.text = [NSString stringWithFormat:@"%@:", propertyName];
	
	if (oddIndex) {
		self.imageBackground.image = kImageBackgroundLight;
	}
	else {
		self.imageBackground.image = kImageBackgroundDark;
	}
}

- (void) updateValue: (NSString*) _value {
	value = _value;
	
	if (!value) {
		self.activityIndicator.hidden = NO;
		self.labelPropertyValue.text = @"";
	}
	else {
		self.activityIndicator.hidden = YES;
		self.labelPropertyValue.text = value;
	}
}

- (BOOL) isProperty: (NSString*) _propertyName {
	return [propertyName isEqualToString:_propertyName];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
