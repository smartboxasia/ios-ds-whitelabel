//
//  EnergieAmpelVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface EnergieAmpelVC : UIViewController {
	IBOutlet UIImageView *ampelFarben;
	NSTimer *waberTimer;
}


- (float) _calculateValue;
- (void) _showValue: (float)_value ;
- (void) _showStandBy;

-(void) showValue: (float)_value ;

-(void) showStandBy;

-(void) startWabern;
-(void) stopWabern;

- (void) storeInML;

@end
