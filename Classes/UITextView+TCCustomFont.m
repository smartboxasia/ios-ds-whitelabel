//
//  UITextView+TCCustomFont.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "UITextView+TCCustomFont.h"

@implementation UITextView (TCCustomFont)

- (NSString *)fontName {
    return self.font.fontName;
}

- (void)setFontName:(NSString *)_fontName {
    //self.font = [UIFont fontWithName:_fontName size:self.font.pointSize];
	[self setFont:[UIFont fontWithName:@"PFBeauSansPro-Regular" size:self.font.pointSize]];
	NSLog(@"font: %@", self.font);
}

@end
