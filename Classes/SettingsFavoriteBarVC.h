//
//  SettingsFavoriteBarVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class CustomLabel;

@interface SettingsFavoriteBarVC : GAITrackedViewController {
	UIScrollView *scrollview;
	bool editmodeOn;
    IBOutlet CustomLabel *titleLabel;
	NSMutableArray *items;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (void) buildScrollview;

- (IBAction) actionBack;
- (IBAction) actionAddActivity;
- (void) toggleEditmode;

- (void) magneticPointHasChanged: (int) from toIndex: (int) to;

@end
