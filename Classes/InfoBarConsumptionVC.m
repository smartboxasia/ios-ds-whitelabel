//
//  InfoBarConsumptionVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "InfoBarConsumptionVC.h"
#import "ModelLocator.h"

@implementation InfoBarConsumptionVC

@synthesize labelConsumption;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]) {
		NSNumber *consumption = [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION];
        if([consumption integerValue] > -1){
            self.labelConsumption.text = [NSString stringWithFormat:@"%@ W", [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]];
        }
        else{
            self.labelConsumption.text = @"-- W";
        }
	}
	else {
		self.labelConsumption.text = @"-- W";
	}
	
	[[ModelLocator instance] addObserver:self forKeyPath:k_INFO_TOTAL_CONSUMPTION options:0 context:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_INFO_TOTAL_CONSUMPTION];
    }
    @catch (NSException *exception) {
        //just in case
    }
	
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_INFO_TOTAL_CONSUMPTION]) {
		if ([[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]) {
			NSNumber *consumption = [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION];
            if([consumption integerValue] > -1){
                self.labelConsumption.text = [NSString stringWithFormat:@"%@ W", [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]];
            }
            else{
                self.labelConsumption.text = @"-- W";
            }
		}
		else {
			self.labelConsumption.text = @"-- W";
		}
	}
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) prepareDealloc {
}

- (void)dealloc {
	
    [super dealloc];
}


@end
