//
//  SettingsDeviceModeListItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSDevice;

@interface SettingsDeviceModeListItemVC : UIViewController {
    DSDevice *_device;
	int _modeNumber;
	UIImageView *imageBackground;
	UILabel *labelName;
	UIButton *buttonStepInto;
	UIButton *buttonBig;
	
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UILabel *labelName;
@property (nonatomic, retain) IBOutlet UIButton *buttonStepInto;
@property (nonatomic, retain) IBOutlet UIButton *buttonBig;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

- (id) initWithMode: (int)modeNumber andDevice: (DSDevice*)device;

- (IBAction) actionSetMode;

@end
