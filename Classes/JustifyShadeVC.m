//
//  JustifyShadeVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "JustifyShadeVC.h"
#import "DataController.h"
#import "DSDevice.h"

@implementation JustifyShadeVC

@synthesize labelDevicename, isMoving;

- (id) initWithDevice: (DSDevice*) _device {
	self = [super init];

	if (self) {
		device = _device;
	}

	return self;
}

- (id) initWithRoom: (DSRoom*) _room {
	self = [super init];

	if (self) {
		room = _room;
	}

	return self;
}

- (id) initWithScene: (DSScene*) _scene {
	self = [super init];

	if (self) {
		scene = _scene;
	}

	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    //iphone 5 support.
//    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//    NSLog(@"iphone5 support: %f ---------5-- %f", [UIScreen mainScreen].scale, screenHeight);
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        //manually move the elements of the justify button down to the center of the screen
//        int offset = 50;
//        justifyButton1.frame = CGRectMake(justifyButton1.frame.origin.x, justifyButton1.frame.origin.y + offset, justifyButton1.frame.size.width, justifyButton1.frame.size.height);
//        justifyButton2.frame = CGRectMake(justifyButton2.frame.origin.x, justifyButton2.frame.origin.y + offset, justifyButton2.frame.size.width, justifyButton2.frame.size.height);
//        justifyButton3.frame = CGRectMake(justifyButton3.frame.origin.x, justifyButton3.frame.origin.y + offset, justifyButton3.frame.size.width, justifyButton3.frame.size.height);
//        justifyButton4.frame = CGRectMake(justifyButton4.frame.origin.x, justifyButton4.frame.origin.y + offset, justifyButton4.frame.size.width, justifyButton4.frame.size.height);
//        justifyButton5.frame = CGRectMake(justifyButton5.frame.origin.x, justifyButton5.frame.origin.y + offset, justifyButton5.frame.size.width, justifyButton5.frame.size.height);
//        imageButton1.frame = CGRectMake(imageButton1.frame.origin.x, imageButton1.frame.origin.y + offset, imageButton1.frame.size.width, imageButton2.frame.size.height);
//        imageButton2.frame = CGRectMake(imageButton2.frame.origin.x, imageButton2.frame.origin.y + offset, imageButton2.frame.size.width, imageButton2.frame.size.height);
//    }

	if (device) {
		self.labelDevicename.text = device.name;
	}
	else if(room) {
		self.labelDevicename.text = room.name;
	}
	else if(scene) {
        //for shades we show room name instead
//		self.labelDevicename.text = scene.name;
        self.labelDevicename.text = scene.room.name;

//        [TestFlight passCheckpoint:@"Entered 5-way"];
    }

    self.screenName = @"Overview Advanced Shade";
}
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //disable rotating to energy visualisation while on this view
    [DataController instance].rotationEnabled = NO;

    //enable tilt notifications and start listening
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];

    self.isMoving = NO;
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
	[[UIDevice currentDevice]endGeneratingDeviceOrientationNotifications];
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    @catch (NSException *exception) {
        //just in case
    }

}



- (void) didRotate:(NSNotification *)notification{

    UIDeviceOrientation currentRotation = [[UIDevice currentDevice] orientation];

    switch (currentRotation) {
        case UIDeviceOrientationLandscapeLeft:
            [self goToMin];
            self.isMoving = YES;
            break;

        case UIDeviceOrientationLandscapeRight:
            [self goToMax];
            self.isMoving = YES;
            break;

        default:
            if(self.isMoving){
                [self stopMovement];
                self.isMoving = NO;
            }
            break;
    }
}

#pragma mark -
#pragma mark IB Actions

- (IBAction) actionBack {
    //re-enable rotation to energy visualisation
    [DataController instance].rotationEnabled = YES;

    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
		self.view.frame = CGRectMake(0, self.view.superview.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	} completion:^(BOOL finished) {
		[self.view removeFromSuperview];
		[self release];
	}];
}

- (IBAction) actionStop {
	[self stopMovement];
}

- (IBAction) actionStepIncrement {
    [self stepIncrement];
}

- (IBAction) actionStepDecrement {
    [self stepDecrement];
}

- (IBAction) actionMin {
    [self goToMin];
}

- (IBAction) actionMax {
    [self goToMax];
}

- (void) stepIncrement {
	NSLog(@"Incrementing 1 step");
	if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_INC withDevice:device withGroup:2];
		});
	}
	else if(scene.room) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_INC withRoom:scene.room withGroup:2];
		});
	}
}

- (void) stepDecrement {
	NSLog(@"Decrementing 1 step");
	if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_DEC withDevice:device withGroup:2];
		});
	}
	else if(scene.room) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_DEC withRoom:scene.room withGroup:2];
		});
	}
}

- (void) goToMax {
	NSLog(@"Going to max position");
	if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_MAX withDevice:device withGroup:2];
		});
	}
	else if(scene.room) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_MAX withRoom:scene.room withGroup:2];
		});
	}
}

- (void) goToMin {
	NSLog(@"Going to min position");
	if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_MIN withDevice:device withGroup:2];
		});
	}
	else if(scene.room) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_MIN withRoom:scene.room withGroup:2];
		});
	}
}

- (void) stopMovement {
	NSLog(@"Stopping movement");
	if (device) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [[DataController instance] callStandardScene:k_SERVICE_SCENE_STOP withDevice:device withGroup:2];
        });
	}
	else if(scene.room) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_STOP withRoom:scene.room withGroup:2];
		});
	}
}


//- (IBAction) actionPoint {
//	if (device) {
//		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//			[[DataController instance] setDevicePower:device withBool:![device.powerOn boolValue]];
//		});
//	}
//
//	if (scene) {
//		if ([scene.sceneNo intValue] != [scene.room.lastCalledScene intValue]) {
//			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//				[[DataController instance] turnSceneOn:scene];
//			});
//		} else {
//			GSLog(@"CALLING STANDBY SCENE on ROOM %@", scene.room);
//			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//					[[DataController instance] callStandbyScene:scene.room];
//				});
//		}
//	}
//}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

    [super dealloc];
}


@end
