//
//  DataController.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "DataController.h"
#import "ModelLocator.h"
#import "SBJSON.h"
#import "FacilityController.h"
#import "DSServer.h"


@implementation DataController

@synthesize fetchedResultsController, objectContext, rooms, scenes, meters, rotationEnabled, dssLatitude, dssLongitude;


+ (DataController*) instance {
	static DataController *instance;
	
	@synchronized(self) {
		if(!instance) {
			instance = [DataController new];
		}
	}
	
	return instance;
}

- (id) init {
	if (self = [super init]) {
		
		self.rooms = [NSMutableArray array];
		newRooms = [[NSMutableArray alloc] init];
		self.scenes = [NSMutableArray array];
		self.meters = [NSMutableArray array];
		self.objectContext = [self managedObjectContext];
        self.rotationEnabled = YES;
        self.dssLatitude = 0;
        self.dssLongitude = 0;
		
		devicesScenesUpdateMaxValue = 0;
		devicesScenesUpdateStand = 0;
		devicesScenesUpdateState = 0;
		currentDss = nil;
	}
	
	return self;
}

- (DSServer*) getCurrentDSS {
	return currentDss;
}

- (void) setCurrentDSS: (DSServer*) dsServer {
	currentDss = dsServer;
}


#pragma mark -
#pragma mark Consumption Methods

- (void) startRoomConsumptionPolling: (DSRoom*) room {
	[[FacilityController instance] startRoomConsumptionPolling:room];
}

- (void) endRoomConsumptionPolling: (DSRoom*) room {
	[[FacilityController instance] endRoomConsumptionPolling:room];
}

- (void) startMeterConsumptionPolling {
	meterConsumptionPollingRunning = YES;
	if (!meterConsumptionBlockRunning) {
		[self meterConsumptionExecutor];
	}
}

- (BOOL) endMeterConsumptionPolling {
	if (meterConsumptionPollingRunning) {
		meterConsumptionPollingRunning = NO;
		return YES;
	}
	else {
		meterConsumptionPollingRunning = NO;
		return NO;
	}
}

- (void) meterConsumptionExecutor {
	if (!meterConsumptionPollingRunning) {
		return;
	}
	meterConsumptionBlockRunning = YES;
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2000000000), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		if (!meterConsumptionPollingRunning) {
			meterConsumptionBlockRunning = NO;
			return;
		}
		
		NSInteger totalConsumption = 0;
		
		if (useDemo) {
			
            
            for (DSMeter *meter in self.meters) {
                
                NSNumber *con = meter.powerConsumption;
                if (arc4random()%100>50) {
                    con = [NSNumber numberWithInt:[con intValue]+arc4random()%10];
                }
                else {
                    con = [NSNumber numberWithInt:[con intValue]-arc4random()%10];
                }
                
                
                
                if (!con) {
                    con = [NSNumber numberWithInt:0];
                }
                
                if ([con intValue]<0) {
                    con = [NSNumber numberWithInt:0];
                }
                meter.powerConsumption = con;
                
                totalConsumption += [con intValue];
            }
			
		}
		else {
            NSDictionary *metersCons = [service getMetersConsumption];
            NSString *dsid;
            
            //if there aren't any meters in the dictonary, there's no connection yet. Set consumption to -1
            if (metersCons.count == 0 ) {
                totalConsumption = -1;
            }
            else{
                for (dsid in metersCons) {
                    NSDictionary *valueDict = [metersCons objectForKey:dsid];
                    NSNumber *powerConsumption = [valueDict objectForKey:@"powerConsumption"];
                    NSNumber *energyMeterValue = [valueDict objectForKey:@"energyMeterValue"];
                    
                    totalConsumption += [powerConsumption integerValue];
                    //                    GSLog(@"dsid: %@: %i", dsid, [value intValue]);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        DSMeter *meter = [currentDss getMeter:dsid];
                        if (meter) {
                            
                            meter.powerConsumption = powerConsumption;
                            
                            //there's a bug that returns negative meter value. Recalculate:
                            if([energyMeterValue intValue] < 0){
                                float tempValue = [energyMeterValue floatValue];
                                tempValue = tempValue * 3600;
                                tempValue += powf(2,32);
                                tempValue = tempValue / 3600;
                                meter.energyMeterValue = [NSNumber numberWithFloat: tempValue];
                            }
                            else{
                                meter.energyMeterValue = energyMeterValue;
                            }
                            
                        }
                    });
                    
                }
            }
        }
        
        
		dispatch_sync(dispatch_get_main_queue(), ^{
			[[ModelLocator instance] setValue:[NSNumber numberWithInteger:totalConsumption] forKey:k_INFO_TOTAL_CONSUMPTION];
			meterConsumptionBlockRunning = NO;
			[self meterConsumptionExecutor];
		});
	});
}


//Sets the total house consumption value in the ModelLocator.
- (NSNumber*) getTotalConsumption {
	/*if ([[ModelLocator instance] valueForKey:k_DSS_IP] && [[[ModelLocator instance] valueForKey:k_DSS_IP] length] > 6) {
	 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
	 NSNumber *con = [service getApartementConsumption];
	 GSLog(@"consumption: %i W", [con intValue]);
	 dispatch_async(dispatch_get_main_queue(), ^{
	 [[ModelLocator instance] setValue:con forKey:k_INFO_TOTAL_CONSUMPTION];
	 });
	 });
	 }*/
	
	if (useDemo) {
		NSInteger totalCon = 0;
		
		for (DSRoom *room in self.rooms) {
			totalCon += [[self getRoomConsumption:room] integerValue];
		}
		
		//[[ModelLocator instance] setValue:[NSNumber numberWithInteger:totalCon] forKey:k_INFO_TOTAL_CONSUMPTION];
		
		NSNumber *con = [NSNumber numberWithInteger:totalCon];
		if (!con) {
			con = [NSNumber numberWithInt:0];
		}
		return con;
	}
	
	return [service getApartmentConsumption];
}

//Returns the consumption of the specified device.
- (NSNumber*) getDeviceConsumption: (DSDevice*) device {
	return ([service getDeviceConsumption:device]);
}

//Returns the consumption of the specified room.
- (NSNumber*) getRoomConsumption: (DSRoom*) room {
	// GSLog(@"getting room consumption for room %@", room.name);
	
	if (useDemo) {
        
		NSNumber *con = [[ModelLocator instance] valueForKey:[NSString stringWithFormat:@"%@%@", k_INFO_PREFIX_ROOM_CONSUMPTION, room.identifier]];
        
		if (arc4random()%100>50) {
			con = [NSNumber numberWithInt:[con intValue]+arc4random()%10];
		}
		else {
			con = [NSNumber numberWithInt:[con intValue]-arc4random()%10];
		}
        
		
		
		if (!con) {
			con = [NSNumber numberWithInt:0];
		}
		
		if ([con intValue]<0) {
			con = [NSNumber numberWithInt:0];
		}
		return con;
        
	}
	
	return ([service getRoomConsumption:room]);
}


-(void) loadMeterConsumptionInfo: (NSTimer*)timer {
	DSMeter *meter = [timer userInfo];
	
	NSMutableDictionary *consumptionInfo = [[NSMutableDictionary alloc] init];
	NSArray *a = [service getDSMConsumptionInfo:meter];
	if (a) {
		if ([a count]>0) {
			
			NSNumber *powerConsumption = [NSNumber numberWithInt:[[[a objectAtIndex:0] objectForKey:@"powerConsumption"] intValue]];
			NSNumber *energyMeterValue = [NSNumber numberWithInt:[[[a objectAtIndex:0] objectForKey:@"energyMeterValue"] intValue]];
			
            //			GSLog(@"PowerC: %i Energy: %i", [powerConsumption intValue], [energyMeterValue intValue]);
			
			[consumptionInfo setValue:powerConsumption forKey:@"powerConsumption"];
			[consumptionInfo setValue:energyMeterValue forKey:@"energyMeterValue"];
			
			[meter setValue:powerConsumption forKey:@"powerConsumption"];
			[meter setValue:energyMeterValue forKey:@"energyMeterValue"];
		}
	}
    [consumptionInfo release];
}

#pragma mark scene updater functions

- (void) startScenePolling: (DSRoom*)room {
    _scenePollingShouldRun = YES;
    if (!_scenePollingBlockRunning) {
        //fire manually first time
        [self updateLastCalledScenesWithArray:[service getLastCalledScenesForRoom: room] forRoom:room];
        //start polling
		[self scenePollingExecutor:room];
	}
}

- (void) endScenePolling {
    _scenePollingShouldRun = NO;
}

- (void) scenePollingExecutor: (DSRoom*)room  {
	if (!_scenePollingShouldRun) {
		return;
	}
	_scenePollingBlockRunning = YES;
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2000000000), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		if (!_scenePollingShouldRun) {
			_scenePollingBlockRunning = NO;
			return;
		}
        [self updateLastCalledScenesWithArray:[service getLastCalledScenesForRoom: room] forRoom:room];
        
        _scenePollingBlockRunning = NO;
        [self scenePollingExecutor:room];
	});
}


- (void) updateLastCalledScenesWithArray:(NSArray*)lastCalledArray forRoom:(DSRoom*)room {
    if(lastCalledArray){
        for (int i = 0; i < [lastCalledArray count]; i++) {
            
            switch ([[[lastCalledArray objectAtIndex:i]objectForKey:@"group"]integerValue]) {
                case 1:
                    [room setLastCalledScene:[[[lastCalledArray objectAtIndex:i] objectForKey:@"lastCalledScene"]stringValue]];
                    break;
                case 2:
                    [room setLastCalledShadeScene:[[[lastCalledArray objectAtIndex:i] objectForKey:@"lastCalledScene"]stringValue]];
                    break;
                case 3:
                    [room setLastCalledBlueScene:[[[lastCalledArray objectAtIndex:i] objectForKey:@"lastCalledScene"]stringValue]];
                    break;
                case 4:
                    [room setLastCalledAudioScene:[[[lastCalledArray objectAtIndex:i] objectForKey:@"lastCalledScene"]stringValue]];
                    break;
                case 5:
                    [room setLastCalledVideoScene:[[[lastCalledArray objectAtIndex:i] objectForKey:@"lastCalledScene"]stringValue]];
                    break;
                default:
                    break;
            }
        }
    }
}

#pragma mark -
#pragma mark Structure Methods

//Inits the current apartment-structure.
- (void) initStructure {
    
	useDemo = [[[ModelLocator instance] valueForKey:k_DATA_DEMOMODE_ACTIVE] boolValue];
//	DSServer *oldDss = currentDss;
    
	//currentDss = [self getDssForIpAddressInCoreData:[[ModelLocator instance] valueForKey:k_DSS_IP]];
	
	if (currentDss) {
		[currentDss retain];
		self.rooms = [NSMutableArray arrayWithArray:[currentDss.rooms allObjects]];
		self.scenes = [NSMutableArray arrayWithArray:[currentDss.scenes allObjects]];
		self.meters = [NSMutableArray arrayWithArray:[currentDss.meters allObjects]];
				
		if (currentDss.version) {
			[[ModelLocator instance] setValue:currentDss.version forKey:k_DSS_VERSION];
		}
		else {
			[[ModelLocator instance] setValue:NSLocalizedString(@"key_unknown", @"") forKey:k_DSS_VERSION];
		}
        
		
		if (currentDss.name) {
			[[ModelLocator instance] setValue:currentDss.name forKey:k_DSS_NAME];
		}
		else {
			[[ModelLocator instance] setValue:NSLocalizedString(@"key_unknown", @"") forKey:k_DSS_NAME];
		}
		
		if (currentDss.appToken) {
			[[ModelLocator instance] setValue:currentDss.appToken forKey:k_DSS_APPLICATION_TOKEN];
			[DigitalstromJSONService instance].dssApplicationToken = currentDss.appToken;
		}
		//if (oldDss && currentDss && ![oldDss isEqual:currentDss]) {
        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_ROOMLIST_HAS_CHANGED];
		//}
	}
    
}

- (BOOL) initServerStructure {
	if (initServerStructRunning) {
		return NO;
	}
	
	initServerStructRunning = YES;
	
	//@synchronized (self) {
    if (useDemo) {
        NSString *language;
        if([[NSLocale preferredLanguages] count]>0){
            language = [[NSLocale preferredLanguages] objectAtIndex:0];
            NSLog(@"lang: %@", language);
        }
        
        NSString *jsonFileName;
        
        if ([language isEqualToString:@"de"]) {
            jsonFileName = @"demo_de";
        }
        else if ([language isEqualToString:@"fr"]) {
            jsonFileName = @"demo_fr";
        }
        else if ([language isEqualToString:@"tr"]) {
            jsonFileName = @"demo_tr";
        }
        else {
            jsonFileName = @"demo_en";
        }
        
        NSError *error;
        NSString *jsonString = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:jsonFileName ofType:@"json"] encoding:NSUTF8StringEncoding error:&error];
        //NSLog(@"JSON: %@", jsonString);
        
        SBJSON *parser = [[SBJSON alloc] init];
        
        NSError *error2;
        demoStructure = [[NSMutableDictionary alloc] initWithDictionary:[parser objectWithString:jsonString error:&error2]];
        [jsonString release];
        [parser release];
        //			NSLog(@"Struc: %@", demoStructure);
        
    }
    
    if (service) {
        //[service release];
        service = nil;
    }
    
    service = [DigitalstromJSONService instance];
    NSDictionary *strucDict = nil;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        DSServer *dss = currentDss;
        
        
        if (dss) {
			//	fetch application token out of modellocator. It has been set previously in the StartupController.
			dss.appToken = [[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN];
			
			dss.lastConnectionTS = [NSDate date];
        }
        
        
        
        if (!currentDss || (currentDss && ![currentDss.ip isEqualToString:dss.ip])) {
            currentDss = dss;
            [currentDss retain];
            
            self.rooms = [NSMutableArray arrayWithArray:[currentDss.rooms allObjects]];
            self.scenes = [NSMutableArray arrayWithArray:[currentDss.scenes allObjects]];
            self.meters = [NSMutableArray arrayWithArray:[currentDss.meters allObjects]];
            
            [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_ROOMLIST_HAS_CHANGED];
        }
        
        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_SCENELIST_HAS_CHANGED];
        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_FAVORITELIST_HAS_CHANGED];
        NSError *error = nil;
        [self.objectContext save:&error];
        
        if(error)
            GSLog(@"save error: %@", error);
    });
    if (useDemo) {
        strucDict = [[demoStructure objectForKey:@"result"]objectForKey:@"apartment"];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_demo_mode", @"") message:NSLocalizedString(@"key_message_demo_mode", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"") otherButtonTitles:nil];
            [alert show];
            [alert release];
        });
        
    } else {
        strucDict = [service getStructure: YES];
    }
    
    
    
    //[self getDSSVersion];
    
    if (strucDict) {
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            //	Alle räume als dirty markieren:
            for (DSRoom *room in currentDss.rooms) {
                room.dirty = YES;
            }
            
            BOOL updated = NO;
            NSArray *roomArray = [service copyRoomProperties:(NSMutableDictionary*) strucDict];
            for (int i = 0; i<[roomArray count]; i++) {
                if ([self updateRoomAndDevicesWithDict: [roomArray objectAtIndex:i]]) {
                    updated = YES;
                }
            }
            [roomArray release];
            
            //Then udpate all the reachable groups in all the zones
            NSMutableArray * reachableGroupsNonDs = [service getReachableNonDsGroups];

            for (int i = 0; i<[reachableGroupsNonDs count]; i++) {
                [self updateReachableGroupsWithNonDsDeviceDict: [reachableGroupsNonDs objectAtIndex:i]];
            }
            
            //	Jetzt alle dirty räume abräumen:
            //				GSLog(@"rooms: %@", currentDss.rooms);
            for (DSRoom *room in currentDss.rooms) {
                if (room.dirty) {
                    [self.objectContext deleteObject:room];
                }
            }
            
                       
            [self.objectContext save:nil];
            
            if (updated) {
                
                self.rooms = [NSMutableArray arrayWithArray:[currentDss.rooms allObjects]];
                
                [[ModelLocator instance] setValue:@"1" forKey:k_DATA_STRUCTURE_HAS_CHANGED];
                [[NSNotificationCenter defaultCenter] postNotificationName:k_DATA_STRUCTURE_HAS_CHANGED object:@"1"];
            }
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:k_DATA_LOGIN_SUCCESSFUL object:self];
            [[ModelLocator instance] setValue:@"1" forKey:k_DSS_AVAILABLE];
            
            for (DSMeter *meter in [DataController instance].meters) {
                meter.powerConsumption = [NSNumber numberWithInteger:[meter.powerConsumption integerValue]];
            }
            
            if (useDemo) {
                
                [self loadDemoScenes];
                //[self loadApartmentScenes];
                [self loadDemoMeters];
                
                self.rooms = [NSMutableArray arrayWithArray:[currentDss.rooms allObjects]];
                self.scenes = [NSMutableArray arrayWithArray:[currentDss.scenes allObjects]];
                self.meters = [NSMutableArray arrayWithArray:[currentDss.meters allObjects]];
                
                [self startMeterConsumptionPolling];
                [self getDSSLocation];
                
                [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_FAVORITELIST_HAS_CHANGED];
                [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_SCENELIST_HAS_CHANGED];
            }
            else {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    //	apartementscenes holen
                    [self loadApartmentScenes];
                    
                    //update with scenes
                    NSDictionary *scenesDict = [service allocScenes];
                    if ([scenesDict count] > 0) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            [self updateScenesWithDict:scenesDict];
                        });
                    }
                    [scenesDict release];
                    
                    //update with high level events
                    NSMutableArray *highLevelEventArray = [service getHighLevelEvents];
                    if ([highLevelEventArray count] > 0) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            [self updateHighLevelEventsWithArray:highLevelEventArray];
                        });
                    }
                    //                        NSLog(@"%@", highLevelEventArray.description);
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self getDSSLocation];
                    });
                    
                    
                    //	observer benachrichtigen und array updaten
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        self.scenes = [NSMutableArray arrayWithArray:[currentDss.scenes allObjects]];
                        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_FAVORITELIST_HAS_CHANGED];
                        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_SCENELIST_HAS_CHANGED];
                    });
                    
                    //	dsMeters laden
                    NSArray *metersArray = [service allocDSMs];
                    [self updateMetersWithArray:metersArray];
                    [metersArray release];
                    
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        self.meters = [NSMutableArray arrayWithArray:[currentDss.meters allObjects]];
                        
                    });
                });
            }
            
            //TODO: Das reinehmen um die Geräte zu den Szenen zu laden
            //[self setDevicesToScenes];
        });
        
        initServerStructRunning = NO;
        return YES;
    }
	//}
    
	initServerStructRunning = NO;
	return NO;
}

- (void) updateMetersWithArray: (NSArray*) metersArray {
	dispatch_sync(dispatch_get_main_queue(), ^{
        
        if (!metersArray || [metersArray count] == 0) {
            return;
        }
        
        
        if (metersArray) {
            for (DSMeter *meter in currentDss.meters) {
                meter.dirty = YES;
            }
            
            for (NSMutableDictionary *dict in metersArray) {
                
                NSString *dsid = [dict objectForKey:@"dsid"];
//                NSNumber *powerConsumption = [dict objectForKey:@"powerConsumption"];
//                NSNumber *energyMeterValue = [dict objectForKey:@"energyMeterValue"];
                NSNumber *apiVersion = [dict objectForKey:@"apiVersion"];
                NSString *name = [dict objectForKey:@"name"];
                NSNumber *meteringEnabled = [dict objectForKey:@"hasMetering"];
                
                DSMeter *meter = nil;
                
                for (DSMeter *currentMeter in currentDss.meters) {
                    if ([currentMeter.identifier isEqualToString:dsid]) {
                        meter = currentMeter;
                        currentMeter.dirty = NO;
                        break;
                    }
                }
                
                if (!meter) {
                    meter = [NSEntityDescription insertNewObjectForEntityForName:@"DSMeter" inManagedObjectContext:self.objectContext];
                    meter.dirty = NO;
                    meter.identifier = dsid;
                    [currentDss addMetersObject:meter];
                }
                
                meter.apiVersion = apiVersion;
                meter.meteringEnabled = [meteringEnabled boolValue];
                meter.name = name;
                
//                meter.powerConsumption = powerConsumption;
//                //there's a bug that returns negative meter value. Recalculate:
//                if([energyMeterValue intValue] < 0){
//                    float tempValue = [energyMeterValue floatValue];
//                    tempValue = tempValue * 3600;
//                    tempValue += powf(2,32);
//                    tempValue = tempValue / 3600;
//                    meter.energyMeterValue = [NSNumber numberWithFloat: tempValue];
//                }
//                else{
//                    meter.energyMeterValue = energyMeterValue;
//                }

                
                for (DSRoom *room in currentDss.rooms) {
                    
                    if ([room.meterID isEqualToString:meter.identifier]) {
                        room.meter = meter;
                    }
                }
            }
            
            //	dirty items abräumen
            NSMutableArray *dirtyMeters = [NSMutableArray arrayWithCapacity:1];
            for (DSMeter *meter in currentDss.meters) {
                if (meter.dirty) {
                    [dirtyMeters addObject:meter];
                }
            }
            
            for (DSMeter *meter in [NSMutableArray arrayWithArray:[currentDss.meters allObjects]]) {
                if (meter.dirty) {
                    [currentDss removeMetersObject:meter];
                    for (DSRoom *room in currentDss.rooms) {
                        if (room.meter && [room.meter.identifier isEqualToString:meter.identifier]) {
                            room.meter = nil;
                            room.meterID = nil;
                        }
                    }
                    [self.objectContext deleteObject:meter];
                }
            }
            
            NSError * error = nil;
            [self.objectContext save:&error];
            
            if (error) {
                
            }
        }
        
        self.meters = [NSMutableArray arrayWithArray:[currentDss.meters allObjects]];
        
    });
}



//Inits the rooms in the aparment.
//Checks if the rooms already exist in the CoreData-Database. If not one new room gets created.
-(BOOL) updateRoomAndDevicesWithDict: (NSMutableDictionary*)roomDict {
    
    NSString *identifier = [[roomDict valueForKey:@"identifier"] stringValue];
	NSString *name = [roomDict valueForKey:@"name"];
	NSArray *deviceArray = [roomDict objectForKey:@"devices"];
	
	BOOL updated = NO;
	
	// nach raum im dss suchen:
	DSRoom *r = [currentDss getRoom:identifier];
    
	if (!r) {
		// raum nicht gefunden:
		
		DSRoom *room = [NSEntityDescription insertNewObjectForEntityForName:@"DSRoom" inManagedObjectContext:self.objectContext];
		room.dss = currentDss;
		
		[currentDss addRoomsObject:room];
		[room setValue:name forKey:@"name"];
		[room setValue:[NSString stringWithFormat:@"%@", identifier] forKey:@"identifier"];
        
		r = room;
		r.dirty = NO;
		updated = YES;
		
	}
	else {
		// raum gefunden:
		r.dirty = NO;
		[r setValue:name forKey:@"name"];
	}
	
	//	geräte checken:
	
	//	Alle Geräte als dirty markieren
	for (DSDevice *device in r.devices) {
		device.dirty = YES;
	}
	
	for (NSDictionary *deviceDict in deviceArray) {
        NSString *deviceIdentifier;
        NSString *old_dsid = nil;
    
        if([deviceDict objectForKey:@"dSUID"]){
            deviceIdentifier = [deviceDict objectForKey:@"dSUID"];
            old_dsid = [deviceDict objectForKey:@"id"];
        }
        else{
            deviceIdentifier = [deviceDict objectForKey:@"id"];
        }
        
        NSString *meterID;
        if([deviceDict objectForKey:@"meterDSUID"]){
            meterID = [deviceDict objectForKey:@"meterDSUID"];
        }
        else{
            meterID = [deviceDict valueForKey:@"meterDSID"];
        }
        
		NSString *deviceName = [deviceDict objectForKey:@"name"];
        NSString *gtin = [deviceDict valueForKey:@"GTIN"];
        NSString *oemEanNumber =[deviceDict valueForKey:@"OemEanNumber"];
		NSNumber *powerOn = [NSNumber numberWithBool:[[deviceDict valueForKey:@"on"] boolValue]];
		BOOL isPresent = [[deviceDict valueForKey:@"isPresent"] boolValue];
        NSNumber *outputMode = [deviceDict valueForKey:@"outputMode"];
        NSNumber *productId = [deviceDict valueForKey:@"productID"];
        
        NSArray *groupArray = [deviceDict valueForKey:@"groups"];
        NSString *groupNumber = @"0";
        if([groupArray count] > 0){
            //the first group number is what the device behaves as (in case of black, what it simulates)
            groupNumber = [NSString stringWithFormat:@"%@",[groupArray objectAtIndex:0]];
        }
		
		DSDevice *d = nil;
		
		for (DSDevice *dev in r.devices) {
			if ([dev.identifier isEqualToString:deviceIdentifier]) {
				d = dev;
			}
		}
		
		if (d && isPresent) {
			d.powerOn = powerOn;
			d.name = deviceName;
            d.outputMode = outputMode;
			d.dirty = NO;
            d.group = groupNumber;
            d.type = productId;
            d.gtin = gtin;
            d.oemEanNumber = oemEanNumber;
            d.old_dsid = old_dsid;
			
			r.meterID = meterID;
			if ([currentDss getMeter:meterID]) {
				r.meter = [currentDss getMeter:meterID];
			}
		}
		else if (!d && isPresent) {
			[r addDevicesObject:[self createDevice: deviceDict withGroup:nil withRoom:r]];
			updated = YES;
			
			r.meterID = meterID;
			if ([currentDss getMeter:meterID]) {
				r.meter = [currentDss getMeter:meterID];
			}
		}
	}
	
	//	Alle dirty devices abräumen
	
	NSMutableArray *dirtyDevices = [NSMutableArray arrayWithCapacity:1];
	for (DSDevice *device in r.devices) {
		if (device.dirty) {
			[dirtyDevices addObject:device];
		}
	}
	
	for (DSDevice *device in dirtyDevices) {
		if (device.dirty) {
			[r removeDevicesObject:device];
			[self.objectContext deleteObject:device];
			updated = YES;
		}
	}
    
	NSError * error = nil;
	[self.objectContext save:&error];
	
	return updated;
	
}

//Update reachable groups for rooms, using the room dict for non-ds devices
- (void) updateReachableGroupsWithNonDsDeviceDict: (NSMutableDictionary*)roomDict; {
    
    NSString *zoneId = [NSString stringWithFormat:@"%i", [[roomDict objectForKey:@"ZoneID"] intValue]];
	NSArray *groups = [roomDict objectForKey:@"groups"];
	
	//look up room in database:
	DSRoom *r = [currentDss getRoom:zoneId];
	
    if(r){
        r.group1present = NO;
        r.group2present = NO;
        r.group3present = NO;
        r.group4present = NO;
        r.group5present = NO;
        
        //	check is any groups have (non-ds) connected devices:
        for (int i =0 ; i < [groups count]; i++){
            
            if([[[groups objectAtIndex:i] objectForKey: @"connectedDevices"]intValue] > 0){
                int groupNumber = [[[groups objectAtIndex:i] objectForKey:@"group"]intValue];
                switch (groupNumber) {
                    case 1:
                        r.group1present = YES;
                        break;
                    case 2:
                        r.group2present = YES;
                        break;
                    case 3:
                        r.group3present = YES;
                        break;
                    case 4:
                        r.group4present = YES;
                        break;
                    case 5:
                        r.group5present = YES;
                        break;
                        
                    default:
                        break;
                        
                }
            }
        }
        
        //then check the devices
        for (DSDevice *device in r.devices){
            if([device.group isEqualToString:@"1"]){
                r.group1present = YES;
            }
            else if([device.group isEqualToString:@"2"]){
                r.group2present = YES;
            }
            else if([device.group isEqualToString:@"3"]){
                r.group3present = YES;
            }
            else if([device.group isEqualToString:@"4"]){
                r.group4present = YES;
            }
            else if([device.group isEqualToString:@"5"]){
                r.group5present = YES;
            }
        }

        
        NSError * error = nil;
        [self.objectContext save:&error];
    }
	
}



//Bereinigt die Datenbank zu Applikationsstart, d.h. Räume die sich nicht mehr in der Struktur befinden bzw. aus
//der Struktur eines anderen dSS stammen werden aus der Datenbnak gelöscht.
- (BOOL) updateDatabase {
	BOOL updated = NO;
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSRoom" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	
	for (int i = 0; i<[fetchedObjects count]; i++) {
		DSRoom *r = [fetchedObjects objectAtIndex:i];
		BOOL found = NO;
		for (int j = 0; j<[newRooms count]; j++) {
			if ([r.identifier isEqualToString:((DSRoom*)[newRooms objectAtIndex:j]).identifier] &&
				[r.name isEqualToString:((DSRoom*)[newRooms objectAtIndex:j]).name]) {
				found = YES;
				break;
			}
		}
		if (!found) {
			[self.objectContext deleteObject:r];
			[self.objectContext save:nil];
			updated = YES;
		}
	}
	
	[fetchRequest release];
	return updated;
}

//Checks if the specified device is already inside the CoreData-Database. If yes the device is returned.
- (DSDevice*) getExistingDeviceInCoreData:(NSString *)identifier withPowerOn:(NSNumber*)powerOn {
	DSDevice *d;
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSDevice" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier == %@", identifier];
	[fetchRequest setPredicate:predicate];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	
	if ([fetchedObjects count] > 0) {
		d = [fetchedObjects objectAtIndex:0];
	} else {
		d = nil;
	}
	
	[fetchRequest release];
	
	return d;
}


//Returns an array of all rooms in the core data-database in case there is no connection to one dSS.
- (NSArray*) getRoomsInCoreData {
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSRoom" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	
	[fetchRequest release];
	
	return fetchedObjects;
}

//Creates one "empty" device, with only identifier and name.
- (DSDevice *) createDevice: (NSDictionary*)deviceDict withGroup:(NSDictionary*)groupDict withRoom:(DSRoom*)room {
	NSString *identifier;
    NSString *old_dsid;
    if([deviceDict objectForKey:@"dSUID"]){
        identifier = [deviceDict objectForKey:@"dSUID"];
        old_dsid = [deviceDict objectForKey:@"id"];
    }
    else{
        identifier = [deviceDict objectForKey:@"id"];
    }

    NSString *name = [deviceDict objectForKey:@"name"];
    NSNumber *outputMode = [deviceDict valueForKey:@"outputMode"];
    NSString *gtin = [deviceDict objectForKey:@"GTIN"];
    NSString *oemEanNumber =[deviceDict objectForKey:@"OemEanNumber"];
    NSNumber *powerOn = [NSNumber numberWithBool:[[deviceDict objectForKey:@"on"] boolValue]];
    NSNumber *productId = [deviceDict objectForKey:@"productID"];
    
    NSArray *groupArray = [deviceDict objectForKey:@"groups"];
    NSString *groupNumber = @"0";
    if([groupArray count] > 0){
        groupNumber = [NSString stringWithFormat:@"%@",[groupArray objectAtIndex:0]];
    }
    
	if ([name isEqualToString:@" "] || [name isEqualToString:@""]) {
		name=identifier;
	}
	
	DSDevice *device = [NSEntityDescription insertNewObjectForEntityForName:@"DSDevice" inManagedObjectContext:self.objectContext];
	device.dirty = NO;
    device.name = name;
    device.identifier = [NSString stringWithFormat:@"%@", identifier];
    device.old_dsid = old_dsid;
    device.powerOn = powerOn;
    device.outputMode = outputMode;
    device.group = groupNumber;
    device.type = productId;
    device.gtin = gtin;
    device.oemEanNumber = oemEanNumber;
    device.room = room;
	[room addDevicesObject:device];
    
	NSError * error = nil;
	[self.objectContext save:&error];
	
	return device;
}

- (int) getProductID: (int)productIDBaseTen {
	//Für das Auslesen der Geräteklasse muss ein Mapping in Binär erfolgen bei dem die 12+ Bits beachtet werden müssen
	NSMutableString *str = [[NSMutableString alloc] init];
	for(NSInteger numberCopy = productIDBaseTen; numberCopy > 0; numberCopy >>= 1) {
		[str insertString:((numberCopy & 1) ? @"1" : @"0") atIndex:0];
	}
	
	NSMutableString *strBinary = [[NSMutableString alloc] init];
	
	if ([str length]>10) {
		for (int i = 0; i<[str length]-10; i++) {
			unichar ch = [str characterAtIndex:i];
            //			GSLog(@"Char: %c %@ %i", ch, str, [str length]);
			[strBinary appendString:[NSString stringWithCharacters:&ch length:1 ]];
		}
	} else if ([str length]<10) {
		unichar ch = '0';
		[strBinary appendString:[NSString stringWithCharacters:&ch length:1 ]];
	}
    
	int productIDBaseTwo = [strBinary intValue];
	[strBinary release];
    [str release];
    
    
	return productIDBaseTwo;
}



#pragma mark -
#pragma mark Device Methods

//Retruns a boolean-value if the power-state of the specified device could been successfully set.
- (BOOL) setDevicePower: (DSDevice*)device withBool:(BOOL) powerOn withGroup:(int)group{
	if ([service setDevicePower:device withBool:powerOn withGroup:group]) {
		dispatch_sync(dispatch_get_main_queue(), ^{
			[device setPowerOn:[NSNumber numberWithBool:powerOn]];
			NSError * error = nil;
			[self.objectContext save:&error];
			if (error) {
				GSLog(@"error: %@", [error userInfo]);
			}
		});
        
		return YES;
	} else {
		
		if (useDemo) {
			dispatch_sync(dispatch_get_main_queue(), ^{
				[device setPowerOn:[NSNumber numberWithBool:powerOn]];
				NSError * error = nil;
				[self.objectContext save:&error];
				if (error) {
					GSLog(@"error: %@", [error userInfo]);
				}
			});
			
			return YES;
		} else {
			return NO;
		}
        
		
		
	}
}

//Retruns a boolean-value if the special-value of the specified device could been successfully set.
- (BOOL) setDeviceValue: (DSDevice*)device withValue: (NSNumber*) value {
    NSNumber *dimmValue;
    
    //shade devices use 16bit
    if([device.group isEqualToString:@"2"])
        dimmValue = [NSNumber numberWithFloat:65535*[value floatValue]];
    else
        dimmValue = [NSNumber numberWithFloat:255*[value floatValue]];
	
	if ([service setDeviceValue:device withValue:dimmValue]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            [device setDimmFactor:value];
            NSError * error = nil;
            [self.objectContext save:&error];
        });
		return YES;
	} else {
		if (useDemo) {
			dispatch_sync(dispatch_get_main_queue(), ^{
				[device setDimmFactor:value];
				NSError * error = nil;
				[self.objectContext save:&error];
				if (error) {
					GSLog(@"error: %@", [error userInfo]);
				}
			});
			
			return YES;
		} else {
			return NO;
		}
	}
}

- (BOOL) setShadeAngle: (DSDevice*)device withValue: (NSNumber*) value {
    
    NSNumber *angleValue= [NSNumber numberWithFloat:255*[value floatValue]];
	
	if ([service setShadeAngle:device withAngle:angleValue]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            [device setAngleFactor:value];
            NSError * error = nil;
            [self.objectContext save:&error];
        });
		return YES;
    }
    else
        return NO;
}

//Retruns a boolean-value if the name of the specified device could been successfully set.
- (BOOL) setDeviceName: (DSDevice*)device withString: (NSString*) name {
    //#7115 Store the value before saving to avoid timing problems
    [device setName:name];
    __block NSError * error = nil;
    
	if ([service setDeviceName:device withString:name]) {
        [self.objectContext save:&error];
        if (error) {
            GSLog(@"error: %@", [error userInfo]);
        }
		return YES;
	} else {
		if (useDemo) {
			dispatch_sync(dispatch_get_main_queue(), ^{
				[self.objectContext save:&error];
				if (error) {
					GSLog(@"error: %@", [error userInfo]);
				}
			});
			
			return YES;
		} else {
            //#7115 Failure while saving, reset the old values
            [self.objectContext refreshObject:device mergeChanges:NO];
			return NO;
		}
		
	}
}

- (BOOL) moveDevice: (DSDevice*)device toRoom: (DSRoom*) room{
    if ([service moveDevice:device toRoom:room]) {
		[device setRoom:room];
        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_DEVICELIST_HAS_CHANGED];
		NSError * error = nil;
		[self.objectContext save:&error];
		return YES;
	} else {
		if (useDemo) {
			dispatch_sync(dispatch_get_main_queue(), ^{
                [device setRoom:room];
                [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_DEVICELIST_HAS_CHANGED];
				NSError * error = nil;
				[self.objectContext save:&error];
				if (error) {
					GSLog(@"error: %@", [error userInfo]);
				}
			});
			
			return YES;
		} else {
			return NO;
		}
	}
    
}

- (BOOL) setOutputModeForDevice: (DSDevice*)device toMode: (int) newMode{
    if ([service setOutputModeForDevice:device toMode:newMode]) {
		[device setOutputMode:[NSNumber numberWithInt:newMode]];
        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_DEVICELIST_HAS_CHANGED];
		NSError * error = nil;
		[self.objectContext save:&error];
		return YES;
	} else {
		if (useDemo) {
			dispatch_sync(dispatch_get_main_queue(), ^{
                [device setOutputMode:[NSNumber numberWithInt:newMode]];
                [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_DEVICELIST_HAS_CHANGED];
				NSError * error = nil;
				[self.objectContext save:&error];
				if (error) {
					GSLog(@"error: %@", [error userInfo]);
				}
			});
			
			return YES;
		} else {
			return NO;
		}
	}
    
}

//Sets several properties like powerOn etc. of one specified device.
- (void) setDeviceProperties: (NSMutableDictionary*) deviceDict {
    dispatch_sync(dispatch_get_main_queue(), ^{
        NSString *identifier = [deviceDict objectForKey:@"identifier"];
        NSString *name = [deviceDict valueForKey:@"name"];
        NSNumber *powerOn = [NSNumber numberWithBool:[[deviceDict valueForKey:@"powerOn"] boolValue]];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSDevice" inManagedObjectContext:self.objectContext];
        [fetchRequest setEntity:entity];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier == %@", identifier];
        [fetchRequest setPredicate:predicate];
        
        NSError * error = nil;
        NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
        
        if ([fetchedObjects count]>0) {
            DSDevice *device = [fetchedObjects objectAtIndex:0];
            [device setPowerOn:powerOn];
            if (![name isEqualToString:@""]) [device setName:name];
        }
        
        [self.objectContext save:&error];
        
        [fetchRequest release];
    });
}


- (BOOL) updateValueOfDevice: (DSDevice*)device{
    
    //if it's a jalosie device, we need to get 2 values
    if([device.type isEqualToNumber:[NSNumber numberWithInt:3292]]){
        
        NSDictionary *newValues = [service getOutputsOfShadeDevice:device];
        
        if(newValues){
            int newPosition = (int)[[newValues objectForKey:@"position"] integerValue];
            int newAngle = (int)[[newValues objectForKey:@"angle"] integerValue];
            dispatch_async(dispatch_get_main_queue(), ^{
                //we want a factor of 0-1 , position is a 16bit value
                float dimmFactor = (float)newPosition/65535.0f;
                device.dimmFactor = [NSNumber numberWithFloat:dimmFactor];
                //angle factor is 8 bit
                float angleFactor = (float)newAngle/255.0f;
                device.angleFactor = [NSNumber numberWithFloat:angleFactor];
            });
            return YES;
        }
        else{
            return NO;
        }
    }
    else{
        
        int newValue = [service getOutputOfDevice:device];
        
        if(newValue != -1){
            //if dimable we want to update the dimmFactor
            dispatch_async(dispatch_get_main_queue(), ^{
                //                if([device.outputMode isEqualToNumber:[NSNumber numberWithInt:22]]){
                //
                //                    //we want a dimmfactor of 0-1
                //                    float dimmFactor = (float)newValue/255.0f;
                //                    device.dimmFactor = [NSNumber numberWithFloat:dimmFactor];
                //                }
                
                //position values special case, 16bit
                if([device.outputMode isEqualToNumber:[NSNumber numberWithInt:33]]){
                    float dimmFactor = (float)newValue/65535.0f;
                    device.dimmFactor = [NSNumber numberWithFloat:dimmFactor];
                }
                
                //it's switchable, update powerOn instead (we also update dimmfactor for showing in device detail)
                else{
                    //standard threshold
                    if(newValue>128){
                        device.powerOn = [NSNumber numberWithBool:YES];
                        float dimmFactor = (float)newValue/255.0f;
                        device.dimmFactor = [NSNumber numberWithFloat:dimmFactor];
                    }
                    else{
                        device.powerOn = [NSNumber numberWithBool:NO];
                        float dimmFactor = (float)newValue/255.0f;
                        device.dimmFactor = [NSNumber numberWithFloat:dimmFactor];
                    }
                }
            });
            return YES;
        }
        else
            return NO;
    }
}

- (void) updateConsumptionValuesOfDevice: (DSDevice*)device{
    
    int newConsumption = [service getDeviceConsumption:device];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        device.consumption = [NSNumber numberWithInt:newConsumption];
    });
    
    int newMetering = [service getDeviceMetering:device];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        device.metering = [NSNumber numberWithInt:newMetering];
    });
}

#pragma mark -
#pragma mark Room Methods

//Retruns a boolean-value if the specified room could be successfully renamed.
- (BOOL) setRoomName: (NSString*) name withRoom: (DSRoom*) room {
	if ([service setRoomName:name withRoom:room]) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[room setName:name];
		});
		
		NSError * error = nil;
		[self.objectContext save:&error];
		return YES;
	} else {
		return NO;
	}
}

//Retruns a boolean-value if the power-state of the specified room could been successfully set.
- (BOOL) setRoomPower: (DSRoom*)room withBool:(BOOL) powerOn {
	return ([service setRoomPower:room withBool:powerOn]);
}


#pragma mark -
#pragma mark Scene Methods



- (BOOL) createScene: (NSString*)sceneName withRoom:(DSRoom*)room withGroup:(NSNumber*)group favouriteScene:(BOOL)favouriteFlag {
	
	int min = 34;
	for (int i = 34; i<64; i++) {
		
		BOOL found = NO;
		
		for (DSScene *s in self.scenes) {
			if (i == [s.sceneNo intValue]) {
				found = YES;
				break;
			}
		}
		
		if (!found) {
			min = i;
			break;
		}
	}
	
    //	GSLog(@"min: %i", min);
	
	
	BOOL nameFail = NO;
	if (sceneName == nil || [sceneName isEqualToString:@""]) {
		nameFail = YES;
	} else if ([sceneName length] == 7) {
		
		NSRange range = [sceneName rangeOfString:@"Scene"];
		if (range.location != NSNotFound){
			NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
			if ([f numberFromString:[NSString stringWithFormat:@"%c", [sceneName characterAtIndex:5]]] != nil) {
				if ([f numberFromString:[NSString stringWithFormat:@"%c", [sceneName characterAtIndex:6]]] != nil) {
					nameFail = YES;
				}
				
			}
            [f release];
		}
		
	} else if ([sceneName length] == 6) {
		NSRange range = [sceneName rangeOfString:@"Scene"];
		if (range.location != NSNotFound){
			NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
			if ([f numberFromString:[NSString stringWithFormat:@"%c", [sceneName characterAtIndex:5]]] != nil) {
				
                nameFail = YES;
				
				
			}
            [f release];
		}
	}
	
	if (nameFail == NO) {
		return [self createScene:[NSNumber numberWithInt:min] withName:sceneName withRoom:room withGroup:group favouriteScene:favouriteFlag];
	} else {
		return NO;
	}
	
}



//Set's up one scene.
- (BOOL) createScene: (NSNumber*)sceneNo withName:(NSString*)sceneName withRoom:(DSRoom*)room withGroup:(NSNumber*)group favouriteScene:(BOOL)favouriteFlag {
	DSScene *scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:group favouriteScene:favouriteFlag showOnList:NO];
	return [self setSceneName:scene withName:sceneName withRoom:room];
}

//Creates one scene and save it in property "scene".
- (DSScene*) createDSScene: (NSNumber*)sceneNo withName:(NSString*)sceneName withRoom:(DSRoom*)room withGroup:(NSNumber*)group favouriteScene:(BOOL)favouriteFlag showOnList:(BOOL)listFlag {
	DSScene *scene = [NSEntityDescription insertNewObjectForEntityForName:@"DSScene" inManagedObjectContext:self.objectContext];
	scene.dirty = NO;
	[scene setValue:sceneName forKey:@"name"];
	[scene setValue:sceneNo forKey:@"sceneNo"];
    [scene setValue:group forKey:@"group"];
	[scene setValue:(favouriteFlag ? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0]) forKey:@"favourit"];
    [scene setValue:(listFlag ? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0]) forKey:@"mainList"];
	
	if (room) {
		[scene setValue:room forKey:@"room"];
		[room addScenesObject:scene];
	}
	scene.dss = currentDss;
	[currentDss addScenesObject:scene];
	
	NSError * error = nil;
	[self.objectContext save:&error];
	//[self.scenes addObject:scene];
	
	return scene;
}

- (BOOL) setSceneName: (DSScene*)scene withName:(NSString*)sceneName withRoom:(DSRoom*)room {
	
	if ([service setSceneName:sceneName withRoom:room withScene:scene]) {
		scene.name = sceneName;
		
		NSError * error = nil;
		[self.objectContext save:&error];
		
		return YES;
	} else {
		return NO;
	}
}

- (BOOL) deleteScene: (DSScene*)scene withRoom:(DSRoom*)room {
	if ([service setSceneName:@"" withRoom:room withScene:scene]) {
		
		[self.scenes removeObject:scene];
		[room removeScenesObject:scene];
		[self.objectContext deleteObject:scene];
		
		NSError * error = nil;
		[self.objectContext save:&error];
		
		return YES;
	} else {
		return NO;
	}
	
}

- (BOOL) saveContext {
	NSError * error = nil;
	[self.objectContext save:&error];
	
	if (!error) {
		
		if (currentDss) {
			self.rooms = [NSMutableArray arrayWithArray:[currentDss.rooms allObjects]];
			self.scenes = [NSMutableArray arrayWithArray:[currentDss.scenes allObjects]];
			//self.meters = [NSMutableArray arrayWithArray:[currentDss.meters allObjects]];
		}
		
		return YES;
	}
	
	return NO;
}

//Sets the favourite-Flag for one DSScene.
- (void) setSceneFavouriteFlag: (DSScene*) scene withFavourite:(BOOL)favouriteFlag {
	NSNumber *identifier = scene.sceneNo;
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSScene" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sceneNo == %@", identifier];
	[fetchRequest setPredicate:predicate];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
	
	if ([fetchedObjects count]>0) {
		DSScene *s = [fetchedObjects objectAtIndex:0];
		[s setFavourit:(favouriteFlag ? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0])];
	}
	
	[self.objectContext save:&error];
}

//Loads the scenes from the CoreData-Database.
- (void) loadScenesFromCoreData {
	if ([self.scenes count]>0) {
		[self.scenes removeAllObjects];
	}
	
	NSMutableArray *newScenes = [[NSMutableArray alloc] init];
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSScene" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	for (DSScene *scene in fetchedObjects) {
		[newScenes addObject:scene];
	}
	[fetchRequest release];
	
	self.scenes = newScenes;
    [newScenes release];
}

- (void) loadApartmentScenes {
	
	BOOL panicSceneFound = NO;
	BOOL comingSceneFound = NO;
	BOOL goingSceneFound = NO;
    BOOL bellSceneFound = NO;
	
	for (DSScene *scene in currentDss.scenes) {
		if (scene.room) {
			continue;
		}
		if ([scene.sceneNo longLongValue] == 65) {
			panicSceneFound = YES;
		}
		
		if ([scene.sceneNo longLongValue] == 71) {
			comingSceneFound = YES;
		}
		
		if ([scene.sceneNo longLongValue] == 72) {
			goingSceneFound = YES;
		}
        
        if ([scene.sceneNo longLongValue] == 73) {
			bellSceneFound = YES;
		}
        
	}
	
	dispatch_sync(dispatch_get_main_queue(), ^{
		if (!panicSceneFound) {
			[self createDSScene:[NSNumber numberWithInt:65] withName:NSLocalizedString(@"key_activity_panic", @"") withRoom:nil withGroup:[NSNumber numberWithInt:0] favouriteScene:NO showOnList:YES];
		}
		
		if (!comingSceneFound) {
			[self createDSScene:[NSNumber numberWithInt:71] withName:NSLocalizedString(@"key_activity_kommen", @"") withRoom:nil withGroup:[NSNumber numberWithInt:0] favouriteScene:NO showOnList:YES];
		}
		
		if (!goingSceneFound) {
			[self createDSScene:[NSNumber numberWithInt:72] withName:NSLocalizedString(@"key_activity_gehen", @"") withRoom:nil withGroup:[NSNumber numberWithInt:0] favouriteScene:YES showOnList:YES];
		}
        
        if (!bellSceneFound) {
			[self createDSScene:[NSNumber numberWithInt:73] withName:NSLocalizedString(@"key_activity_bell", @"") withRoom:nil withGroup:[NSNumber numberWithInt:0] favouriteScene:NO showOnList:YES];
		}
		
		if (!panicSceneFound || !comingSceneFound || !goingSceneFound || !bellSceneFound) {
			[[DataController instance] saveContext];
			
			[[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_FAVORITELIST_HAS_CHANGED];
			[[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_SCENELIST_HAS_CHANGED];
		}
	});
}

- (void) loadDemoScenes {
	if ([currentDss.scenes count] > 0) {
		return;
	}
	
	[self createDSScene:[NSNumber numberWithInt:65] withName:NSLocalizedString(@"key_activity_panic", @"") withRoom:nil withGroup:[NSNumber numberWithInt:0] favouriteScene:NO showOnList:YES];
	[self createDSScene:[NSNumber numberWithInt:71] withName:NSLocalizedString(@"key_activity_kommen", @"") withRoom:nil withGroup:[NSNumber numberWithInt:0] favouriteScene:NO showOnList:YES];
	[self createDSScene:[NSNumber numberWithInt:72] withName:NSLocalizedString(@"key_activity_gehen", @"") withRoom:nil withGroup:[NSNumber numberWithInt:0] favouriteScene:YES showOnList:YES];
    [self createDSScene:[NSNumber numberWithInt:73] withName:NSLocalizedString(@"key_activity_bell", @"") withRoom:nil withGroup:[NSNumber numberWithInt:0] favouriteScene:NO showOnList:YES];
	
	for (DSRoom *room in self.rooms) {
		NSMutableArray *roomScenes = [self allocDemoSceneArray:room];
        NSMutableArray *roomShadeScenes = [self allocDemoSceneArrayShades:room];
		
		if ([room.scenes count] == 0) {
			for (NSDictionary *dict in roomScenes) {
				NSNumber *sceneNo = [dict objectForKey:@"id"];
				NSString *sceneName = [dict objectForKey:@"name"];
				[self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:[NSNumber numberWithInt:1] favouriteScene:NO showOnList:YES];
			}
            if([roomShadeScenes count]>1){
                for (NSDictionary *dict in roomShadeScenes) {
                    NSNumber *sceneNo = [dict objectForKey:@"id"];
                    NSString *sceneName = [dict objectForKey:@"name"];
                    [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:[NSNumber numberWithInt:2] favouriteScene:NO showOnList:YES];
                }
            }
		}
		else {
            //			GSLog(@"DemoScenes already loaded");
		}
        [roomScenes release];
        [roomShadeScenes release];
	}
	
	[[DataController instance] saveContext];
}

- (void) loadDemoMeters {
	if ([currentDss.meters count] > 0) {
		return;
	}
	
	[self createDemoMeter:NSLocalizedString(@"key_demo_meter_living_room", @"") withId:@"3504175fe0000010000004ce"];
}


- (NSMutableArray*) allocDemoSceneArray: (DSRoom*)room {
	NSMutableArray *sceneArray = [[NSMutableArray alloc] init];
	
	NSDictionary *standby = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], NSLocalizedString(@"key_default_scene_name_0", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
	[sceneArray addObject:standby];
    [standby release];
	
	if ([room.name isEqualToString:NSLocalizedString(@"key_demo_room_living_room", @"")]) {
		NSDictionary *scene1Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], NSLocalizedString(@"key_demo_act_relax", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene2Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], NSLocalizedString(@"key_demo_act_tv", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene3Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], NSLocalizedString(@"key_demo_act_eat", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene4Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], NSLocalizedString(@"key_demo_act_read", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
        
		
		[sceneArray addObject:scene1Dict];
		[sceneArray addObject:scene2Dict];
		[sceneArray addObject:scene3Dict];
		[sceneArray addObject:scene4Dict];
        
        [scene1Dict release];
        [scene2Dict release];
        [scene3Dict release];
        [scene4Dict release];
        
		
	} else if ([room.name isEqualToString:NSLocalizedString(@"key_demo_room_kitchen", @"")]) {
		NSDictionary *scene7Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], NSLocalizedString(@"key_demo_act_cooking", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene8Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], NSLocalizedString(@"key_demo_act_apero", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene9Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:9], NSLocalizedString(@"key_demo_act_breakfast", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene10Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:10], NSLocalizedString(@"key_demo_act_party", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
        
        
		[sceneArray addObject:scene7Dict];
		[sceneArray addObject:scene8Dict];
		[sceneArray addObject:scene9Dict];
		[sceneArray addObject:scene10Dict];
        
        [scene7Dict release];
        [scene8Dict release];
        [scene9Dict release];
        [scene10Dict release];
        
		
	} else if ([room.name isEqualToString:NSLocalizedString(@"key_demo_room_bedroom", @"")]) {
		NSDictionary *scene12Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:12], NSLocalizedString(@"key_demo_act_relax", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene13Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:13], NSLocalizedString(@"key_demo_act_read", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene14Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:14], NSLocalizedString(@"key_demo_act_wakeup", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene15Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:15], NSLocalizedString(@"key_demo_act_cleaning", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
        
		[sceneArray addObject:scene12Dict];
		[sceneArray addObject:scene13Dict];
		[sceneArray addObject:scene14Dict];
		[sceneArray addObject:scene15Dict];
        [scene12Dict release];
        [scene13Dict release];
        [scene14Dict release];
        [scene15Dict release];
        
	} else if ([room.name isEqualToString:NSLocalizedString(@"key_demo_room_hall", @"")]) {
		NSDictionary *scene16Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:16], NSLocalizedString(@"key_demo_act_save", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene17Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:17], NSLocalizedString(@"key_demo_act_party", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene18Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:18], NSLocalizedString(@"key_demo_act_cleaning", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
        NSDictionary *scene19Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:19], NSLocalizedString(@"key_demo_act_relax", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		
		[sceneArray addObject:scene16Dict];
		[sceneArray addObject:scene17Dict];
		[sceneArray addObject:scene18Dict];
        [sceneArray addObject:scene19Dict];
        
        [scene16Dict release];
        [scene17Dict release];
        [scene18Dict release];
        [scene19Dict release];
	}
	
	return sceneArray;
}

- (NSMutableArray*) allocDemoSceneArrayShades: (DSRoom*)room {
	NSMutableArray *sceneArray = [[NSMutableArray alloc] init];
    NSDictionary *standby = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], NSLocalizedString(@"key_default_shade_scene_name_0", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
    [sceneArray addObject:standby];
    [standby release];
	
	if ([room.name isEqualToString:NSLocalizedString(@"key_demo_room_living_room", @"")]) {
		NSDictionary *scene1Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], NSLocalizedString(@"key_demo_act_shade1", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene2Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], NSLocalizedString(@"key_demo_act_shade2", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene3Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], NSLocalizedString(@"key_demo_act_shade3", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene4Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], NSLocalizedString(@"key_demo_act_shade4", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		
		[sceneArray addObject:scene1Dict];
		[sceneArray addObject:scene2Dict];
		[sceneArray addObject:scene3Dict];
		[sceneArray addObject:scene4Dict];
        
        [scene1Dict release];
        [scene2Dict release];
        [scene3Dict release];
        [scene4Dict release];
		
		
	} else if ([room.name isEqualToString:NSLocalizedString(@"key_demo_room_bedroom", @"")]) {
		NSDictionary *scene12Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:12], NSLocalizedString(@"key_demo_act_shade1", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene13Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:13], NSLocalizedString(@"key_demo_act_shade2", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene14Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:14], NSLocalizedString(@"key_demo_act_shade3", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
		NSDictionary *scene15Dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:15], NSLocalizedString(@"key_demo_act_shade4", @""), nil] forKeys:[NSArray arrayWithObjects:@"id",@"name", nil]];
        
        
		[sceneArray addObject:scene12Dict];
		[sceneArray addObject:scene13Dict];
		[sceneArray addObject:scene14Dict];
		[sceneArray addObject:scene15Dict];
        [scene12Dict release];
        [scene13Dict release];
        [scene14Dict release];
        [scene15Dict release];
    }
    
	return sceneArray;
}



- (BOOL) updateScenesWithDict: (NSDictionary*) scenesDict {
	//if ([service loginUser]) {
	BOOL updated = NO;
    NSNumber *currentGroup= nil;
	
	NSSet *myrooms = [NSSet setWithSet:currentDss.rooms];
	
	for (DSRoom *room in myrooms) {
		NSSet *myscenes = [NSSet setWithSet:room.scenes];
		
		for (DSScene *scene in myscenes) {
			scene.dirty = YES;
		}
		
		NSDictionary *_roomGroups = [scenesDict objectForKey: room.identifier];
        //		GSLog(@"groups: %@", _roomGroups);
		
		if ([_roomGroups objectForKey:@"lastCalledScene"]) {
            room.lastCalledScene = [[_roomGroups objectForKey:@"lastCalledScene"] stringValue];
		}
        if ([_roomGroups objectForKey:@"lastCalledShadeScene"]) {
            room.lastCalledShadeScene = [[_roomGroups objectForKey:@"lastCalledShadeScene"] stringValue];
		}
        if ([_roomGroups objectForKey:@"lastCalledBlueScene"]) {
            room.lastCalledBlueScene = [[_roomGroups objectForKey:@"lastCalledBlueScene"] stringValue];
		}
        if ([_roomGroups objectForKey:@"lastCalledAudioScene"]) {
            room.lastCalledShadeScene = [[_roomGroups objectForKey:@"lastCalledAudioScene"] stringValue];
		}
        if ([_roomGroups objectForKey:@"lastCalledVideoScene"]) {
            room.lastCalledBlueScene = [[_roomGroups objectForKey:@"lastCalledVideoScene"] stringValue];
		}
		
		//	Licht-Gruppe
		if (room.group1present) {
			NSArray *groupLightArray = [_roomGroups objectForKey:@"1"];
            currentGroup = [NSNumber numberWithInt:1];
			if (groupLightArray && [groupLightArray count] > 0) {
				for (NSArray *sceneArray in groupLightArray) {
					NSNumber *sceneNo = [sceneArray objectAtIndex:0];
					NSString *sceneName = [sceneArray objectAtIndex:1];
					DSScene *scene = [currentDss getScene:sceneNo inRoom:room withGroup:currentGroup];
					
					if (!scene) {
                        //per default we only show 0,5,17,18,19 on the list
                        if([sceneNo intValue] == 0 || [sceneNo intValue] == 5 ||[sceneNo intValue] == 17 ||[sceneNo intValue] == 18 ||[sceneNo intValue] == 19 ){
                            
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
                        }
                        else{
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:NO];
                        }
						updated = YES;
					}
					else {
						scene.name = sceneName;
						scene.dirty = NO;
					}
                    
					
				}
			}
			else {
				// server liefert kein ergebnis, räume NICHT löschen!!
				NSSet *myscenes = [NSSet setWithSet:room.scenes];
				
				for (DSScene *scene in myscenes) {
					scene.dirty = NO;
				}
			}
            
            //add standard scenes if needed
            bool scene0Found = NO;
            bool scene5Found = NO;
            bool scene17Found = NO;
            bool scene18Found = NO;
            bool scene19Found = NO;
            
            
            NSSet *myscenes = [NSSet setWithSet:room.scenes];
            
            for (DSScene *scene in myscenes) {
                if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene0Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:5]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene5Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:17]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene17Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:18]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene18Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:19]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene19Found = YES;
                    scene.dirty = NO;
                }
            }
            
            if (!scene0Found) {
                [self createDSScene:[NSNumber numberWithInt:0] withName:NSLocalizedString(@"key_default_scene_name_0", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene5Found) {
                [self createDSScene:[NSNumber numberWithInt:5] withName:NSLocalizedString(@"key_default_scene_name_5", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene17Found) {
                [self createDSScene:[NSNumber numberWithInt:17] withName:NSLocalizedString(@"key_default_scene_name_17", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene18Found) {
                [self createDSScene:[NSNumber numberWithInt:18] withName:NSLocalizedString(@"key_default_scene_name_18", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene19Found) {
                [self createDSScene:[NSNumber numberWithInt:19] withName:NSLocalizedString(@"key_default_scene_name_19", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            
		}
        
        //	Shade Gruppe
		if (room.group2present) {
			NSArray *groupShadeArray = [_roomGroups objectForKey:@"2"];
            currentGroup = [NSNumber numberWithInt:2];
			if (groupShadeArray && [groupShadeArray count] > 0) {
				for (NSArray *sceneArray in groupShadeArray) {
					NSNumber *sceneNo = [sceneArray objectAtIndex:0];
					NSString *sceneName = [sceneArray objectAtIndex:1];
					DSScene *scene = [currentDss getScene:sceneNo inRoom:room withGroup:currentGroup];
					
					if (!scene) {
                        //per default we only show 0,5,17,18,19 on the list
                        if([sceneNo intValue] == 0 || [sceneNo intValue] == 5 ||[sceneNo intValue] == 17 ||[sceneNo intValue] == 18 ||[sceneNo intValue] == 19 ){
                            
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
                        }
                        else{
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:NO];
                        }
						updated = YES;
					}
					else {
						scene.name = sceneName;
						scene.dirty = NO;
					}
                    
					
				}
			}
			else {
				// server liefert kein ergebnis, räume NICHT löschen!!
				NSSet *myscenes = [NSSet setWithSet:room.scenes];
				
				for (DSScene *scene in myscenes) {
					scene.dirty = NO;
				}
			}
            
            //add standard scenes if needed
            bool scene0Found = NO;
            bool scene5Found = NO;
            bool scene17Found = NO;
            bool scene18Found = NO;
            bool scene19Found = NO;
            
            
            NSSet *myscenes = [NSSet setWithSet:room.scenes];
            
            for (DSScene *scene in myscenes) {
                if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene0Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:5]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene5Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:17]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene17Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:18]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene18Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:19]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene19Found = YES;
                    scene.dirty = NO;
                }
            }
            
            if(!scene0Found){
                [self createDSScene:[NSNumber numberWithInt:0] withName:NSLocalizedString(@"key_default_shade_scene_name_0", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene5Found) {
                [self createDSScene:[NSNumber numberWithInt:5] withName:NSLocalizedString(@"key_default_shade_scene_name_5", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene17Found) {
                [self createDSScene:[NSNumber numberWithInt:17] withName:NSLocalizedString(@"key_default_shade_scene_name_17", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene18Found) {
                [self createDSScene:[NSNumber numberWithInt:18] withName:NSLocalizedString(@"key_default_shade_scene_name_18", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene19Found) {
                [self createDSScene:[NSNumber numberWithInt:19] withName:NSLocalizedString(@"key_default_shade_scene_name_19", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            
		}
		
        // Blue group (climate)
        if (room.group3present) {
			NSArray *groupBlueArray = [_roomGroups objectForKey:@"3"];
            currentGroup = [NSNumber numberWithInt:3];
			if (groupBlueArray && [groupBlueArray count] > 0) {
				for (NSArray *sceneArray in groupBlueArray) {
					NSNumber *sceneNo = [sceneArray objectAtIndex:0];
					NSString *sceneName = [sceneArray objectAtIndex:1];
					DSScene *scene = [currentDss getScene:sceneNo inRoom:room withGroup:currentGroup];
					
					if (!scene) {
                        //per default we only show 0,5,17,18,19 on the list
                        if([sceneNo intValue] == 0 || [sceneNo intValue] == 5 ||[sceneNo intValue] == 17 ||[sceneNo intValue] == 18 ||[sceneNo intValue] == 19 ){
                            
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
                        }
                        else{
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:NO];
                        }
						updated = YES;
					}
					else {
						scene.name = sceneName;
						scene.dirty = NO;
					}
                    
					
				}
			}
			else {
				// server liefert kein ergebnis, räume NICHT löschen!!
				NSSet *myscenes = [NSSet setWithSet:room.scenes];
				
				for (DSScene *scene in myscenes) {
					scene.dirty = NO;
				}
			}
            
            //add standard scenes if needed
            bool scene0Found = NO;
            bool scene5Found = NO;
            bool scene17Found = NO;
            bool scene18Found = NO;
            bool scene19Found = NO;
            
            
            NSSet *myscenes = [NSSet setWithSet:room.scenes];
            
            for (DSScene *scene in myscenes) {
                if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene0Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:5]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene5Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:17]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene17Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:18]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene18Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:19]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene19Found = YES;
                    scene.dirty = NO;
                }
            }
            
            if (!scene0Found) {
                [self createDSScene:[NSNumber numberWithInt:0] withName:NSLocalizedString(@"key_default_scene_name_0", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene5Found) {
                [self createDSScene:[NSNumber numberWithInt:5] withName:NSLocalizedString(@"key_default_scene_name_5", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene17Found) {
                [self createDSScene:[NSNumber numberWithInt:17] withName:NSLocalizedString(@"key_default_scene_name_17", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene18Found) {
                [self createDSScene:[NSNumber numberWithInt:18] withName:NSLocalizedString(@"key_default_scene_name_18", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene19Found) {
                [self createDSScene:[NSNumber numberWithInt:19] withName:NSLocalizedString(@"key_default_scene_name_19", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            
		}

        // audio group
        if (room.group4present) {
			NSArray *groupAudioArray = [_roomGroups objectForKey:@"4"];
            currentGroup = [NSNumber numberWithInt:4];
			if (groupAudioArray && [groupAudioArray count] > 0) {
				for (NSArray *sceneArray in groupAudioArray) {
					NSNumber *sceneNo = [sceneArray objectAtIndex:0];
					NSString *sceneName = [sceneArray objectAtIndex:1];
					DSScene *scene = [currentDss getScene:sceneNo inRoom:room withGroup:currentGroup];
					
					if (!scene) {
                        //per default we only show 0,5,17,18,19 on the list
                        if([sceneNo intValue] == 0 || [sceneNo intValue] == 5 ||[sceneNo intValue] == 17 ||[sceneNo intValue] == 18 ||[sceneNo intValue] == 19 ){
                            
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
                        }
                        else{
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:NO];
                        }
						updated = YES;
					}
					else {
						scene.name = sceneName;
						scene.dirty = NO;
					}
                    
					
				}
			}
			else {
				// server liefert kein ergebnis, räume NICHT löschen!!
				NSSet *myscenes = [NSSet setWithSet:room.scenes];
				
				for (DSScene *scene in myscenes) {
					scene.dirty = NO;
				}
			}
            
            //add standard scenes if needed
            bool scene0Found = NO;
            bool scene5Found = NO;
            bool scene17Found = NO;
            bool scene18Found = NO;
            bool scene19Found = NO;
            
            
            NSSet *myscenes = [NSSet setWithSet:room.scenes];
            
            for (DSScene *scene in myscenes) {
                if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene0Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:5]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene5Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:17]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene17Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:18]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene18Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:19]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene19Found = YES;
                    scene.dirty = NO;
                }
            }
            
            if (!scene0Found) {
                [self createDSScene:[NSNumber numberWithInt:0] withName:NSLocalizedString(@"key_default_scene_name_0", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene5Found) {
                [self createDSScene:[NSNumber numberWithInt:5] withName:NSLocalizedString(@"key_default_scene_name_5", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene17Found) {
                [self createDSScene:[NSNumber numberWithInt:17] withName:NSLocalizedString(@"key_default_scene_name_17", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene18Found) {
                [self createDSScene:[NSNumber numberWithInt:18] withName:NSLocalizedString(@"key_default_scene_name_18", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene19Found) {
                [self createDSScene:[NSNumber numberWithInt:19] withName:NSLocalizedString(@"key_default_scene_name_19", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            
		}

        // video group
        if (room.group5present) {
			NSArray *groupVideoArray = [_roomGroups objectForKey:@"5"];
            currentGroup = [NSNumber numberWithInt:5];
			if (groupVideoArray && [groupVideoArray count] > 0) {
				for (NSArray *sceneArray in groupVideoArray) {
					NSNumber *sceneNo = [sceneArray objectAtIndex:0];
					NSString *sceneName = [sceneArray objectAtIndex:1];
					DSScene *scene = [currentDss getScene:sceneNo inRoom:room withGroup:currentGroup];
					
					if (!scene) {
                        //per default we only show 0,5,17,18,19 on the list
                        if([sceneNo intValue] == 0 || [sceneNo intValue] == 5 ||[sceneNo intValue] == 17 ||[sceneNo intValue] == 18 ||[sceneNo intValue] == 19 ){
                            
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
                        }
                        else{
                            scene = [self createDSScene:sceneNo withName:sceneName withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:NO];
                        }
						updated = YES;
					}
					else {
						scene.name = sceneName;
						scene.dirty = NO;
					}
                    
					
				}
			}
			else {
				// server liefert kein ergebnis, räume NICHT löschen!!
				NSSet *myscenes = [NSSet setWithSet:room.scenes];
				
				for (DSScene *scene in myscenes) {
					scene.dirty = NO;
				}
			}
            
            //add standard scenes if needed
            bool scene0Found = NO;
            bool scene5Found = NO;
            bool scene17Found = NO;
            bool scene18Found = NO;
            bool scene19Found = NO;
            
            
            NSSet *myscenes = [NSSet setWithSet:room.scenes];
            
            for (DSScene *scene in myscenes) {
                if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene0Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:5]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene5Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:17]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene17Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:18]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene18Found = YES;
                    scene.dirty = NO;
                }
                else if ([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:19]] && [scene.group isEqualToNumber:currentGroup]) {
                    scene19Found = YES;
                    scene.dirty = NO;
                }
            }
            
            if (!scene0Found) {
                [self createDSScene:[NSNumber numberWithInt:0] withName:NSLocalizedString(@"key_default_scene_name_0", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene5Found) {
                [self createDSScene:[NSNumber numberWithInt:5] withName:NSLocalizedString(@"key_default_scene_name_5", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene17Found) {
                [self createDSScene:[NSNumber numberWithInt:17] withName:NSLocalizedString(@"key_default_scene_name_17", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene18Found) {
                [self createDSScene:[NSNumber numberWithInt:18] withName:NSLocalizedString(@"key_default_scene_name_18", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            if (!scene19Found) {
                [self createDSScene:[NSNumber numberWithInt:19] withName:NSLocalizedString(@"key_default_scene_name_19", @"") withRoom:room withGroup:currentGroup favouriteScene:NO showOnList:YES];
            }
            
		}

		// add dirty scenes
		
		NSMutableArray *dirtyScenes = [NSMutableArray arrayWithCapacity:1];
		myscenes = [NSSet setWithSet:room.scenes];
		
		for (DSScene *scene in myscenes) {
			if (scene.dirty) {
				[dirtyScenes addObject:scene];
			}
		}
		
        //we don't delete dirty scenes now! (since we create some locally on the phone)
        //		for (DSScene *scene in dirtyScenes) {
        //			if (scene.dirty) {
        //				[room removeScenesObject:scene];
        //				[currentDss removeScenesObject:scene];
        //				[self.objectContext deleteObject:scene];
        //				updated = YES;
        //			}
        //		}
	}
	
    
	return updated;
	
    //legacy code deleted
}

- (BOOL) updateHighLevelEventsWithArray: (NSArray*) eventArray{
    BOOL updated = NO;
    
    NSSet *myScenes = [NSSet setWithSet:currentDss.scenes];
    
    //all high level events dirty
	for (DSScene *scene in myScenes) {
        //high level events dont have a room and their id/scene number is over 1023
        if (!scene.room && ([scene.sceneNo longLongValue] > 1023)) {
			scene.dirty = YES;
		}
    }
	
    //for every HLE event retrieved, check if it needs updating or creation
	for (NSArray *highLevelEvent in eventArray) {
        
        if([highLevelEvent count] > 1){
            NSNumber *sceneNo = [highLevelEvent objectAtIndex:0];
            NSString *sceneName = [highLevelEvent objectAtIndex:1];
            
            DSScene *scene = [currentDss getHighLevelEvent:sceneNo];
            
            if (!scene) {
                scene = [self createDSScene:sceneNo withName:sceneName withRoom:nil withGroup:[NSNumber numberWithInt:0] favouriteScene:NO showOnList:YES];
                updated = YES;
            }
            else {
                scene.name = sceneName;
                scene.dirty = NO;
            }
        }
    }
    
    //delete dirty scenes
    NSMutableArray *dirtyScenes = [NSMutableArray arrayWithCapacity:1];
    
    for (DSScene *scene in myScenes) {
        if (scene.dirty && !scene.room && [scene.sceneNo longLongValue] > 1023) {
            [dirtyScenes addObject:scene];
        }
    }
    
    for (DSScene *scene in dirtyScenes) {
        [currentDss removeScenesObject:scene];
        [self.objectContext deleteObject:scene];
        updated = YES;
    }
    
    return updated;
}

- (BOOL) renameScene: (NSString*) newName withScene: (DSScene*) scene withRoom:(DSRoom*)room {
	BOOL sceneRenamed = [service setSceneName:newName withRoom:room withScene:scene];
	
	if (sceneRenamed) {
		[scene setName:newName];
		
		NSError * error = nil;
		[self.objectContext save:&error];
	}
	
	return sceneRenamed;
}

- (BOOL) saveScene:(DSScene*) scene{
    
	return [service saveScene:scene];
}




- (BOOL) turnSceneOn: (DSScene*)scene {
	
    //	GSLog(@"Scene: %@", scene);
	
	BOOL result;
	
	if (useDemo) {
		// schalten funktioniert immer :-)
		result = YES;
	} else {
		result = [service setScenePowerOn:scene];
	}
    
    //reset lastcalled scenes on leave home call
    if([scene.sceneNo isEqualToNumber: [NSNumber numberWithInt:72]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            for (DSRoom *room in [DataController instance].rooms ) {
                room.lastCalledScene = [NSString stringWithFormat:@"%i",72];
                room.lastCalledShadeScene = [NSString stringWithFormat:@"%i",72];
                room.lastCalledBlueScene = [NSString stringWithFormat:@"%i",72];
                room.lastCalledAudioScene = [NSString stringWithFormat:@"%i",72];
                room.lastCalledVideoScene = [NSString stringWithFormat:@"%i",72];
            }
        });
    }
	
	if (result && scene.room) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if([scene.group isEqualToNumber:[NSNumber numberWithInt:1]])
                scene.room.lastCalledScene = [NSString stringWithFormat:@"%lld",[scene.sceneNo longLongValue]];
            else if([scene.group isEqualToNumber:[NSNumber numberWithInt:2]])
                scene.room.lastCalledShadeScene = [NSString stringWithFormat:@"%lld",[scene.sceneNo longLongValue]];
            else if([scene.group isEqualToNumber:[NSNumber numberWithInt:3]])
                scene.room.lastCalledBlueScene = [NSString stringWithFormat:@"%lld",[scene.sceneNo longLongValue]];
            else if([scene.group isEqualToNumber:[NSNumber numberWithInt:4]])
                scene.room.lastCalledAudioScene = [NSString stringWithFormat:@"%lld",[scene.sceneNo longLongValue]];
            else if([scene.group isEqualToNumber:[NSNumber numberWithInt:5]])
                scene.room.lastCalledVideoScene = [NSString stringWithFormat:@"%lld",[scene.sceneNo longLongValue]];

        });
	}
	else if(result) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[[ModelLocator instance] setValue:scene.sceneNo forKey:k_DATA_APARTMENT_LAST_CALLED_SCENE];
		});
	}
	
	
	if (useDemo) {
		// Stromverbrauch für Raum festlegen
		[[ModelLocator instance] setValue:[NSNumber numberWithInteger:[self demoSceneConsumption:[scene.sceneNo integerValue]]] forKey:[NSString stringWithFormat:@"%@%@", k_INFO_PREFIX_ROOM_CONSUMPTION, scene.room.identifier]];
		
		// Gesamtverbrauch festlegen
		NSInteger totalCon = 0;
		
		for (DSRoom *room in self.rooms) {
			totalCon += [[self getRoomConsumption:room] integerValue];
		}
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[[ModelLocator instance] setValue:[NSNumber numberWithInteger:totalCon] forKey:k_INFO_TOTAL_CONSUMPTION];
		});
	}
	
	return result;
}

- (BOOL) undoScene: (DSScene *) scene {
	
	if (useDemo) {
		// schalten funktioniert immer :-)
		dispatch_async(dispatch_get_main_queue(), ^{
			[[ModelLocator instance] setValue:[NSNumber numberWithInt:0] forKey:k_DATA_APARTMENT_LAST_CALLED_SCENE];
		});
		return YES;
	} else {
		if ([service undoScene:scene]) {
			if (scene.room) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([scene.group isEqualToNumber:[NSNumber numberWithInt:1]])
                        scene.room.lastCalledScene = [NSString stringWithFormat:@"%i",999];
                    else if([scene.group isEqualToNumber:[NSNumber numberWithInt:2]])
                        scene.room.lastCalledShadeScene = [NSString stringWithFormat:@"%i",999];
                    else if([scene.group isEqualToNumber:[NSNumber numberWithInt:3]])
                        scene.room.lastCalledBlueScene = [NSString stringWithFormat:@"%i",999];
                    else if([scene.group isEqualToNumber:[NSNumber numberWithInt:4]])
                        scene.room.lastCalledAudioScene = [NSString stringWithFormat:@"%i",999];
                    else if([scene.group isEqualToNumber:[NSNumber numberWithInt:5]])
                        scene.room.lastCalledVideoScene = [NSString stringWithFormat:@"%i",999];
                    
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[ModelLocator instance] setValue:[NSNumber numberWithInt:0] forKey:k_DATA_APARTMENT_LAST_CALLED_SCENE];
                });
            }
            
			return YES;
		}
		return NO;
	}
}

- (NSInteger) demoSceneConsumption: (NSInteger) sceneNo {
	NSInteger con = 0;
	
	switch (sceneNo) {
		case 0:
			con = 0;
			break;
		case 1:
			con = 120;
			break;
		case 2:
			con = 210;
			break;
		case 3:
			con = 160;
			break;
		case 4:
			con = 70;
			break;
		case 5:
			con = 280;
			break;
		case 6:
			con = 400;
			break;
		case 7:
			con = 150;
			break;
		case 8:
			con = 80;
			break;
		case 9:
			con = 140;
			break;
		case 10:
			con = 170;
			break;
		case 11:
			con = 210;
			break;
		case 12:
			con = 60;
			break;
		case 13:
			con = 80;
			break;
		case 14:
			con = 40;
			break;
		case 15:
			con = 180;
			break;
		case 16:
			con = 30;
			break;
		case 17:
			con = 60;
			break;
		case 18:
			con = 100;
			break;
		default:
			break;
	}
	
	return con;
}

- (BOOL) callStandbyScene:(DSRoom*)room  {
    int lightGroup = 1;
    if([self callStandardScene:k_SERVICE_SCENE_STANDBY withRoom:room withGroup:lightGroup]){
        dispatch_async(dispatch_get_main_queue(), ^{
            NSInteger sceneNo = 0;
			room.lastCalledScene = [NSString stringWithFormat:@"%i",(int)sceneNo];
        });
        return YES;
    }
    else
        return NO;
}

- (BOOL) callStandardScene: (NSString*)standardScene withRoom:(DSRoom*)room withGroup:(int)group {
	return [service callStandardScene:standardScene withRoom:room withGroup:group];
    
}


- (BOOL) callStandardScene: (NSString*)standardScene withDevice:(DSDevice*)device withGroup:(int)group{
	return [service callStandardScene:standardScene withDevice:device withGroup:group];
}

#pragma mark -
#pragma mark dSS Methods

- (DSServer*) createDsServerEntity {
	return [NSEntityDescription insertNewObjectForEntityForName:@"DSServer" inManagedObjectContext:self.objectContext];
}

- (NSString*) getDSSVersion {
	if (useDemo) {
		if (currentDss) {
			dispatch_async(dispatch_get_main_queue(), ^{
				currentDss.version = @"v1.0.0";
				[self saveContext];
			});
		}
		return @"v1.0.0";
	}
	
	NSString *version = [service getDSSVersion];
	
	if (!version) {
		version = @"Test-DSS";
	}
	
	if (currentDss) {
		dispatch_async(dispatch_get_main_queue(), ^{
			currentDss.version = version;
			[self saveContext];
		});
		
	}
	
	return version;
}

- (void) getDSSLocation {
    if(useDemo){
        //use aizo brandstrasse 33
        self.dssLatitude = [NSNumber numberWithFloat:47.401022f];
        self.dssLongitude = [NSNumber numberWithFloat:8.442496f];
    }
    else{
        NSMutableDictionary *locDict = [service getDSSLocation];
        self.dssLatitude = [locDict objectForKey:@"latitude"];
        self.dssLongitude = [locDict objectForKey:@"longitude"];
    }
    [[FacilityController instance]loadWeatherInformation];
    
}


- (NSString*) getApartmentName {
	if (useDemo) {
		if (currentDss) {
			dispatch_async(dispatch_get_main_queue(), ^{
				currentDss.name = NSLocalizedString(@"key_demo_apartment_name", @"");
				[self saveContext];
			});
			
		}
		return NSLocalizedString(@"key_demo_apartment_name", @"");
	}
	
	NSString *apartmentName = [service getApartmentName];
	
	if (currentDss) {
		NSString *oldName = currentDss.name;
		
		dispatch_async(dispatch_get_main_queue(), ^{
			currentDss.name = apartmentName;
			[self saveContext];
			
			if (apartmentName) {
				if (!oldName || ![apartmentName isEqualToString:oldName]) {
					//	Tell that something has changed within the dss list
					[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDssListChanged object:nil];
				}
			}
		});
	}
	return apartmentName;
}

- (DSServer*) getDssForMacAddressInCoreData: (NSString*)macAddress {
	DSServer *server;
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSServer" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"macAddress == %@", macAddress];
	[fetchRequest setPredicate:predicate];
	
	NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastConnectionTS" ascending:NO];
	[fetchRequest setSortDescriptors:[NSArray arrayWithObjects:dateDescriptor, nil]];
    [dateDescriptor release];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	
	if (!error && [fetchedObjects count] > 0) {
		server = [fetchedObjects objectAtIndex:0];
	} else {
		server = nil;
	}
	
	[fetchRequest release];
	
	return server;
}

- (DSServer*) getDssForIpAddressInCoreData: (NSString*)ipAddress {
	DSServer *server;
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSServer" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ip == %@", ipAddress];
	[fetchRequest setPredicate:predicate];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	
	if (!error && [fetchedObjects count] > 0) {
		server = [fetchedObjects objectAtIndex:0];
	} else {
		server = nil;
	}
	
	[fetchRequest release];
	
	return server;
}

- (DSServer*) getLastDssInCoreData {
	DSServer *server;
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSServer" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastConnectionTS" ascending:NO];
	[fetchRequest setSortDescriptors:[NSArray arrayWithObjects:dateDescriptor, nil]];
	[fetchRequest setFetchLimit:1];
    [dateDescriptor release];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	
	if (!error && [fetchedObjects count] > 0) {
		server = [fetchedObjects objectAtIndex:0];
	} else {
		server = nil;
	}
	
	[fetchRequest release];
	
	return server;
}

- (NSArray*) fetchAllDss {
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSServer" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	[fetchRequest setSortDescriptors:[NSArray arrayWithObjects:dateDescriptor, nil]];
    [dateDescriptor release];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	[fetchRequest release];
	
	if (!error) {
		return fetchedObjects;
	} else {
		return nil;
	}
}

- (NSArray*) fetchAllManualDss {
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSServer" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	[fetchRequest setSortDescriptors:[NSArray arrayWithObjects:dateDescriptor, nil]];
    [dateDescriptor release];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"detectedByBonjour == false"];
	[fetchRequest setPredicate:predicate];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	[fetchRequest release];
	
	if (!error) {
		return fetchedObjects;
	} else {
		return nil;
	}
}

- (NSArray*) fetchAllAutoDetectedDss {
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSServer" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	[fetchRequest setSortDescriptors:[NSArray arrayWithObjects:dateDescriptor, nil]];
    [dateDescriptor release];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"detectedByBonjour == true"];
	[fetchRequest setPredicate:predicate];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	[fetchRequest release];
	
	if (!error) {
		return fetchedObjects;
	} else {
		return nil;
	}
}

- (NSArray*) allocAllKnownDss {
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DSServer" inManagedObjectContext:self.objectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastConnectionTS" ascending:NO];
	[fetchRequest setSortDescriptors:[NSArray arrayWithObjects:dateDescriptor, nil]];
    [dateDescriptor release];
	
	NSError * error = nil;
	NSArray *fetchedObjects = [self.objectContext executeFetchRequest:fetchRequest error:&error];
	[fetchRequest release];
	
	if (!error) {
		NSMutableArray *servers = [NSMutableArray new];
		for (DSServer *server in fetchedObjects) {
			if (![servers containsObject:server.ip]) {
				[servers addObject:server.ip];
			}
		}
		
		return servers;
	} else {
		return nil;
	}
}

- (void) setMeterName: (NSString*) name withMeter: (DSMeter*) meter {
	if ([name isEqualToString:meter.name]) {
		return;
	}
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		if ([service setMeterName:name withMeter:meter]) {
			dispatch_sync(dispatch_get_main_queue(), ^{
				meter.name = name;
				[self saveContext];
			});
		}
		else {
			dispatch_sync(dispatch_get_main_queue(), ^{
				meter.name = meter.name;
			});
		}
	});
}

- (DSMeter*) createDemoMeter: (NSString*) meterName withId: (NSString*) identifier {
	DSMeter *meter = [NSEntityDescription insertNewObjectForEntityForName:@"DSMeter" inManagedObjectContext:self.objectContext];
	meter.dirty = NO;
	meter.name = meterName;
	meter.identifier = identifier;
	meter.apiVersion = [NSNumber numberWithInt:258];
	meter.energyMeterValue = [NSNumber numberWithInt:0];
	meter.powerConsumption = [NSNumber numberWithInt:0];
	
	meter.dss = currentDss;
	[currentDss addMetersObject:meter];
	
	NSError * error = nil;
	[self.objectContext save:&error];
	//[self.scenes addObject:scene];
	
	return meter;
}

- (void) markDsServersAsDisconnected {
	/*
	 *	mark all other dsServers in database as "not connected"
	 */
	NSArray *allDss = [self fetchAllDss];
	
	for (DSServer* dss in allDss) {
		dss.connected = NO;
	}
	
	[self saveContext];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationConnectedToDss object:nil];
}

- (void) deleteDsServer: (DSServer*) dsServer {
	if (dsServer) {
		
		
		[self.objectContext deleteObject:dsServer];
		
		[self saveContext];
	}
}

#pragma mark -
#pragma mark Core-Data Methods


//Explicitly write Core Data accessors
- (NSManagedObjectContext *) managedObjectContext {
	if (self.objectContext != nil) {
		return self.objectContext;
	}
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator != nil) {
        self.objectContext = [[NSManagedObjectContext alloc] init];
        [self.objectContext setPersistentStoreCoordinator: coordinator];
	}
	
	return self.objectContext;
}

- (NSManagedObjectContext *) switchManagedObjectContext {
	persistentStoreCoordinator = nil;
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	
	self.objectContext = nil;
	if (coordinator != nil) {
        self.objectContext = [[NSManagedObjectContext alloc] init];
        [self.objectContext setPersistentStoreCoordinator: coordinator];
	}
	
	return self.objectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
	if (managedObjectModel != nil) {
		return managedObjectModel;
	}

    NSString *path = [[NSBundle mainBundle] pathForResource:@"DS iPhone4" ofType:@"mom" inDirectory:@"DS iPhone.momd"];
    NSURL *momURL = [NSURL fileURLWithPath:path];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
    //	managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles: nil] retain];

	return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	if (persistentStoreCoordinator != nil) {
		return persistentStoreCoordinator;
	}
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsPath = [paths objectAtIndex:0];
	
	NSError * ee;
	NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&ee];
	
	for (NSString *file in directoryContents) {
		NSLog(@"file found: %@", file);
	}
    
	//This is  the last time we rename the database! Never again!
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"DS iPhone_v8.sqlite"]];
	
    //v7 new data type for scene id (UDA now requires 64bit)
    NSURL *oldURL_7 = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"DS iPhone_v7.sqlite"]];
    
    //v6 includes device consumption and metering
    NSURL *oldURL_6 = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"DS iPhone_v6.sqlite"]];
    
    //v5 includes anglefactor in devices
    NSURL *oldURL_5 = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"DS iPhone_v5.sqlite"]];
    
    NSURL *oldURL_4 = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"DS iPhone_v4.sqlite"]];
    
	NSURL *oldURL_3 = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"DS iPhone_v3.sqlite"]];
	
    NSURL *oldURL_2 = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"DS iPhone_v2.sqlite"]];
	
	NSURL *oldURL_1 = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"DS iPhone.sqlite"]];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
    //legacy deleting of old databases
    if ([fileManager fileExistsAtPath:[oldURL_1 path]]) {
		GSLog(@"old file v1 is there");
		[fileManager removeItemAtPath: [oldURL_1 path] error:nil];
	}
    if ([fileManager fileExistsAtPath:[oldURL_2 path]]) {
		GSLog(@"old file v2 is there");
		[fileManager removeItemAtPath: [oldURL_2 path] error:nil];
	}
    if ([fileManager fileExistsAtPath:[oldURL_3 path]]) {
		GSLog(@"old file v3 is there");
		[fileManager removeItemAtPath: [oldURL_3 path] error:nil];
	}
    if ([fileManager fileExistsAtPath:[oldURL_4 path]]) {
		GSLog(@"old file v4 is there");
		[fileManager removeItemAtPath: [oldURL_4 path] error:nil];
	}
    if ([fileManager fileExistsAtPath:[oldURL_5 path]]) {
		GSLog(@"old file v5 is there");
		[fileManager removeItemAtPath: [oldURL_5 path] error:nil];
	}
    if ([fileManager fileExistsAtPath:[oldURL_6 path]]) {
		GSLog(@"old file v6 is there");
		[fileManager removeItemAtPath: [oldURL_6 path] error:nil];
	}
	if ([fileManager fileExistsAtPath:[oldURL_7 path]]) {
		GSLog(@"old file v7 is there");
		[fileManager removeItemAtPath: [oldURL_7 path] error:nil];
	}

    // handle db upgrade
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    
    
	NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
        // Handle error
    }
	
	return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark -
#pragma mark Cloud Service methods

- (void) cloudServiceGetAllTags: (DSDevice*) device {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		//	testlampe: 4290046001154
		//	device.identifier
		
		
		void (^responseHandlerBlockBlock)(NSDictionary*) = ^(NSDictionary *dict){
			NSDictionary *dictResponse = [dict valueForKey:@"Response"];
			if (dictResponse) {
				NSArray *arrayTags = [dictResponse valueForKey:@"Tags"];
				
                BOOL foundMaxPower = NO;
                BOOL foundSocket = NO;
				if (arrayTags) {
					for (NSDictionary *dictItem in arrayTags) {
						NSString *propName = [[dictItem valueForKey:@"PropertyShortName"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
						
						if (propName && [@"MAX_POWER" isEqualToString:propName]) {
							//	max power --> store in device
                            foundMaxPower = YES;
							dispatch_async(dispatch_get_main_queue(), ^{
								device.maxPower = [NSNumber numberWithDouble:[[dictItem valueForKey:@"Value"] doubleValue]];
							});
						}
						
						else if (propName && [@"SOCKET_TYPE" isEqualToString:propName]) {
							//	socket --> store in device
                            foundSocket = YES;
							dispatch_async(dispatch_get_main_queue(), ^{
                                if ([dictItem valueForKey:@"ValueShortName"] == (id)[NSNull null]) {
                                    device.socket = [dictItem valueForKey:@"Value"];
                                }
                                else{
                                    device.socket = [dictItem valueForKey:@"ValueShortName"];
                                }
							});
						}
						
						else if (propName && [@"INFO_PAGE_URL" isEqualToString:propName]) {
							//	info url --> store in device
							dispatch_async(dispatch_get_main_queue(), ^{
								device.infoUrl = [dictItem valueForKey:@"Value"];
							});
						}
						
						else if (propName && [@"SERVICE_PAGE_URL" isEqualToString:propName]) {
							//	service url --> store in device
							dispatch_async(dispatch_get_main_queue(), ^{
								device.serviceUrl = [dictItem valueForKey:@"Value"];
							});
							
						}
					}
                    
                    //make sure they're actually set to something
                    if(!foundMaxPower){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            device.maxPower = [NSNumber numberWithInt:-1];
                        });
                    }
                    if(!foundSocket){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            device.socket = @"-";
                        });
                    }
				}
			}
		};
		
        NSString *identifierToUse = device.identifier;
        //cloud calls does not support the new dsuid parameter, so we have to use the old dsid
        if (device.isUsingDsuid) {
            identifierToUse = device.old_dsid;
        }
        
		NSDictionary *dict = [[DigitalstromJSONService instance] cloudServiceGetAllTagsWithAuthentication:[[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN] andObjectId:identifierToUse andType:@"DSDevice"];
        
        NSLog(@"gettag dict description %@", [dict description]);
		
		NSDecimalNumber *returnCode = [dict valueForKey:@"ReturnCode"];
		if (dict && returnCode && [returnCode integerValue] == 0) {
			responseHandlerBlockBlock(dict);
		}
        //no tags available, set default values
        else if (dict && returnCode && [returnCode integerValue] == 35) {
            dispatch_async(dispatch_get_main_queue(), ^{
                device.maxPower = [NSNumber numberWithInt:-1];
                device.socket = @"-";
            });
        }
        
		else if (dict){
            NSLog(@"getAllTags didn't succeed: %@", [dict valueForKey:@"ReturnMessage"]);
        }
        else{
            NSLog(@"getAllTags didn't succeed, no valid dictonary");
        }
	});
}

-(void) cloudServiceDeleteMaxPowerForDevice:(DSDevice *)device
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *identifierToUse = device.identifier;
        //cloud calls does not support the new dsuid parameter, so we have to use the old dsid
        if (device.isUsingDsuid) {
            identifierToUse = device.old_dsid;
        }
        
        NSDictionary *dict = [[DigitalstromJSONService instance] cloudServiceDeleteTags:@[@"MAX_POWER"] withAuthentication:[[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN] andDeviceId:identifierToUse];
        
        if ([dict objectForKey:@"ReturnCode"] && [[dict objectForKey:@"ReturnCode"] isEqualToNumber:[NSNumber numberWithInteger:0]]) {
            device.maxPower = @(-1);
        }
        else {
            NSLog(@"Could not delete tags");
        }
    });
}

-(void) cloudServiceDeleteSocketTypeForDevice:(DSDevice *)device
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *identifierToUse = device.identifier;
        //cloud calls does not support the new dsuid parameter, so we have to use the old dsid
        if (device.isUsingDsuid) {
            identifierToUse = device.old_dsid;
        }
        
        NSDictionary *dict = [[DigitalstromJSONService instance] cloudServiceDeleteTags:@[@"SOCKET_TYPE"] withAuthentication:[[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN] andDeviceId:identifierToUse];
        
        if ([dict objectForKey:@"ReturnCode"] && [[dict objectForKey:@"ReturnCode"] isEqualToNumber:[NSNumber numberWithInteger:0]]) {
            device.socket = @"-";
        }
        else {
            NSLog(@"Could not delete tags");
        }
    });
}

- (void) cloudServiceSetMaxPower:(NSNumber*) maxPower forDevice: (DSDevice*) device
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *identifierToUse = device.identifier;
        //cloud calls does not support the new dsuid parameter, so we have to use the old dsid
        if (device.isUsingDsuid) {
            identifierToUse = device.old_dsid;
        }
        
		NSDictionary *dict = [[DigitalstromJSONService instance] cloudServiceSetTagMaxPower:[NSString stringWithFormat:@"%@", maxPower] withAuthentication:[[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN] andDeviceId:identifierToUse];
		
		if ([dict objectForKey:@"ReturnCode"] && [[dict objectForKey:@"ReturnCode"] isEqualToNumber:[NSNumber numberWithInteger:0]]) {
            [device setMaxPower:maxPower];
		}
		else {
			//	unknown exception
		}
	});
}

- (void) cloudServiceSetSocket:(NSString*) socket forDevice: (DSDevice*) device
{
	dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *identifierToUse = device.identifier;
        //cloud calls does not support the new dsuid parameter, so we have to use the old dsid
        if (device.isUsingDsuid) {
            identifierToUse = device.old_dsid;
        }
    
		NSDictionary *dict = [[DigitalstromJSONService instance] cloudServiceSetTagSocket:socket withAuthentication:[[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN] andDeviceId:identifierToUse];
		if ([dict objectForKey:@"ReturnCode"] && [[dict objectForKey:@"ReturnCode"] isEqualToNumber:[NSNumber numberWithInteger:0]])
        {
			[device setSocket:socket];
		}
		else
        {
            NSLog(@"Couldn't set socket, response: %@", [dict description]);
			//	unknown exception
		}
	});
}

- (void) cloudServiceGetDeviceDetailsForDevice: (DSDevice*) device {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSLog(@"device gtin is: %@", device.gtin);
		NSDictionary *dict = [[DigitalstromJSONService instance] cloudServiceGetDeviceDetailsForDeviceWithGtin:device.gtin];
		
		if ([dict objectForKey:@"ReturnCode"] && [[dict objectForKey:@"ReturnCode"] isEqualToNumber:[NSNumber numberWithInteger:0]]) {
			if ([dict objectForKey:@"Response"]) {
                NSLog(@"device detail dict response: %@", [[dict objectForKey:@"Response"] description]);
				NSDictionary *respDict = [dict objectForKey:@"Response"];
				
				if (respDict) {
					// NSString *ean = [respDict valueForKey:@"EAN"];
					NSString *serviceLink = [respDict valueForKey:@"ServiceLink"];
					NSString *infoLink = [respDict valueForKey:@"InfoLink"];
					
					NSDictionary *deviceProperties = [respDict valueForKey:@"DeviceSpecificProperties"];
					
					if (deviceProperties) {
						NSString *socketType = [deviceProperties valueForKey:@"SocketType"];
						NSNumber *dimmingCurveId = [deviceProperties valueForKey:@"DimmingCurveID"];
						NSNumber *maxPower = [deviceProperties valueForKey:@"MaximumWatt"];
						
						dispatch_async(dispatch_get_main_queue(), ^{
							device.maxPower = maxPower;
							device.socket = socketType;
							device.dimmingCurveId = dimmingCurveId;
							device.serviceUrl = serviceLink;
							device.infoUrl = infoLink;
							
						});
					}
				}
			}
		}
		else {
			//	unknown exception
		}
		
		
	});
}

- (void) cloudServiceCheckIlluminantForDevice: (DSDevice*) device andEan: (NSString*) ean {
	NSLog(@"posting notification %@", kNotificationSettingsDevicesIlluminantCheckStarted);
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSettingsDevicesIlluminantCheckStarted object:nil];
	
	NSString *sessionToken = [[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN];
	
	if (!sessionToken) {
		[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSettingsDevicesIlluminantCheck99UnknownException object:nil];
		return;
	}
	
	// 0f192
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *identifierToUse = device.identifier;
        //cloud calls does not support the new dsuid parameter, so we have to use the old dsid
        if (device.isUsingDsuid) {
            identifierToUse = device.old_dsid;
        }
        
		NSDictionary *dict = [[DigitalstromJSONService instance] cloudServiceCheckIlluminantForDss:sessionToken andDeviceId:identifierToUse andEan:ean];
		NSLog(@"illuminant check response: %@", dict);
		
		if ([dict objectForKey:@"ReturnCode"] && [[dict objectForKey:@"ReturnCode"] isEqualToNumber:[NSNumber numberWithInteger:0]]) {
			if ([dict objectForKey:@"Response"]) {
				
				NSDictionary *respDict = [dict objectForKey:@"Response"];
				
				if (respDict) {
					
					/*NSString *articleName = [respDict valueForKey:@"Name"];
					 NSString *illuminantId = [respDict valueForKey:@"ID"];
					 NSArray *dimmingCurves = [respDict valueForKey:@"DimmingCurves"];*/
					
					dispatch_async(dispatch_get_main_queue(), ^{
						NSLog(@"posting notification %@", kNotificationSettingsDevicesIlluminantCheck0OK);
						
						NSMutableDictionary *dictResponseData = [NSMutableDictionary new];
						[dictResponseData setValue:[respDict valueForKey:@"FittingCheckResult"] forKey:@"FittingCheckResult"];
						[dictResponseData setValue:[respDict valueForKey:@"DeviceDimmableResult"] forKey:@"DeviceDimmableResult"];
						[dictResponseData setValue:[respDict valueForKey:@"IlluminantDimmingResult"] forKey:@"IlluminantDimmingResult"];
						
						[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSettingsDevicesIlluminantCheck0OK object:nil userInfo:dictResponseData];
					});
				}
			}
		}
		else {
			//	unknown exception
			
			dispatch_async(dispatch_get_main_queue(), ^{
				NSLog(@"posting notification %@", kNotificationSettingsDevicesIlluminantCheck99UnknownException);
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSettingsDevicesIlluminantCheck99UnknownException object:nil];
			});
		}
		
		
	});
}

/*
 *	Transfers the dimming curve of a illuminant for the fitting device.
 *	The device must be asserted before, by CheckIlluminant.
 *	The transfer is a process, consisting of two calls: GetIlluminants (which loads all fitting illuminants of a device) and after selection of correct illuminant (by gtin), TransferDimmingCurve.
 *	This only inits the transfer. The transfer is not completed after this method returns! Check state periodically with GetCurrentTransferStatus.
 */
- (void) cloudServiceTransferDimmingCurveForDevice: (DSDevice*) device andGtin: (NSString*) gtin {
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSettingsDevicesTransferDimmingCurveTransferStarted object:nil];
	
	NSString *sessionToken = [[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN];
	
	if (!sessionToken) {
		[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSettingsDevicesTransferDimmingCurveTransferFailed object:nil];
		return;
	}
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *identifierToUse = device.identifier;
        //cloud calls does not support the new dsuid parameter, so we have to use the old dsid
        if (device.isUsingDsuid) {
            identifierToUse = device.old_dsid;
        }
        
		NSDictionary *dictIlluminants = [[DigitalstromJSONService instance] cloudServiceGetAllIlluminantsForDevice:identifierToUse withAuthentication:sessionToken];
		NSDictionary *responseDict = nil;
		if ([dictIlluminants objectForKey:@"ReturnCode"] && [[dictIlluminants objectForKey:@"ReturnCode"] isEqualToNumber:[NSNumber numberWithInteger:0]]) {
			NSDictionary *dictResponse = [dictIlluminants valueForKey:@"Response"];
//            NSLog(@"getallillum dict: %@", [dictResponse description]);
			
			if (dictResponse) {
				NSArray *arrayIlluminants = [dictResponse valueForKey:@"Illuminants"];
				for (NSString *item in arrayIlluminants) {
					NSString *ean = [item valueForKey:@"EAN"];
					NSString *illuminantId = [item valueForKey:@"ID"];
					NSArray *dimmingCurves = [item valueForKey:@"DimmingCurves"];
					
					if (ean && [item valueForKey:@"EAN"] != [NSNull null]) {
						if ([ean isEqualToString:gtin]) {
							
							if (dimmingCurves && [item valueForKey:@"DimmingCurves"] != [NSNull null]) {
								if ([dimmingCurves count] > 0) {
									NSDictionary *dCurve = [dimmingCurves objectAtIndex:0];
									
									if (dCurve) {
										NSString *dCurveId = [dCurve valueForKey:@"ID"];
										
										NSLog(@"will transfer dCurve (%@) for illuminant (%@) and device (%@)", dCurveId, illuminantId, identifierToUse);
										responseDict = [[DigitalstromJSONService instance] cloudServiceTransferDimmingCurve:dCurveId andDeviceId:identifierToUse andIlluminantId:illuminantId withAuthentication:sessionToken];
										break;
									}
								}
							}
						}
					}
					
				}
			}
			
		}
		else {
			//	unknown exception
			dispatch_async(dispatch_get_main_queue(), ^{
				NSLog(@"posting notification %@", kNotificationSettingsDevicesTransferDimmingCurveTransferFailed);
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSettingsDevicesTransferDimmingCurveTransferFailed object:nil];
			});
		}
		if([responseDict objectForKey:@"ReturnCode"] && [[responseDict objectForKey:@"ReturnCode"]integerValue] == 0){
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"posting notification %@", kNotificationSettingsDevicesTransferDimmingCurveTransferDone);
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSettingsDevicesTransferDimmingCurveTransferDone object:nil];
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
				NSLog(@"posting notification %@", kNotificationSettingsDevicesTransferDimmingCurveTransferFailed);
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSettingsDevicesTransferDimmingCurveTransferFailed object:nil];
			});
        }
	});
}

- (void) cloudServiceGetArticleDataForDevice:(DSDevice*) device {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    
        if(device.gtin) {
            NSDictionary *dict = [[DigitalstromJSONService instance] cloudServiceGetArticleDataForGtin:device.gtin];
            
            if ([dict objectForKey:@"ReturnCode"] && [[dict objectForKey:@"ReturnCode"] isEqualToNumber:[NSNumber numberWithInteger:0]]) {
                NSDictionary *articleDict = [[dict objectForKey:@"Response"] objectForKey:@"Article"];
                
                device.serviceUrl = [articleDict objectForKey:@"ServiceLink"];
                device.infoUrl = [articleDict objectForKey:@"Infolink"];
            }
            else {
                NSLog(@"Could not call articel data: return code %@", [dict objectForKey:@"ReturnCode"]);
            }
        }
        else {
            NSLog(@"Could not call article data because device %@ have no gtin", device.identifier);
        }
    });
}

@end
