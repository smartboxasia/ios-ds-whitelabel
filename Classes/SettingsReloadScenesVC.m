//
//  SettingsReloadScenesVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsReloadScenesVC.h"
#import "DataController.h"
#import "SettingsVC.h"
#import "ModelLocator.h"

@implementation SettingsReloadScenesVC

@synthesize progressView;

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadStateChanged:) name:@"devicesScenesUpdateState" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processChanged:) name:@"devicesScenesUpdateStand" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(maximumChanged:) name:@"devicesScenesUpdateMaxValue" object:nil];
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"devicesScenesUpdateState" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"devicesScenesUpdateStand" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"devicesScenesUpdateMaxValue" object:nil];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

- (void) reloadStateChanged: (NSNotification*) notification {
	int state = [[notification object] intValue];
	GSLog(@"state: %i", state);
	
	if (state == 2) {
		SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
		[settings hideSceneReload];
	}
}

- (void) processChanged: (NSNotification*) notification {
	currentValue = [[notification object] integerValue];
	
	self.progressView.progress = (float)currentValue/maxValue;
	
	GSLog(@"process: %i", currentValue);
}

- (void) maximumChanged: (NSNotification*) notification {
	maxValue = [[notification object] integerValue];
	
	self.progressView.progress = (float)0.5/maxValue;
	
	GSLog(@"max: %i", maxValue);
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

    [super dealloc];
}


@end
