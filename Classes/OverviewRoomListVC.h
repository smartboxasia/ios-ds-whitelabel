//
//  OverviewRoomListVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface OverviewRoomListVC : GAITrackedViewController {
	UIScrollView *scrollView;
	int offset;
	NSMutableArray *items;
    
    CGFloat sWidth;
    CGFloat sHeight;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

- (void) buildScrollview;
- (id) initWithOffset: (int) scrollViewOffset;
- (void) storeInML;
- (void) clearScrollview;

- (void) prepareDealloc;

@end
