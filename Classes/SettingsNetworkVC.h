//
//  SettingsNetworkVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"
#import "GAI.h"

@interface SettingsNetworkVC : GAITrackedViewController<UIAlertViewDelegate> {
	UIScrollView *scrollview;
	NSMutableArray *serverItems;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;

@property (nonatomic, retain) IBOutlet UIView *viewHeadManualConnections;
@property (nonatomic, retain) IBOutlet UIView *viewHeadCloudConnections;

@property (retain, nonatomic) IBOutlet CustomLabel *titleLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *digitalStromLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *manualLabel;

- (IBAction) actionBack;
- (IBAction) actionAddServer;

- (void) buildScrollview;
- (void) activateReloadButton;
- (void) deactivateReloadButton: (BOOL) animated;
- (void) actionReloadServers;

- (void) displayCredentialsAlertView:(BOOL)wrongPass;

@end
