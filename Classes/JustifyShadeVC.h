//
//  JustifyShadeVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSRoom;
@class DSDevice;
@class DSScene;

@interface JustifyShadeVC : GAITrackedViewController {
	UILabel *labelDevicename;
	DSDevice *device;
	DSRoom *room;
	DSScene *scene;
    BOOL isMoving;
    
    IBOutlet UIButton *justifyButton1;
    IBOutlet UIButton *justifyButton2;
    IBOutlet UIButton *justifyButton3;
    IBOutlet UIButton *justifyButton4;
    IBOutlet UIButton *justifyButton5;
    IBOutlet UIImageView *imageButton1;
    IBOutlet UIImageView *imageButton2;

}

@property (nonatomic, retain) IBOutlet UILabel *labelDevicename;
@property (assign) BOOL isMoving;

- (id) initWithDevice: (DSDevice*) _device;
- (id) initWithRoom: (DSRoom*) _room;
- (id) initWithScene: (DSScene*) _scene;

- (IBAction) actionBack;
- (IBAction) actionStop;
- (IBAction) actionStepIncrement;
- (IBAction) actionStepDecrement;
- (IBAction) actionMin;
- (IBAction) actionMax;

- (void) stepIncrement;
- (void) stepDecrement;
- (void) goToMax;
- (void) goToMin;
- (void) stopMovement;

- (void) didRotate: (NSNotification *) notification;


@end
