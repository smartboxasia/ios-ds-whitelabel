//
//  CustomTextView.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "CustomTextView.h"

@implementation CustomTextView

- (void)drawRect:(CGRect)rect {
	[super drawRect:rect];
	
	//GSLog(@"font: %@", self.font.fontName);
	if ([self.font.fontName isEqualToString:@"Helvetica"] || [self.font.fontName isEqualToString:@".HelveticaNeueUI"]) {
		[self setFont:[UIFont fontWithName:@"PFBeauSansPro-Regular" size:self.font.pointSize]];
	}
	if ([self.font.fontName isEqualToString:@"Helvetica-Bold"]) {
		[self setFont:[UIFont fontWithName:@"PFBeauSansPro-Bold" size:self.font.pointSize]];
	}
	if ([self.font.fontName isEqualToString:@"Helvetica-BoldOblique"]) {
		[self setFont:[UIFont fontWithName:@"PFBeauSansPro-BoldItalic" size:self.font.pointSize]];
	}
	if ([self.font.fontName isEqualToString:@"Helvetica-Oblique"]) {
		[self setFont:[UIFont fontWithName:@"PFBeauSansPro-Italic" size:self.font.pointSize]];
	}
	else {
		//[self setFont:[UIFont fontWithName:[self.font.fontName stringByReplacingOccurrencesOfString:self.font.familyName withString:@"NeoSansPro"] size:self.font.pointSize]];
	}
	
	
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
