//
//  SettingsOverviewVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class CustomLabel;

@interface SettingsOverviewVC : UIViewController {
	UILabel *labelVersion;
    IBOutlet CustomLabel *titleLabel;
    IBOutlet CustomLabel *label1;
    IBOutlet CustomLabel *label2;
    IBOutlet CustomLabel *label3;
    IBOutlet CustomLabel *label4;
    IBOutlet CustomLabel *label5;
    IBOutlet CustomLabel *label6;
    IBOutlet CustomLabel *label7;
}

@property (nonatomic, retain) IBOutlet UILabel *labelVersion;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (IBAction) buttonAction1: (id) sender;
- (IBAction) buttonAction2: (id) sender;
- (IBAction) buttonAction3: (id) sender;
- (IBAction) buttonAction4: (id) sender;
- (IBAction) buttonAction5: (id) sender;
- (IBAction) buttonAction6: (id) sender;
- (IBAction) buttonAction7: (id) sender;
- (IBAction) buttonAction8: (id) sender;
- (IBAction) buttonAction9: (id) sender;

@end
