//
//  SettingsDeviceChooseSocketVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceChooseSocketVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "CustomLabel.h"
#import "SettingsDeviceChooseSocketItemVC.h"
#import "SettingsDeviceWebviewVC.h"

@interface SettingsDeviceChooseSocketVC ()

@end

@implementation SettingsDeviceChooseSocketVC

@synthesize scrollview;
@synthesize background;
@synthesize titelLabel;
@synthesize buttonBack;


- (id) initWithCurrentDevice: (DSDevice*)device{
    if (self = [super init]) {
        _currentDevice = device;
	}

	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_DEVICE_CHOOSE_SOCKET_LIST];

    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

    titelLabel.text = NSLocalizedString(@"key_settings_device_select_socket_title", "Socket");
	items = [NSMutableArray new];

	[self buildScrollView];

    self.screenName = @"Settings Devices Socket Type";
}

- (void)buildScrollView {
	for (UIViewController *item in items) {
		[item.view removeFromSuperview];
		[item removeFromParentViewController];
		[item release];
	}
	[items removeAllObjects];

    NSInteger yOffset = 0;

    NSArray *socketArray = [[NSArray alloc] initWithObjects:@"E14",@"E27",@"G4",@"G5,3",@"G9",@"GU10",@"B15d",@"B22d",
                                                            @"E10",@"E12",@"E40",@"Fa4",@"G6.35",@"GU5.3",@"GX5.3",
                                                            @"GY6.35",@"S14d",@"S14s",@"R7s", nil];

    // Used to reset the socket type.
    SettingsDeviceChooseSocketItemVC *emptyItem = [[SettingsDeviceChooseSocketItemVC alloc] initWithSocket:nil andDevice:_currentDevice];
    emptyItem.view.frame = CGRectMake(0, yOffset, emptyItem.view.frame.size.width, emptyItem.view.frame.size.height);
    [self.scrollview addSubview:emptyItem.view];
    [self addChildViewController:emptyItem];
    yOffset += emptyItem.view.frame.size.height;
    [items addObject:emptyItem];

    for (int i=0; i<[socketArray count]; i++) {

        SettingsDeviceChooseSocketItemVC *item = [[SettingsDeviceChooseSocketItemVC alloc] initWithSocket:[socketArray objectAtIndex:i] andDevice:_currentDevice];
        item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
        [self.scrollview addSubview:item.view];
        [self addChildViewController:item];
        yOffset += item.view.frame.size.height;
        [items addObject:item];
    };

    [socketArray release];

    self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, yOffset);
}


#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {

	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];

	[[ModelLocator instance] setValue:nil forKey:k_VIEW_SETTINGS_DEVICE_CHOOSE_SOCKET_LIST];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark ib actions

- (IBAction)showInfoAction:(id)sender {
    SettingsDeviceWebviewVC *webviewVc = [[SettingsDeviceWebviewVC alloc] initWithTitle:NSLocalizedString(@"key_socket_info", nil) andTargetUrl: @"http://www.digitalstrom.com/lightbulbsockets"];

	UIViewController *mainVc = [[ModelLocator instance] valueForKey:k_VIEW_MAIN];

	[UIView transitionFromView:mainVc.view
                        toView:webviewVc.view
                      duration:0.6
                       options: UIViewAnimationOptionTransitionFlipFromLeft
                    completion:^(BOOL finished) {
						if (finished) {

						}
					}
	 ];
}

- (void)dealloc {


	for (UIViewController *child in items) {
		[child.view removeFromSuperview];
		[child release];
	}
	[items removeAllObjects];

    [super dealloc];
}

@end
