//
//  ColorDotsView.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "ColorDotsView.h"

@implementation ColorDotsView


- (void)dealloc {
    [color1 release];
    [color2 release];
    [color3 release];
    [color4 release];
    [color5 release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, COBIPAGECONTROL_HEIGHT);
        self.backgroundColor = [UIColor clearColor];
        
        self.color1 = [UIColor yellowColor];
        self.color2 = [UIColor grayColor];
        self.color3 = [UIColor blueColor];
        self.color4 = [UIColor cyanColor];
        self.color5 = [UIColor magentaColor];
    }
    return self;
}

-(void) setNumberOfPages: (int) number
{         
    numberOfPages = MAX(number, 0);
    currentDot = 0;
    
    CGPoint tempCenter = self.center;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y,
                            15 + numberOfPages * DOT_WIDTH + MAX(numberOfPages - 1, 0) * DOT_SPACING, self.frame.size.height);
    self.center = tempCenter;
    
    [self setNeedsDisplay];
}

-(int) numberOfPages
{
    return numberOfPages;
}

-(void) setCurrentDot: (int) index
{
    if (index >= numberOfPages)
        currentDot = 0;
    else
        currentDot = MAX(0, index);
    
    [self setNeedsDisplay];
}

-(int) currentDot
{
    return currentDot;
}

-(void) setColor1: (UIColor*) color
{
    [color1 release];
    color1 = [color retain];
    [self setNeedsDisplay];
}

-(UIColor*) color1
{
    return color1;
}

-(void) setColor2: (UIColor*) color
{
    [color2 release];
    color2 = [color retain];
    [self setNeedsDisplay];
}

-(UIColor*) color2
{
    return color2;
}

-(void) setColor3: (UIColor*) color
{
    [color3 release];
    color3 = [color retain];
    [self setNeedsDisplay];
}

-(UIColor*) color3
{
    return color3;
}


-(void) setColor4: (UIColor*) color
{
    [color4 release];
    color4 = [color retain];
    [self setNeedsDisplay];
}

-(UIColor*) color4
{
    return color4;
}

-(void) setColor5: (UIColor*) color
{
    [color5 release];
    color5 = [color retain];
    [self setNeedsDisplay];
}

-(UIColor*) color5
{
    return color5;
}


- (void)drawRect:(CGRect)rect {
    
    for (int i = 0; i < numberOfPages; i++) {
        
        CGContextRef contextRef = UIGraphicsGetCurrentContext();
        
        //set the klemme color
        if(i==0)
            CGContextSetFillColorWithColor(contextRef, self.color1.CGColor);
        else if(i==1)
            CGContextSetFillColorWithColor(contextRef, self.color2.CGColor);
        else if(i==2)
            CGContextSetFillColorWithColor(contextRef, self.color3.CGColor);
        else if(i==3)
            CGContextSetFillColorWithColor(contextRef, self.color4.CGColor);
        else if(i==4)
            CGContextSetFillColorWithColor(contextRef, self.color5.CGColor);
        
        //set to a bit opaque if not selected
        if(i!=currentDot)
            CGContextSetAlpha(contextRef, 0.7);
        else
            CGContextSetAlpha(contextRef, 1.0);
        
        //set shadow if selected
        if(i==currentDot)
            CGContextSetShadowWithColor(contextRef, CGSizeZero, 7, [UIColor whiteColor].CGColor);
        else
            CGContextSetShadowWithColor(contextRef, CGSizeZero, 0, [UIColor clearColor].CGColor);
        
        //draw next to each other
        CGContextFillEllipseInRect(contextRef, CGRectMake(7 + DOT_WIDTH * i + DOT_SPACING * i,
                                                          (COBIPAGECONTROL_HEIGHT - DOT_WIDTH) / 2, DOT_WIDTH, DOT_WIDTH));
    }
}@end
