//
//  SettingsMetersDetailVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSMeter.h"
#import "GAI.h"

@class DSMeter;
@class CustomLabel;


@interface SettingsMetersDetailVC : GAITrackedViewController <UITextFieldDelegate> {
	DSMeter *meter;
	
	IBOutlet UILabel *labelHeadIndex;
	IBOutlet UITextField *textHeadName;
	IBOutlet UILabel *labelSN;
	IBOutlet UILabel *labelPowerConsumption;
	IBOutlet UILabel *labelEnergyMeterValue;
	
    IBOutlet CustomLabel *labelIdTitle;
    IBOutlet CustomLabel *labelReadingTitle;
    IBOutlet CustomLabel *labelConsumptionTitle;
}

@property (nonatomic, retain) IBOutlet UIImageView *background;

- (id) initWithMeter: (DSMeter*) theMeter;
- (IBAction) actionBack;
- (IBAction) actionEdit;

@end
