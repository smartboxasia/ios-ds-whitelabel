//
//  ModelLocator.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "ModelLocator.h"
#import "DigitalstromJSONService.h"

@implementation ModelLocator

@synthesize /*serverlist,*/ appController;
@synthesize localServerAddresses;

+(ModelLocator *)instance {
	static ModelLocator *instance;
	
	@synchronized(self) {
		if(!instance) {
			instance = [ModelLocator new];
			/*instance.serverlist = [NSMutableArray new];*/
			
			[instance setValue:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:k_DATA_APP_VERSION];
		}
	}
	
	return instance;
}

- (id) init {
	if (self = [super init]) {
		_observedKeys = [NSMutableDictionary new];
		observers = [NSMutableSet new];
		self.localServerAddresses = [NSMutableArray new];
		[self setValue:[[NSMutableDictionary alloc] init] forKey:k_DATA_DNS_DICT];
	}
    if([[NSLocale preferredLanguages] count] > 0){
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        NSLog(@"locale: %@", language);
    
        [self setValue:[NSNumber numberWithInt:0] forKey:k_DATA_APARTMENT_LAST_CALLED_SCENE];
    }
	return self;
}

- (void) loadDefaults {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	/*NSString *port = [defaults valueForKey:@"dss_port"];
	
	if (port && [port length] > 0) {
		[self setValue:port forKey:k_DSS_PORT];
	}
	else {
		[self setValue:@"8080" forKey:k_DSS_PORT];
		port = @"8080";
	}
	
	NSString *ip = [defaults valueForKey:@"dss_ip"];
	
	[self.serverlist removeAllObjects];
	
	if (ip) {
		if ([ip length] > 0) {
			if (![self.serverlist containsObject:[NSString stringWithFormat:@"%@:%@", ip, port]]) {
				[self.serverlist addObject:[NSString stringWithFormat:@"%@:%@", ip, port]];
				NSMutableDictionary *dns = [self valueForKey:k_DATA_DNS_DICT];
				[dns setValue:[NSString stringWithFormat:@"%@:%@", ip, port] forKey:[NSString stringWithFormat:@"%@:%@", ip, port]];
			}
			
		}
	}
	*/
	
	
//	NSNumber *demomode = [defaults valueForKey:@"settings_demomodus"];
//	
//	if (demomode && [demomode boolValue]) {
//		[self setValue:demomode  forKey:k_DATA_DEMOMODE_ACTIVE];
//	}
//	else if (!demomode) {
//		[self setValue:[NSNumber numberWithBool:YES]  forKey:k_DATA_DEMOMODE_ACTIVE];
//	}
//	else  {
//		[self setValue:[NSNumber numberWithBool:NO]  forKey:k_DATA_DEMOMODE_ACTIVE];
//	}
    
    NSNumber *demomode = [defaults valueForKey:@"settings_demomodus"];
    
    [self setValue:demomode  forKey:k_DATA_DEMOMODE_ACTIVE];
	
	
	if([defaults valueForKey:k_STANDARD_OUTSIDE_TEMP]) {
		[self setValue:[defaults valueForKey:k_STANDARD_OUTSIDE_TEMP] forKey:k_STANDARD_OUTSIDE_TEMP];
	}
	
	NSString *username = [defaults valueForKey:k_DSS_USERNAME];
	NSString *password = [defaults valueForKey:k_DSS_PASSWORD];
	
	if (username) {
		[self setValue:username forKey:k_DSS_USERNAME];
	}
	else {
		[self setValue:@"dssadmin" forKey:k_DSS_USERNAME];
	}
	
	
	if (password) {
		[self setValue:password forKey:k_DSS_PASSWORD];
	}
	else {
		[self setValue:@"dssadmin" forKey:k_DSS_PASSWORD];
	}
	
	[DigitalstromJSONService instance].dssUsername = [self valueForKey:k_DSS_USERNAME];
	[DigitalstromJSONService instance].dssPassword = [self valueForKey:k_DSS_PASSWORD];
	
	NSString *maxValue = [defaults valueForKey:k_DATA_ENERGY_MAX_VALUE];
	if(maxValue && [maxValue intValue] > 0) {
		[self setValue:maxValue forKey:k_DATA_ENERGY_MAX_VALUE];
	}
}

- (id)valueForKey:(NSString *)key {
	id value = [_observedKeys objectForKey:key];
	
	if(!value)
		return nil;
	else {
		return value;
	}
}

- (void)setValue:(id)value forKey:(NSString *)key {
//	GSLog(@"ML setValue: \"%@\" forKey: \"%@\"", value, key);
	[self willChangeValueForKey:key];
	
//	id oldValue = [_observedKeys objectForKey:key];
		
	if (!value) {
		[_observedKeys removeObjectForKey:key];
	}
	else {
		[_observedKeys setObject:value forKey:key];
	}
	
//	if (oldValue && [oldValue isKindOfClass:[NSObject class]]) {
//		[(NSObject*)oldValue release];
//	}
	
	[value retain];
	//NSLog(@"complete observerlist (key=\"%@\"): %@", key, observers);
	[self didChangeValueForKey:key];
}

- (void) traceObjects {
	GSLog(@"JoinCenter Content: \n%@", _observedKeys);
}

- (void) addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context {
	[observers addObject:observer];
	[super addObserver:observer forKeyPath:keyPath options:options context:context];
}

- (void) removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath {
	[observers removeObject:observer];
    @try {
        [super removeObserver:observer forKeyPath:keyPath];
    }
    @catch (NSException *exception) {
        //just in case
    }
}

@end
