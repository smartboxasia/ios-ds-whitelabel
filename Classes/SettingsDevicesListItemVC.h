//
//  SettingsDevicesListItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSDevice;

@interface SettingsDevicesListItemVC : UIViewController {
	DSDevice *device;
	BOOL hasDarkBackground;
	
	UIImageView *imageBackground;
	UILabel *labelName;
	UIButton *buttonStepInto;
    UIImageView *imageGroupIcon;
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UIImageView *imageGroupIcon;
@property (nonatomic, retain) IBOutlet UILabel *labelName;
@property (nonatomic, retain) IBOutlet UIButton *buttonStepInto;

- (id) initWithDevice: (DSDevice*) theDevice andDarkBg: (BOOL) isDark;
- (IBAction) actionShowDetail;


@end
