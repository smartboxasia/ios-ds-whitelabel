//
//  RoomDeviceListVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSRoom;

@interface RoomDeviceListVC : GAITrackedViewController {
	DSRoom *room;

	UILabel *labelTitle;
	UIScrollView *scrollView;
    NSMutableArray *availableColors;
	UIView *viewRibbon;

	BOOL cameFromOverview;
	float offset;
    int currentIndex;
    BOOL animateItems;
    
    CGFloat sWidth;
    CGFloat sHeight;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UILabel *labelTitle;
@property (nonatomic, retain) IBOutlet UIView *viewRibbon;
@property (assign) BOOL cameFromOverview;
@property (assign) float offset;
@property (assign) int currentIndex;
@property (assign) BOOL animateItems;

- (id) initWithRoom: (DSRoom*) theRoom andOffset: (float) theOffset withIndex:(int)index;
- (void) buildScrollView;
- (DSRoom*) currentRoomShowing;
- (void) storeInML;
- (void) animateStartup;
- (void) animateShutdown;
- (void) handleLeftSwipeGesture: (UISwipeGestureRecognizer *) recognizer;
- (void) handleRightSwipeGesture: (UISwipeGestureRecognizer *) recognizer;

- (IBAction) buttonActionBackward: (id) sender;

@end
