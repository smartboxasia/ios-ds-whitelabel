//
//  WelcomeDefaultVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "WelcomeDefaultVC.h"
#import "WelcomeLocalVC.h"
#import "DigitalstromJSONService.h"
#import "ModelLocator.h"
#import "StartupController.h"
#import "DSServer.h"
#import "DataController.h"

@interface WelcomeDefaultVC ()

@end

@implementation WelcomeDefaultVC

@synthesize buttonLocalConnection,buttonConnect,welcomeLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.welcomeLabel.text = NSLocalizedString(@"key_welcome", nil);
    self.useYourLabel.text = NSLocalizedString(@"key_settings_new_connection_info1", nil);
    self.pleaseNoteLabel.text = NSLocalizedString(@"key_settings_new_connection_info2", nil);
    [self.buttonConnect setTitle:NSLocalizedString(@"key_connect", nil) forState:UIControlStateNormal];
    [self.buttonLocalConnection setTitle:NSLocalizedString(@"key_use_local_connection", nil) forState:UIControlStateNormal];
    self.textUsername.placeholder = NSLocalizedString(@"key_email", nil);
    self.textPassword.placeholder = NSLocalizedString(@"key_password", nil);
    
//	if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//		//	move some controls down for iphone 5 display
//		
//		//self.buttonConnect.center = CGPointMake(self.buttonConnect.center.x, self.buttonConnect.center.y + 75);
//		//[self.buttonConnect updateConstraints];
//		
//		self.imageBackground.image = [UIImage imageNamed:@"SplashBG-568h.png"];
//    }

    // fix stupid hardcode height and width
    CGFloat screenWidth  = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    [self.view setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    
    // iphone 4 support
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 480.0f) {
        GSLog(@"11111");
        self.imageLogo.hidden = YES;
    } else {
        GSLog(@"22222");
        self.imageLogo.hidden = NO;
    }

    GSLog(@"self.image.hidden: %hhd", self.imageLogo.hidden);

	self.indicatorConnecting.alpha = 0;
	
	[[ModelLocator instance] setValue:self forKey:k_VIEW_WELCOME];
	
    [[StartupController instance] startDssSearch];
    
    self.screenName = @"Login";
    
    GSLog(@"self.view.bounds.size %@", NSStringFromCGSize(self.view.bounds.size));

}

- (IBAction) actionConnect:(id)sender {
	NSString *username = [self.textUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	NSString *password = [self.textPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	
	if ([username length] == 0 || [password length] == 0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"error") message:NSLocalizedString(@"key_welcome_login_specify_credentials", @"error") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"OK") otherButtonTitles: nil];
		
		[alert show];
		
		return;
	}
		
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		BOOL result = [[DigitalstromJSONService instance] performDsLogin:username withPassword:password andCloudUrl:keyProductionCloudUrl];
		
		if (result) {
            //wait on dss (workaround for possible bug on dss)
            sleep(2);
            
			dispatch_async(dispatch_get_main_queue(), ^{
				DSServer *dss = [[StartupController instance] createDssForToken:[[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN] andRelayLink:[[ModelLocator instance] valueForKey:k_DSS_IP]];
				dss.cloudEmail = username;
				dss.cloudPassword = password;
                dss.cloudUrl = keyProductionCloudUrl;
				dss.connectionType = DSS_CONNECTION_TYPE_CLOUD;
				dss.detectedByBonjour = NO;
				
				/*
				 *	mark all other dsServers in database as "not connected"
				 */
				NSArray *allDss = [[DataController instance] fetchAllDss];
				
				for (DSServer* dss in allDss) {
					if ([[DataController instance] getCurrentDSS] != dss) {
						dss.connected = NO;
					}
				}
				
				/*
				 *	make sure current dss is connected
				 */
				dss.connected = YES;
				
				[[DataController instance] saveContext];
				
				/*
				 *	send notification about connection state
				 */
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationConnectedToDss object:nil];
				
				[[StartupController instance] performDssConnectWithServer:dss];
				
				/*[UIView animateWithDuration:0.3 animations:^{
					self.view.alpha = 1;
					self.indicatorConnecting.alpha = 0;
					
				} completion:^(BOOL finished) {
					[self.view removeFromSuperview];
					[self removeFromParentViewController];
				}];*/
			});
		}
		else {
			dispatch_async(dispatch_get_main_queue(), ^{
				[UIView animateWithDuration:0.3 animations:^{
					self.buttonConnect.alpha = 1;
					self.buttonLocalConnection.alpha = 1;
					self.indicatorConnecting.alpha = 0;
					
				} completion:^(BOOL finished) {
					
				}];
				
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"error") message:NSLocalizedString(@"key_credentials_wrong_pass_title", @"error") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"OK") otherButtonTitles: nil];
				
				[alert show];
			});
		}
	});
	
	[UIView animateWithDuration:0.3 animations:^{
		self.buttonConnect.alpha = 0;
		self.buttonLocalConnection.alpha = 0;
		self.indicatorConnecting.alpha = 1;
		
	} completion:^(BOOL finished) {
		
	}];
	
	
}

- (IBAction) actionUseLocalConnection:(id)sender {
    GSLog(@"log");

	WelcomeLocalVC *localVc = [[WelcomeLocalVC alloc] init];

	localVc.view.frame = self.view.window.frame;
	localVc.view.alpha = 0;

	[self.view.superview addSubview:localVc.view];
	[self.parentViewController addChildViewController:localVc];

	GSLog(@"self.view.bounds.size: %@", NSStringFromCGSize(self.view.bounds.size));
    GSLog(@"localVc.view.bounds.size: %@", NSStringFromCGSize(localVc.view.bounds.size));
    GSLog(@"self.view.window.bounds.size %@", NSStringFromCGSize(self.view.window.bounds.size));

	[UIView animateWithDuration:0.4 animations:^{
		localVc.view.alpha = 1;
		self.view.alpha = 0;
	} completion:^(BOOL finished) {
		[self.view removeFromSuperview];
		[self removeFromParentViewController];
	}];
}

#pragma mark - 
#pragma mark UITextfieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.textUsername) {
		[self.textPassword becomeFirstResponder];
	}
	else {
		[textField resignFirstResponder];
		[self actionConnect:textField];
	}
	return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
