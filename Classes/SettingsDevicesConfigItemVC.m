//
//  SettingsDevicesConfigItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDevicesConfigItemVC.h"
#import "DSDevice.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "DSDevice+Group1.h"

@implementation SettingsDevicesConfigItemVC

@synthesize imageBackground, labelName,labelValue,dimmerSlider,loadingWheel;

- (id) initWithDevice: (DSDevice*) theDevice andDarkBg: (BOOL) isDark {
	if(self = [super init]) {
		device = theDevice;
		hasDarkBackground = isDark;
        isDimmable = NO;
	}
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	if(device) {
		self.labelName.text = device.name;
		
		if(hasDarkBackground) {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_hell_doppelt-02.png"];		
		}
		else {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_hell_doppelt.png"];
		}
        
        
        //depending on outputmode show matching control
        
        //no output mode
        if ([device.outputMode integerValue] == kDeviceOutputModeDisabled) {
            onOffSwitch.hidden = YES;
            self.dimmerSlider.hidden = YES;
            self.labelValue.hidden = YES;
        }
        //dimmable
        else if ([device isOutputModeDimmable]) {
            onOffSwitch.hidden = YES;
            self.dimmerSlider.hidden = NO;
            self.labelValue.hidden = NO;
            isDimmable = YES;
        }
        //switching
        else{
            onOffSwitch.hidden = NO;
            self.dimmerSlider.hidden = YES;
            self.labelValue.hidden = YES;
            isDimmable = NO;
            //reduce height to label height 37
            [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width,37)];
        }
        
        //make orange slider
        [self.dimmerSlider setMinimumTrackImage:[[UIImage imageNamed:@"orangeslider.png"] stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0] forState:UIControlStateNormal];
        //fix custom orange switch
        onOffSwitch.frame = CGRectMake(169, 5, 94, 27);
        
        //we start out by showing the loading animation
        [self toggleLoadingAnimation:YES];
        
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //add observer for the dimmFactor
    if(isDimmable)
        [device addObserver:self forKeyPath:@"dimmFactor" options:0 context:nil];
    else
        [device addObserver:self forKeyPath:@"powerOn" options:0 context:nil];

}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //remove observers
    if(isDimmable)
        [device removeObserver:self forKeyPath:@"dimmFactor"];
    else
        [device removeObserver:self forKeyPath:@"powerOn"];
    
}

- (void) toggleLoadingAnimation: (BOOL) turnOn {
    if (turnOn){
        self.loadingWheel.hidden = NO;
        [self.loadingWheel startAnimating];
        
        onOffSwitch.hidden = YES;
        self.dimmerSlider.hidden = YES;
        self.labelValue.hidden = YES;
    }
    else{
        self.loadingWheel.hidden = YES;
        [self.loadingWheel stopAnimating];
        
        if(isDimmable){
            self.dimmerSlider.hidden = NO;
            self.labelValue.hidden = NO;
        }
        else{
            onOffSwitch.hidden = NO;
        }
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"dimmFactor"]) {
        [self toggleLoadingAnimation:NO];
        self.dimmerSlider.value = [device.dimmFactor floatValue];
        labelValue.text = [NSString stringWithFormat:@"%.0f%%",[device.dimmFactor floatValue]*100];
        
	}
    else if ([keyPath isEqualToString:@"powerOn"]) {
        [self toggleLoadingAnimation:NO];
        onOffSwitch.on = [device.powerOn boolValue];
	}
}

- (IBAction) switchChanged: (id) sender{
    if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] setDevicePower:device withBool:((UICustomSwitch*) sender).on withGroup:[device.group integerValue]];
		});
	}
}

- (IBAction) sliderChanged: (id) sender{
    labelValue.text = [NSString stringWithFormat:@"%.0f%%",((UISlider*)sender).value*100];
}

- (IBAction) sliderTouchUpInside: (id) sender{
    if (device) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] setDeviceValue:device withValue:[NSNumber numberWithFloat:((UISlider*)sender).value]];
		});
	}
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
    
}


@end
