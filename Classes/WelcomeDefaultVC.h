//
//  WelcomeDefaultVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"
#import "GAI.h"

@interface WelcomeDefaultVC : GAITrackedViewController <UITextFieldDelegate>

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicatorConnecting;
@property (nonatomic, retain) IBOutlet UITextField *textUsername;
@property (nonatomic, retain) IBOutlet UITextField *textPassword;

@property (nonatomic, retain) IBOutlet UIButton *buttonConnect;
@property (nonatomic, retain) IBOutlet UIButton *buttonLocalConnection;

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UIImageView *imageLogo;

@property (nonatomic, retain) IBOutlet CustomLabel *welcomeLabel;
@property (nonatomic, retain) IBOutlet CustomLabel *useYourLabel;
@property (nonatomic, retain) IBOutlet CustomLabel *pleaseNoteLabel;

- (IBAction) actionConnect:(id)sender;
- (IBAction) actionUseLocalConnection:(id)sender;


@end
