//
//  GAIHelper.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "GAIHelper.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "DSServer.h"
#import "DSScene.h"
#import "DataController.h"

static NSString *const kTestGaPropertyId = @"UA-59963157-6";
static NSString *const kProductiveGaPropertyId = @"UA-30239782-10";
static NSString *const kTrackingPreferenceKey = @"allowTracking";
static BOOL const kGaDryRun = NO;
static BOOL const kGaTrackUncaughtException = YES;
static int const kGaDispatchPeriod = 30;
static id<GAITracker> tracker;

static NSString *const kUIAction = @"ui_action";

@implementation GAIHelper

+(void)configure:(BOOL)isTestVersion {
    if(![[NSUserDefaults standardUserDefaults] objectForKey:kTrackingPreferenceKey]) {
        //Check if currently a cloud connection is available
        for(DSServer* dssServer in [[DataController instance] fetchAllDss]) {
            if ([dssServer.connectionType isEqual:DSS_CONNECTION_TYPE_CLOUD]) {
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:kTrackingPreferenceKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }
    
    [[GAI sharedInstance] setDispatchInterval:kGaDispatchPeriod];
    [[GAI sharedInstance] setDryRun:kGaDryRun];
    [[GAI sharedInstance] setTrackUncaughtExceptions:kGaTrackUncaughtException];
    [[GAI sharedInstance] setOptOut:![[NSUserDefaults standardUserDefaults] boolForKey:kTrackingPreferenceKey]];
    
    if(isTestVersion) {
        tracker = [[GAI sharedInstance] trackerWithTrackingId:kTestGaPropertyId];
    }
    else {
        tracker = [[GAI sharedInstance] trackerWithTrackingId:kProductiveGaPropertyId];
    }

}

+(void)allowTracking:(BOOL)allowTracking {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kTrackingPreferenceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [GAI sharedInstance].optOut = NO;
}

+(void)sendUIActionWithCategory:(NSString*)category andLabel:(NSString*)label {
    [self sendAction:kUIAction withCategory:category label:label andValue:nil];
}

+(void)sendAction:(NSString*)action withCategory:(NSString*)category label:(NSString*)label andValue:(NSNumber*)value {
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:category action:action label:label value:value] build]];
}

+(void)sendSceneCall:(DSScene*)scene {
    if (!scene.room) {
        if([scene.sceneNo longLongValue] < 1024){
            [GAIHelper sendUIActionWithCategory:@"scene_called" andLabel:@"global"];
        }
        else{
            [GAIHelper sendUIActionWithCategory:@"scene_called" andLabel:@"uda"];
        }
    } else {
        [GAIHelper sendUIActionWithCategory:@"scene_called" andLabel:@"room"];
    }
}

+(void)trackViewControllerWithName:(NSString *)name
{
    // Track in Google Analytics
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

@end
