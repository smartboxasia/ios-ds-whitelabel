//
//  SettingsNetworkConnectionDetailVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"
#import "GAI.h"

@class DSServer;

@interface SettingsNetworkConnectionDetailVC : GAITrackedViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;

@property (nonatomic, retain) IBOutlet UIButton *buttonDelete;

@property (nonatomic, retain) IBOutlet UITextField *textfieldName;
@property (nonatomic, retain) IBOutlet UITextField *textfieldEmail;
@property (nonatomic, retain) IBOutlet UITextField *textfieldPassword;
@property (nonatomic, retain) IBOutlet UITextField *textfieldIp;
@property (nonatomic, retain) IBOutlet UITextField *textfieldPort;
@property (nonatomic, retain) IBOutlet UILabel *labelTitle;
@property (nonatomic, retain) IBOutlet UILabel *labelAccessToken;
@property (nonatomic, retain) IBOutlet UILabel *labelConnectionType;
@property (retain, nonatomic) IBOutlet CustomLabel *labelName;
@property (retain, nonatomic) IBOutlet CustomLabel *labelAccessTokenTitle;
@property (retain, nonatomic) IBOutlet CustomLabel *labelEmail;
@property (nonatomic, retain) IBOutlet UIButton *buttonEditName;
@property (nonatomic, retain) IBOutlet UIButton *buttonEditEmail;
@property (nonatomic, retain) IBOutlet UIButton *buttonEditIp;
@property (nonatomic, retain) IBOutlet UIButton *buttonEditPort;

@property (nonatomic, retain) IBOutlet UIView *viewName;
@property (nonatomic, retain) IBOutlet UIView *viewEmail;
@property (nonatomic, retain) IBOutlet UIView *viewIp;
@property (nonatomic, retain) IBOutlet UIView *viewPort;
@property (nonatomic, retain) IBOutlet UIView *viewToken;

- (id)initWithConnection: (DSServer*) _dsServer;

- (IBAction) actionEditButtonName: (id)sender;
- (IBAction) actionEditButtonEmail: (id)sender;
- (IBAction) actionEditButtonPassword: (id)sender;
- (IBAction) actionEditButtonIp: (id)sender;
- (IBAction) actionEditButtonPort: (id)sender;

- (IBAction) actionSaveAndConnect: (id)sender;
- (IBAction) actionSave: (id)sender;
- (IBAction) actionDelete: (id)sender;
- (IBAction) actionBack: (id)sender;

@end
