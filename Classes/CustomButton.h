//
//  CustomButton.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ActionBlock)();

@interface CustomButton : UIButton {
	ActionBlock _actionBlock;
}


- (void) setBold: (BOOL) bold;
- (void) handleControlEvent:(UIControlEvents)event withBlock:(ActionBlock) action;
- (void) callActionBlock:(id)sender;

@end
