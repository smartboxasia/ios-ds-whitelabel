//
//  CustomButton.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "CustomSwipeRecognizer.h"


@implementation CustomSwipeRecognizer


-(void) addTargetBlock: (ActionBlock) action{
	_actionBlock = Block_copy(action);
	[self addTarget:self action:@selector(callActionBlock:)];
}

-(void) callActionBlock:(id)sender{
	_actionBlock();
}

-(void) dealloc{
	Block_release(_actionBlock);
	[super dealloc];
}

@end
