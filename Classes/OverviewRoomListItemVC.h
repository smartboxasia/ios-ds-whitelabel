//
//  OverviewRoomListItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSRoom;

@interface OverviewRoomListItemVC : UIViewController {
	UIButton *buttonRoomLabel;
	
	DSRoom *room;
}

@property (nonatomic, retain) IBOutlet UIButton *buttonRoomLabel;

- (id) initWithRoom: (DSRoom*) theRoom;

//- (IBAction) actionButtonRoomLabel: (id) sender;
- (IBAction) actionButtonRoomArrow: (id) sender;

- (void) prepareDealloc;

@end
