//
//  SettingsInfoBarVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class CustomLabel;

@interface SettingsInfoBarVC : GAITrackedViewController {
	UIView *scrollview;	
	NSMutableArray *items;
    IBOutlet CustomLabel *titleLabel;
}

@property (nonatomic, retain) IBOutlet UIView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (void) buildScrollview;
- (void) magneticPointHasChanged: (int) from toIndex: (int) to;
- (void) arrangeItems;

- (IBAction) actionBack;
@end
