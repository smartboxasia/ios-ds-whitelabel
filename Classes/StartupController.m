//
//  StartupController.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "StartupController.h"
#import <arpa/inet.h>
#import <netinet/in.h>
#import <sys/socket.h>
#import "ModelLocator.h"
#import "FacilityController.h"
#import "DataController.h"
#import "MainVC.h"
#import "DSServer.h"
#import "StartupServerSelectionVC.h"
#import "SB_iPhoneViewController.h"


@implementation StartupController

+ (StartupController*) instance {
	static StartupController *instance;
	
	@synchronized(self) {
		if(!instance) {
			instance = [[StartupController alloc] init];
		}
	}
	
	return instance;
}

- (id) init {
	if (self =[super init]) {
		tokenInvalid = NO;
        loginShowed = NO;
	}
	
	[[NSNotificationCenter defaultCenter] addObserverForName:k_DATA_APPLICATION_TOKEN_NOT_VALID object:nil queue:nil usingBlock:^(NSNotification *arg1) {
		if (!tokenInvalid) {
			tokenInvalid = YES;
//			[[DataController instance] endMeterConsumptionPolling];
			
			DSServer *dss = [[DataController instance] getCurrentDSS];
			dss.appToken = nil;
			//[DigitalstromJSONService instance].dssApplicationToken = nil;
			[self prepareStructure];
		}		
	}];
	
	return self;
}

- (void) startDssSearch {
	
	[[ModelLocator instance].localServerAddresses removeAllObjects];
	
	aNetServiceBrowser = [[NSNetServiceBrowser alloc] init];
	if(!aNetServiceBrowser) {
        // The NSNetServiceBrowser couldn't be allocated and initialized.
		return;
	}
	
	aNetServiceBrowser.delegate = self;
	[aNetServiceBrowser searchForServicesOfType:@"_dssweb._tcp" inDomain:@"local"];
	
	if ([[ModelLocator instance] valueForKey:k_DATA_DEMOMODE_ACTIVE] && [[[ModelLocator instance] valueForKey:k_DATA_DEMOMODE_ACTIVE] boolValue]) {
		if (![[ModelLocator instance].localServerAddresses containsObject:@"demo.dss.local"]) {
			[[ModelLocator instance].localServerAddresses addObject:@"demo.dss.local"];
		}
	}
	
    //add known servers
    /*NSArray *knownServers = [[DataController instance] allocAllKnownDss];
	
    if (knownServers) {
        for (int i = 0; i<[knownServers count] ; i++) {
            if (![[ModelLocator instance].localServerAddresses containsObject:[knownServers objectAtIndex:i]]){
                [[ModelLocator instance].localServerAddresses addObject:[knownServers objectAtIndex:i]];
            }
        }
        [ModelLocator instance].localServerAddresses = [ModelLocator instance].localServerAddresses;
    }*/
	
	
	//[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(loadStructureTimeout) userInfo:nil repeats:NO];
}

- (void) stopDssSearch {
	[aNetServiceBrowser stop];
	if (currentResolve != nil) {
		[currentResolve stop];
		[currentResolve release];
		currentResolve = nil;
	}
}

- (void) prepareStructure {
    //	GSLog(@"ip: %@", [[ModelLocator instance] valueForKey:k_DSS_IP]);

        [[DataController instance] initStructure];
        
        DSServer *dss = [[DataController instance] getCurrentDSS];
        
        NSNumber *demoMode = [[ModelLocator instance] valueForKey:k_DATA_DEMOMODE_ACTIVE];
        
        if (![demoMode boolValue] && (!dss || (dss && !dss.appToken)) && !loginShowed && tokenInvalid) {
            NSString *appToken = [[DigitalstromJSONService instance] getApplicationToken:[[ModelLocator instance] valueForKey:k_DSS_IP]];
						
            if (appToken) {
				
                [DigitalstromJSONService instance].dssApplicationToken = appToken;
                [[ModelLocator instance] setValue:appToken forKey:k_DSS_APPLICATION_TOKEN];
				dss.appToken = appToken;
                loginShowed = YES;
				[self displayCredentialsAlertView];
            }
            else {
				
                dispatch_async(dispatch_get_main_queue(), ^{
                [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] removeSplashAndAddOverview];
                    });

            }
        }
        else {
            [self loadStructure];
        }

}

- (void) loadStructure {
	BOOL coldStart = YES;
	
	if ([[ModelLocator instance] valueForKey:k_VIEW_ENERGIE_AMPEL]) {
		coldStart = NO;
	}
		
	dispatch_async(dispatch_get_main_queue(), ^{
		[[ModelLocator instance] setValue:@"" forKey:k_DSS_JSON_STRING];
	});
	
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		if([[DataController instance] initServerStructure]) {
			NSString *version = [[DataController instance] getDSSVersion];
			NSString *name = [[DataController instance] getApartmentName];
			
			dispatch_sync(dispatch_get_main_queue(), ^{
				[[ModelLocator instance] setValue:version forKey:k_DSS_VERSION];
				[[ModelLocator instance] setValue:name forKey:k_DSS_NAME];
				
				[self stopDssSearch];
				//[[FacilityController instance] startConsumptionPolling];
				//[[DataController instance] startMeterConsumptionPolling];
			});
		}
		else {
			dispatch_sync(dispatch_get_main_queue(), ^{
				[[NSNotificationCenter defaultCenter] postNotificationName:k_DATA_LOGIN_FAILED object:self];
			});
		}
		
		dispatch_sync(dispatch_get_main_queue(), ^{
			if (coldStart) {
				[[ModelLocator instance].appController initEV];
			}
			
			
		});
	});
	
	dispatch_async(dispatch_get_main_queue(), ^{
		[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] removeSplashAndAddOverview];
	});
}

- (void) continueTokenAuthenticationWithUser: (NSString*) username andPassword: (NSString*) password {
	NSString *token = [[DigitalstromJSONService instance] performLogin:username withPassword:password withIp:[[ModelLocator instance] valueForKey:k_DSS_IP]];
	
	if (token) {
		BOOL tokenEnabled = [[DigitalstromJSONService instance] performEnableToken:[[ModelLocator instance] valueForKey:k_DSS_APPLICATION_TOKEN] withIp:[[ModelLocator instance] valueForKey:k_DSS_IP] andLoginToken: token];
		
		if (tokenEnabled) {
			tokenInvalid = NO;
            loginShowed = NO;
			[self loadStructure];
		}
		else {
			dispatch_async(dispatch_get_main_queue(), ^{
				[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] removeSplashAndAddOverview];
			});
		}
	}
	else {
		[self displayCredentialsAlertView];
	}
}

- (BOOL) performDigitalstromLoginWith: (NSString*) username andPassword: (NSString*) password {
    [[DataController instance] initStructure];
	BOOL success = [[DigitalstromJSONService instance] performDsLogin:username withPassword:password andCloudUrl:keyProductionCloudUrl];
	
	if (success) {
        tokenInvalid = NO;
        loginShowed = NO;
        [self loadStructure];
    }
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationConnectedToDss object:nil];
    //[ModelLocator instance].serverlist = [ModelLocator instance].serverlist;
//		else {
//			dispatch_async(dispatch_get_main_queue(), ^{
//				[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] removeSplashAndAddOverview];
//			});
//		}
//	}
    return success;
}

- (void) loadStructureTimeout {
	if ([[ModelLocator instance] valueForKey:k_DSS_IP]) {
		return;
	}
	if (![[ModelLocator instance] valueForKey:k_DSS_IP]) {
		[[ModelLocator instance] setValue:@"192.168.178.82" forKey:k_DSS_IP];
	}
	
	if (![[ModelLocator instance] valueForKey:k_DSS_JSON_STRING]) {
			//[[ModelLocator instance] setValue:@"" forKey:k_DSS_JSON_STRING];
	}
	
	
	dispatch_async(dispatch_get_main_queue(), ^{
		[[ModelLocator instance] setValue:@"0" forKey:k_DSS_AVAILABLE];
	});
	
}

- (void) performDssConnectWithServer: (DSServer*) dsServer {
	
	
	if ([[DataController instance] getCurrentDSS] != dsServer) {
		[[DataController instance] setCurrentDSS:dsServer];
	}
	
	if (dsServer.detectedByBonjour) {
		dsServer.detectedByBonjour = NO;
	}
	
	if (!dsServer.appToken) {
		tokenInvalid = YES;
	}
	
	dsServer.lastConnectionTS = [NSDate date];
	[[DataController instance] saveContext];
	
	[[ModelLocator instance] setValue:dsServer.appToken forKey:k_DSS_APPLICATION_TOKEN];
	
	/*
	 *	add port to url, if necessary
	 */
	if (dsServer.ip && ([[dsServer.ip componentsSeparatedByString:@":"] count] <= 1)) {
		if ([dsServer.port integerValue] > 0) {
			[[ModelLocator instance] setValue:[NSString stringWithFormat:@"%@:%i", dsServer.ip, (int)[dsServer.port integerValue]] forKey:k_DSS_IP];
		}
		else {
			[[ModelLocator instance] setValue:[NSString stringWithFormat:@"%@:%i", dsServer.ip, 8080] forKey:k_DSS_IP];
		}
		
	}
	else {
		[[ModelLocator instance] setValue:dsServer.ip forKey:k_DSS_IP];
	}
	
	[[ModelLocator instance] setValue:dsServer.name forKey:k_DSS_NAME];
	
	[self doPostConnectionAndInterfaceStartup];
}

- (void) performDssConnect: (NSString*) serverUrl {
	NSMutableDictionary *dns = [[ModelLocator instance] valueForKey:k_DATA_DNS_DICT];
	NSString *ip = [dns valueForKey:serverUrl];
	
	if (ip) {
		//	real IP
		[[ModelLocator instance] setValue:ip forKey:k_DSS_IP];
	}
	else {
		// host name
		[[ModelLocator instance] setValue:serverUrl forKey:k_DSS_IP];
	}
	
	[self doPostConnectionAndInterfaceStartup];
}

- (void) doPostConnectionAndInterfaceStartup {
		
	[[StartupController instance] prepareStructure];
	
	[UIView animateWithDuration:0.4 animations:^{
		((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_SERVERLIST]).view.alpha = 0;
	} completion:^(BOOL finished) {
		((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_SERVERLIST]).view.hidden = YES;
		[((UIViewController*)[[ModelLocator instance] valueForKey:k_VIEW_SERVERLIST]).view removeFromSuperview];
		[(StartupServerSelectionVC*)[[ModelLocator instance] valueForKey:k_VIEW_SERVERLIST] prepareDealloc];
		[[[ModelLocator instance] valueForKey:k_VIEW_SERVERLIST] release];
		[[ModelLocator instance] setValue:nil forKey:k_VIEW_SERVERLIST];
	} ];
	
    //navigate to overview when changing server
    if([[ModelLocator instance] valueForKey:k_VIEW_ROOM_DEVICELIST] != nil){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 50000000), dispatch_get_main_queue(), ^{
            [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] animateBackFromRoomDevices:nil];
        });
    }
    else if([[ModelLocator instance] valueForKey:k_VIEW_ROOM_SCENELIST] != nil){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 50000000), dispatch_get_main_queue(), ^{
            [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] animateBackFromRoomScenes:nil];
        });
    }
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		if ([[DataController instance] initServerStructure]) {
			dispatch_sync(dispatch_get_main_queue(), ^{
				[[ModelLocator instance] setValue:@"1" forKey:k_DSS_AVAILABLE];
			});
			NSString *version = [[DataController instance] getDSSVersion];
			NSString *name = [[DigitalstromJSONService instance] getApartmentName];
			
			dispatch_sync(dispatch_get_main_queue(), ^{
				[[ModelLocator instance] setValue:version forKey:k_DSS_VERSION];
				[[ModelLocator instance] setValue:name forKey:k_DSS_NAME];
				
				[[ModelLocator instance].appController initEV];
			});
		}
		else {
			dispatch_sync(dispatch_get_main_queue(), ^{
				[[ModelLocator instance] setValue:@"0" forKey:k_DSS_AVAILABLE];
			});
			
		}
		
		
		
	});
}

- (DSServer*) createDssForToken: (NSString*) token andRelayLink: (NSString*) relayLink {
    
    // Look out for existing connection, if available update it in order to prevent duplicates.
    DSServer *dss = [[DataController instance] getDssForIpAddressInCoreData:relayLink];

    if (!dss)
    {
        dss = [[DataController instance] createDsServerEntity];
    }
	
	dss.appToken = token;
	dss.ip = relayLink;
	
	NSArray *urlComps = [relayLink componentsSeparatedByString:@":"];
	
	if (urlComps && urlComps.count > 0) {
		NSString *portStr = [urlComps objectAtIndex:urlComps.count-1];
		
		if (portStr) {
			NSInteger port = [portStr integerValue];
			dss.port = [NSNumber numberWithInteger:port];
		}
	}
	
	return dss;
}

#pragma mark -
#pragma mark Delegate Methods
#pragma mark Einstiegspunkt wenn dss gefunden
- (void) netServiceDidResolveAddress:(NSNetService *)service {
	[service retain];
	
	NSString *sURLResolved=[NSString stringWithFormat:@"%@:%d",[service hostName], 8080];
	NSArray *pAdresse=[service addresses];
	bool fResolved=false;
	for (int i=0;(i<[pAdresse count]) && (!fResolved);i++)
	{
		struct sockaddr_in  *socketAddress = nil;
		NSData *address = [pAdresse objectAtIndex:i];
		socketAddress = (struct sockaddr_in *)	[address bytes];
		{
			sURLResolved = [NSString stringWithFormat: @"%s:%d", inet_ntoa (socketAddress->sin_addr), 8080];
			
			fResolved=true;
		}
	}
	
	if (fResolved) {
		NSString *hostname = [NSString stringWithFormat:@"%@",[service hostName]];
		
		if (![[ModelLocator instance].localServerAddresses containsObject:hostname]) {
//			GSLog(@"dss found: %@", sURLResolved);
			NSMutableDictionary *dns = [[ModelLocator instance] valueForKey:k_DATA_DNS_DICT];
			[dns setValue:sURLResolved forKey:hostname];
			[[ModelLocator instance].localServerAddresses addObject:hostname];
			[ModelLocator instance].localServerAddresses = [ModelLocator instance].localServerAddresses;
			
			/*
			 *	find dss by hostname
			 */
			DSServer *dss = [[DataController instance] getDssForIpAddressInCoreData:hostname];
			
			if (!dss) {
				
				/*
				 *	find dss by ip  address
				 */
				dss = [[DataController instance] getDssForIpAddressInCoreData:sURLResolved];
				
				if (!dss) {
					/*
					 *	dss not found, create a new one in DB
					 */
					dss = [[DataController instance] createDsServerEntity];
					dss.connectionType = DSS_CONNECTION_TYPE_MANUAL;
					dss.ip = hostname;
					dss.name = hostname;
					dss.connectionName = hostname;
					dss.port = [NSNumber numberWithInteger:[DSServer getPortFromUrl:sURLResolved]];
					dss.detectedByBonjour = YES;
					
					[[DataController instance] saveContext];
					
					[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDssListChanged object:nil];
				}
			}
		}
		
		/*[[ModelLocator instance] setValue:sURLResolved forKey:k_DSS_IP];
		[[ModelLocator instance] setValue:@"1" forKey:k_DSS_AVAILABLE];
		[[FacilityController instance] startUpdatingLocation];*/
		
		// struktur vom server holen:
		//dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		//	[[DataController instance] initServerStructure];
			//[[DataController instance] setDevicesToScenes];
		//});
		
		//[[FacilityController instance] startConsumptionPolling];
	}
}

- (void)netServiceBrowser:(NSNetServiceBrowser*)netServiceBrowser didFindService:(NSNetService*)service moreComing:(BOOL)moreComing 
{
//	GSLog(@"service found...");
	// If a service came online, add it to the list and update the table view if no more events are queued.
	[service retain];
	currentResolve = service;
	
	[currentResolve setDelegate:self];
	// Attempt to resolve the service. A value of 0.0 sets an unlimited time to resolve it. The user can
	// choose to cancel the resolve by selecting another service in the table view.
	[currentResolve resolveWithTimeout:0.0];
	
	// If moreComing is NO, it means that there are no more messages in the queue from the Bonjour daemon, so we should update the UI.
	// When moreComing is set, we don't update the UI so that it doesn't 'flash'.
	//if (moreComing==NO)
		//[aNetServiceBrowser stop];
}

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict 
{
//	GSLog(@"connection could not be resolved");
}

- (void)netServiceBrowser:(NSNetServiceBrowser*)netServiceBrowser didRemoveService:(NSNetService*)service moreComing:(BOOL)moreComing 
{
	// If a service went away, stop resolving it if it's currently being resolve,
	// remove it from the list and update the table view if no more events are queued.
	
}


- (void) displayCredentialsAlertView {
	//UITextField *textField; UITextField *textField2;
	
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_enter_credentials", @"")
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"key_cancel",nil)
                                              otherButtonTitles:NSLocalizedString(@"key_OK",nil), nil];
    
    alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
	
	/*UIAlertView *prompt = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"key_enter_credentials", @"") message:@"\n\n\n" delegate:self cancelButtonTitle:NSLocalizedString(@"key_cancel", @"") otherButtonTitles:NSLocalizedString(@"key_OK", @""), nil];
						   
	textField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 50.0, 260.0, 25.0)];  
	//[textField setBackgroundColor:[UIColor whiteColor]];
	textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	textField.borderStyle = UITextBorderStyleRoundedRect;
	textField.delegate = self;
	[textField setPlaceholder:NSLocalizedString(@"key_username", @"")];
	[prompt addSubview:textField];
	textField.tag = 12;
						   
	textField2 = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 85.0, 260.0, 25.0)];
	[textField2 setBackgroundColor:[UIColor whiteColor]];
	[textField2 setPlaceholder:NSLocalizedString(@"key_password", @"")];
	[textField2 setSecureTextEntry:YES];
	
	[prompt addSubview:textField2];
	
	textField2.borderStyle = UITextBorderStyleRoundedRect;
	textField2.delegate = self;
	textField2.tag = 13;
	
	//[prompt setTransform:CGAffineTransformMakeTranslation(0.0, 110.0)];
	[prompt show];
	[prompt release];
						   
	[textField becomeFirstResponder];*/
	[alertView show];
	[alertView release];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex // before animation and hiding view
{
	if (buttonIndex == 1) {
		/*UITextField *myTextField =(UITextField *) [alertView viewWithTag:12];
		UITextField *myTextField2 =(UITextField *) [alertView viewWithTag:13];*/
		
		UITextField *userNameField = [alertView textFieldAtIndex:0];
		UITextField *passwordField = [alertView textFieldAtIndex:1];
		
//		NSLog(@"%@ // %@", myTextField.text, myTextField2.text);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self continueTokenAuthenticationWithUser:userNameField.text andPassword:passwordField.text];
        });
		
	}
	
}

/*- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	UIView *superview = textField.superview;
	
	for (UIView *v in superview.subviews) {
		if ([v isKindOfClass:[UITextField class]] && v.tag == 13 && textField.tag == 12) {
			[(UITextField*)v becomeFirstResponder];
		}
		else if ([v isKindOfClass:[UITextField class]] && v.tag == 12 && textField.tag == 13) {
			[self continueTokenAuthenticationWithUser:((UITextField*)v).text andPassword:textField.text];
		}
	}
	return YES;
}*/

- (void)dealloc{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:k_DATA_APPLICATION_TOKEN_NOT_VALID object:nil];
    }
    @catch (NSException *exception) {
        //just in case
    }

    [super dealloc];
}

@end
