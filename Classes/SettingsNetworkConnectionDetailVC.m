//
//  SettingsNetworkConnectionDetailVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsNetworkConnectionDetailVC.h"
#import "DSServer.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"


@interface SettingsNetworkConnectionDetailVC () {
	DSServer *dsServer;
}
@end

@implementation SettingsNetworkConnectionDetailVC

@synthesize imageBackground;
@synthesize buttonDelete;
@synthesize textfieldEmail, textfieldIp, textfieldName, textfieldPassword, textfieldPort;
@synthesize labelAccessToken, labelConnectionType, labelTitle;
@synthesize buttonEditEmail, buttonEditIp, buttonEditName, buttonEditPort;

@synthesize viewEmail, viewIp, viewName, viewPort, viewToken;

- (id)initWithConnection: (DSServer*) _dsServer {
	self = [super init];
	
	if (self) {
		dsServer = _dsServer;

		if (dsServer) {
			[dsServer retain];
		}
		
    }
    
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	// iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        self.imageBackground.frame = CGRectMake(0, 0, 320, 513);
        [self.imageBackground setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }
	
    self.labelTitle.text = NSLocalizedString(@"key_connection", nil);
    self.labelName.text = NSLocalizedString(@"key_name", nil);
    self.labelAccessTokenTitle.text = NSLocalizedString(@"key_access_token", nil);
    self.labelEmail.text = NSLocalizedString(@"key_email", nil);
    
	if (dsServer) {
		if (dsServer.connectionType && [dsServer.connectionType isEqualToString:DSS_CONNECTION_TYPE_MANUAL]) {
			self.labelConnectionType.text = NSLocalizedString(@"key_settings_connection_type_manual", @"manual connection");
			
			/*self.buttonEditName.enabled = YES;
			self.buttonEditEmail.enabled = NO;
			self.buttonEditPassword.enabled = NO;
			self.buttonEditIp.enabled = YES;
			self.buttonEditPort.enabled = YES;*/
			
			/*self.textfieldName.enabled = YES;
			self.textfieldEmail.enabled = NO;
			self.textfieldPassword.enabled = NO;
			self.textfieldIp.enabled = YES;
			self.textfieldPort.enabled = YES;*/
			
			self.textfieldName.text = dsServer.name;
			self.textfieldEmail.text = @"";
			self.textfieldPassword.text = @"";
			self.textfieldIp.text = dsServer.ip;
			self.textfieldPort.text = [dsServer.port stringValue];
			
			if (dsServer.appToken && [[dsServer.appToken stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0) {
				self.labelAccessToken.text = NSLocalizedString(@"key_yes", @"yes");
			}
			else {
				self.labelAccessToken.text = NSLocalizedString(@"key_no", @"no");
			}
		}
		else if (dsServer.connectionType && [dsServer.connectionType isEqualToString:DSS_CONNECTION_TYPE_CLOUD]) {
			self.labelConnectionType.text = NSLocalizedString(@"key_settings_connection_type_cloud", @"cloud connection");
			
			/*
			self.buttonEditName.enabled = YES;
			self.buttonEditEmail.enabled = YES;
			self.buttonEditPassword.enabled = YES;
			self.buttonEditIp.enabled = NO;
			self.buttonEditPort.enabled = NO;
			
			self.textfieldName.enabled = YES;
			self.textfieldEmail.enabled = YES;
			self.textfieldPassword.enabled = YES;
			self.textfieldIp.enabled = NO;
			self.textfieldPort.enabled = NO;
			*/
			
			if (dsServer.connectionName) {
				self.textfieldName.text = dsServer.connectionName;
			}
			else {
                if(dsServer.cloudEmail)
                    self.textfieldName.text = dsServer.cloudEmail;
                else
                    self.textfieldName.text = dsServer.name;
			}

			self.textfieldEmail.text = dsServer.cloudEmail;
			self.textfieldPassword.text = dsServer.cloudPassword;
			self.textfieldIp.text = dsServer.ip;
			self.textfieldPort.text = [dsServer.port stringValue];
		}
	}
    
    self.screenName = @"Settings Server Connection Detail";
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	//NSLog(@"center: %@", NSStringFromCGPoint(self.viewIp.center));
	
	if (dsServer) {
		if (dsServer.connectionType && [dsServer.connectionType isEqualToString:DSS_CONNECTION_TYPE_MANUAL]) {
			
			
			//NSLog(@"center: %@", NSStringFromCGPoint(self.viewIp.center));
			self.viewIp.frame = CGRectMake(self.viewIp.frame.origin.x, self.viewIp.frame.origin.y - self.viewEmail.frame.size.height, self.viewIp.frame.size.width, self.viewIp.frame.size.height);
			//NSLog(@"center: %@", NSStringFromCGPoint(self.viewIp.center));
			self.viewPort.center = CGPointMake(self.viewPort.center.x, self.viewPort.center.y - self.viewEmail.frame.size.height);
			self.viewToken.center = CGPointMake(self.viewToken.center.x, self.viewToken.center.y - self.viewEmail.frame.size.height);
			
			[self.viewEmail updateConstraints];
			[self.viewIp updateConstraints];
			[self.viewPort updateConstraints];
			[self.viewToken updateConstraints];
			
			[self updateViewConstraints];
			[self.viewIp.superview updateConstraints];
			
			self.viewEmail.hidden = YES;
			self.buttonDelete.hidden = NO;
		}
		else if (dsServer.connectionType && [dsServer.connectionType isEqualToString:DSS_CONNECTION_TYPE_CLOUD]) {
			self.viewName.hidden = YES;
			
			self.viewEmail.center = CGPointMake(self.viewEmail.center.x, self.viewEmail.center.y - self.viewName.frame.size.height);
			self.viewIp.center = CGPointMake(self.viewIp.center.x, self.viewIp.center.y - self.viewName.frame.size.height);
			self.viewPort.center = CGPointMake(self.viewPort.center.x, self.viewPort.center.y - self.viewName.frame.size.height);
			self.viewToken.center = CGPointMake(self.viewToken.center.x, self.viewToken.center.y - self.viewName.frame.size.height);
			
			[self updateViewConstraints];
			[self.viewIp.superview updateConstraints];
		}
		
		
		NSInteger counter = 0;
		
		for (UIView *v1 in self.viewIp.superview.subviews) {
			if (v1.hidden) {
				continue;
			}
			
			for (UIView *v2 in v1.subviews) {
				
				if ([v2 isKindOfClass:[UIImageView class]]) {
					if (counter%2 == 0) {
						((UIImageView*)v2).image = [UIImage imageNamed:@"Einst_Liste_dunkel.png"];
					}
					else {
						((UIImageView*)v2).image = [UIImage imageNamed:@"Einst_Liste_hell.png"];
					}
					
					counter++;
					break;
				}
			}
		}
	}
}


#pragma mark -
#pragma mark IB Actions

- (IBAction) actionEditButtonName: (id)sender {
	if ([self.textfieldName isFirstResponder]) {
		[self.textfieldName resignFirstResponder];
	}
	else {
		[self.textfieldName becomeFirstResponder];
	}
}

- (IBAction) actionEditButtonEmail: (id)sender {
	if ([self.textfieldEmail isFirstResponder]) {
		[self.textfieldEmail resignFirstResponder];
	}
	else {
		[self.textfieldEmail becomeFirstResponder];
	}
}

- (IBAction) actionEditButtonPassword: (id)sender {
	if ([self.textfieldPassword isFirstResponder]) {
		[self.textfieldPassword resignFirstResponder];
	}
	else {
		[self.textfieldPassword becomeFirstResponder];
	}
}

- (IBAction) actionEditButtonIp: (id)sender {
	if ([self.textfieldIp isFirstResponder]) {
		[self.textfieldIp resignFirstResponder];
	}
	else {
		[self.textfieldIp becomeFirstResponder];
	}
}

- (IBAction) actionEditButtonPort: (id)sender {
	if ([self.textfieldPort isFirstResponder]) {
		[self.textfieldPort resignFirstResponder];
	}
	else {
		[self.textfieldPort becomeFirstResponder];
	}
}

- (IBAction) actionSaveAndConnect: (id)sender {
	[self writeTextFieldsToModelObject];
}

- (IBAction) actionSave: (id)sender {
	[self writeTextFieldsToModelObject];
}

- (IBAction) actionDelete: (id)sender {
	/*
	 *	permit deletion of current dss
	 */
	if ([dsServer isEqual:[[DataController instance] getCurrentDSS]]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Warning", @"title") message:NSLocalizedString(@"key_settings_network_edit_connection_deletion_impossible_alert_text", @"text") delegate:self cancelButtonTitle:NSLocalizedString(@"key_OK", @"OK") otherButtonTitles: nil];
		[alert show];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_settings_network_edit_connection_delete_alert_title", @"title") message:NSLocalizedString(@"key_settings_network_edit_connection_delete_alert_text", @"text") delegate:self cancelButtonTitle:NSLocalizedString(@"key_no", @"No") otherButtonTitles:NSLocalizedString(@"key_yes", @"yes"), nil];
		[alert show];
	}
	
}

- (IBAction) actionBack: (id)sender {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
}

- (void) writeTextFieldsToModelObject {
	if (dsServer) {
		dsServer.connectionName = self.textfieldName.text;
		dsServer.cloudEmail = self.textfieldEmail.text;
		dsServer.cloudPassword = self.textfieldPassword.text;
		
		if (dsServer.connectionType && [dsServer.connectionType isEqual:DSS_CONNECTION_TYPE_MANUAL]) {
			dsServer.ip = self.textfieldIp.text;
		}
		
		
		if ([self.textfieldPort.text integerValue] > 0 && [self.textfieldPort.text integerValue] < 65536) {
			dsServer.port = [NSNumber numberWithInteger:[self.textfieldPort.text integerValue]];
		}
		
	}
	
	[[DataController instance] saveContext];
	
	[self actionBack:nil];
}


#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	
	return YES;
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		//	cancel
	}
	else if (buttonIndex == 1) {
		//	deletion confirmed
		NSLog(@"will delete...");
		
		[[DataController instance] deleteDsServer:dsServer];
		[self actionBack:nil];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidUnload {
	if (dsServer) {
		[dsServer release];
		dsServer = nil;
	}
}

- (void)dealloc {
    [_labelName release];
    [_labelEmail release];
    [_labelAccessTokenTitle release];
    [super dealloc];
}
@end
