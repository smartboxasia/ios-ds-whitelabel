//
//  SettingsScenesDetailAddDeviceVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSScene;
@class DSDevice;

@interface SettingsScenesDetailAddDeviceVC : UIViewController {
	DSScene *scene;
	UIScrollView *scrollview;
}

@property(nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (id) initWithScene: (DSScene*) _scene;
- (IBAction) actionBack;
- (UIView*) allocViewForDeviceItem: (DSDevice*) device withYOffset:(float) yOffset;
- (BOOL) containsDevice: (DSDevice*) device inSet: (NSSet*) set;

- (void) activateReloadButton;

@end
