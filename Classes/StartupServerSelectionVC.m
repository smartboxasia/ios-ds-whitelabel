//
//  StartupServerSelectionVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "StartupServerSelectionVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "DSServer.h"
#import "StartupServerSelectionItemVC.h"

@implementation StartupServerSelectionVC

@synthesize scrollview;

- (id) init {
	self = [super init];
	
	if (self) {
		[[ModelLocator instance] setValue:self forKey:k_VIEW_SERVERLIST];
	}
	
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	serverItems = [NSMutableArray new];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
	[self buildScrollview];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(buildScrollview) name:kNotificationDssListChanged object:nil];
	//[[ModelLocator instance] addObserver:self forKeyPath:@"serverlist" options:0 context:nil];

}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationDssListChanged object:nil];
    }
    @catch (NSException *exception) {
        //just in case
    }

	//[[ModelLocator instance] removeObserver:self forKeyPath:@"serverlist"];
}


- (void) buildScrollview {
	
	for (StartupServerSelectionItemVC *vc in serverItems) {
		if (vc.view.superview) {
			[vc.view removeFromSuperview];
		}
	}
	
	[serverItems removeAllObjects];
	
	NSArray *dssList = [[DataController instance] fetchAllDss];
	
	NSInteger yOffset = 0;
	for (DSServer *dss in dssList) {
		StartupServerSelectionItemVC *item = [[StartupServerSelectionItemVC alloc] initWithDss:dss];
		item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
		
		yOffset += item.view.frame.size.height;
		
		[self.scrollview addSubview:item.view];
		[serverItems addObject:item];
	}
	
	self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, yOffset);
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void) prepareDealloc {
	for (StartupServerSelectionItemVC *vc in serverItems) {
		if (vc.view.superview) {
			[vc.view removeFromSuperview];
			[vc prepareDealloc];
			[vc release];
		}
	}
	
	[serverItems removeAllObjects];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
