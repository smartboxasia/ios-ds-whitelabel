//
//  WelcomeLocalVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"
#import "CustomButton.h"
#import "GAI.h"

@interface WelcomeLocalVC : GAITrackedViewController

@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UIImageView *imageLogo;

@property (retain, nonatomic) IBOutlet CustomLabel *welcomeLabel;
@property (retain, nonatomic) IBOutlet CustomLabel *selectLabel;
@property (retain, nonatomic) IBOutlet CustomButton *skipButton;

- (IBAction) actionSkip:(id)sender;

@end
