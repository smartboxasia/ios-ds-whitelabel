//
//  LoginWaitingVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "LoginWaitingVC.h"
#import "ModelLocator.h"
#import "StartupController.h"

@implementation LoginWaitingVC

@synthesize labelMessage, buttonRepeat, activityIndicator;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
    self.labelMessage.text = NSLocalizedString(@"key_establishing_connection", @"");
    self.buttonRepeat.titleLabel.text = NSLocalizedString(@"key_try_again", @"");
    
	observers = [NSMutableArray new];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.activityIndicator.hidden = NO;
	self.buttonRepeat.hidden = YES;
    

	
	id obs1 = [[NSNotificationCenter defaultCenter] addObserverForName:k_DATA_LOGIN_SUCCESSFUL object:nil queue:nil usingBlock:^(NSNotification *arg1) {
        //		GSLog(@"login successful");
		[self hideMe];
		self.activityIndicator.hidden = YES;
	}];
	
	id obs2 = [[NSNotificationCenter defaultCenter] addObserverForName:k_DATA_LOGIN_FAILED object:nil queue:nil usingBlock:^(NSNotification *arg1) {
		self.labelMessage.text = NSLocalizedString(@"key_error_dss_did_not_respond", @"");
		self.buttonRepeat.hidden = NO;
		self.activityIndicator.hidden = YES;
		GSLog(@"login failed");
	}];
	
	[observers addObject:obs1];
	[observers addObject:obs2];
    
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    for (id observer in observers) {
        @try {
            [[NSNotificationCenter defaultCenter] removeObserver:observer];
            
        }
        @catch (NSException *exception) {
            //just in case
        }
    }
}

- (void) hideMe {
	[UIView animateWithDuration:0.4 animations:^{
		self.view.alpha = 0;
	} completion:^(BOOL finished) {
		[self.view removeFromSuperview];
		//[self release];
	}];
}

- (IBAction) actionRetry {
	self.labelMessage.text = NSLocalizedString(@"key_establishing_connection", @"");
	self.buttonRepeat.hidden = YES;
	self.activityIndicator.hidden = NO;
	//[self hideMe];
	//dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
	[[StartupController instance] loadStructure];
	[[StartupController instance] startDssSearch];
	//});
	
}

- (IBAction) actionClose {
	[self hideMe];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
	
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
    [super dealloc];
}


@end
