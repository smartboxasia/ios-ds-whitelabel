//
//  DSRoom.h
//  DS iPhone
//
//  Created by rolf on 20.12.10.
//  Copyright 2010 Granny&Smith Technikagentur GmbH & Co. KG. All rights reserved.
//

#import <CoreData/CoreData.h>

@class DSDevice;
@class DSScene;

@interface DSRoom :  NSManagedObject  
{
}

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSSet* devices;
@property (nonatomic, retain) NSSet* scenes;

@end


@interface DSRoom (CoreDataGeneratedAccessors)
- (void)addDevicesObject:(DSDevice *)value;
- (void)removeDevicesObject:(DSDevice *)value;
- (void)addDevices:(NSSet *)value;
- (void)removeDevices:(NSSet *)value;

- (void)addScenesObject:(DSScene *)value;
- (void)removeScenesObject:(DSScene *)value;
- (void)addScenes:(NSSet *)value;
- (void)removeScenes:(NSSet *)value;

@end

