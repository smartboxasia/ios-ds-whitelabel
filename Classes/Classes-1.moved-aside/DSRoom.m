// 
//  DSRoom.m
//  DS iPhone
//
//  Created by rolf on 20.12.10.
//  Copyright 2010 Granny&Smith Technikagentur GmbH & Co. KG. All rights reserved.
//

#import "DSRoom.h"

#import "DSDevice.h"
#import "DSScene.h"

@implementation DSRoom 

@dynamic name;
@dynamic identifier;
@dynamic devices;
@dynamic scenes;

@end
