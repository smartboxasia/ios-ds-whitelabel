//
//  CustomButton.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "CustomButton.h"


@implementation CustomButton

- (void)drawRect:(CGRect)rect {
	
	//GSLog(@"font: %@", self.titleLabel.font.fontName);
	if ([self.titleLabel.font.fontName isEqualToString:@"Helvetica"] || [self.titleLabel.font.fontName isEqualToString:@".HelveticaNeueUI"]) {
		[self.titleLabel setFont:[UIFont fontWithName:@"PFBeauSansPro-Regular" size:self.titleLabel.font.pointSize]];
	}
	if ([self.titleLabel.font.fontName isEqualToString:@"Helvetica-Bold"]) {
		[self.titleLabel setFont:[UIFont fontWithName:@"PFBeauSansPro-Bold" size:self.titleLabel.font.pointSize]];
	}
	if ([self.titleLabel.font.fontName isEqualToString:@"Helvetica-BoldOblique"]) {
		[self.titleLabel setFont:[UIFont fontWithName:@"PFBeauSansPro-BoldItalic" size:self.titleLabel.font.pointSize]];
	}
	if ([self.titleLabel.font.fontName isEqualToString:@"Helvetica-Oblique"]) {
		[self.titleLabel setFont:[UIFont fontWithName:@"PFBeauSansPro-Italic" size:self.titleLabel.font.pointSize]];
	}
	else {
		[self.titleLabel setFont:[UIFont fontWithName:[self.titleLabel.font.fontName stringByReplacingOccurrencesOfString:self.titleLabel.font.familyName withString:@"PFBeauSansPro"] size:self.titleLabel.font.pointSize]];
	}
    
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
	
	[super drawRect:rect];
	
}

- (void) setBold: (BOOL) bold {
	if (bold) {
		[self.titleLabel setFont:[UIFont fontWithName:@"PFBeauSansPro-Bold" size:self.titleLabel.font.pointSize]];
	}
	else {
		[self.titleLabel setFont:[UIFont fontWithName:@"PFBeauSansPro-Regular" size:self.titleLabel.font.pointSize]];
	}

}

-(void) handleControlEvent:(UIControlEvents)event withBlock:(ActionBlock) action{
	_actionBlock = Block_copy(action);
	[self addTarget:self action:@selector(callActionBlock:) forControlEvents:event];
}

-(void) callActionBlock:(id)sender {
	_actionBlock();
}

-(void) dealloc{
	Block_release(_actionBlock);
	[super dealloc];
}

@end
