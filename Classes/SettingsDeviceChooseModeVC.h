//
//  SettingsDeviceChooseModeVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSDevice;

@interface SettingsDeviceChooseModeVC : GAITrackedViewController {
	UIScrollView *scrollview;
	NSMutableArray *items;
    DSDevice *_currentDevice;
}

@property(nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;
@property (nonatomic, retain) IBOutlet UILabel *titelLabel;

- (id) initWithCurrentDevice: (DSDevice*)device;

- (void) buildScrollView;
- (IBAction) actionBack;

@end
