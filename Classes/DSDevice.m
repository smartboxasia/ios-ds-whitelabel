// 
//  DSDevice.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "DSDevice.h"

#import "DSRoom.h"

@implementation DSDevice 

@synthesize dirty;

@dynamic powerOn;
@dynamic dimmFactor;
@dynamic angleFactor;
@dynamic identifier;
@dynamic group;
@dynamic type;
@dynamic name;
@dynamic outputMode;
@dynamic room;
@dynamic consumption;
@dynamic metering;
@dynamic gtin;
@dynamic oemEanNumber;
@synthesize old_dsid;
@synthesize maxPower;
@synthesize dimmingCurveId;
@synthesize socket;
@synthesize infoUrl;
@synthesize serviceUrl;

- (BOOL) getPowerStatus {
	if ([self.powerOn intValue] == 1) {
		return YES;
	}
	return NO;
}


//new dSUID is 34 chars long, the old dSID is 24 chars long
- (BOOL) isUsingDsuid{
    if (self.identifier.length > 25) {
        return YES;
    }
    else
        return NO;
    
}


- (id) init {
	self = [super init];
	
	if (self) {
		allObserver = [NSMutableArray new];
        maxPower = nil;
        socket = nil;
	}
    return self;
}

- (void) addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context {
	if (!allObserver) {
		allObserver = [NSMutableArray new];
	}
	[super addObserver:observer forKeyPath:keyPath options:options context:context];
	
	NSMutableDictionary *d = [NSMutableDictionary new];
	[d setObject:observer forKey:keyPath];
	[allObserver addObject:d];
}

- (void) removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath {
	for (NSDictionary *dict in allObserver) {
		for (NSString *myKeypath in [dict allKeys]) {
			id theObserver = [dict objectForKey:myKeypath];
			
			if (observer == theObserver && [keyPath isEqualToString:myKeypath]) {
				[allObserver removeObject:dict];
                @try {
                    [super removeObserver:observer forKeyPath:keyPath];
                }
                @catch (NSException *exception) {
                    //just in case
                }
				return;
			}
		}
	}
	
    @try {
        [super removeObserver:observer forKeyPath:keyPath];
    }
    @catch (NSException *exception) {
        //just in case
    }
}

- (void)prepareForDeletion {
	while ([allObserver count] > 0) {
		NSDictionary *dict = [allObserver objectAtIndex:0];
		NSString *keypath = [[dict allKeys] objectAtIndex:0];
		id observer = [dict objectForKey:keypath];
		
		//if ([observer respondsToSelector:@selector(releaseObservation)]) {
		[self removeObserver:observer forKeyPath:keypath];
			//[observer releaseObservation];
		//}
	}
	
	[super prepareForDeletion];
}

@end
