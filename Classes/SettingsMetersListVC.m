//
//  SettingsMetersListVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsMetersListVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "SettingsMetersListItemVC.h"
#import "SettingsVC.h"
#import "CustomLabel.h"


@implementation SettingsMetersListVC

@synthesize scrollView;
@synthesize background;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    titleLabel.text = NSLocalizedString(@"key_meters_title", nil);
    
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_METERS_LIST];
	
	childControllers = [NSMutableArray new];
	
	[self buildScrollView];
    
    self.screenName = @"Settings Meters";
}

- (void) buildScrollView {
	
	for (UIView *v in [self.scrollView subviews]) {
		[v removeFromSuperview];
	}
	
//	for (UIViewController *c in childControllers) {
//		
//	}
	
	[childControllers removeAllObjects];
	
	int y=0;
	
	NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortedScenes = [[DataController instance].meters sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
    [sorter release];
	
	for (DSMeter* meter in sortedScenes) {
		GSLog(@"meter: %@", meter.name);
		
		SettingsMetersListItemVC *item = [[SettingsMetersListItemVC alloc] initWithMeter:meter withDarkBg:y%2==1 andIndex:y];
		item.view.frame = CGRectMake(0, y, item.view.frame.size.width, item.view.frame.size.height);
		[self.scrollView addSubview:item.view];
		y++;
		y += item.view.frame.size.height;
		[childControllers addObject:item];
		
	}
	
	self.scrollView.contentSize = CGSizeMake(275, y);
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
}





/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	for (UIViewController *child in childControllers) {
		[child.view removeFromSuperview];
		[child release];
	}
	[childControllers removeAllObjects];
	
    [titleLabel release];
    [super dealloc];
}


@end
