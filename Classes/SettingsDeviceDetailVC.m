//
//  SettingsDeviceDetailVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceDetailVC.h"
#import "SettingsDeviceChooseRoomVC.h"
#import "SettingsDeviceChooseModeVC.h"
#import "SettingsDeviceChooseSocketVC.h"
#import "DSDevice.h"
#import "DSDevice+Group1.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DSRoom.h"
#import "DSServer.h"
#import "DataController.h"
#import "SettingsDeviceWebviewVC.h"
#import "SettingsDeviceCheckIlluminantVC.h"
#import "GAIHelper.h"

@implementation SettingsDeviceDetailVC

@synthesize labelNameTitle;
@synthesize background;
@synthesize labelMaxPower, maxPowerField, labelSockettype, labelSockettypeContent;
@synthesize indicatorMaxPower, indicatorSockettype;
@synthesize buttonSockettype, buttonMaxPower;
@synthesize buttonDeviceInfo, buttonIlluminantCheck, buttonServiceInfo, buttonSubmitIlluminant, buttonDeviceInfo2, buttonServiceInfo2;
@synthesize imageBackgroundMaxPower, imageBackgroundSockettype;
@synthesize scrollView;
@synthesize viewFourButtonsBottom, viewTwoButtonsBottom;

- (id) initWithDevice: (DSDevice *) theDevice {
	if (self = [super init]) {
		device = theDevice;
	}
    
	return self;
}

- (BOOL)isRealDevice {
    //check for real device: if it's using new dsuid AND it doesn't have an "old dsid" value, it must be a virtual device
    BOOL isRealDevice = YES;
    if (device.isUsingDsuid) {
        if (!device.old_dsid) {
            isRealDevice = NO;
        }
    }
    return isRealDevice;
}

-(BOOL)isOutputModeAvailable {
    BOOL outputModeAvailable = [device.group isEqualToString:@"1"] && [self isRealDevice];
    
    NSArray* availableOutputModes = device.availableOutputModes;
    outputModeAvailable = outputModeAvailable && (availableOutputModes.count > 0);
    
    return outputModeAvailable;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void) viewDidLoad {
	[super viewDidLoad];

    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

	if (device) {
		nameField.text = device.name;
        labelNameTitle.text = device.name;
        
        NSString *meterName = @"N/A";
        for (DSMeter* meter in [[DataController instance] meters]) {
            if([meter.identifier isEqualToString:device.room.meterID])
                meterName = meter.name;
        }
        
		stromkreisID.text = meterName;
		labelRaumName.text = device.room.name;
        
        //localization
        modeTitleLabel.text = NSLocalizedString(@"key_outputmode", nil);
        self.labelMaxPower.text = NSLocalizedString(@"key_settings_device_max_power", nil);
        self.labelSockettype.text = NSLocalizedString(@"key_settings_device_lamp_holder", nil);
        label1.text = NSLocalizedString(@"key_settings_device_device_value", nil);
        label2.text = NSLocalizedString(@"key_settings_device_consumption", nil);
        label3.text = NSLocalizedString(@"key_settings_device_meter_reading", nil);
        label4.text = NSLocalizedString(@"key_name", nil);
        label5.text = NSLocalizedString(@"key_room", nil);
        label6.text = NSLocalizedString(@"key_meter", nil);
        
        [self.buttonDeviceInfo setTitle:NSLocalizedString(@"key_device_info",nil) forState:UIControlStateNormal];
        [self.buttonDeviceInfo2 setTitle:NSLocalizedString(@"key_device_info", nil) forState:UIControlStateNormal];
        [self.buttonServiceInfo setTitle:NSLocalizedString(@"key_service_info", nil) forState:UIControlStateNormal];
        [self.buttonServiceInfo2 setTitle:NSLocalizedString(@"key_service_info", nil) forState:UIControlStateNormal];
        
        NSMutableAttributedString *checkTitle = [[NSMutableAttributedString alloc] initWithAttributedString:[self.buttonIlluminantCheck attributedTitleForState:UIControlStateNormal]];
        [checkTitle.mutableString setString: NSLocalizedString(@"key_illuminant_check", nil)];
        [self.buttonIlluminantCheck setAttributedTitle:checkTitle forState:UIControlStateNormal];

        NSMutableAttributedString *submitTitle = [[NSMutableAttributedString alloc] initWithAttributedString:[self.buttonSubmitIlluminant attributedTitleForState:UIControlStateNormal]];
        [submitTitle.mutableString setString: NSLocalizedString(@"key_settings_device_scan_overlay_right_button_transfer_dimming_curve", nil)];
        [self.buttonSubmitIlluminant setAttributedTitle:submitTitle forState:UIControlStateNormal];
        
        if([self isOutputModeAvailable]){
            modeNameLabel.text = device.outputModeName;
			
			self.scrollView.contentSize = CGSizeMake(275, 8*45);
            
            //if we're not connected to a cloud account, disable the maxpower and socket fields
            if(![[[DataController instance] getCurrentDSS].connectionType isEqual:DSS_CONNECTION_TYPE_CLOUD]){
                self.labelMaxPower.enabled = NO;
                self.maxPowerField.enabled = NO;
                self.buttonMaxPower.enabled = NO;
                self.labelSockettype.enabled = NO;
                self.labelSockettypeContent.enabled = NO;
                self.buttonSockettype.enabled = NO;
                self.buttonMaxPower.enabled = NO;
                [self.indicatorMaxPower stopAnimating];
                [self.indicatorSockettype stopAnimating];
                
            }
        }
        //hide the outputmode option and move the other views up
        else{
			
			//	hide the maxPower and socket fields
			
			self.labelMaxPower.hidden = YES;
			self.maxPowerField.hidden = YES;
            self.buttonMaxPower.hidden = YES;
			self.labelSockettype.hidden = YES;
			self.labelSockettypeContent.hidden = YES;
			self.buttonSockettype.hidden = YES;
            self.buttonMaxPower.hidden = YES;
			self.imageBackgroundMaxPower.hidden = YES;
			self.imageBackgroundSockettype.hidden = YES;
			[self.indicatorMaxPower stopAnimating];
			[self.indicatorSockettype stopAnimating];
			
			//	adjust content size for scrollview
			self.scrollView.contentSize = CGSizeMake(275, 5*45);
			
            modeButton.hidden = YES;
            modeNameLabel.hidden = YES;
            modeTitleLabel.hidden = YES;
            modeImage.hidden = YES;
            self.imageBackgroundMode.hidden = YES;
            
            [label1 setFrame:CGRectMake(label1.frame.origin.x, label1.frame.origin.y - 45, label1.frame.size.width, label1.frame.size.height)];
            [label2 setFrame:CGRectMake(label2.frame.origin.x, label2.frame.origin.y - 45, label2.frame.size.width, label2.frame.size.height)];
            [label3 setFrame:CGRectMake(label3.frame.origin.x, label3.frame.origin.y - 45, label3.frame.size.width, label3.frame.size.height)];
            
            [dimmerValue setFrame: CGRectMake(dimmerValue.frame.origin.x, dimmerValue.frame.origin.y - 45, dimmerValue.frame.size.width, dimmerValue.frame.size.height)];
            [consumptionValue setFrame: CGRectMake(consumptionValue.frame.origin.x, consumptionValue.frame.origin.y - 45, consumptionValue.frame.size.width, consumptionValue.frame.size.height)];
            [meteringValue setFrame: CGRectMake(meteringValue.frame.origin.x, meteringValue.frame.origin.y - 45, meteringValue.frame.size.width, meteringValue.frame.size.height)];
            
            [loadingWheelValue setFrame: CGRectMake(loadingWheelValue.frame.origin.x, loadingWheelValue.frame.origin.y - 45, loadingWheelValue.frame.size.width, loadingWheelValue.frame.size.height)];
            [loadingWheelConsumption setFrame: CGRectMake(loadingWheelConsumption.frame.origin.x, loadingWheelConsumption.frame.origin.y - 45, loadingWheelConsumption.frame.size.width, loadingWheelConsumption.frame.size.height)];
            [loadingWheelMetering setFrame: CGRectMake(loadingWheelMetering.frame.origin.x, loadingWheelMetering.frame.origin.y - 45, loadingWheelMetering.frame.size.width, loadingWheelMetering.frame.size.height)];
        }
        
        self.maxPowerField.hidden = YES;
        self.buttonMaxPower.hidden = YES;
		self.labelSockettypeContent.hidden = YES;
        self.buttonSockettype.hidden = YES;
		
        
        
        NSString* identifier = device.identifier;
        if(device.isUsingDsuid && device.old_dsid) {
            identifier = device.old_dsid;
        }
		internID.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"key_ID", @""), [identifier substringWithRange:NSMakeRange([identifier length]-8, 8)]];
    }
    
    if(device){
		/*
		 *	configure url buttons
		 */
		if (device.infoUrl) {
			self.buttonDeviceInfo.enabled = YES;
            self.buttonDeviceInfo2.enabled = YES;
		}
		else {
			self.buttonDeviceInfo.enabled = NO;
            self.buttonDeviceInfo2.enabled = NO;
		}
		
		if (device.serviceUrl) {
			self.buttonServiceInfo.enabled = YES;
            self.buttonServiceInfo2.enabled = YES;
		}
		else {
			self.buttonServiceInfo.enabled = NO;
            self.buttonServiceInfo2.enabled = NO;
		}
		
        [self updateIlluminatButtons];
		
        [device addObserver:self forKeyPath:@"dimmFactor" options:0 context:nil];
        [device addObserver:self forKeyPath:@"consumption" options:0 context:nil];
        [device addObserver:self forKeyPath:@"metering" options:0 context:nil];
        [device addObserver:self forKeyPath:@"room" options:0 context:nil];
        [device addObserver:self forKeyPath:@"outputMode" options:0 context:nil];
		
		[device addObserver:self forKeyPath:@"socket" options:0 context:nil];
		[device addObserver:self forKeyPath:@"maxPower" options:0 context:nil];
		[device addObserver:self forKeyPath:@"infoUrl" options:0 context:nil];
		[device addObserver:self forKeyPath:@"serviceUrl" options:0 context:nil];
    }
    
    if([self isOutputModeAvailable] && device.isOutputModeReadonly)  {
        modeButton.hidden = YES;
        //Use the whole screen to show the name because the detail indicators is hidden
        CGFloat width = self.view.frame.size.width - modeNameLabel.frame.origin.x - 60;
        modeNameLabel.frame = CGRectMake(modeNameLabel.frame.origin.x, modeNameLabel.frame.origin.y, width, modeNameLabel.frame.size.height);
    }
    
    self.screenName = @"Settings Devices Details";
}


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
	[self.scrollView scrollRectToVisible:CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height) animated:NO];
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    
	if(![device.outputMode isEqualToNumber:[NSNumber numberWithInt:0]])
        [self updateDeviceValues];
    else{
        loadingWheelValue.hidden = YES;
        loadingWheelConsumption.hidden = YES;
        loadingWheelMetering.hidden = YES;
        refreshButton.hidden = YES;
        
        [[DataController instance] cloudServiceGetArticleDataForDevice:device];
    }
}

- (void)updateIlluminatButtons {
    /*
     *	configure illuminant buttons
     */
    if ([device.group isEqualToString:@"1"] && [self isRealDevice]) {
        //if outputmode is
        if (![device.outputMode isEqualToNumber:[NSNumber numberWithInteger:kDeviceOutputModeDisabled]]) {
          //for switched output mode (16) we disable transfer
          if([device.outputMode intValue]==kDeviceOutputModeSwitched){
              self.buttonSubmitIlluminant.enabled = NO;
          }
          else if([device.outputMode intValue]==kDeviceOutputModeDimmed ||
                  [device.outputMode intValue]==kDeviceOutputModeDimmed_0_10V ||
                  [device.outputMode intValue]==kDeviceOutputModeDimmed_1_10V){
              self.buttonSubmitIlluminant.enabled = YES;
          }
          //any !0 output mode is allowed to check illuminants
          self.buttonIlluminantCheck.enabled = YES;
        }
        else{
          self.buttonIlluminantCheck.enabled = NO;
          self.buttonSubmitIlluminant.enabled = NO;
          
        }
      
        if([self isOutputModeAvailable]) {
            self.viewFourButtonsBottom.hidden = NO;
            self.viewTwoButtonsBottom.hidden = YES;
        }
        else {
            self.viewFourButtonsBottom.hidden = YES;
            self.viewTwoButtonsBottom.hidden = NO;
        }
    }
    else {
      
      self.viewFourButtonsBottom.hidden = YES;
      self.viewTwoButtonsBottom.hidden = NO;
      
      self.buttonIlluminantCheck.enabled = NO;
      self.buttonSubmitIlluminant.enabled = NO;
    }
    
    // Only allow to run illuminant check if max power and socket type are set.
    BOOL valuesNotAvailable = [device.maxPower integerValue] == -1 || [device.socket isEqualToString:@"-"];
    
    
    // Only allow to run illuminant check if is remote connection (cloud)
    DSServer *dsServer = [[DataController instance] getCurrentDSS];
    BOOL isLocalConnection = dsServer && ![dsServer isRemoteDss];

    if (valuesNotAvailable || isLocalConnection)
    {
        self.buttonIlluminantCheck.enabled = NO;
        self.buttonSubmitIlluminant.enabled = NO;
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"dimmFactor"]) {
        loadingWheelValue.hidden = YES;
        [loadingWheelValue stopAnimating];
        dimmerValue.hidden = NO;
        dimmerValue.text = [NSString stringWithFormat:@"%.0f%%",[device.dimmFactor floatValue]*100];
	}
    else if ([keyPath isEqualToString:@"consumption"]) {
        loadingWheelConsumption.hidden = YES;
        [loadingWheelConsumption stopAnimating];
        consumptionValue.hidden = NO;
        if([device.consumption isEqualToNumber:[NSNumber numberWithInt:-1]])
            consumptionValue.text = @"- W";
        else
            consumptionValue.text = [NSString stringWithFormat:@"%@ W", device.consumption];
	}
    else if ([keyPath isEqualToString:@"metering"]) {
        loadingWheelMetering.hidden = YES;
        [loadingWheelMetering stopAnimating];
        meteringValue.hidden = NO;
        if([device.metering isEqualToNumber:[NSNumber numberWithInt:-1]])
            meteringValue.text = @"- kWh";
        else
            meteringValue.text = [NSString stringWithFormat:@"%.2f kWh",[device.metering floatValue]/100];
	}
    else if ([keyPath isEqualToString:@"room"]) {
        labelRaumName.text = device.room.name;
	}
    else if ([keyPath isEqualToString:@"outputMode"]) {
        modeNameLabel.text = device.outputModeName;
        
        [self updateIlluminatButtons];
	}
	else if ([keyPath isEqualToString:@"maxPower"]) {
        if([self isOutputModeAvailable]) {
            if([device.group isEqualToString:@"1"] && ![device.outputMode isEqualToNumber:[NSNumber numberWithInteger:0]]){
                
                //if device has an oemEanNumber (defaults to "0"), don't enable the edit button
                if([device.oemEanNumber length]<2)
                    self.buttonMaxPower.hidden = NO;

                [self.indicatorMaxPower stopAnimating];
                NSString *meterValue = nil;
                
                if([device.maxPower isEqualToNumber:[NSNumber numberWithInt:-1]]) {
                    meterValue = @"- W";
                }
                else {
                    meterValue = [NSString stringWithFormat:@"%li W", (long)[device.maxPower integerValue]];
                }
                
                self.maxPowerField.hidden = NO;
                self.maxPowerField.text = meterValue;
            }
        }
	}
	else if ([keyPath isEqualToString:@"socket"]) {
        if([self isOutputModeAvailable]) {
            if([device.group isEqualToString:@"1"] && ![device.outputMode isEqualToNumber:[NSNumber numberWithInteger:0]]){
                //if device has an oemEanNumber (defaults to "0"), don't enable the edit button
                if([device.oemEanNumber length]<2)
                    self.buttonSockettype.hidden = NO;
                
                [self.indicatorSockettype stopAnimating];
                NSString *newValue = device.socket;
                
                if (!newValue || newValue == (id)[NSNull null]) {
                    device.socket = NSLocalizedString(@"key_settings_not_available", @"n.a.");
                    newValue = device.socket;
                }
                
                self.labelSockettypeContent.hidden = NO;
                self.labelSockettypeContent.text = newValue;
            }
        }
	}
	else if ([keyPath isEqualToString:@"infoUrl"]) {
		if (device.infoUrl && device.infoUrl != (id)[NSNull null]) {
			self.buttonDeviceInfo.enabled = YES;
            self.buttonDeviceInfo2.enabled = YES;
		}
		else {
			self.buttonDeviceInfo.enabled = NO;
            self.buttonDeviceInfo2.enabled = NO;
		}
		
	}
	else if ([keyPath isEqualToString:@"serviceUrl"]) {
		if (device.serviceUrl && device.serviceUrl != (id)[NSNull null]) {
			self.buttonServiceInfo.enabled = YES;
            self.buttonServiceInfo2.enabled = YES;
		}
		else {
			self.buttonServiceInfo.enabled = NO;
            self.buttonServiceInfo2.enabled = NO;
		}
	}
    
    [self updateIlluminatButtons];
}

- (void) updateDeviceValues{
    loadingWheelValue.hidden = NO;
    loadingWheelConsumption.hidden = NO;
    loadingWheelMetering.hidden = NO;
    
    [loadingWheelValue startAnimating];
    [loadingWheelConsumption startAnimating];
    [loadingWheelMetering startAnimating];
    
    dimmerValue.hidden = YES;
    consumptionValue.hidden = YES;
    meteringValue.hidden = YES;
	
	if([device.group isEqualToString:@"1"] && [[[DataController instance] getCurrentDSS].connectionType isEqual:DSS_CONNECTION_TYPE_CLOUD]) {

        self.labelSockettypeContent.hidden = YES;
        self.maxPowerField.hidden = YES;
        self.buttonMaxPower.hidden = YES;
        self.buttonSockettype.hidden = YES;
        if([self isOutputModeAvailable]) {
            self.indicatorMaxPower.hidden = NO;
            self.indicatorSockettype.hidden = NO;
            [self.indicatorSockettype startAnimating];
            [self.indicatorMaxPower startAnimating];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [[DataController instance]cloudServiceGetAllTags:device];
        });
	}
    else {
        [[DataController instance] cloudServiceGetArticleDataForDevice:device];
    }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[DataController instance]updateValueOfDevice:device];
        [[DataController instance]updateConsumptionValuesOfDevice:device];
    });
    
}
#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC *)[[ModelLocator instance] valueForKey: k_VIEW_SETTINGS];
	[settings goBack];
}

- (IBAction) actionDeviceInfo:(id)sender {
    [GAIHelper sendUIActionWithCategory:@"tagging_button_clicked" andLabel:@"device_info"];
	[self showWebViewWithTitle:[sender titleForState:UIControlStateNormal] andTargetUrl:device.infoUrl];
}

- (IBAction) actionServiceInfo:(id)sender {
    [GAIHelper sendUIActionWithCategory:@"tagging_button_clicked" andLabel:@"service_info"];
	[self showWebViewWithTitle:[sender titleForState:UIControlStateNormal] andTargetUrl:device.serviceUrl];
}

- (void) showWebViewWithTitle: (NSString*) title andTargetUrl: (NSString*) targetUrl {
	SettingsDeviceWebviewVC *webviewVc = [[SettingsDeviceWebviewVC alloc] initWithTitle:title andTargetUrl: targetUrl];
	
	UIViewController *mainVc = [[ModelLocator instance] valueForKey:k_VIEW_MAIN];
	
	[UIView transitionFromView:mainVc.view
                        toView:webviewVc.view
                      duration:0.6
                       options: UIViewAnimationOptionTransitionFlipFromLeft
                    completion:^(BOOL finished) {
						if (finished) {
							
						}
					}
	 ];
}

- (IBAction) actionIlluminantCheck:(id)sender {
    [GAIHelper sendUIActionWithCategory:@"tagging_button_clicked" andLabel:@"illuminant_check"];
    
	SettingsDeviceCheckIlluminantVC *camVc = [[SettingsDeviceCheckIlluminantVC alloc] initWithActionType:kActionTypeIlluminantCheck andDevice:device];
	
	UIViewController *mainVc = [[ModelLocator instance] valueForKey:k_VIEW_MAIN];
	
	[UIView transitionFromView:mainVc.view
                        toView:camVc.view
                      duration:0.6
                       options: UIViewAnimationOptionTransitionFlipFromLeft
                    completion:^(BOOL finished) {
						if (finished) {
							
						}
					}
	 ];
}

- (IBAction) actionSubmitIlluminant:(id)sender {
    [GAIHelper sendUIActionWithCategory:@"tagging_button_clicked" andLabel:@"transfer_dimming_curve"];
    
	SettingsDeviceCheckIlluminantVC *camVc = [[SettingsDeviceCheckIlluminantVC alloc] initWithActionType:kActionTypeIlluminantTransfer andDevice:device];
	
	UIViewController *mainVc = [[ModelLocator instance] valueForKey:k_VIEW_MAIN];
	
	[UIView transitionFromView:mainVc.view
                        toView:camVc.view
                      duration:0.6
                       options: UIViewAnimationOptionTransitionFlipFromLeft
                    completion:^(BOOL finished) {
						if (finished) {
							
						}
					}
	 ];
}

-(IBAction) refreshDevice:(id)sender{
    [self updateDeviceValues];
}

-(IBAction) nameEdit:(id)sender{
	//nameField.keyboardType = UIKeyboardTypeDefault;
	if (nameField.enabled == NO){
		nameField.enabled = YES;
		[nameField becomeFirstResponder];
	} else {
		nameField.enabled = NO;
		[nameField resignFirstResponder];
	}
}

-(IBAction) maxPowerEdit:(id)sender{
    
    if (maxPowerField.enabled == NO){
        self.maxPowerField.text = @"";
        
        maxPowerField.enabled = YES;
        [maxPowerField becomeFirstResponder];
        
        //scroll to show field
        CGSize tempSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height+100);
        [scrollView setContentSize:tempSize];
        CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
        [scrollView setContentOffset:bottomOffset animated:YES];
        
    } else {
        
        NSNumber *newPower = [NSNumber numberWithInteger:[maxPowerField.text integerValue]];
        
        if((![newPower integerValue]) > 0)
        {
            device.maxPower = [NSNumber numberWithInt:-1];
            maxPowerField.text = @"- W";
        }
        else{
            maxPowerField.text = [NSString stringWithFormat:@"%lu W",(long)[newPower integerValue]];
        }
        
        if (device)
        {
            //if it's not a new value, we just skip
            if([device.maxPower integerValue] != [newPower integerValue] && [newPower integerValue] > 0)
            {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    [[DataController instance] cloudServiceSetMaxPower:newPower forDevice:device];
                });
            }else if([device.maxPower integerValue] != [newPower integerValue] && [newPower integerValue] == 0)
            {
                [[DataController instance] cloudServiceDeleteMaxPowerForDevice:device];
            }
        }
        
		[maxPowerField resignFirstResponder];
        maxPowerField.enabled = NO;
        //scroll back up
        CGSize tempSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height-100);
        [scrollView setContentSize:tempSize];
        CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }
    
}

- (IBAction) changeRoom:(id)sender{
    SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[[SettingsDeviceChooseRoomVC alloc] initWithCurrentRoom:device.room andCurrentDevice:device]];
}

- (IBAction) changeMode:(id)sender{
    if(!device.isOutputModeReadonly) {
        SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
        [settings showSubGroup:[[SettingsDeviceChooseModeVC alloc] initWithCurrentDevice:device]];
    }
}

- (IBAction) changeSocket:(id)sender{
    SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[[SettingsDeviceChooseSocketVC alloc] initWithCurrentDevice:device]];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == nameField) {
		
		NSString *newName = textField.text;
		
		if (device) {
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
				[[DataController instance] setDeviceName:device withString:newName];
			});
		}
        nameField.enabled = NO;
		[textField resignFirstResponder];
	}
    else if (textField == maxPowerField) {
        [self maxPowerEdit:maxPowerField];
	}
    
	return YES;
}

- (void) didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
    
	// Release any cached data, images, etc. that aren't in use.
}

- (void) viewDidUnload {
	[super viewDidUnload];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void) dealloc {
	if(device){
		[device removeObserver:self forKeyPath:@"dimmFactor"];
        [device removeObserver:self forKeyPath:@"consumption"];
        [device removeObserver:self forKeyPath:@"metering"];
        [device removeObserver:self forKeyPath:@"room"];
        [device removeObserver:self forKeyPath:@"outputMode"];
		[device removeObserver:self forKeyPath:@"socket"];
		[device removeObserver:self forKeyPath:@"maxPower"];
		[device removeObserver:self forKeyPath:@"serviceUrl"];
		[device removeObserver:self forKeyPath:@"infoUrl"];
    }
	
    [label4 release];
    [label5 release];
    [label6 release];
	[super dealloc];
}

@end