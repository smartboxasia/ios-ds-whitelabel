//
//  SettingsVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingsOverviewVC.h"
#import "ModelLocator.h"
#import "MainVC.h"
#import "StartupServerSelectionVC.h"
#import "UIBlockButton.h"
#import "CustomLabel.h"
#import "SettingsReloadScenesVC.h"
#import "RoomSceneListVC.h"
#import "RoomDeviceListVC.h"
#import "SettingsScenesListVC.h"
#import "SettingsDevicesListVC.h"

@implementation SettingsVC

@synthesize labelHeader, buttonHeaderLogo, buttonBearbeiten, buttonReload;


- (void)viewDidLoad {
    [super viewDidLoad];

	viewStack = [NSMutableArray new];

	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS];

    labelHeader.text = NSLocalizedString(@"key_settings_title", nil);

    sHeight = [UIScreen mainScreen].bounds.size.height;
    sWidth  = [UIScreen mainScreen].bounds.size.width;

//    //iphone 5 support.
//    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        [self.view setFrame:CGRectMake(0, 0, 320, 575)];
//    }
    [self.view setFrame:CGRectMake(0, 0, sWidth, sHeight)];

	SettingsOverviewVC *vc = [SettingsOverviewVC new];
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        vc.view.frame = CGRectMake(0, 50, 320, 513);
//    }
//    else{
//        vc.view.frame = CGRectMake(0, 50, 320, 425);
//    }
    vc.view.frame = CGRectMake(0, 50, sWidth, sHeight-50);

	[self.view addSubview:vc.view];
	currentSubController = vc;
    //[viewStack addObject:vc];


	self.buttonHeaderLogo.alpha = 1;
	self.labelHeader.alpha = 0;

    self.screenName = @"Settings";
}


- (IBAction) actionToggleSettings: (id) sender {
	if(self.buttonHeaderLogo.alpha == 1) {
		// Settings sind aus
		[UIView animateWithDuration:0.4 animations:^{
			self.buttonHeaderLogo.alpha = 0;
			self.labelHeader.alpha = 1;

			if (!self.buttonBearbeiten.hidden) {
				self.buttonBearbeiten.alpha = 1;
			}

			if (!self.buttonReload.hidden) {
				self.buttonReload.alpha = 1;
			}
		}];

        //if we're in a room, show the activity settings for that room
        RoomSceneListVC *sceneList = [[ModelLocator instance] valueForKey:k_VIEW_ROOM_SCENELIST];
        RoomDeviceListVC *deviceList = [[ModelLocator instance] valueForKey:k_VIEW_ROOM_DEVICELIST];

        if (sceneList && !deviceList) {
            DSRoom *currentRoom = [sceneList currentRoomShowing];
            int currentIndexShowing = [sceneList currentIndex];
            [(SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS] showController:[[SettingsScenesListVC alloc] initWithRoom:currentRoom isLookingForFavorite:NO withIndex:currentIndexShowing]];
        }
        else if(deviceList){
			[(SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS] showController:[[SettingsDevicesListVC alloc] initWithRoom:[deviceList currentRoomShowing]]];
        }

		[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] showSettings];
	}
	else {
		// settings sind an
		[UIView animateWithDuration:0.4 animations:^{
			self.buttonHeaderLogo.alpha = 1;
			self.labelHeader.alpha = 0;

			if (!self.buttonBearbeiten.hidden) {
				self.buttonBearbeiten.alpha = 0;
			}

			if (!self.buttonReload.hidden) {
				self.buttonReload.alpha = 0;
			}
		}];

        RoomSceneListVC *sceneList = [[ModelLocator instance] valueForKey:k_VIEW_ROOM_SCENELIST];
        DSRoom *currentRoomInOverview = [sceneList currentRoomShowing];
        SettingsScenesListVC *settingsSceneList = [[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_SCENES_LIST];
        DSRoom *currentRoomInSettings = [settingsSceneList currentRoomshowing];

		[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] hideSettings];

        if ([currentRoomInOverview.identifier isEqualToString:currentRoomInSettings.identifier]) {
            sceneList.currentIndex = settingsSceneList.currentIndex;

            [sceneList buildScrollview];
        }
	}
}

- (IBAction) actionLogo {
	return;
	[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] showServerList];
}

- (void) showWithoutIntelligence {
    if(self.buttonHeaderLogo.alpha == 1) {
		// Settings sind aus
		[UIView animateWithDuration:0.4 animations:^{
			self.buttonHeaderLogo.alpha = 0;
			self.labelHeader.alpha = 1;

			if (!self.buttonBearbeiten.hidden) {
				self.buttonBearbeiten.alpha = 1;
			}

			if (!self.buttonReload.hidden) {
				self.buttonReload.alpha = 1;
			}
		}];

		[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] showSettings];
	}
	else {
		// settings sind an
		[UIView animateWithDuration:0.4 animations:^{
			self.buttonHeaderLogo.alpha = 1;
			self.labelHeader.alpha = 0;

			if (!self.buttonBearbeiten.hidden) {
				self.buttonBearbeiten.alpha = 0;
			}

			if (!self.buttonReload.hidden) {
				self.buttonReload.alpha = 0;
			}
		}];

		[(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] hideSettings];
	}

}


- (void) showSubGroup: (UIViewController*) groupComponent {
    GSLog("log");

    //iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        groupComponent.view.frame = CGRectMake(0, 50, 320, 513);
    }
    else{
        groupComponent.view.frame = CGRectMake(0, 50, 320, 425);
    }

    // groupComponent.view.frame = CGRectMake(320, currentSubController.view.frame.origin.y, groupComponent.view.frame.size.width, groupComponent.view.frame.size.height);
    groupComponent.view.frame = CGRectMake(sWidth, currentSubController.view.frame.origin.y, groupComponent.view.frame.size.width, groupComponent.view.frame.size.height);
	[self.view addSubview:groupComponent.view];

	[UIView animateWithDuration:0.4 animations:^{
		groupComponent.view.frame = CGRectMake(0, currentSubController.view.frame.origin.y, groupComponent.view.frame.size.width, groupComponent.view.frame.size.height);
        // currentSubController.view.frame = CGRectMake(-320, currentSubController.view.frame.origin.y, currentSubController.view.frame.size.width, currentSubController.view.frame.size.height);
        currentSubController.view.frame = CGRectMake(-sWidth, currentSubController.view.frame.origin.y, currentSubController.view.frame.size.width, currentSubController.view.frame.size.height);
	} completion:^(BOOL finished){
		[viewStack addObject:currentSubController];
		[currentSubController.view removeFromSuperview];

		currentSubController = groupComponent;
	}];
}

- (void) goBack {
    GSLog("log");

	UIViewController *groupComponent = [viewStack lastObject];
    // groupComponent.view.frame = CGRectMake(-320, currentSubController.view.frame.origin.y, groupComponent.view.frame.size.width, groupComponent.view.frame.size.height);
    groupComponent.view.frame = CGRectMake(-sWidth, currentSubController.view.frame.origin.y, groupComponent.view.frame.size.width, groupComponent.view.frame.size.height);
	[self.view addSubview:groupComponent.view];

	[UIView animateWithDuration:0.4 animations:^{
		groupComponent.view.frame = CGRectMake(0, currentSubController.view.frame.origin.y, groupComponent.view.frame.size.width, groupComponent.view.frame.size.height);
        // currentSubController.view.frame = CGRectMake(320, currentSubController.view.frame.origin.y, currentSubController.view.frame.size.width, currentSubController.view.frame.size.height);
        currentSubController.view.frame = CGRectMake(sWidth, currentSubController.view.frame.origin.y, currentSubController.view.frame.size.width, currentSubController.view.frame.size.height);
	} completion:^(BOOL finished){
		[currentSubController.view removeFromSuperview];
		[currentSubController release];
		currentSubController = groupComponent;
		[viewStack removeLastObject];
	}];
}

- (void) goBackTwice {
    GSLog("log");

	UIViewController *groupComponent = [viewStack lastObject];
    // groupComponent.view.frame = CGRectMake(-320, currentSubController.view.frame.origin.y, groupComponent.view.frame.size.width, groupComponent.view.frame.size.height);
    groupComponent.view.frame = CGRectMake(-sWidth, currentSubController.view.frame.origin.y, groupComponent.view.frame.size.width, groupComponent.view.frame.size.height);
	[self.view addSubview:groupComponent.view];

	[UIView animateWithDuration:0.2 animations:^{
		groupComponent.view.frame = CGRectMake(0, currentSubController.view.frame.origin.y, groupComponent.view.frame.size.width, groupComponent.view.frame.size.height);
        // currentSubController.view.frame = CGRectMake(320, currentSubController.view.frame.origin.y, currentSubController.view.frame.size.width, currentSubController.view.frame.size.height);
        currentSubController.view.frame = CGRectMake(sWidth, currentSubController.view.frame.origin.y, currentSubController.view.frame.size.width, currentSubController.view.frame.size.height);
	} completion:^(BOOL finished){
		[currentSubController.view removeFromSuperview];
		[currentSubController release];
		currentSubController = groupComponent;
		[viewStack removeLastObject];

        UIViewController *groupComponent2 = [viewStack lastObject];
        // groupComponent2.view.frame = CGRectMake(-320, currentSubController.view.frame.origin.y, groupComponent2.view.frame.size.width, groupComponent2.view.frame.size.height);
        groupComponent2.view.frame = CGRectMake(-sWidth, currentSubController.view.frame.origin.y, groupComponent2.view.frame.size.width, groupComponent2.view.frame.size.height);
        [self.view addSubview:groupComponent2.view];

        [UIView animateWithDuration:0.2 animations:^{
            groupComponent2.view.frame = CGRectMake(0, currentSubController.view.frame.origin.y, groupComponent2.view.frame.size.width, groupComponent2.view.frame.size.height);
            // currentSubController.view.frame = CGRectMake(320, currentSubController.view.frame.origin.y, currentSubController.view.frame.size.width, currentSubController.view.frame.size.height);
            currentSubController.view.frame = CGRectMake(sWidth, currentSubController.view.frame.origin.y, currentSubController.view.frame.size.width, currentSubController.view.frame.size.height);
        } completion:^(BOOL finished){
            [currentSubController.view removeFromSuperview];
            [currentSubController release];
            currentSubController = groupComponent2;
            [viewStack removeLastObject];
        }];

	}];
}

- (void) showController: (UIViewController*) controller {
	GSLog(@"current controller: %@", currentSubController);
	GSLog(@"viewstack: %@", viewStack);

	controller.view.frame = CGRectMake(0, currentSubController.view.frame.origin.y, controller.view.frame.size.width, controller.view.frame.size.height);

	if ([currentSubController isKindOfClass:[SettingsOverviewVC class]]) {
		//[self showSubGroup:controller];
		[viewStack addObject:currentSubController];
		[currentSubController.view removeFromSuperview];
	}
	else {
		while ([viewStack count] > 1) {
			UIViewController *item = [viewStack lastObject];
			[viewStack removeLastObject];
			[item release];
		}

		[currentSubController.view removeFromSuperview];
		[currentSubController release];
	}

	currentSubController = controller;
	[self.view addSubview:currentSubController.view];
}

- (void) displaySceneReload {
	viewControllerSceneReload = [SettingsReloadScenesVC new];
	viewControllerSceneReload.view.alpha = 0;
	[self.view addSubview:viewControllerSceneReload.view];

	[UIView animateWithDuration:0.4 animations:^{
		viewControllerSceneReload.view.alpha = 1;
	} completion:^(BOOL finished) {}];

	return;

    GSLog("NEVER COMES HERE???#########");

	//	Geräte zu den Aktivitäten laden...
    // iphone 5 support
    CGRect fullscreenRect;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        fullscreenRect = CGRectMake(0, 0, 320, 568);
    }
    else{
        fullscreenRect = CGRectMake(0, 0, 320, 480);
    }


	UIView *myView = [[UIView alloc] initWithFrame:fullscreenRect];
	[myView setBackgroundColor:[UIColor blackColor]];
	myView.alpha = 0.6;

	CustomLabel *label = [[CustomLabel alloc] initWithFrame:CGRectMake(0, 250, 320, 60)];
	label.numberOfLines = 3;
	label.text = @"Gerätestatus wird geladen.\nDies kann einige Minuten in Anspruch nehmen.";
	label.textAlignment = NSTextAlignmentCenter;
	label.textColor = [UIColor whiteColor];
	label.backgroundColor = [UIColor clearColor];

	[myView addSubview:label];

	UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	indicator.frame = CGRectMake(135, 215, 50, 50);
	[myView addSubview:indicator];

	[self.view addSubview:myView];

	//viewSceneReload = myView;
}

- (void) hideSceneReload {
	[UIView animateWithDuration:0.4 animations:^{
		viewControllerSceneReload.view.alpha = 0;
	} completion:^(BOOL finished) {
		[viewControllerSceneReload.view removeFromSuperview];
		[viewControllerSceneReload release];
	}];


}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
