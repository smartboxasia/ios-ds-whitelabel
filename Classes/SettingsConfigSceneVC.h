//
//  SettingsScenesListByRoomVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSScene;


@interface SettingsConfigSceneVC : GAITrackedViewController {
    DSScene *scene;
	UIScrollView *scrollview;
	NSMutableArray *items;
    BOOL isRunningDeviceValueUpdate;
    BOOL cancelUpdating;
    
    IBOutlet UILabel *titleLabel;
    IBOutlet UIButton *callButton;
    IBOutlet UIButton *saveButton;
}

@property(nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (id) initWithScene: (DSScene*) theScene;
- (void) buildScrollView;
- (void) updateDeviceValues;
- (IBAction) actionBack;
- (IBAction) actionCallScene;
- (IBAction) actionSaveScene;

@end
