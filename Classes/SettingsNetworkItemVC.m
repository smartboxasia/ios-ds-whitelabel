//
//  SettingsNetworkItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsNetworkItemVC.h"
#import "ModelLocator.h"
#import "FacilityController.h"
#import "DataController.h"
#import "StartupController.h"
#import "DigitalstromJSONService.h"
#import "DSServer.h"
#import "SettingsVC.h"
#import "SettingsNetworkConnectionDetailVC.h"

@interface SettingsNetworkItemVC()
@property(nonatomic, strong) DSServer *backupServer;
@end

@implementation SettingsNetworkItemVC

@synthesize buttonLabel, imageSelectionState, imageBackground;
@synthesize buttonArrow;

- (id)initWithServer: (DSServer*) server andDarkBgFlag: (BOOL) darkBg {
	self = [super init];
    if (self) {
		dsServer = server;
		hasDarkBackground = darkBg;
		servername = server.name;
		
		[dsServer retain];
    }
    return self;
}

- (id)initWithServerName: (NSString *) name andDarkBgFlag: (BOOL) darkBg {
    self = [super init];
    if (self) {
		dsServer = nil;
		servername = name;
		hasDarkBackground = darkBg;
    }
    return self;
}



- (void) viewDidLoad {
	[super viewDidLoad];
	
	if (dsServer) {
		if (dsServer.connectionName) {
			[self.buttonLabel setTitle:dsServer.connectionName forState:UIControlStateNormal];
		}
		else {
            if(dsServer.cloudEmail)
                [self.buttonLabel setTitle:dsServer.cloudEmail forState:UIControlStateNormal];
            else
                [self.buttonLabel setTitle:dsServer.name forState:UIControlStateNormal];
		}
	}
	else {
		[self.buttonLabel setTitle:servername forState:UIControlStateNormal];
		self.buttonArrow.hidden = YES;
		self.imageSelectionState.hidden = YES;
		self.buttonLabel.enabled = NO;
	}
	
	
	if(hasDarkBackground) {
		self.imageBackground.image = [UIImage imageNamed:@"Einst_Gerate_Liste_dunkel.png"];		
	}
	else {
		self.imageBackground.image = [UIImage imageNamed:@"Einst_Gerate_Liste_hell.png"];
	}
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"connectionName"]) {
		[self.buttonLabel setTitle:dsServer.connectionName forState:UIControlStateNormal];
	}
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
	[self updateConnectionState];
	
	if (dsServer) {
		[dsServer addObserver:self forKeyPath:@"connectionName" options:0 context:nil];
	}
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateConnectionState) name:kNotificationConnectedToDss object:nil];
}

- (void) updateConnectionState {
	if (dsServer.connected) {
		[self setActive];
	}
	else {
		[self setInactive];
	}
}

- (void) viewWillDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
    
	if (dsServer)
    {
		[dsServer removeObserver:self forKeyPath:@"connectionName"];
		[dsServer release];
		
		dsServer = nil;
	}
    
    @try
    {
        [[NSNotificationCenter defaultCenter] removeObserver: self];
    }
    @catch (NSException *exception)
    {
        //just in case
    }
	
}

- (IBAction) actionConnect
{
    if ([[DataController instance] getCurrentDSS] != nil && [[DataController instance] getCurrentDSS] != dsServer)
    {
        _backupServer = [[DataController instance] getCurrentDSS];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionFailedHandling:) name:k_DATA_LOGIN_FAILED object:nil];
    }
    
	[[StartupController instance] performDssConnectWithServer:dsServer];
}

- (void) connectionFailedHandling:(NSNotification *)notification
{
    if (_backupServer)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        __block DSServer *weakServer = _backupServer;
        _backupServer = nil;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[StartupController instance] performDssConnectWithServer:weakServer];
        });
    }
}

- (IBAction) actionShowDetails {
	if (dsServer) {
		/*
		 *	show details for this connection
		 */
		
		SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
		[settings showSubGroup:[[SettingsNetworkConnectionDetailVC alloc] initWithConnection:dsServer]];
	}
	
	/*
	 *	I am the "no connection" button -> noop ;)
	 */
	
}

- (void) setActive {
	self.imageSelectionState.image = [UIImage imageNamed:@"Svr_Ausw_RadioBtn_aktiv.png"];
}

- (void) setInactive {
	self.imageSelectionState.image = [UIImage imageNamed:@"Svr_Ausw_RadioBtn_inaktiv.png"];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
