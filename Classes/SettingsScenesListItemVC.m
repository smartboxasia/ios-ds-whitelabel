//
//  SettingsScenesListItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsScenesListItemVC.h"
#import "DSScene.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "SettingsScenesDetailVC.h"
#import "DataController.h"
#import "SettingsScenesListVC.h"

@implementation SettingsScenesListItemVC

@synthesize imageBackground, labelName, buttonStepInto, buttonDelete, imageGroupIcon;

- (id) initWithScene: (DSScene*) theScene withDarkBg: (BOOL) isDark andIndex: (NSInteger) _index isAnimated: (BOOL)animationFlag isLookingForFavorite:(BOOL)favorite {
	if(self = [super init]) {
        isLookingForFavorite = favorite;
		scene = theScene;
		hasDarkBackground = isDark;
		index = _index;
		editModeOn = NO;
        doAnimation = animationFlag;
	}
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	if(scene) {
		self.labelName.text = scene.name;
		
		if(hasDarkBackground) {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Gerate_Liste_dunkel.png"];		
		}
		else {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Gerate_Liste_hell.png"];
		}
        
        
        if([scene.group isEqualToNumber:[NSNumber numberWithInt:1]]){
            //yellow scenes
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                self.imageGroupIcon.image = [UIImage imageNamed:@"lightbulb_off.png"];
            else
                self.imageGroupIcon.image = [UIImage imageNamed:@"lightbulb.png"];
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:2]]){
            //gray scenes
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                self.imageGroupIcon.image = [UIImage imageNamed:@"shade_deact_full.png"];
            else if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:5]])
                self.imageGroupIcon.image = [UIImage imageNamed:@"shade_open.png"];
            else
                self.imageGroupIcon.image = [UIImage imageNamed:@"shade.png"];
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:3]]){
            //blue scenes
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                self.imageGroupIcon.image = [UIImage imageNamed:@"blue_off.png"];
            else
                self.imageGroupIcon.image = [UIImage imageNamed:@"blue_on.png"];
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:4]]){
            //music scenes
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                self.imageGroupIcon.image = [UIImage imageNamed:@"music_off.png"];
            else
                self.imageGroupIcon.image = [UIImage imageNamed:@"music.png"];
        }

        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:5]]){
            //television scenes
            if([scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:0]])
                self.imageGroupIcon.image = [UIImage imageNamed:@"television_off.png"];
            else
                self.imageGroupIcon.image = [UIImage imageNamed:@"television.png"];
        }


	}
    
    //animate color bar with fade effect
    if(doAnimation){
        self.imageBackground.alpha = 0.0;
        self.imageGroupIcon.alpha = 0.0;
        self.labelName.alpha = 0.0;
        self.buttonStepInto.alpha = 0.0;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        self.imageBackground.alpha = 1.0;
        self.imageGroupIcon.alpha = 1.0;
        self.labelName.alpha = 1.0;
        self.buttonStepInto.alpha = 1.0;
        [UIView commitAnimations];
    }


}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(scene){
        self.labelName.text = scene.name;
        [scene addObserver:self forKeyPath:@"name" options:0 context:nil];

    }
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (scene) {
        [scene removeObserver:self forKeyPath:@"name"];
    }
    
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"name"]) {
		self.labelName.text = scene.name;
	}
}

- (IBAction) actionDeleteScene {
	GSLog(@"delete scene...");
	[[DataController instance] deleteScene:scene withRoom:scene.room];
	[(SettingsScenesListVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_SCENES_LIST] buildScrollView];
}

- (IBAction) actionButtonClicked {	
	// Aktivität anzeigen
    if(isLookingForFavorite){
        if (scene) {
            scene.favourit = [NSNumber numberWithBool:YES];
            [[DataController instance] saveContext];
        }
        
        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_FAVORITELIST_HAS_CHANGED];
        [[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_SCENELIST_HAS_CHANGED];
        
        SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
        [settings goBackTwice];

    }
    else{
        SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
        [settings showSubGroup:[[SettingsScenesDetailVC alloc] initWithScene:scene andIndex:index]];
    }
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

    [super dealloc];
}


@end
