//
//  DSScene.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <CoreData/CoreData.h>

@class DSRoom;
@class DSServer;

@interface DSScene :  NSManagedObject  
{
	NSMutableArray *allObserver;
	BOOL dirty;
}

@property (nonatomic, assign) BOOL dirty;

@property (nonatomic, retain) NSNumber * favourit;
@property (nonatomic, retain) NSNumber * mainList;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * sceneNo;
@property (nonatomic, retain) NSSet* devices;
@property (nonatomic, retain) DSRoom * room;
@property (nonatomic, retain) NSNumber * sortOrder;
@property (nonatomic, retain) DSServer *dss;
@property (nonatomic, retain) NSNumber * group;

@end


@interface DSScene (CoreDataGeneratedAccessors)

- (void)addDevices:(NSSet *)value;
- (void)removeDevices:(NSSet *)value;

@end

