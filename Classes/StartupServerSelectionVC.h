//
//  StartupServerSelectionVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface StartupServerSelectionVC : UIViewController {
	UIScrollView *scrollview;
	NSMutableArray *serverItems;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollview;

- (void) buildScrollview;
- (void) prepareDealloc;

@end
