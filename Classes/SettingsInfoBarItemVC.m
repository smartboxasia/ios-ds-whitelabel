//
//  SettingsInfoBarItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsInfoBarItemVC.h"
#import "ModelLocator.h"
#import "SettingsInfoBarVC.h"

@implementation SettingsInfoBarItemVC

@synthesize labelName, imageDragMe, labelIndex;

- (id) initWithName: (NSString*) _name andOffset:(float) _offset andMaximum:(NSInteger) _max {
	self = [super init];
	
	if (self) {
		propertyName = _name;
		offset = _offset;
		maximum = _max;
	}
	
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.labelName.text = propertyName;
	
	magneticY = (int)floor(offset/self.view.frame.size.height);
	
	self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
}


- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	
}

- (void) gotoPosition: (NSInteger) _pos {
	magneticY = (int)_pos;
	[UIView beginAnimations:@"" context:nil];
	[UIView setAnimationDuration:0.2];
	
	self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	
	[UIView commitAnimations];
	
	self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setValue:[NSNumber numberWithInteger:magneticY] forKey:propertyName];
}

- (void) incrementMagneticPoint {
	//GSLog(@"increment before: %i", magneticY);
	magneticY ++;
	//GSLog(@"increment after: %i", magneticY);
	//scene.sortOrder = [NSNumber numberWithInteger:magneticY];
	
	[UIView beginAnimations:@"" context:nil];
	[UIView setAnimationDuration:0.2];
	
	self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	
	[UIView commitAnimations];
	
	self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
	
	/*[UIView animateWithDuration:0.2 animations:^{
	 self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	 }];*/
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setValue:[NSNumber numberWithInteger:magneticY] forKey:propertyName];
//	GSLog(@"setting property %@: %i", propertyName, magneticY);
}

- (void) decrementMagneticPoint {
//	GSLog(@"decrement before: %i", magneticY);
	magneticY --;
//	GSLog(@"decrement after: %i", magneticY);
	//scene.sortOrder = [NSNumber numberWithInteger:magneticY];
	
	[UIView beginAnimations:@"" context:nil];
	[UIView setAnimationDuration:0.2];
	
	self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	
	[UIView commitAnimations];
	
	self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
	/*[UIView animateWithDuration:0.2 animations:^{
	 self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	 }];*/
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setValue:[NSNumber numberWithInteger:magneticY] forKey:propertyName];
//	GSLog(@"setting property %@: %i", propertyName, magneticY);
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
		CGPoint beginCenter = self.view.frame.origin;
		
		UITouch *touch = [touches anyObject];
		CGPoint touchPoint = [touch locationInView:self.view.superview];
		
		deltaY = touchPoint.y - beginCenter.y;
//		GSLog(@"delta was: %i", deltaY);
		[self.view.superview bringSubviewToFront:self.view];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	
		UITouch *touch = [touches anyObject];
		CGPoint touchPoint = [touch locationInView:self.view.superview];
		
		// Set the correct center when touched 
		
		
		self.view.frame = CGRectMake(self.view.frame.origin.x, touchPoint.y-deltaY, self.view.frame.size.width, self.view.frame.size.height);
		
		if (self.view.frame.origin.y - magneticY < self.view.frame.size.height || self.view.frame.origin.y - magneticY > self.view.frame.size.height) {

			int old = magneticY;
			magneticY = (int)floor(self.view.frame.origin.y/self.view.frame.size.height);
			
			if (magneticY < 0) {
				magneticY = 0;
			}
			
			if (magneticY >= maximum) {
				magneticY = (int)maximum-1;
			}
			
			if (old-magneticY == 1 || old-magneticY == -1) {
				self.labelIndex.text = [NSString stringWithFormat:@"%i.", magneticY+1];
				//scene.sortOrder = [NSNumber numberWithInteger:magneticY];
				
				NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				[defaults setValue:[NSNumber numberWithInteger:magneticY] forKey:propertyName];
//				GSLog(@"setting property %@: %i", propertyName, magneticY);
				
				[(SettingsInfoBarVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_INFOBAR_LIST] magneticPointHasChanged:old toIndex:magneticY];
			}

		}
	
	
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
	[UIView animateWithDuration:0.2 animations:^{
		self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	}];
	
	[(SettingsInfoBarVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_INFOBAR_LIST] arrangeItems];
	
	[[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_INFOLIST_HAS_CHANGED];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {	
	
	[UIView animateWithDuration:0.2 animations:^{
		self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	}];
	
	[(SettingsInfoBarVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_INFOBAR_LIST] arrangeItems];
	
	[[ModelLocator instance] setValue:[NSNumber numberWithBool:YES] forKey:k_DATA_INFOLIST_HAS_CHANGED];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
