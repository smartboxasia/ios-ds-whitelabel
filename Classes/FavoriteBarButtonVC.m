//
//  FavoriteBarButtonVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "FavoriteBarButtonVC.h"
#import "DSScene.h"
#import "DataController.h"
#import "ModelLocator.h"
#import "SettingsVC.h"
#import "SettingsFavoriteBarVC.h"
#import "CustomButton.h"
#import "GAIHelper.h"

@implementation FavoriteBarButtonVC

@synthesize button, imageBg;

- (id) initWithScene: (DSScene*) theScene {
	if (self = [super init]) {
		scene = theScene;
	}
    
    shouldUndoScene = NO;
	
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
	
	if (scene) {
		[self.button setTitle:scene.name forState:UIControlStateNormal];
	}
	else {
		self.imageBg.image =[UIImage imageNamed:@"HS_bottomIconPlus.png"];
	}
    
}
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
	if (scene && scene.room) {
        if ([scene.group isEqualToNumber:[NSNumber numberWithInt:1]]) {
            [scene.room addObserver:self forKeyPath:@"lastCalledScene" options:0 context:nil];
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:2]]){
            [scene.room addObserver:self forKeyPath:@"lastCalledShadeScene" options:0 context:nil];
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:3]]){
            [scene.room addObserver:self forKeyPath:@"lastCalledBlueScene" options:0 context:nil];
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:4]]){
            [scene.room addObserver:self forKeyPath:@"lastCalledAudioScene" options:0 context:nil];
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:5]]){
            [scene.room addObserver:self forKeyPath:@"lastCalledVideoScene" options:0 context:nil];
        }
        
	}
    
	[[ModelLocator instance] addObserver:self forKeyPath:k_DATA_APARTMENT_LAST_CALLED_SCENE options:0 context:nil];
    
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_APARTMENT_LAST_CALLED_SCENE];
    }
    @catch (NSException *exception) {
        //just in case
    }
    
    
    if (scene && scene.room) {
        if ([scene.group isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            @try {
                [scene.room removeObserver:self forKeyPath:@"lastCalledScene"];
            }
            @catch (NSException *exception) {
                //just in case
            }
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:2]]){
            @try {
                [scene.room removeObserver:self forKeyPath:@"lastCalledShadeScene"];
                
            }
            @catch (NSException *exception) {
                //just in case
            }
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:3]]){
            @try {
                [scene.room removeObserver:self forKeyPath:@"lastCalledBlueScene"];
            }
            @catch (NSException *exception) {
                //just in case
            }
            
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:4]]){
            @try {
                [scene.room removeObserver:self forKeyPath:@"lastCalledAudioScene"];
            }
            @catch (NSException *exception) {
                //just in case
            }
            
        }
        else if([scene.group isEqualToNumber:[NSNumber numberWithInt:5]]){
            @try {
                [scene.room removeObserver:self forKeyPath:@"lastCalledVideoScene"];
            }
            @catch (NSException *exception) {
                //just in case
            }
            
        }

	}
    
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_DATA_APARTMENT_LAST_CALLED_SCENE]) {
        if ([scene.sceneNo isEqualToNumber:[[ModelLocator instance] valueForKey:k_DATA_APARTMENT_LAST_CALLED_SCENE]] 
            && ![scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:72]]
            && ![scene.sceneNo isEqualToNumber:[NSNumber numberWithInt:73]]
            && [scene.sceneNo longLongValue] < 1024) {
            [(CustomButton*)self.button setBold:YES];
            shouldUndoScene = YES;
        }
        else {
            if(!scene.room){
                [(CustomButton*)self.button setBold:NO];
                shouldUndoScene = NO;
            }
        }
	}
    else if ([keyPath isEqualToString:@"lastCalledScene"] || [keyPath isEqualToString:@"lastCalledShadeScene"] || [keyPath isEqualToString:@"lastCalledBlueScene"]|| [keyPath isEqualToString:@"lastCalledAudioScene"]|| [keyPath isEqualToString:@"lastCalledVideoScene"]) {
       	if(scene.room){
            if ((scene.sceneNo && scene.room.lastCalledScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:1]]) {
                [(CustomButton*)self.button setBold:YES];
                shouldUndoScene = YES;
            }
            else if ((scene.sceneNo && scene.room.lastCalledShadeScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledShadeScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:2]]) {
                [(CustomButton*)self.button setBold:YES];
                shouldUndoScene = YES;
            }
            else if ((scene.sceneNo && scene.room.lastCalledBlueScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledBlueScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:3]]) {
                [(CustomButton*)self.button setBold:YES];
                shouldUndoScene = YES;
            }
            else if ((scene.sceneNo && scene.room.lastCalledAudioScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledAudioScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:4]]) {
                [(CustomButton*)self.button setBold:YES];
                shouldUndoScene = YES;
            }
            else if ((scene.sceneNo && scene.room.lastCalledVideoScene) && [scene.sceneNo longLongValue] == [scene.room.lastCalledVideoScene intValue] && [scene.group isEqualToNumber:[NSNumber numberWithInt:5]]) {
                [(CustomButton*)self.button setBold:YES];
                shouldUndoScene = YES;
            }

            else {
                [(CustomButton*)self.button setBold:NO];
                shouldUndoScene = NO;
            }
        }
	}
}


- (IBAction) actionButtonDown: (id) sender {
	if (
        //leave home case
        scene 
        && !scene.room 
        && ([scene.sceneNo longLongValue] == 72)) {
        
        [GAIHelper sendUIActionWithCategory:@"favorite_called" andLabel:@"leave"];
        
        needConfirmation = YES;
        //show confirmation dialog
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message:NSLocalizedString(@"key_confirm_leave_home", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"key_no", @"") otherButtonTitles:NSLocalizedString(@"key_yes", @""),nil];
        [alert show];
        [alert release];

	}
    else if (
        //panic case
        scene 
        && !scene.room 
        && ([scene.sceneNo longLongValue] == 65) 
        && ![scene.sceneNo isEqualToNumber:[[ModelLocator instance] valueForKey:k_DATA_APARTMENT_LAST_CALLED_SCENE]]) {
        
        [GAIHelper sendUIActionWithCategory:@"favorite_called" andLabel:@"panic"];
        
        needConfirmation = YES;
        //show confirmation dialog
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message:NSLocalizedString(@"key_confirm_panic", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"key_no", @"") otherButtonTitles:NSLocalizedString(@"key_yes", @""),nil];
        [alert show];
        [alert release];
        
	}

	else {
        
        if(scene.room) {
            [GAIHelper sendUIActionWithCategory:@"favorite_called" andLabel:@"scene"];
        }
        else if([scene.sceneNo integerValue] >= 1024) {
            [GAIHelper sendUIActionWithCategory:@"favorite_called" andLabel:@"uda"];
        }
        
		needConfirmation = NO;
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    //if user selected yes, activate the scene
    if (buttonIndex == 1) {
        [self executeScene];
    }
    needConfirmation = NO;
}


- (IBAction) actionButtonUp: (id) sender {
	if (!needConfirmation) {
		[self executeScene];
	}
}

- (void) executeScene {
	if (scene) {
		// scene button pressed
        if (shouldUndoScene){
            [[DataController instance] undoScene:scene];
        }
		else {
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
				[[DataController instance] turnSceneOn:scene];
			});
		}
	}
	else {
		// plus button pressed
		[(SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS] showController:[SettingsFavoriteBarVC new]];
		[(SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS] showWithoutIntelligence];
	}

}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

    [super dealloc];
}


@end
