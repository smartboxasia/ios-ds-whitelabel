//
//  SettingsDevicesConfigItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomSwitch.h"

@class DSDevice;

@interface SettingsDevicesConfigItemVC : UIViewController {
	DSDevice *device;
	BOOL hasDarkBackground;
    BOOL isDimmable;
	
	UIImageView *imageBackground;
	UILabel *labelName;
    UILabel *labelValue;
    IBOutlet UICustomSwitch *onOffSwitch;
    UISlider *dimmerSlider;
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UILabel *labelName;
@property (nonatomic, retain) IBOutlet UILabel *labelValue;
@property (nonatomic, retain) IBOutlet UISlider *dimmerSlider;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *loadingWheel;

- (id) initWithDevice: (DSDevice*) theDevice andDarkBg: (BOOL) isDark;
- (void) toggleLoadingAnimation: (BOOL) turnOn;
- (IBAction) switchChanged: (id) sender;
- (IBAction) sliderChanged: (id) sender;
- (IBAction) sliderTouchUpInside: (id) sender;

@end
