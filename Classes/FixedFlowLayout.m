//
//  FixedFlowLayout.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "FixedFlowLayout.h"

@implementation FixedFlowLayout

- (id) init {
	if (self = [super init]) {
		self.itemSize = CGSizeMake(280, 35);
	}
	return self;
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}

@end
