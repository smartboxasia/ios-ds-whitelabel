//
//  DetachedDevice.h
//  DS iPhone
//
//  Created by rolf on 17.12.10.
//  Copyright 2010 Granny&Smith Technikagentur GmbH & Co. KG. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "DSDevice.h"


@interface DetachedDevice :  DSDevice  
{

}

@property (nonatomic, retain) NSNumber * sceneNo;

@end



