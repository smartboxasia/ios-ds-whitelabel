//
//  SettingsDeviceChooseRoomVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSRoom;
@class DSDevice;

@interface SettingsDeviceChooseRoomVC : GAITrackedViewController {
	UIScrollView *scrollview;
	NSMutableArray *items;
    DSRoom *_currentRoom;
    DSDevice *_currentDevice;
}

@property(nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;
@property (nonatomic, retain) IBOutlet UILabel *titelLabel;

- (id) initWithCurrentRoom: (DSRoom*)room andCurrentDevice: (DSDevice*)device;

- (void) buildScrollView;
- (IBAction) actionBack;

@end
