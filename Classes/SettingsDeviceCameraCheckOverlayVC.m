//
//  SettingsDeviceCameraCheckOverlayVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceCameraCheckOverlayVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "SettingsDeviceWebviewVC.h"

#define kImageDotGreen [UIImage imageNamed:@"settings_devices_scanmodal_dot_green.png"]
#define kImageDotYellow [UIImage imageNamed:@"settings_devices_scanmodal_dot_yellow.png"]
#define kImageDotRed [UIImage imageNamed:@"settings_devices_scanmodal_dot_red.png"]

#define kImageCheckOk [UIImage imageNamed:@"settings_devices_scanmodal_check.png"]
#define kImageCheckNotOk [UIImage imageNamed:@"settings_devices_scanmodal_xmark.png"]

@interface SettingsDeviceCameraCheckOverlayVC ()

@end

@implementation SettingsDeviceCameraCheckOverlayVC

@synthesize labelContent;
@synthesize labelHeadline;
@synthesize labelDimmingDetails;
@synthesize imageContent;
@synthesize imageHeadline;
@synthesize activityIndicator;
@synthesize buttonLeft;
@synthesize buttonRight;
@synthesize indicatorTransferDimmingCurveRunning;
@synthesize ean;

- (id) initWithDevice: (DSDevice*) _device andActionType: (NSString*) _actionType {
    self = [super init];
    if (self) {
		actionType = _actionType;
		device = _device;
		availableDimmingCurves = nil;
		currentObservers = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.activityIndicator.hidden = NO;
	self.indicatorTransferDimmingCurveRunning.hidden = YES;
	self.imageHeadline.image = kImageDotYellow;
	self.labelHeadline.text = NSLocalizedString(@"key_settings_device_scan_overlay_searching", @"searching");
	self.labelContent.text = @"";
	self.labelDimmingDetails.text = @"";
    self.labelDimmingDetails.hidden = YES;
	self.imageContent.hidden = YES;
	self.buttonRight.hidden = YES;
	
    NSMutableAttributedString *rightTitle = [[NSMutableAttributedString alloc] initWithAttributedString:[self.buttonRight attributedTitleForState:UIControlStateNormal]];
    [rightTitle.mutableString setString: NSLocalizedString(@"key_settings_device_scan_overlay_right_button_transfer_dimming_curve", @"transfer dimming")];
    [self.buttonRight setAttributedTitle:rightTitle forState:UIControlStateNormal];
    
    [self.buttonLeft setTitle:NSLocalizedString(@"key_scan_again", @"scan again") forState:UIControlStateNormal];

    
	id currObserver = nil;
	
	currObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationSettingsDevicesIlluminantCheckStarted object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
		self.activityIndicator.hidden = NO;
		self.imageHeadline.image = kImageDotYellow;
		self.labelHeadline.text = NSLocalizedString(@"key_settings_device_scan_overlay_searching", @"searching");
		self.labelDimmingDetails.text = @"";
		self.labelContent.text = @"";
		self.imageContent.hidden = YES;
		self.buttonRight.hidden = YES;
	}];
	
	[currentObservers addObject:currObserver];
	
	/*
	 *	 0 - OK
	 */
	currObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationSettingsDevicesIlluminantCheck0OK object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
		
		self.activityIndicator.hidden = YES;
		
		
		NSString *fittingCheckResult = [note.userInfo valueForKey:@"FittingCheckResult"];
		NSString *illuminantDimmingStatus = [note.userInfo valueForKey:@"IlluminantDimmingResult"];
		
		if ([note.userInfo valueForKey:@"FittingCheckResult"] == [NSNull null]) {
			fittingCheckResult = nil;
		}
		
		if ([note.userInfo valueForKey:@"IlluminantDimmingResult"] == [NSNull null]) {
			illuminantDimmingStatus = nil;
		}
		
		if (fittingCheckResult) {
			if ([fittingCheckResult isEqualToString:@"fits"]) {
				//	set top trafic light to green and display message: illuminant found
				self.labelHeadline.text = NSLocalizedString(@"key_settings_device_scan_overlay_illuminant_ok_headline", @"Illuminant found");
				
				//	display content message: illuminant fits to device
				self.imageContent.hidden = NO;
				self.imageContent.image = kImageCheckOk;
				self.labelContent.text = NSLocalizedString(@"key_settings_device_scan_overlay_illuminant_ok_content", @"You can use this illuminant!");
			}
			else if ([fittingCheckResult isEqualToString:@"no_fit_socket_type_no_match"]) {
				//	set top trafic light to green and display message: illuminant found
				self.labelHeadline.text = NSLocalizedString(@"key_settings_device_scan_overlay_illuminant_not_ok_headline", @"Wrong socket");
				
				//	display content message: socket not matching
				self.imageContent.hidden = NO;
				self.imageContent.image = kImageCheckNotOk;
				self.labelContent.text = NSLocalizedString(@"key_settings_device_scan_overlay_illuminant_not_ok_content", @"This illuminant does not fit to your device!");
			}
			else if ([fittingCheckResult isEqualToString:@"no_fit_max_power_no_match"]) {
				//	set top trafic light to green and display message: illuminant found
				self.labelHeadline.text = NSLocalizedString(@"key_settings_device_scan_overlay_device_not_enough_power_headline", @"Power rating exceeded");
				
				//	display content message: max power not matching
				self.imageContent.hidden = NO;
				self.imageContent.image = kImageCheckNotOk;
				self.labelContent.text = NSLocalizedString(@"key_settings_device_scan_overlay_illuminant_not_ok_content", @"Bulb exceeds recommended power rating.");
			}
			else if ([fittingCheckResult isEqualToString:@"no_fit_socket_type_max_power"]) {
				//	set top trafic light to green and display message: illuminant found
				self.labelHeadline.text = NSLocalizedString(@"key_settings_device_scan_overlay_device_not_enough_power_headline", @"Power rating exceeded");
				
				//	display content message: max power not matching
				self.imageContent.hidden = NO;
				self.imageContent.image = kImageCheckNotOk;
				self.labelContent.text = NSLocalizedString(@"key_settings_device_scan_overlay_illuminant_not_ok_content", @"Bulb exceeds recommended power rating.");
			}
			else if ([fittingCheckResult isEqualToString:@"no_check_missing_device_data"]) {
				//	set top trafic light to red and display message: device not found
				self.labelHeadline.text = NSLocalizedString(@"key_settings_device_scan_overlay_dsid_not_found_headline", @"Device not found");
				
				//	display content message: device not found
				self.imageContent.hidden = NO;
				self.imageContent.image = kImageCheckNotOk;
				self.labelContent.text = NSLocalizedString(@"key_settings_device_scan_overlay_dsid_not_found_content", @"Your device ID is unknown.");
			}
			else if ([fittingCheckResult isEqualToString:@"no_check_missing_illuminant_data"]) {
				//	set top trafic light to red and display message: illuminant not found
				self.labelHeadline.text = NSLocalizedString(@"key_settings_device_scan_overlay_illuminant_not_found_headline", @"Illuminant not found");
				
				//	display content message: illumninant not found
				self.imageContent.hidden = NO;
				self.imageContent.image = kImageCheckNotOk;
				self.labelContent.text = NSLocalizedString(@"key_settings_device_scan_overlay_illuminant_not_found_content", @"We have no data about this illuminant.");
			}
		}
		
			if (fittingCheckResult && illuminantDimmingStatus && [fittingCheckResult isEqualToString:@"fits"] &&
                ([illuminantDimmingStatus isEqualToString:@"Green"] || [illuminantDimmingStatus isEqualToString:@"Yellow"])
                && [actionType isEqualToString:kActionTypeIlluminantTransfer]) {
				
				//	enable button for transfering dimming curve
				self.buttonRight.hidden = NO;
				self.buttonRight.enabled = YES;
			}
			else {
				//	disable button for transfering dimming curve
				self.buttonRight.hidden = YES;
				self.buttonRight.enabled = NO;
			}
		
		if (illuminantDimmingStatus && [illuminantDimmingStatus isEqualToString:@"Green"]) {
			//	set trafic light image to green
			self.imageHeadline.image = kImageDotGreen;
            self.labelDimmingDetails.hidden = NO;
            self.labelDimmingDetails.text = NSLocalizedString(@"key_dimming_green", nil);
		}
		else if (illuminantDimmingStatus && [illuminantDimmingStatus isEqualToString:@"Yellow"]) {
			//	set trafic light image to yellow
			self.imageHeadline.image = kImageDotYellow;
            self.labelDimmingDetails.hidden = NO;
            self.labelDimmingDetails.text = NSLocalizedString(@"key_dimming_yellow", nil);
		}
		else if (illuminantDimmingStatus && [illuminantDimmingStatus isEqualToString:@"Red"]) {
			//	set trafic light image to red
			self.imageHeadline.image = kImageDotRed;
            self.labelDimmingDetails.hidden = NO;
            self.labelDimmingDetails.text = NSLocalizedString(@"key_dimming_red", nil);
		}
		else {
			//	set trafic light image to red
			self.imageHeadline.image = kImageDotRed;
            self.labelDimmingDetails.hidden = NO;
            self.labelDimmingDetails.text = NSLocalizedString(@"key_dimming_red", nil);
		}
		
	}];
	
	[currentObservers addObject:currObserver];
	
	
	
	/*
	 *	 99 - UNKNOWN_EXCEPTION
	 */
	
	currObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationSettingsDevicesIlluminantCheck99UnknownException object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {

		self.labelDimmingDetails.hidden = YES;
        self.labelDimmingDetails.text = @"";
		self.activityIndicator.hidden = YES;
		self.imageHeadline.image = kImageDotRed;
		self.labelHeadline.text = NSLocalizedString(@"key_settings_device_scan_overlay_general_error_headline", @"General error");
		self.labelContent.text = NSLocalizedString(@"key_settings_device_scan_overlay_general_error_content", @"Sorry, an error occurred.\nPlease try again.");;
		self.imageContent.hidden = NO;
		self.imageContent.image = kImageCheckNotOk;
		self.buttonRight.hidden = YES;
		self.buttonRight.enabled = NO;
	}];
	
	[currentObservers addObject:currObserver];
	
	
	
	currObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationSettingsDevicesTransferDimmingCurveTransferStarted object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
		self.indicatorTransferDimmingCurveRunning.hidden = NO;
		self.buttonRight.hidden = YES;
		
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_settings_device_scan_overlay_transfer_dimming_curve_success_title", @"Success") message:NSLocalizedString(@"key_settings_device_scan_overlay_transfer_dimming_curve_success_message", @"Dimming curve transferred") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"OK") otherButtonTitles:nil];
        [alert show];
	}];
	
	[currentObservers addObject:currObserver];
	
	
	currObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationSettingsDevicesTransferDimmingCurveTransferDone object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
		self.indicatorTransferDimmingCurveRunning.hidden = YES;
		self.buttonRight.hidden = NO;
        
        //close the scanner
        for (id observer in currentObservers) {
            @try {
                [[NSNotificationCenter defaultCenter] removeObserver:observer];
            }
            @catch (NSException *exception) {
                //just in case
            }
            
        }
        
        [UIView animateWithDuration:0.4 animations:^{
            self.view.alpha = 0;
        } completion:^(BOOL finished) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOverlayCloseAndExit object:nil];
            [self.view removeFromSuperview];
            [self release];
            
        }];
	}];
	
	[currentObservers addObject:currObserver];
	
	
	currObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationSettingsDevicesTransferDimmingCurveTransferFailed object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
		NSLog(@"kNotificationSettingsDevicesTransferDimmingCurveTransferFailed received");
		self.indicatorTransferDimmingCurveRunning.hidden = YES;
		self.buttonRight.hidden = NO;
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_settings_device_scan_overlay_transfer_dimming_curve_failed_title", @"Failed") message:NSLocalizedString(@"key_settings_device_scan_overlay_transfer_dimming_curve_failed_message", @"Dimming curve not transferred") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"OK") otherButtonTitles:nil];
		[alert show];
	}];
	
	[currentObservers addObject:currObserver];
	
}

- (IBAction)actionClose:(id)sender {
	
	for (id observer in currentObservers) {
        @try {
            [[NSNotificationCenter defaultCenter] removeObserver:observer];
        }
        @catch (NSException *exception) {
            //just in case
        }
		
	}
	
	[UIView animateWithDuration:0.4 animations:^{
		self.view.alpha = 0;
	} completion:^(BOOL finished) {
		[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOverlayClosed object:nil];
		[self.view removeFromSuperview];
		[self release];
		
	}];
}

- (IBAction)actionButtonRight:(id)sender {
	
	[[DataController instance] cloudServiceTransferDimmingCurveForDevice:device andGtin:self.ean];
	
	return;
	if ([actionType isEqualToString:kActionTypeIlluminantTransfer]) {
		if (availableDimmingCurves && [availableDimmingCurves count] > 0) {
			NSDictionary *dimmingCurveObj = [availableDimmingCurves objectAtIndex:0];
			if (dimmingCurveObj && dimmingCurveObj != (id)[NSNull null] && [dimmingCurveObj valueForKey:@"ID"] && [dimmingCurveObj valueForKey:@"ID"] != (id)[NSNull null]) {
                [[DataController instance] cloudServiceTransferDimmingCurveForDevice:device andGtin:self.ean];
//              // commented out because function does not exist
//				[[DataController instance] cloudServiceTransferDimmingCurveForDevice:device andEan:self.ean andDimmingCurve:[dimmingCurveObj valueForKey:@"ID"]];
			}
			
		}
	}
	else {
		/*
		 *	call url, depending on return code of illuminant check
		 */
		
		SettingsDeviceWebviewVC *webviewVc = [[SettingsDeviceWebviewVC alloc] initWithTitle:NSLocalizedString(@"key_settings_device_scan_overlay_right_button_further_information", @"Further Information") andTargetUrl: @"http://digitalstrom.org"];
		
		UIViewController *mainVc = [[ModelLocator instance] valueForKey:k_VIEW_MAIN];
		
		[UIView transitionFromView:mainVc.view
							toView:webviewVc.view
						  duration:0.6
						   options: UIViewAnimationOptionTransitionFlipFromLeft
						completion:^(BOOL finished) {
							if (finished) {
								
							}
						}
		 ];
	}
	

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
