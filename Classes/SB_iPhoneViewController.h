//
//  SB_iPhoneViewController.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EV_RootVC;

@interface SB_iPhoneViewController : UIViewController {
	EV_RootVC* ev_rootVC;
	UIViewController *mainView;

	bool evAllowed;
	BOOL receivedMemoryWarning;
}

- (void) showPortraitMode;
- (void) showPortraitModeWithoutAnimation;
- (void) resetMemoryWarning;
- (void) initEV;

@end
