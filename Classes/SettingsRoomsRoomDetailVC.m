//
//  SettingsRoomsRoomDetailVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsRoomsRoomDetailVC.h"
#import "ModelLocator.h"
#import "SettingsVC.h"
#import "DataController.h"
#import "DSMeter.h"
#import "CustomLabel.h"

@implementation SettingsRoomsRoomDetailVC

@synthesize labelHeadName, textRoomName, labelCircuitName,background;

- (id) initWithRoom: (DSRoom*) theRoom {
	if(self = [super init]) {
		room = theRoom;
	}
	
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    labelNameTitle.text = NSLocalizedString(@"key_name", nil);
    labelMeterTitle.text = NSLocalizedString(@"key_meter", nil);
    
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }
	
	self.textRoomName.text = room.name;
	

    NSString *meterName = @"N/A";
    for (DSMeter* meter in [[DataController instance] meters]) {
        if([meter.identifier isEqualToString: room.meterID])
            meterName = meter.name;
    }
	self.labelCircuitName.text = meterName;

    self.screenName = @"Settings Room Details";
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortedRooms = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [orderDescriptor release];
    [nameDescriptor release];
	
	int index = (int)[sortedRooms indexOfObject:room];
	self.labelHeadName.text = [NSString stringWithFormat:@"2.%i %@", index+1, room.name];

	[room addObserver:self forKeyPath:@"name" options:0 context:nil];
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [room removeObserver:self forKeyPath:@"name"];

}

#pragma mark -
#pragma mark Observer method

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"name"]) {
		NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
		NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
		NSArray *sortedRooms = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
        [orderDescriptor release];
        [nameDescriptor release];
		
		int index = (int)[sortedRooms indexOfObject:room];
		self.labelHeadName.text = [NSString stringWithFormat:@"2.%i %@", index+1, room.name];
	}
}

#pragma mark -
#pragma mark IB Outlets

- (IBAction) actionBack {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
}

- (IBAction) actionEnableRoomEditing {
	self.textRoomName.enabled = YES;
	[self.textRoomName becomeFirstResponder];
	
}

#pragma mark -
#pragma mark UITextFieldDelegate methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		if (![[DataController instance] setRoomName:self.textRoomName.text withRoom:room]) {
			dispatch_async(dispatch_get_main_queue(), ^{
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Error", @"") message:NSLocalizedString(@"key_could_not_submit_name", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK", @"") otherButtonTitles:nil];
				[alert show];
                [alert release];
				
				self.textRoomName.text = room.name;
			});
		}
	});
	
	self.textRoomName.enabled = NO;
	[self.textRoomName resignFirstResponder];
	
	return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
    [labelNameTitle release];
    [labelMeterTitle release];
    [super dealloc];
}


@end
