//
//  SettingsDeviceCameraCheckOverlayVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

#define kActionTypeIlluminantCheck @"illuminantCheck"
#define kActionTypeIlluminantTransfer @"illuminantTransfer"

#define kNotificationOverlayClosed @"notificationOverlayClosed"
#define kNotificationOverlayCloseAndExit @"notificationOverlayCloseAndExit"

@class DSDevice;

@interface SettingsDeviceCameraCheckOverlayVC : UIViewController {
	NSString *actionType;
	
	NSArray *availableDimmingCurves;
	DSDevice *device;
	NSMutableArray *currentObservers;
	
}

@property (nonatomic, retain) NSString *ean;
@property (nonatomic, retain) IBOutlet UILabel *labelHeadline;
@property (nonatomic, retain) IBOutlet UILabel *labelContent;
@property (nonatomic, retain) IBOutlet UILabel *labelDimmingDetails;
@property (nonatomic, retain) IBOutlet UIImageView *imageHeadline;
@property (nonatomic, retain) IBOutlet UIImageView *imageContent;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicatorTransferDimmingCurveRunning;
@property (nonatomic, retain) IBOutlet UIButton *buttonLeft;
@property (nonatomic, retain) IBOutlet UIButton *buttonRight;


- (id) initWithDevice: (DSDevice*) _device andActionType: (NSString*) _actionType;
- (IBAction)actionClose:(id)sender;
- (IBAction)actionButtonRight:(id)sender;

@end
