//
//  JustifyVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "JustifyVC.h"
#import "DataController.h"
#import "DSDevice.h"
#import "NSTimer+blocks.h"

@implementation JustifyVC

@synthesize imageStatus, labelDevicename;

- (id) initWithDevice: (DSDevice*) _device {
	self = [super init];

	if (self) {
		device = _device;
	}

	return self;
}

- (id) initWithRoom: (DSRoom*) _room {
	self = [super init];

	if (self) {
		room = _room;
	}

	return self;
}

- (id) initWithScene: (DSScene*) _scene {
	self = [super init];

	if (self) {
		scene = _scene;
	}

	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
//    [TestFlight passCheckpoint:@"Entered 3-way"];

//    //iphone 5 support.
//    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        //manually move the elements of the justify button down to the center of the screen
//        int offset = 54;
//        meteringValue.frame = CGRectMake(meteringValue.frame.origin.x, meteringValue.frame.origin.y + offset, meteringValue.frame.size.width, meteringValue.frame.size.height);
//        consumptionValue.frame = CGRectMake(consumptionValue.frame.origin.x, consumptionValue.frame.origin.y + offset, consumptionValue.frame.size.width, consumptionValue.frame.size.height);
//        refreshButton.frame = CGRectMake(refreshButton.frame.origin.x, refreshButton.frame.origin.y + offset, refreshButton.frame.size.width, refreshButton.frame.size.height);
//        imageStatus.frame = CGRectMake(imageStatus.frame.origin.x, imageStatus.frame.origin.y + offset, imageStatus.frame.size.width, imageStatus.frame.size.height);
//        imageButton.frame = CGRectMake(imageButton.frame.origin.x, imageButton.frame.origin.y + offset, imageButton.frame.size.width, imageButton.frame.size.height);
//        justifyButton1.frame = CGRectMake(justifyButton1.frame.origin.x, justifyButton1.frame.origin.y + offset, justifyButton1.frame.size.width, justifyButton1.frame.size.height);
//        justifyButton2.frame = CGRectMake(justifyButton2.frame.origin.x, justifyButton2.frame.origin.y + offset, justifyButton2.frame.size.width, justifyButton2.frame.size.height);
//        justifyButton3.frame = CGRectMake(justifyButton3.frame.origin.x, justifyButton3.frame.origin.y + offset, justifyButton3.frame.size.width, justifyButton3.frame.size.height);
//        loadingWheelConsumption.frame = CGRectMake(loadingWheelConsumption.frame.origin.x, loadingWheelConsumption.frame.origin.y + offset, loadingWheelConsumption.frame.size.width, loadingWheelConsumption.frame.size.height);
//        loadingWheelMetering.frame = CGRectMake(loadingWheelMetering.frame.origin.x, loadingWheelMetering.frame.origin.y + offset, loadingWheelMetering.frame.size.width, loadingWheelMetering.frame.size.height);
//
//    }

    self.screenName = @"Overview Advanced";
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //disable rotating to energy visualisation while on this view
    [DataController instance].rotationEnabled = NO;

    //enable tilt notifications and start listening
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];

	[self applyBackground];
	if (device) {
		self.labelDevicename.text = device.name;
		[device addObserver:self forKeyPath:@"powerOn" options:0 context:nil];
        [device addObserver:self forKeyPath:@"consumption" options:0 context:nil];
        [device addObserver:self forKeyPath:@"metering" options:0 context:nil];
        consumptionValue.text = @"- W";
        meteringValue.text = @"- kWh";
        loadingWheelMetering.hidden = YES;
        loadingWheelConsumption.hidden = YES;
	}
	else if(room) {
		self.labelDevicename.text = room.name;
	}
	else if(scene) {
		self.labelDevicename.text = scene.name;

		if (scene.room) {
			[scene.room addObserver:self forKeyPath:@"lastCalledScene" options:0 context:nil];
		}
	}

    if(!device){
        consumptionValue.hidden = YES;
        meteringValue.hidden = YES;
        refreshButton.hidden = YES;
        loadingWheelMetering.hidden = YES;
        loadingWheelConsumption.hidden = YES;
    }

	timerPlus = nil;
	timerMinus = nil;
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (device) {
        @try {
            [device removeObserver:self forKeyPath:@"powerOn"];
            [device removeObserver:self forKeyPath:@"consumption"];
            [device removeObserver:self forKeyPath:@"metering"];
        }
        @catch (NSException *exception) {
            //just in case
        }

	}
	else if (scene && scene.room) {
        @try {
            [scene.room removeObserver:self forKeyPath:@"lastCalledScene"];
        }
        @catch (NSException *exception) {
            //just in case
        }
    }

    [[UIDevice currentDevice]endGeneratingDeviceOrientationNotifications];
    @try {
         [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    @catch (NSException *exception) {
        //just in case
    }


}


- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    [self becomeFirstResponder];
}

- (BOOL) canBecomeFirstResponder{
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event{
    if(event.type == UIEventSubtypeMotionShake){
        [self actionPoint];
    }
}

#pragma mark -
#pragma mark Observer
- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"powerOn"]) {
		[self applyBackground];
	}
	if([keyPath isEqualToString:@"lastCalledScene"]) {
		if ([scene.sceneNo longLongValue] == [scene.room.lastCalledScene intValue]) {
			self.imageStatus.image = [UIImage imageNamed:@"Anp_Schalter_BG_On.png"];
		}
		else {
			self.imageStatus.image = [UIImage imageNamed:@"Anp_Schalter_BG.png"];
		}
	}
    if ([keyPath isEqualToString:@"consumption"]) {
        loadingWheelConsumption.hidden = YES;
        [loadingWheelConsumption stopAnimating];
        consumptionValue.hidden = NO;
        if([device.consumption isEqualToNumber:[NSNumber numberWithInt:-1]])
            consumptionValue.text = @"- W";
        else
            consumptionValue.text = [NSString stringWithFormat:@"%@ W", device.consumption];
	}
    if ([keyPath isEqualToString:@"metering"]) {
        loadingWheelMetering.hidden = YES;
        [loadingWheelMetering stopAnimating];
        meteringValue.hidden = NO;
        if([device.metering isEqualToNumber:[NSNumber numberWithInt:-1]])
            meteringValue.text = @"- kWh";
        else
            meteringValue.text = [NSString stringWithFormat:@"%.2f kWh",[device.metering floatValue]/100];
	}

}



- (void) didRotate:(NSNotification *)notification{
    UIDeviceOrientation currentRotation = [[UIDevice currentDevice] orientation];
    switch (currentRotation) {
        case UIDeviceOrientationLandscapeLeft:
            [self actionMinusDown];
            if (timerPlus) {
                [timerPlus invalidate];
                timerPlus = nil;
            }
            break;

        case UIDeviceOrientationLandscapeRight:
            [self actionPlusDown];
            if (timerMinus) {
                [timerMinus invalidate];
                timerMinus= nil;
            }
            break;

        default:
            if (timerPlus) {
                [timerPlus invalidate];
                timerPlus = nil;
            }
            if (timerMinus) {
                [timerMinus invalidate];
                timerMinus= nil;
            }
            break;
    }
}

- (void) updateDeviceValues{

    loadingWheelConsumption.hidden = NO;
    loadingWheelMetering.hidden = NO;

    [loadingWheelConsumption startAnimating];
    [loadingWheelMetering startAnimating];

    consumptionValue.hidden = YES;
    meteringValue.hidden = YES;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[DataController instance]updateConsumptionValuesOfDevice:device];
    });

}


#pragma mark -
#pragma mark View functions
- (void) applyBackground {
	if ((device && [device.powerOn boolValue]) || (scene && [scene.sceneNo longLongValue] == [scene.room.lastCalledScene intValue])) {
		self.imageStatus.image = [UIImage imageNamed:@"Anp_Schalter_BG_On.png"];
	}
	else {
		self.imageStatus.image = [UIImage imageNamed:@"Anp_Schalter_BG.png"];
	}
}


#pragma mark -
#pragma mark IB Actions

- (IBAction) actionRefreshValues{
    [self updateDeviceValues];
}

- (IBAction) actionBack {
	//re-enable rotation to energy visualisation
    [DataController instance].rotationEnabled = YES;

    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
		self.view.frame = CGRectMake(0, self.view.superview.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	} completion:^(BOOL finished) {
		[self.view removeFromSuperview];
		[self release];
	}];
}

- (IBAction) actionPlusDown {
	[self increment];

	NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.25 repeats:YES usingBlock:^(NSTimer *timer) {
		[self increment];
	}];
	timerPlus = timer;
}

- (IBAction) actionPlusUp {
	if (timerPlus) {
		[timerPlus invalidate];
		timerPlus = nil;
	}
}

- (IBAction) actionMinusDown {
	[self decrement];

	NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.25 repeats:YES usingBlock:^(NSTimer *timer) {
		[self decrement];
	}];
	timerMinus = timer;
}

- (IBAction) actionMinusUp {
	if (timerMinus) {
		[timerMinus invalidate];
		timerMinus = nil;
	}
}


- (void) increment {
	NSLog(@"increment");
	if (device) {
        consumptionValue.text = @"- W";
        meteringValue.text = @"- kWh";
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_INC withDevice:device withGroup:1];
		});
	}
	else if(scene.room) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_INC withRoom:scene.room withGroup:1];
		});
	}

}



- (void) decrement {
	NSLog(@"decrement");
	if (device) {
        consumptionValue.text = @"- W";
        meteringValue.text = @"- kWh";
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_DEC withDevice:device withGroup:1];
		});
	}
	else if(scene.room) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] callStandardScene:k_SERVICE_SCENE_DEC withRoom:scene.room withGroup:1];
		});
	}
}

- (IBAction) actionPoint {
	if (device) {
        consumptionValue.text = @"- W";
        meteringValue.text = @"- kWh";
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] setDevicePower:device withBool:![device.powerOn boolValue] withGroup:1];
		});
	}

	if (scene) {
		if ([scene.sceneNo longLongValue] != [scene.room.lastCalledScene intValue]) {
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
				[[DataController instance] turnSceneOn:scene];
			});
		} else {
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [[DataController instance] callStandbyScene:scene.room];
            });




		}


	}
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

    [super dealloc];
}

@end
