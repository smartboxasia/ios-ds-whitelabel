//
//  OverviewRoomListVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "OverviewRoomListVC.h"
#import "OverviewRoomListItemVC.h"
#import "DSRoom.h"
#import "ModelLocator.h"
#import "DataController.h"

@implementation OverviewRoomListVC

@synthesize scrollView;


#pragma mark -
#pragma mark tear up methods

- (id) init {
	if (self = [super init]) {
		offset = -999;
	}
	
	return self;
}


- (id) initWithOffset: (int) scrollViewOffset {
	if (self = [super init]) {
		offset = scrollViewOffset;
		
	}
	
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    items =[NSMutableArray new];

    // iphone 5 support.
    sHeight = [UIScreen mainScreen].bounds.size.height;
    sWidth = [UIScreen mainScreen].bounds.size.width;
    scrollView.frame = CGRectMake(0, 99, sWidth, sHeight-193);

    self.screenName = @"Overview Rooms";
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self buildScrollview];
	[[ModelLocator instance] addObserver:self forKeyPath:k_DATA_ROOMLIST_HAS_CHANGED options:0 context:nil];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_ROOMLIST_HAS_CHANGED];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_DATA_ROOMLIST_HAS_CHANGED]) {
		[self buildScrollview];
	}
}

- (void) buildScrollview {
	//GSLog(@"room rows: %@", [DataController instance].rooms);
	for (UIViewController *item in items) {
		[item.view removeFromSuperview];
		[item release];
	}
	[items removeAllObjects];
	
	NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];


	NSArray *rooms = [[NSArray alloc] initWithArray:[DataController instance].rooms];
	//NSLog(@"room 0: %@", ((DSRoom*)[rooms objectAtIndex:0]).name);
	NSArray *sorted = [rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [rooms release];
    [orderDescriptor release];
    [nameDescriptor release];
	
	//GSLog(@"room rows: %@", sorted);
	
	int i;

    //iphone 5 support.
    GSLog(@"OverviewRoomListVC item: %f, %f", sWidth, sHeight);

	for (i=0; i<[sorted count]; i++) {
		OverviewRoomListItemVC *item = [[OverviewRoomListItemVC alloc] initWithRoom:(DSRoom*)[sorted objectAtIndex:i]];
		[items addObject:item];
    
		if (offset == -999) {
            item.view.frame = CGRectMake(item.view.frame.origin.x, (52*i)+sHeight-193, sWidth, item.view.frame.size.height);
			[self.scrollView addSubview:item.view];

			[UIView animateWithDuration:0.5 delay:(sqrt(i)/4)+0.1 options:0 animations:^{
                item.view.frame = CGRectMake(item.view.frame.origin.x, (52*i)+8, sWidth, item.view.frame.size.height);
			} completion:^(BOOL finished) {
				
			}];
		}
		else {
            item.view.frame = CGRectMake(item.view.frame.origin.x, (52*i)+8, sWidth, item.view.frame.size.height);
			[self.scrollView addSubview:item.view];

			//TODO: scroll scrollview to offset
		}
		
	}
	
	if(offset != -999) {
		self.scrollView.contentOffset = CGPointMake(0, offset);
	}
	
	self.scrollView.contentSize = CGSizeMake(sWidth, i*55+3);
}


- (void) clearScrollview {
	int i = 0;
	int max = (int)[self.scrollView.subviews count];
	float delay = 0.0;
	
	for (UIView *view in self.scrollView.subviews) {
		delay = (sqrt(i)/4)+0.1;
		
    // iphone 5 support.
    
    [UIView animateWithDuration:0.5 delay:delay options:0 animations:^{
        view.frame = CGRectMake(view.frame.origin.x, (52*i)+sHeight-193, view.frame.size.width, view.frame.size.height);
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
        [view release];
        if (i >= max-1) {
        }
    }];
		
		i++;
	}
	
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay*1000000000), dispatch_get_main_queue(), ^{
		[self buildScrollview];
	});
}

- (void) storeInML {
	[[ModelLocator instance] setValue:self forKey:k_VIEW_OVERVIEW_ROOMLIST];
}


#pragma mark -
#pragma mark tear down methods

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) prepareDealloc {
	for (OverviewRoomListItemVC *item in items) {
		[item.view removeFromSuperview];
		[item prepareDealloc];
		[item release];
	}
	
	[items removeAllObjects];
	[items release];
}

- (void)dealloc {
	
    [super dealloc];
}


@end
