//
//  SettingsScenesListByRoomVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsConfigSceneVC.h"
#import "SettingsDevicesConfigItemVC.h"
#import "SettingsGreyConfigItemVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "CustomLabel.h"
#import "GAIHelper.h"

@implementation SettingsConfigSceneVC

@synthesize scrollview;
@synthesize background;

- (void)viewDidLoad {
    [super viewDidLoad];
	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_CONFIGURE_SCENE];
    
    [callButton setTitle:NSLocalizedString(@"key_call", nil) forState:UIControlStateNormal];
    [saveButton setTitle:NSLocalizedString(@"key_save", nil) forState:UIControlStateNormal];
    
    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }

	items = [NSMutableArray new];
    
    if(scene)
        titleLabel.text = [NSString stringWithFormat:@"3. %@", scene.name];
	
	[self buildScrollView];
    
    cancelUpdating = NO;

    //then start the device updating
    [self updateDeviceValues];
    
    self.screenName = @"Settings Activities Edit Config";
}

- (id) initWithScene: (DSScene*) theScene{
	if (self = [super init]) {
		scene= theScene;
    }
	return self;
}

- (void)buildScrollView {
	for (UIViewController *item in items) {
		[item.view removeFromSuperview];
		[item release];
	}
	[items removeAllObjects];
	
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sorted = [scene.room.devices sortedArrayUsingDescriptors:[NSArray arrayWithObjects: nameDescriptor, nil]];
    [nameDescriptor release];
	
	NSInteger yOffset = 0;
    int i = 0;
	
	for (DSDevice* device in sorted) {
		//skip if no output
        if([device.outputMode isEqualToNumber:[NSNumber numberWithInt:0]]){
            continue;
        }
        //we only show devices that have same group as the scene we're modifying
        if([device.group isEqualToString:[NSString stringWithFormat:@"%@",scene.group]]){
            //grey devices have their own controller
            if([device.group isEqualToString:@"2"]){
                SettingsGreyConfigItemVC *item = [[SettingsGreyConfigItemVC alloc] initWithDevice:device andDarkBg:i%2==1];
                item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
                [self.scrollview addSubview:item.view];
                
                yOffset += item.view.frame.size.height;
                [items addObject:item];
                i++;
            }
            else{
                SettingsDevicesConfigItemVC *item = [[SettingsDevicesConfigItemVC alloc] initWithDevice:device andDarkBg:i%2==1];
                item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
                [self.scrollview addSubview:item.view];
                
                yOffset += item.view.frame.size.height;
                [items addObject:item];
                i++;
            }
        }
	}
    
	self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, yOffset);
}

- (void) updateDeviceValues{
    //start by launching the loading animation
    
    if([scene.group isEqualToNumber:[NSNumber numberWithInt:2]]){
        for (SettingsGreyConfigItemVC *item in items){
            [item toggleLoadingAnimation:YES];
        }
    }
    else{
        for (SettingsDevicesConfigItemVC *item in items){
            [item toggleLoadingAnimation:YES];
        }
    }
    
    //check if a previous device update is running
    if(isRunningDeviceValueUpdate){
        cancelUpdating = YES;
    }
        
    
    //then async update all device values
    isRunningDeviceValueUpdate=YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        NSArray *sorted = [scene.room.devices sortedArrayUsingDescriptors:[NSArray arrayWithObjects: nameDescriptor, nil]];
        [nameDescriptor release];
        
        //only show devices with an output
        for (DSDevice* device in sorted) {
            //check if another one is running
            if([device.outputMode isEqualToNumber:[NSNumber numberWithInt:0]]){
                continue;
            }
            if([device.group isEqualToString:[NSString stringWithFormat:@"%@",scene.group]]){
                //we don't continue before we got a result
                [[DataController instance]updateValueOfDevice:device];
            }
            //check if we need to cancel
            if(cancelUpdating){
                isRunningDeviceValueUpdate = NO;
                cancelUpdating = NO;
                NSLog(@"cancelled update");
                break;
            }
        }
        isRunningDeviceValueUpdate = NO;
    });
}

#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	cancelUpdating = YES;
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
	
	[[ModelLocator instance] setValue:nil forKey:k_VIEW_SETTINGS_CONFIGURE_SCENE];
}

- (IBAction) actionCallScene{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [GAIHelper sendSceneCall:scene];
        
        //call scene
        [[DataController instance] turnSceneOn:scene];
        //then update device values
        [self updateDeviceValues];
    });
}

- (IBAction) actionSaveScene{
    
    [GAIHelper sendUIActionWithCategory:@"scene_saved" andLabel:nil];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[DataController instance] saveScene:scene];
    });
    [self actionBack];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
	
	for (UIViewController *child in items) {
		[child.view removeFromSuperview];
		[child release];
	}
	[items removeAllObjects];
	
    [callButton release];
    [saveButton release];
    [super dealloc];
}


@end
