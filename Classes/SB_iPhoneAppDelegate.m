//
//  SB_iPhoneAppDelegate.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SB_iPhoneAppDelegate.h"
#import "SB_iPhoneViewController.h"
#import "FacilityController.h"
#import "StartupController.h"
#import "ModelLocator.h"
#import "OverviewRoomListVC.h"
#import "DataController.h"
#import "cocos2d.h"
#import "StartupServerSelectionVC.h"
#import "MainVC.h"
#import "EV_Locator.h"
#import "GAIHelper.h"

@implementation SB_iPhoneAppDelegate

@synthesize window;
@synthesize viewController;


#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
	[[ModelLocator instance] loadDefaults];
    
    //create UUID for installation
    NSString *UUID = [[NSUserDefaults standardUserDefaults] objectForKey:kApplicationUUIDKey];
    if (!UUID) {
        CFUUIDRef uuid = CFUUIDCreate(NULL);
        UUID = ( NSString *)CFUUIDCreateString(NULL, uuid);
        CFRelease(uuid);
        
        [[NSUserDefaults standardUserDefaults] setObject:UUID forKey:kApplicationUUIDKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //start testflight
//    [TestFlight takeOff:@"6a7100b1-0b5f-4222-b076-3c1a13ac3996"];

	// Cocos2d Start
	// Try to use CADisplayLink director
	// if it fails (SDK < 3.1) use the default director
	
	if( ! [CCDirector setDirectorType:kCCDirectorTypeDisplayLink] )
		[CCDirector setDirectorType:kCCDirectorTypeDefault];
	
	CCDirector *director = [CCDirector sharedDirector];
	
	[director setAnimationInterval:1.0/60];
	[director setDisplayFPS:NO];
		
	
	
//#endif
	
	
	
	//
	//
	// VERY IMPORTANT:
	// If the rotation is going to be controlled by a UIViewController
	// then the device orientation should be "Portrait".
	//
	// IMPORTANT:
	// By default, this template only supports Landscape orientations.
	// Edit the RootViewController.m file to edit the supported orientations.
	//
	/*
	 #if GAME_AUTOROTATION == kGameAutorotationUIViewController
	 [director setDeviceOrientation:kCCDeviceOrientationPortrait];
	 
	 #else
	 [director setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
	 
	 #endif
	 */
	
	
	
	
	
	
	
	
	// Removes the startup flicker
	//[self removeStartupFlicker];
	
	
	// Cocos2d Ende
	
    [GAIHelper configure:[self isTestVersion]];
   
    self.window.rootViewController = viewController;
	[self.window makeKeyAndVisible];
    
	return YES;
}

#if STORE

#else
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//	UIScreen *externalScreen = [[[UIScreen screens] objectAtIndex:1] retain];
//	UIScreenMode *desiredMode = [screenModes objectAtIndex:buttonIndex];
//	externalScreen.currentMode = desiredMode;
//	
//
//	[[UIApplication sharedApplication] setupScreenMirroringWithFramesPerSecond:15 andScreen:externalScreen];
//	
//
//	
//}
#endif

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    //hide the server selection if it's showing
//    StartupServerSelectionVC *selectionVC = [[ModelLocator instance] valueForKey:k_VIEW_SERVERLIST];
//    if(selectionVC){
//        if (!selectionVC.view.hidden || !selectionVC.view.alpha == 0) {
//            [(MainVC*)[[ModelLocator instance] valueForKey:k_VIEW_MAIN] showServerList];
//        }
//    }
    
    //hide the ball game
    EV_Locator* ev_Locator = [EV_Locator instance];
	[ev_Locator.rootViewController showPortraitModeWithoutAnimation];
	
    [[StartupController instance] stopDssSearch];
	
	//[[FacilityController instance] endConsumptionPolling];
	[[DataController instance] endMeterConsumptionPolling];
	[[ModelLocator instance] setValue:@"0" forKey:k_DSS_AVAILABLE];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [viewController resetMemoryWarning];
	 
	[[NSUserDefaults standardUserDefaults] synchronize];
	
	[[ModelLocator instance] loadDefaults];
	
	[[DataController instance] markDsServersAsDisconnected];
	
	/*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
//	[[FacilityController instance] startUpdatingLocation];
	
	if ([[ModelLocator instance] valueForKey:k_DSS_IP]) {
		[[StartupController instance] prepareStructure];
	}
	
	
	[[StartupController instance] startDssSearch];
	
	[[DataController instance] startMeterConsumptionPolling];
	
	/*if ([[ModelLocator instance] valueForKey:k_VIEW_MAIN] && [[ModelLocator instance] valueForKey:k_DSS_IP]) {
		GSLog(@"loading structure from server");
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
			[[DataController instance] initServerStructure];
		});
	}*/
	
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return YES;
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}



- (void) removeStartupFlicker
{
	//
	// THIS CODE REMOVES THE STARTUP FLICKER
	//
	// Uncomment the following code if you Application only supports landscape mode
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
	
	//	CC_ENABLE_DEFAULT_GL_STATES();
	//	CCDirector *director = [CCDirector sharedDirector];
	//	CGSize size = [director winSize];
	//	CCSprite *sprite = [CCSprite spriteWithFile:@"Default.png"];
	//	sprite.position = ccp(size.width/2, size.height/2);
	//	sprite.rotation = -90;
	//	[sprite visit];
	//	[[director openGLView] swapBuffers];
	//	CC_ENABLE_DEFAULT_GL_STATES();
	
#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController	
}

-(BOOL)isTestVersion {
#if TARGET_IPHONE_SIMULATOR
    return YES;
#endif
    
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSString *receiptURLString = [receiptURL path];
    BOOL isRunningTestFlightBeta = ([receiptURLString rangeOfString:@"sandboxReceipt"].location != NSNotFound);
    
    return isRunningTestFlightBeta;
}


@end
