//
//  untitled.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

//#import <CoreLocation/CoreLocation.h>
//#import <MapKit/MapKit.h>

#import "FacilityController.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "ASIHTTPRequest.h"

@implementation FacilityController

//@synthesize locationManager, currentLocationInfo;

+ (FacilityController*) instance {
	static FacilityController *instance;
	
	@synchronized(self) {
		if(!instance) {
			instance = [FacilityController new];
		}
	}
	
	return instance;
}

- (id) init {
	if(self = [super init]) {
//		[self startUpdatingLocation];
//		self.currentLocationInfo = nil;
		dictRoomConsumptionTimer = [NSMutableDictionary new];
	}
	
	return self;
}

//#pragma mark -
//#pragma mark Core Location methods and delegates for coordinate
//
//- (void) startUpdatingLocation {
//	if ([CLLocationManager locationServicesEnabled]) {
//		self.locationManager = [[[CLLocationManager alloc] init] autorelease];
//		self.locationManager.delegate = self;
//		[self.locationManager startUpdatingLocation];
//	}
//}
//
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
////	GSLog(@"Location: %@", [newLocation description]);
//	
//	[self.locationManager stopUpdatingLocation];
//	
//	MKReverseGeocoder *geocoder = [[MKReverseGeocoder alloc] initWithCoordinate:newLocation.coordinate];
//	geocoder.delegate = self;
//	[geocoder start];	
//}
//
//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
//	GSLog(@"Error: %@", [error description]);
//	[self.locationManager stopUpdatingLocation];
//}
//
//
//#pragma mark -
//#pragma mark Reverse Geocoder delegates for weather location
//
//- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark {
//	//GSLog(@"city: %@,%@", placemark.postalCode, placemark.countryCode);
//		
//	self.currentLocationInfo = [NSString stringWithFormat:@"%@,%@,%@", placemark.postalCode, placemark.locality, placemark.countryCode];
////	GSLog(@"placemark: %@", placemark);
//	
//	location = placemark.coordinate;
//	
//	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//		[self loadWeatherInformation];
//	});
//	
//}								 
//
//- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error {
//	//GSLog(@"error in %@ (Line: %@)", __FUNCTION__, __LINE__ );
//}
//


- (void) loadWeatherInformation {
    
    //we don't want to load the weather if the location is not set (nil,nil)
    if([DataController instance].dssLatitude && [DataController instance].dssLongitude){
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%@&lon=%@&cnt=1&APPID=%@", [DataController instance].dssLatitude, [DataController instance].dssLongitude, @"139a3c0110c5e56884523d266262946f"]];
        
            __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];

        [request setCompletionBlock:^{
            
             SBJSON *parser = [[SBJSON alloc] init];
            
            // Use when fetching binary data
            NSData *response = [request responseData];
            NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
            NSDictionary *resultDict = [parser objectWithString:json_string error:nil];
            
            if(resultDict && [resultDict isKindOfClass:[NSDictionary class]]){
                int conditionCode;
                float temp;
                NSString *cityName = @"";
                
                
                @try {
                    conditionCode = [[[[resultDict objectForKey:@"weather"]
                                       objectAtIndex:0]
                                      objectForKey:@"id"] intValue];
                    
                    
                    temp = [[[resultDict objectForKey:@"main"]
                             objectForKey:@"temp"] floatValue];
                    
                    cityName = [resultDict objectForKey:@"name"];
                    
                }
                @catch (NSException *exception) {
                    NSLog(@"getting weather failed: %@", [exception description]);
                }
                
                
                
                
                // temp is 0 kelvin, we didn't get a result.
                if( temp != 0 ){
                    //adjust for kelvin degrees
                    
                    temp = 0 + temp - 273.15;
                    [[ModelLocator instance] setValue:[NSNumber numberWithInt:(int)floorf(temp)] forKey:k_STANDARD_OUTSIDE_TEMP];
                    
                    [[ModelLocator instance] setValue:cityName forKey:k_STANDARD_CITY];
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setValue:[[ModelLocator instance] valueForKey:k_STANDARD_OUTSIDE_TEMP] forKey:k_STANDARD_OUTSIDE_TEMP];
                    [defaults synchronize];
                }
                
                if (conditionCode) {
                    [self displayWeatherCondition:conditionCode];
                }
            }
        
            [json_string release];
            [parser release];

        }];
        
        [request startAsynchronous];
    }
}

- (void) displayWeatherCondition: (int) condition {
    
    //see descriptions here: http://openweathermap.org/wiki/API/Weather_Condition_Codes
    
    NSString *conditionName = @"";
    
    //thunderstorm
	if(condition >= 200 && condition < 300){
        conditionName = @"Thunderstorm";
    }
    //drizzle
    else if(condition >= 300 && condition < 500){
        conditionName = @"Rain";
    }
    //rain 1
    else if(condition >= 500 && condition < 505){
        conditionName = @"Light Rain";
    }
    //rain and snow
    else if(condition == 511){
        conditionName = @"Rain and Snow";
    }
    //rain 2
    else if(condition >= 512 && condition < 600){
        conditionName = @"Rain";
    }
    //snow
    else if(condition >= 600 && condition < 700){
        conditionName = @"IceSnow";
    }
    //atmosfere
    else if(condition >= 700 && condition < 800){
        conditionName = @"Fog";
    }
    //clear
    else if(condition == 800){
        conditionName =@"Clear";
    }
    //few clouds
    else if(condition == 801){
        conditionName =@"Partly Cloudy";
    }
    //scattered clouds
    else if(condition == 802){
        conditionName =@"Partly Sunny";
    }
    //clouds
    else if(condition >= 803 && condition < 900){
        conditionName = @"Overcast";
    }
    //extreme wind
    else if((condition >= 900 && condition < 903) || condition == 905 ){
        conditionName = @"Chance of Storm";
    }
    //extreme cold
    else if(condition == 903 || condition == 905 ){
        conditionName = @"Icy";
    }
    //extreme hot
    else if(condition == 904 ){
        conditionName = @"Clear";
    }
    
    //didn't find condition, set partly cloudy
    else{
        conditionName = @"Partly Cloudy";
    }
    
    //set the condition icon
    [[ModelLocator instance] setValue:[NSString stringWithFormat:@"%@.png", conditionName] forKey:k_STANDARD_CONDITION_TEMP];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[[ModelLocator instance] valueForKey:k_STANDARD_CONDITION_TEMP] forKey:k_STANDARD_CONDITION_TEMP];
    [defaults synchronize];
    
}


#pragma mark -
#pragma mark Apartement consumption polling

- (void) startConsumptionPolling {
	consumptionPollingRunning = YES;
	if (!consumptionBlockRunning) {
		//[self timerConsumptionSelector:nil];
	}
	
	
	/*[self endConsumptionPolling];
	timerConsumption = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector (timerConsumptionSelector:) userInfo:nil repeats:YES];
	[self timerConsumptionSelector:timerConsumption];
	*/
}

- (void) endConsumptionPolling {
	consumptionPollingRunning = NO;
	/*if (timerConsumption && [timerConsumption isKindOfClass:[NSTimer class]] && [timerConsumption isValid]) {
		[timerConsumption invalidate];
		timerConsumption = nil;
	}*/
}

- (void) timerConsumptionSelector: (NSTimer *) theTimer {
	if (!consumptionPollingRunning) {
		return;
	}
	consumptionBlockRunning = YES;
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3000000000), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		NSNumber *con = [[DataController instance] getTotalConsumption];
		if (con) {
//			GSLog(@"consumption: %i W", [con intValue]);
		}
		else {
//			GSLog(@"consumption: --");
		}
		
		
		dispatch_async(dispatch_get_main_queue(), ^{
			consumptionBlockRunning = NO;
			[[ModelLocator instance] setValue:con forKey:k_INFO_TOTAL_CONSUMPTION];
			[self timerConsumptionSelector:nil];
		});
	});
	
	/*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{	
		
	});*/
}

#pragma mark -
#pragma mark Room consumption polling

- (void) startRoomConsumptionPolling: (DSRoom*) room {
	if ([dictRoomConsumptionTimer objectForKey:room.identifier]) {
		[self endRoomConsumptionPolling:room];
	}
	dispatch_async(dispatch_get_main_queue(), ^{
		NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector (timerRoomConsumptionSelector:) userInfo:room repeats:YES];
		[dictRoomConsumptionTimer setObject:timer forKey:room.identifier];
	});
}

- (void) endRoomConsumptionPolling: (DSRoom*) room {
	NSTimer *timer = [dictRoomConsumptionTimer objectForKey:room.identifier];
	if (timer && [timer isKindOfClass:[NSTimer class]] && [timer isValid]) {
		[timer invalidate];
		[dictRoomConsumptionTimer removeObjectForKey:room.identifier];
	}
}

- (void) timerRoomConsumptionSelector: (NSTimer *) theTimer {
	id userInfo = [theTimer userInfo];
	
	if (userInfo && [userInfo isKindOfClass:[DSRoom class]]) {
		DSRoom *room = (DSRoom*)userInfo;
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{	
			[self loadAndApplyRoomConsumption:room];
		});
		
	}
}

- (void) loadAndApplyRoomConsumption: (DSRoom*) room {
	NSNumber *con = [[DataController instance] getRoomConsumption:room];
	
	if (con) {
		dispatch_async(dispatch_get_main_queue(), ^{
//			GSLog(@"room: %@: %iW", [NSString stringWithFormat:@"%@%@", k_INFO_PREFIX_ROOM_CONSUMPTION, room.identifier], [con intValue]);
			[[ModelLocator instance] setValue:con forKey:[NSString stringWithFormat:@"%@%@", k_INFO_PREFIX_ROOM_CONSUMPTION, room.identifier]];
		});
	}
}


@end
