//
//  SettingsRoomsListItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsRoomsListItemVC.h"
#import "SettingsDevicesListVC.h"
#import "DSRoom.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "SettingsRoomsRoomDetailVC.h"
#import "DataController.h"
#import "SettingsRoomsListVC.h"
#import "SettingsScenesListVC.h"

@implementation SettingsRoomsListItemVC

@synthesize imageBackground, labelName, buttonStepInto, imageDragMe, buttonBig;

- (id) initWithRoom: (DSRoom*) theRoom andOffset:(float) _offset andMaximum:(NSInteger) _max gointToScenes:(BOOL)toScenes isLookingForFavorite:(BOOL)favorite gointToDevices:(BOOL)toDevices{
	if(self = [super init]) {
		room = theRoom;
		editmodeOn = NO;
		offset = _offset;
		maximum = _max;
        goingToScenes = toScenes;
        goingToDevices = toDevices;
        isLookingForFavorite = favorite;
	}
	
	return self;
}

//only used for non-rooms
- (id) initWithName: (NSString*) theNonRoomName isLookingForFavorite:(BOOL)favorite{
	if(self = [super init]) {
		room = nil;
		editmodeOn = NO;
        goingToScenes = YES;
        nonRoomName = theNonRoomName;
        isLookingForFavorite = favorite;
	}
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(room) {
		magneticY = (int)floor(offset/self.view.frame.size.height);
		self.labelName.text = room.name;
		self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_hell.png"];
		[room addObserver:self forKeyPath:@"name" options:0 context:nil];
	}
    else{
        self.labelName.text = nonRoomName;
        self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_hell.png"];
    }
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if(room)
        [room removeObserver:self forKeyPath:@"name"];
}


- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"name"]) {
		self.labelName.text = room.name;
	}
}

- (IBAction) actionShowRoomDetail {
	if (!editmodeOn) {
		if(goingToScenes){
            if(room){
                SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
                [settings showSubGroup:[[SettingsScenesListVC alloc] initWithRoom:room isLookingForFavorite:isLookingForFavorite withIndex:0]];
            }
            else{
                //go to overview of non-room scenes
                SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
                [settings showSubGroup:[[SettingsScenesListVC alloc] initWithName:nonRoomName isLookingForFavorite:isLookingForFavorite]];
            }
            
        }
        else if(goingToDevices){
            if(room){
                SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
                [settings showSubGroup:[[SettingsDevicesListVC alloc] initWithRoom:room]];
            }
        }
        else{
            SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
            [settings showSubGroup:[[SettingsRoomsRoomDetailVC alloc] initWithRoom:room]];
        }
	}
}


#pragma mark -
#pragma mark functions for editing

- (void) toggleEditmode {
	if (!editmodeOn) {
		editmodeOn = YES;
		self.imageDragMe.hidden = NO;
		self.buttonBig.hidden = YES;
		
		[UIView animateWithDuration:0.2 animations:^{
			//self.imageDragMe.alpha = 1;
			self.imageDragMe.frame = CGRectMake(240, self.imageDragMe.frame.origin.y, self.imageDragMe.frame.size.width, self.imageDragMe.frame.size.height);
			self.buttonStepInto.frame = CGRectMake(self.view.frame.size.width, self.buttonStepInto.frame.origin.y, self.buttonStepInto.frame.size.width, self.buttonStepInto.frame.size.height);
		}];
	}
	else {
		editmodeOn = NO;
		self.buttonBig.hidden = NO;
		[[DataController instance] saveContext];
		[UIView animateWithDuration:0.2 animations:^{
			self.imageDragMe.frame = CGRectMake(self.view.frame.size.width, self.imageDragMe.frame.origin.y, self.imageDragMe.frame.size.width, self.imageDragMe.frame.size.height);
			self.buttonStepInto.frame = CGRectMake(239, self.buttonStepInto.frame.origin.y, self.buttonStepInto.frame.size.width, self.buttonStepInto.frame.size.height);
		} completion:^(BOOL finished) {
			self.imageDragMe.hidden = YES;
		}];
		
		
	}
	
}

- (void) gotoPosition: (NSInteger) _pos {
	magneticY = (int)_pos;
	room.sortOrder = [NSNumber numberWithInteger:magneticY];
	
	[UIView beginAnimations:@"" context:nil];
	[UIView setAnimationDuration:0.2];
	
	self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	
	[UIView commitAnimations];
}

- (void) incrementMagneticPoint {
	magneticY ++;
	room.sortOrder = [NSNumber numberWithInteger:magneticY];
	
	[UIView beginAnimations:@"" context:nil];
	[UIView setAnimationDuration:0.2];
	
	self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	
	[UIView commitAnimations];
}

- (void) decrementMagneticPoint {
	magneticY --;
	GSLog(@"sort %@ old: %i", room.name, [room.sortOrder intValue]);
	room.sortOrder = [NSNumber numberWithInteger:magneticY];
	GSLog(@"sort %@ new: %i", room.name, [room.sortOrder intValue]);
	
	[UIView beginAnimations:@"" context:nil];
	[UIView setAnimationDuration:0.2];
	
	self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	
	[UIView commitAnimations];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	GSLog(@"down");
	if (editmodeOn) {
		CGPoint beginCenter = self.view.frame.origin;
		
		UITouch *touch = [touches anyObject];
		CGPoint touchPoint = [touch locationInView:self.view.superview];
		
		deltaY = touchPoint.y - beginCenter.y;
		[self.view.superview bringSubviewToFront:self.view];
	}
	
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	if (editmodeOn) {
		UITouch *touch = [touches anyObject];
		CGPoint touchPoint = [touch locationInView:self.view.superview];
		
		// Set the correct center when touched 
		
		
		self.view.frame = CGRectMake(self.view.frame.origin.x, touchPoint.y-deltaY, self.view.frame.size.width, self.view.frame.size.height);
		
		if (self.view.frame.origin.y - magneticY < self.view.frame.size.height || self.view.frame.origin.y - magneticY > self.view.frame.size.height) {
//			GSLog(@"old magn: %i", magneticY);
			int old = magneticY;
			magneticY = (int)floor(self.view.frame.origin.y/self.view.frame.size.height);
			
			if (magneticY < 0) {
				magneticY = 0;
			}
			
			if (magneticY >= maximum) {
				magneticY = (int)maximum-1;
			}
			
			if (old-magneticY == 1 || old-magneticY == -1) {
				room.sortOrder = [NSNumber numberWithInteger:magneticY];
				
				[(SettingsRoomsListVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_ROOMS_LIST] magneticPointHasChanged:old toIndex:magneticY];
			}
//			GSLog(@"new magn: %i", magneticY);
		}
	}
	
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	if (editmodeOn) {
		[UIView animateWithDuration:0.2 animations:^{
			self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
		}];
		[(SettingsRoomsListVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_ROOMS_LIST] arrangeItems];
	}
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	if (editmodeOn) {
		[UIView animateWithDuration:0.2 animations:^{
			self.view.frame = CGRectMake(self.view.frame.origin.x, magneticY*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
		}];
		[(SettingsRoomsListVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS_ROOMS_LIST] arrangeItems];
	}
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
    [super dealloc];
}


@end
