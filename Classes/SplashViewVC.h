//
//  SplashViewVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"


@interface SplashViewVC : GAITrackedViewController <UIAlertViewDelegate> {
	UILabel *labelStatus;
}

@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UILabel *labelStatus;
@property (nonatomic, retain) IBOutlet UIButton *dsButton;

- (void) displayCredentialsAlertView:(BOOL)wrongPass;

- (IBAction) dsButtonClick: (id) sender;

@end
