//
//  FavoriteBarVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "FavoriteBarVC.h"
#import "ModelLocator.h"
#import "DSScene.h"
#import "FavoriteBarButtonVC.h"
#import "DataController.h"

@implementation FavoriteBarVC

@synthesize scrollview;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	[[ModelLocator instance] setValue:self forKey:k_VIEW_FAVORITEBAR];	
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self showContent];
	[[ModelLocator instance] addObserver:self forKeyPath:k_DATA_SCENELIST_HAS_CHANGED options:0 context:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DATA_SCENELIST_HAS_CHANGED];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

#pragma mark -
#pragma mark Observer responder

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_DATA_SCENELIST_HAS_CHANGED]) {
		if ([[ModelLocator instance] valueForKey:k_DATA_SCENELIST_HAS_CHANGED] && [[[ModelLocator instance] valueForKey:k_DATA_SCENELIST_HAS_CHANGED] boolValue]) {
			[self showContent];
			[[ModelLocator instance] setValue:[NSNumber numberWithBool:NO] forKey:k_DATA_SCENELIST_HAS_CHANGED];
		}
		
	}
}

- (void) showContent {
	NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSArray *scenes = [[DataController instance].scenes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
    [sorter release];
	
	if (scenes) {
		// remove all children
		[[scrollview subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
		
		int i=0;
		if ([scenes count] > 0) {
			for (DSScene *scene in scenes) {
				if (![scene.favourit boolValue]) {
					continue;
				}
				FavoriteBarButtonVC *vc = [[FavoriteBarButtonVC alloc] initWithScene:scene];
				
				vc.view.frame = CGRectMake((i*80)+4, vc.view.frame.origin.y, vc.view.frame.size.width, vc.view.frame.size.height);
				[scrollview addSubview:vc.view];
				i++;
			}
		}
		
		// add plus button
		FavoriteBarButtonVC *vc = [[FavoriteBarButtonVC alloc] initWithScene:nil];
		
		vc.view.frame = CGRectMake(i*80+4, vc.view.frame.origin.y, vc.view.frame.size.width, vc.view.frame.size.height);
		[scrollview addSubview:vc.view];

    // self.scrollview.contentSize = CGSizeMake( ceil((double)((i+1)*80)/320)*320, 72);
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    self.scrollview.contentSize = CGSizeMake( ceil((double)((i+1)*80)/screenWidth)*screenWidth, 72);

		//self.scrollview.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	}
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[[ModelLocator instance] setValue:nil forKey:k_VIEW_FAVORITEBAR];
	
    [super dealloc];
}


@end
