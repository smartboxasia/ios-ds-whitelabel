//
//  SB_iPhoneViewController.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SB_iPhoneViewController.h"
#import "SplashViewVC.h"
#import "MainVC.h"

#import "EV_Locator.h"
#import "EV_RootVC.h"

#import "ModelLocator.h"
#import "DataController.h"
//#import "FacilityController.h"

#import "UIDevice-Capabilities.h"
#import "UIDevice-Hardware.h"


@implementation SB_iPhoneViewController



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    //enable tilt notifications and start listening
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];

	MainVC *vc = [MainVC new];
	[self.view addSubview:vc.view];
	mainView = vc;

	[ModelLocator instance].appController = self;
	ev_rootVC = nil;

    //for performance reasons we don't allow the ball game right after starting the app, so we delay the enabling.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4000000000U), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		evAllowed = YES;
	});

	receivedMemoryWarning = NO;
}


- (void) initEV {
	if (!evAllowed) {
		return;
	}

	EV_Locator* ev_Locator = [EV_Locator instance];
	ev_Locator.rootViewController = self;

	ev_rootVC = [EV_RootVC new];
	[self.view addSubview:ev_rootVC.view];
	//ev_rootVC.view.frame = CGRectMake(0, 0, 480, 320);
	ev_rootVC.view.transform = CGAffineTransformMakeRotation(M_PI/2);

//    //iphone 5 support.
//    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        ev_rootVC.view.frame = CGRectMake(-248, 0, 568, 568);
//    }
//    else {
//        ev_rootVC.view.frame = CGRectMake(-160, 0, 480, 480);
//    }
    
    // fix stupid hardcode height and width
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    ev_rootVC.view.frame = CGRectMake(320-screenHeight, 0, screenHeight, screenHeight);

	ev_rootVC.view.hidden = YES;
}


//new rotation logic, due to shouldAutorotateToInterfaceOrientation being deprecated
- (void) didRotate:(NSNotification *)notification{
    if (!evAllowed) {
		return;
	}

	if (!ev_rootVC || [[ModelLocator instance] valueForKey:k_VIEW_SPLASH]) {
		return;
	}

	if (receivedMemoryWarning) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"key_Warning",nil) message:NSLocalizedString(@"key_memory_warning", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"key_OK",nil) otherButtonTitles:nil];
		[alert show];
        [alert release];
		return;
	}

    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];

    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown){
		// zeige PortraitView
		//ev_rootVC.view.hidden = YES;
		//portraitView.view.hidden = NO;
	}

	if ((orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) && [DataController instance].rotationEnabled){
		//[[DataController instance] startMeterConsumptionPolling];

		// zeige EVView
		if (ev_rootVC.view.alpha < 0.9 || ev_rootVC.view.hidden == YES){
//            [TestFlight passCheckpoint:@"Opened ball game"];
			ev_rootVC.view.alpha = 0;
			ev_rootVC.view.hidden = NO;

			[UIView beginAnimations: nil context: self];
			[UIView setAnimationDuration: 0.5];
			ev_rootVC.view.alpha = 1;
			mainView.view.alpha = 0;
			[UIView setAnimationDelegate: self];
			[UIView setAnimationDidStopSelector: @selector(showLandscapeModeDone: finished: context:)];
			[UIView commitAnimations];
			[[CCDirector sharedDirector] resume];

			[ev_rootVC buildEV];

			for (DSMeter *meter in [DataController instance].meters) {
				meter.powerConsumption = [NSNumber numberWithInteger:[meter.powerConsumption integerValue]];
			}


		}
	}

	if(ev_rootVC!=nil){
		// Update Leisten Position in EV
		if (orientation == UIInterfaceOrientationLandscapeLeft){
			[ev_rootVC updateRotation:@"left"];
		}

		if (orientation == UIInterfaceOrientationLandscapeRight){
			[ev_rootVC updateRotation:@"right"];
		}

	}
}


- (void) showLandscapeModeDone: (NSString *) animationID finished: (BOOL) finished context: (void *) myTarget {
	mainView.view.hidden = YES;

}


-(void) showPortraitMode {
	mainView.view.alpha = 0;
	mainView.view.hidden = NO;

	//[[DataController instance] endMeterConsumptionPolling];

	[UIView beginAnimations: nil context: self];
	[UIView setAnimationDuration: 0.5];
	ev_rootVC.view.alpha = 0;
	mainView.view.alpha = 1;
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(showPortraitModeDone: finished: context:)];
	[UIView commitAnimations];
}


- (void) showPortraitModeDone: (NSString *) animationID finished: (BOOL) finished context: (void *) myTarget {


	ev_rootVC.view.hidden = YES;
	[[CCDirector sharedDirector] pause];


}

- (void) showPortraitModeWithoutAnimation{
	ev_rootVC.view.alpha = 0;
	mainView.view.alpha = 1;
	mainView.view.hidden = NO;
    ev_rootVC.view.hidden = YES;
	[[CCDirector sharedDirector] pause];
}

- (void) resetMemoryWarning {
	receivedMemoryWarning = NO;
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

	receivedMemoryWarning = YES;
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [[UIDevice currentDevice]endGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];

    [super dealloc];
}

@end
