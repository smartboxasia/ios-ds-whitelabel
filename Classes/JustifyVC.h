//
//  JustifyVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class DSRoom;
@class DSDevice;
@class DSScene;


@interface JustifyVC : GAITrackedViewController {
	UIImageView *imageStatus;
	UILabel *labelDevicename;
	DSDevice *device;
	DSRoom *room;
	DSScene *scene;
	
	NSTimer *timerPlus;
	NSTimer *timerMinus;
    
    IBOutlet UIActivityIndicatorView *loadingWheelConsumption;
    IBOutlet UIActivityIndicatorView *loadingWheelMetering;
    IBOutlet UILabel *consumptionValue;
    IBOutlet UILabel *meteringValue;
    IBOutlet UIButton *refreshButton;
    IBOutlet UIButton *justifyButton1;
    IBOutlet UIButton *justifyButton2;
    IBOutlet UIButton *justifyButton3;
    IBOutlet UIImageView *imageButton;

}

@property (nonatomic, retain) IBOutlet UIImageView *imageStatus;
@property (nonatomic, retain) IBOutlet UILabel *labelDevicename;


- (id) initWithDevice: (DSDevice*) _device;
- (id) initWithRoom: (DSRoom*) _room;
- (id) initWithScene: (DSScene*) _scene;

- (void) applyBackground;
- (void) didRotate: (NSNotification *) notification;

- (void) updateDeviceValues;

- (IBAction) actionBack;
- (IBAction) actionPoint;

- (IBAction) actionPlusDown;
- (IBAction) actionPlusUp;
- (IBAction) actionMinusDown;
- (IBAction) actionMinusUp;

- (IBAction) actionRefreshValues;

- (void) increment;
- (void) decrement;

@end
