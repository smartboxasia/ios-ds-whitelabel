//
//  HelloWorldScene.mm
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//


#import "EVScene.h"
#import "EV_Locator.h"
#include <stdlib.h>
#import "ModelLocator.h"
#import "DataController.h"

#define PTM_RATIO 32.0

@implementation EVScene

+ (id)scene {
	
    CCScene *scene = [CCScene node];
	EVScene *layer = [EVScene node];
    [scene addChild:layer];
    return scene;
	
}

- (id)init {
	
    if ((self=[super init])) {
		EV_Locator* ev_Locator = [EV_Locator instance];
		ev_Locator.evScene = self;
		
		raumNodeArray = [NSMutableArray new];
		
		CGSize winSize = [CCDirector sharedDirector].winSize;
		self.isAccelerometerEnabled = YES;
		self.isTouchEnabled = YES;
		
		/*
		 // Create sprite and add it to the layer
		 _ball = [CCSprite spriteWithFile:@"kreis.png" rect:CGRectMake(0, 0, 60, 60)];
		 _ball.position = ccp(100, 100);
		 [self addChild:_ball];
		 
		 _ball2 = [CCSprite spriteWithFile:@"kreis2.png" rect:CGRectMake(0, 0, 120, 120)];
		 _ball2.position = ccp(100, 100);
		 [self addChild:_ball2];
		 */
		
		
		// Create a world
		b2Vec2 gravity = b2Vec2(10.0f, -30.0f);
		bool doSleep = false;
		_world = new b2World(gravity, doSleep);
		
		// Create edges around the entire screen
		b2BodyDef groundBodyDef;
		groundBodyDef.position.Set(0,0);
		groundBody = _world->CreateBody(&groundBodyDef);
		b2PolygonShape groundBox;
		b2FixtureDef boxShapeDef;
		boxShapeDef.shape = &groundBox;
		groundBox.SetAsEdge(b2Vec2(0,0), b2Vec2(winSize.width/PTM_RATIO, 0));
		groundBody->CreateFixture(&boxShapeDef);
		groundBox.SetAsEdge(b2Vec2(0,0), b2Vec2(0, winSize.height/PTM_RATIO));
		groundBody->CreateFixture(&boxShapeDef);
		groundBox.SetAsEdge(b2Vec2(0, winSize.height/PTM_RATIO), 
							b2Vec2(winSize.width/PTM_RATIO, winSize.height/PTM_RATIO));
		groundBody->CreateFixture(&boxShapeDef);
		groundBox.SetAsEdge(b2Vec2(winSize.width/PTM_RATIO, 
								   winSize.height/PTM_RATIO), b2Vec2(winSize.width/PTM_RATIO, 0));
		groundBody->CreateFixture(&boxShapeDef);
		
		/*
		 // Create ball body and shape
		 b2BodyDef ballBodyDef;
		 ballBodyDef.type = b2_dynamicBody;
		 ballBodyDef.position.Set(100/PTM_RATIO, 100/PTM_RATIO);
		 ballBodyDef.userData = _ball;
		 _body = _world->CreateBody(&ballBodyDef);
		 
		 b2CircleShape circle;
		 circle.m_radius = 27.0/PTM_RATIO;
		 
		 b2FixtureDef ballShapeDef;
		 ballShapeDef.shape = &circle;
		 ballShapeDef.density = 1.0f;
		 ballShapeDef.friction = 0.0f;
		 ballShapeDef.restitution = 0.6f;
		 _body->CreateFixture(&ballShapeDef);
		 
		 
		 
		 // Create ball body and shape
		 b2BodyDef ballBodyDef2;
		 ballBodyDef2.type = b2_dynamicBody;
		 ballBodyDef2.position.Set(100/PTM_RATIO, 100/PTM_RATIO);
		 ballBodyDef2.userData = _ball2;
		 _body2 = _world->CreateBody(&ballBodyDef2);
		 
		 b2CircleShape circle2;
		 circle2.m_radius = 57/PTM_RATIO;
		 
		 b2FixtureDef ballShapeDef2;
		 ballShapeDef2.shape = &circle2;
		 ballShapeDef2.density = 1.0f;
		 ballShapeDef2.friction = 0.0f;
		 ballShapeDef2.restitution = 0.6f;
		 _ball2Fixture = _body2->CreateFixture(&ballShapeDef2);
		 */
		
		
		// RaumSprite tests
		
		
		
		// RaumSprite tests Ende
		
		
		[self schedule:@selector(tick:)];
		
		
		/* DEMO-RÄUME */
		/*
		if ([[ModelLocator instance] valueForKey:k_DATA_DEMOMODE_ACTIVE] && [[[ModelLocator instance] valueForKey:k_DATA_DEMOMODE_ACTIVE] boolValue]) {
			[self createNewRaumNode: @"Wohnzimmer"];
			[self createNewRaumNode: @"Küche"];
			[self createNewRaumNode: @"Kinderzimmer"];
			[self createNewRaumNode: @"Esszimmer"];
			[self createNewRaumNode: @"Schlafzimmer"];
			
			[self schedule: @selector(nodeUpdateTick:) interval:4.0];
		}
		else {
			for (DSRoom *room in [DataController instance].rooms) {
				[self createNewRaumNode: room];
			}
		}
		 */
		NSArray *meters = [NSArray arrayWithArray:[DataController instance].meters];
		for (DSMeter *meter in meters) {
            if (meter.meteringEnabled) {
                [self createNewRaumNode: meter];
            }

		}
		
		/*
		 //cpCCSprite *cpCCSOwner = (cpCCSprite *) touchedShape->data;
		 // Let's do 'pulse' special effect on whatever we just touched, just to give the user some feedback        
		 CCFiniteTimeAction *zoomAction = [CCScaleTo actionWithDuration:2.1f scale:raumNode.scale * 1.2f];// zoom in
		 CCFiniteTimeAction *shrinkAction = [CCScaleTo actionWithDuration:2.1f scale:raumNode.scale];// zoom out
		 CCSequence *actions = [CCSequence actions:zoomAction, shrinkAction, nil];// zoom in, then zoom out
		 [raumNode runAction:actions];
		 */
		
		//[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
    }
	
	
    return self;
}

-(void) nodeUpdateTick: (ccTime) dt
{
	// such dir einen Node raus und gib ihm neue Werte
	
	int r = arc4random() % [raumNodeArray count];
	
	[(RaumNode*)[raumNodeArray objectAtIndex:r] updateWithWatt:15*(arc4random()%100)];
}

-(void) createNewRaumNode: (id) _room {
	raumNode =[[RaumNode alloc] initWithRoom:_room];
	//raumNode.position = ccp(-300,450);
	[self addChild:raumNode];
	
	raumNode.groundBody = groundBody;
	[raumNode initMe:self withWorld:_world];
	
	[raumNodeArray addObject:raumNode];
	
	if ([_room isKindOfClass:[NSString class]]) {
		[raumNode updateWithWatt:15*(arc4random()%100)];
	}
}

-(void) rescaleAllNodesWithCallingNode:(RaumNode*)_callingNode{
//	GSLog(@"");
//	GSLog(@"rescaleAllNodes +++++++++++++++++");
	// Alle Nodes werden mit einem Faktor skaliert damit eine vernünftige Gesamtszene entsteht
	
	// Dafür bewerten wir die Gesamtszene anhand der Anzahl der Räume und der jeweiligen Stromverbräuche und
	// berechnen daraus einen Faktor, der den Raum optimal ausnutzen soll
	
	float raumfaktor;
	
	if ([raumNodeArray count]==1){
		raumfaktor = 1;
	} 
	if ([raumNodeArray count]==2){
		raumfaktor = 0.9;
	}
	if ([raumNodeArray count]==3){
		raumfaktor = 0.78;
	}
	if ([raumNodeArray count]==4){
		raumfaktor = 0.75;
	}
	if ([raumNodeArray count]==5){
		raumfaktor = 0.70;
	}
	if ([raumNodeArray count]==6){
		raumfaktor = 0.65;
	}
	if ([raumNodeArray count]==7){
		raumfaktor = 0.62;
	}
	if ([raumNodeArray count]==8){
		raumfaktor = 0.60;
	}
	if ([raumNodeArray count]==9){
		raumfaktor = 0.58;
	}
	if ([raumNodeArray count]>=10){
		raumfaktor = 0.55;
	}
	
	// reicht die Fläche aus?
	
	float gesamtflaeche; 
	for (RaumNode *_node in raumNodeArray) {
		// =((1,2 - 0)*C29 * (101,5*2)*(101,5*2))*B29
		gesamtflaeche += (1.2 - _node.wert)*raumfaktor * (101.5f*2)*(101.5f*2);
//		GSLog(@"gesamtfläche: %f", gesamtflaeche);
	}
	//	gesamtflaeche =
	
	float platzfaktor;
	platzfaktor = 200000 / gesamtflaeche;
	if (platzfaktor < 1){
//		GSLog(@"raumfaktor anpassen *****************");
		raumfaktor = raumfaktor * platzfaktor;
	}
	
	
	if (alterRaumfaktor != raumfaktor){
//		GSLog(@"ALLE ÄNDERN *****************************");
		for (RaumNode *_node in raumNodeArray) {
//			GSLog(@"raumnode watt: %f", _node.watt);
//			GSLog(@"raumfaktor: %f  _node.wert: %f", raumfaktor,_node.wert);
//			GSLog(@"rescaleme:%i", (int)(((1.2 - _node.wert)*raumfaktor)*100));
			if ((int)(((1.2 - _node.wert)*raumfaktor)*100) > 30){
				[_node rescaleMe:(int)(((1.2 - _node.wert)*raumfaktor)*100)];
			}else {
				[_node rescaleMe:30];
			}
		}
	}else {
//		GSLog(@"rescaleme:%i", (int)(((1.2 - _callingNode.wert)*raumfaktor)*100));
		
		if ((int)(((1.2 - _callingNode.wert)*raumfaktor)*100) > 30){
			[_callingNode rescaleMe:(int)(((1.2 - _callingNode.wert)*raumfaktor)*100)];
		}else {
			[_callingNode rescaleMe:30];
		}
		
		//[_callingNode rescaleMe:(int)(((1.2 - _callingNode.wert)*raumfaktor)*100)];
	}

		alterRaumfaktor = raumfaktor;
}

- (void)tick:(ccTime) dt {
	
    _world->Step(dt, 10, 10);
    for(b2Body *b = _world->GetBodyList(); b; b=b->GetNext()) {    
        if (b->GetUserData() != NULL) {
            CCSprite *ballData = (CCSprite *)b->GetUserData();
            ballData.position = ccp(b->GetPosition().x * PTM_RATIO,
                                    b->GetPosition().y * PTM_RATIO);
            ballData.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
        }        
    }
	
}
/*
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
    if (_mouseJoint != NULL) return;
	
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
	
	bool hitABody = NO;
	
	
	
	// Check alle Nodes auf Hit
	//for (int i=0;i<[raumNodeArray count];i++){
	for (RaumNode *rn in raumNodeArray) {
		//if ([(RaumNode*)[raumNodeArray objectAtIndex:i] getFixture]->TestPoint(locationWorld)) {
		if ([rn getFixture]->TestPoint(locationWorld)) {
			hitABody = YES;
			//[rn animateMe];
			
			
			
			b2MouseJointDef md;
			md.bodyA = groundBody;
			md.bodyB = rn._raumBody;
			md.target = locationWorld;
			md.collideConnected = true;
			md.maxForce = 1000.0f * rn._raumBody->GetMass();
			
			_mouseJoint = (b2MouseJoint *)_world->CreateJoint(&md);
			rn._raumBody->SetAwake(true);
		}
	}
	
	
	
	
	
	if (hitABody == NO){
		// ins Leere geklickt also soll Navi angezeigt werden
		//GSLog(@"nix getroffen");
		EV_Locator* ev_Locator = [EV_Locator instance];
		[ev_Locator.ev_RootVC toggleOverlay];
		
		
	}
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	
    if (_mouseJoint == NULL) return;
	
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
	
    _mouseJoint->SetTarget(locationWorld);
	
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	
    if (_mouseJoint) {
        _world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
	
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	//[raumNode animateMe];
    if (_mouseJoint) {
        _world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }  
}
*/
- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
    // Landscape left values
    b2Vec2 gravity(-acceleration.y * 15, acceleration.x *15);
    _world->SetGravity(gravity);
	
}

- (void) removeNodes {
	for (RaumNode *node in raumNodeArray) {
//		GSLog(@"retaincount: %i", [node retainCount]);
		[node prepareDealloc];
		[node release];
	}
	
	[raumNodeArray removeAllObjects];
}

- (void)dealloc {    
    delete _world;
    _body = NULL;
    _world = NULL;
	
	[self removeNodes];
	
    [super dealloc];
}



#pragma mark Touch Logik

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
	//GSLog(@"scene getappt");	
	openMenuFlag = YES;
	return YES;
}

- (void)ccTouchMoved:(UITouch *)myTouch withEvent:(UIEvent *)event
{
		
}


- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
	if (openMenuFlag== YES){
		//GSLog(@"open menu");
		// ins Leere geklickt also soll Navi angezeigt werden
		//GSLog(@"nix getroffen");
//		EV_Locator* ev_Locator = [EV_Locator instance];
//		[ev_Locator.ev_RootVC toggleOverlay];
		openMenuFlag = NO;
	}
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	
    
	
}

-(void) cancelOpenMenu {
	openMenuFlag = NO;
}


@end
