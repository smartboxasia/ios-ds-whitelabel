//
//  ModelLocator.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EV_RootVC.h"
#import "EV_Overlay.h"
#import "EVScene.h"


@interface EV_Locator : NSObject {
	EV_RootVC* ev_RootVC;
	EV_Overlay* ev_Overlay;
	EVScene* evScene;
	
	UIViewController* rootViewController;
}

@property (nonatomic, retain) EV_RootVC* ev_RootVC;
@property (nonatomic, retain) EV_Overlay* ev_Overlay;
@property (nonatomic, retain) EVScene* evScene;

@property (nonatomic, retain) UIViewController* rootViewController;


+(EV_Locator *) instance;



@end
