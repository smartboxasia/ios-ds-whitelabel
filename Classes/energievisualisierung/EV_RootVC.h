//
//  EV_RootVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "EV_Overlay.h"

@interface EV_RootVC : UIViewController {
	UIView* eaglViewContainer;
	
	UIImageView* background;
	
	EV_Overlay* overlay;
}
-(void) updateRotation:(NSString*)_position;
- (void) buildEV;
- (void) dismissEV;
@end
