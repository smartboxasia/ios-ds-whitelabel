//
//  HelloWorldScene.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//


#import "cocos2d.h"
#import "Box2D.h"
#import "RaumNode.h"


@interface EVScene : CCLayer {   
	b2World *_world;
	b2Body *_body;
	b2Body *_body2;
	CCSprite *_ball;
	CCSprite *_ball2;
	RaumNode* raumNode;
	//b2Body *_raumBody;
	//b2Fixture *raumFixture;
	
	
	b2Body *groundBody;
	
	b2Fixture *_ball2Fixture;
	
	b2MouseJoint *_mouseJoint;
	
	NSMutableArray* raumNodeArray;
	
	BOOL openMenuFlag;
	
	float alterRaumfaktor;
}

+ (id) scene;
-(void) createNewRaumNode: (id) _room;
-(void) cancelOpenMenu;
-(void) rescaleAllNodesWithCallingNode:(RaumNode*)_callingNode;
@end