//
//  EV_RootVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "EV_RootVC.h"
#import "GameConfig.h"
#import "EVScene.h"
#import "EV_Locator.h"
#import "ModelLocator.h"
#import "GAIHelper.h"

@implementation EV_RootVC

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	EV_Locator* ev_Locator = [EV_Locator instance];
	ev_Locator.ev_RootVC = self;

	// Hier wird der ganze Cocos2D Kram eingebunden damit er schön gekapselt ist
	CCDirector *director = [CCDirector sharedDirector];

	eaglViewContainer = [UIView new];

	background =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"EV_back"]];
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;

//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        background.frame  = CGRectMake(0, 0, 568, 320);
//    }
//    else{
//        background.frame  = CGRectMake(0,0,480,320);
//    }
    
    background.frame  = CGRectMake(0, 0, screenHeight, screenWidth);

    background.contentMode = UIViewContentModeScaleToFill;
	[self.view addSubview:background];
	//

	// Create the EAGLView manually
	//  1. Create a RGB565 format. Alternative: RGBA8
	//	2. depth format of 0 bit. Use 16 or 24 bit for 3d effects, like CCPageTurnTransition
	//
	//
	/*
	 EAGLView *glView = [EAGLView viewWithFrame:[self.view bounds]
	 pixelFormat:kEAGLColorFormatRGBA8	// kEAGLColorFormatRGBA8
	 depthFormat:0						// GL_DEPTH_COMPONENT16_OES
	 ];

	 */



	// Create an EAGLView with a RGB8 color buffer, and a depth buffer of 24-bits
	/*
	 EAGLView *glView = [EAGLView viewWithFrame:[self.view bounds]
	 pixelFormat:kEAGLColorFormatRGBA8                // RGBA8 color buffer
	 depthFormat:GL_DEPTH_COMPONENT24_OES   // 24-bit depth buffer
	 preserveBackbuffer:NO
	 sharegroup:nil //for sharing OpenGL contexts between threads
	 multiSampling:NO //YES to enable it
	 numberOfSamples:0 //can be 1 - 4 if multiSampling=YES
	 ];


	 */
    CGRect fullScreenFrame;
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        fullScreenFrame = CGRectMake(0, 0, 568, 320);
//    }
//    else{
//        fullScreenFrame = CGRectMake(0,0,480,320);
//    }

    fullScreenFrame  = CGRectMake(0, 0, screenHeight, screenWidth);
    
	EAGLView *glView = [EAGLView viewWithFrame:fullScreenFrame
								   pixelFormat:kEAGLColorFormatRGBA8	// kEAGLColorFormatRGBA8
								   depthFormat:0						// GL_DEPTH_COMPONENT16_OES
						];

	// attach the openglView to the director
	[director setOpenGLView:glView];

	//	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	//	if( ! [director enableRetinaDisplay:YES] )
	//		CCLOG(@"Retina Display Not supported");

	//
	// VERY IMPORTANT:
	// If the rotation is going to be controlled by a UIViewController
	// then the device orientation should be "Portrait".
	//
	// IMPORTANT:
	// By default, this template only supports Landscape orientations.
	// Edit the RootViewController.m file to edit the supported orientations.
	//


	// make the OpenGLView a child of the view controller
	CAEAGLLayer *eaglLayer = (CAEAGLLayer *)glView.layer;
	eaglLayer.opaque = NO;



	[self.view addSubview:glView];
	[glView setMultipleTouchEnabled:YES];
	//[eaglViewContainer setView:glView];
	//glView.opaque = NO;
	[glView setBackgroundColor:[UIColor clearColor]];

	glClearColor(0.0f,0.0f,0.0f,0.0f);
	//glClear(GL_COLOR_BUFFER




	//eaglViewContainer.view.alpha = 0;
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];


	// Removes the startup flicker
	//[self removeStartupFlicker];

	// Run the intro Scene

	if ([[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] && [[[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] isEqualToString:@"1"]) {
//		[self buildEV];
	}


	[self.view addSubview:eaglViewContainer];


	overlay = [[EV_Overlay alloc] init];
	//overlay.view.frame = CGRectMake(0,-50,overlay.view.frame.size.width,overlay.view.frame.size.height);
//    // iphone 5 support.
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        overlay.view.frame = CGRectMake(0, 0, 568, 49);
//    }
//    else{
//        overlay.view.frame = CGRectMake(0,0,480,49);
//    }
  
  overlay.view.frame = CGRectMake(0, 0, screenHeight, 49);

  [self.view addSubview:overlay.view];

  [glView setMultipleTouchEnabled:YES];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self buildEV];
	[[ModelLocator instance] addObserver:self forKeyPath:k_DSS_AVAILABLE options:0 context:nil];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
        [[ModelLocator instance] removeObserver:self forKeyPath:k_DSS_AVAILABLE];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_DSS_AVAILABLE]) {
		if ([[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] && [[[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] isEqualToString:@"1"]) {
//			[self buildEV];
		}
		else if ([[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] && [[[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] isEqualToString:@"0"]) {
			[self dismissEV];
		}

	}
}

- (void) buildEV {
    [GAIHelper trackViewControllerWithName:@"Ball Game"];

	if (![[CCDirector sharedDirector] runningScene]) {
		[[CCDirector sharedDirector] runWithScene:[EVScene scene]];
	}
	else {
        [[CCDirector sharedDirector] stopAnimation];
		[[CCDirector sharedDirector] replaceScene:[EVScene scene]];
        [[CCDirector sharedDirector] startAnimation];
	}
}

- (void) dismissEV {
    [[CCDirector sharedDirector] stopAnimation];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) toggleOverlay {
	/*
	[self.view bringSubviewToFront:overlay.view];
	[UIView beginAnimations: nil context: self];
	[UIView setAnimationDuration: 0.5];
	//[UIView setAnimationDelay:0.5];
	if (overlay.view.frame.origin.y < -20){
		overlay.view.frame = CGRectMake(0,0,overlay.view.frame.size.width,overlay.view.frame.size.height);;
	}else {
		//overlay.view.frame = CGRectMake(0,-50,overlay.view.frame.size.width,overlay.view.frame.size.height);;
		overlay.view.frame = CGRectMake(0,0,overlay.view.frame.size.width,overlay.view.frame.size.height);;
	}
	[UIView setAnimationDelegate: self];
	//[UIView setAnimationDidStopSelector: @selector(fadeInDone: finished: context:)];
	[UIView commitAnimations];
	 */
}

-(void) closeEVAnsicht{
	/*
	 [UIView beginAnimations: nil context: self];
	 [UIView setAnimationDuration: 0.5];
	 self.view.alpha = 0;
	 [UIView setAnimationDelegate: self];
	 [UIView setAnimationDidStopSelector: @selector(closeEVAnsichtDone: finished: context:)];
	 [UIView commitAnimations];
	 */
	EV_Locator* ev_Locator = [EV_Locator instance];
	[ev_Locator.rootViewController showPortraitModeWithoutAnimation];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"BallGameDidDismiss" object:nil];
}

- (void) closeEVAnsichtDone: (NSString *) animationID finished: (BOOL) finished context: (void *) myTarget {
	EV_Locator* ev_Locator = [EV_Locator instance];
	[ev_Locator.rootViewController showPortraitModeWithoutAnimation];

	[[CCDirector sharedDirector] pause];

	self.view.alpha = 1;
}

-(void) updateRotation:(NSString*)_position{
    //	NSLog(@"_position: %@", _position);
	if ([_position isEqualToString:@"left"]){

		//[overlay.view setCenter:CGPointMake(160, 240)];
		[overlay.view setTransform: CGAffineTransformMakeRotation(-M_PI)];

//        //iphone 5 support.
//        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//        if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//            [overlay.view setFrame:CGRectMake(-248, 320-overlay.view.frame.size.height, overlay.view.frame.size.width, overlay.view.frame.size.height)];
//        }
//        else{
//            [overlay.view setFrame:CGRectMake(-160, 320-overlay.view.frame.size.height, overlay.view.frame.size.width, overlay.view.frame.size.height)];
//        }

    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    NSLog(@"EV_RootVC.mm updateRotation: %f height: %f", overlay.view.frame.size.width,overlay.view.frame.size.height);
    [overlay.view setFrame:CGRectMake(screenWidth-overlay.view.frame.size.height, screenWidth-overlay.view.frame.size.height, overlay.view.frame.size.width, overlay.view.frame.size.height)];
	}

	if ([_position isEqualToString:@"right"]){
		//[overlay.view setCenter:CGPointMake(160, 240)];
		[overlay.view setTransform: CGAffineTransformMakeRotation(0)];
    
    NSLog(@"overlay width: %f height: %f", overlay.view.frame.size.width,overlay.view.frame.size.height);
		[overlay.view setFrame:CGRectMake(0, 0, overlay.view.frame.size.width, overlay.view.frame.size.height)];
	}
}

- (void)dealloc {
    [super dealloc];
}


@end
