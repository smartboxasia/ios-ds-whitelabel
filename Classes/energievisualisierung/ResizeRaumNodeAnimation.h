//
//  ResizeRaumNodeAnimation.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "cocos2d.h"


@interface ResizeRaumNodeAnimation : CCScaleTo {
	CCNode *updateTarget;
}

+(id) actionWithDuration: (ccTime) t scale:(float) s andUpdateTarget:(CCNode*)_updateTarget;
-(id) initWithDuration: (ccTime) t scale:(float) s andUpdateTarget:(CCNode*)_updateTarget;
-(void) update: (ccTime) t;

@end
