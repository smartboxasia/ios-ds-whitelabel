//
//  RaumNode.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "RaumNode.h"
#import "CCLabelTTF.h"
#import "EVScene.h"
#import "ResizeRaumNodeAnimation.h"
#import "DSMeter.h"
#import "ModelLocator.h"
#import "EV_Locator.h"

#define PTM_RATIO 32.0

@implementation RaumNode


@synthesize world,_raumBody,groundBody, wert,watt;


- (id) initWithRoom: (DSMeter*) _meter {
	
	self=[super init];
	if (self != nil) {
		if (_meter) {
			if ([_meter isKindOfClass:[DSMeter class]]) {
				meter = (DSMeter*)_meter;
				roomName = meter.name;
			}
			else if ([_meter isKindOfClass:[NSString class]]) {
				meter = nil;
				roomName = (NSString*)_meter;
			}
			
		}
		
        [[CCTextureCache sharedTextureCache] removeTextureForKey:@"EV_SpriteBack.png"];
		myKreis = [CCSprite spriteWithFile:@"EV_SpriteBack.png"];
		myKreis.position = ccp(0, 0);
		[self addChild:myKreis];
		
		/*
		CCSprite *myKreis2 = [CCSprite spriteWithFile:@"EV_SpriteBack.png"];
		[myKreis2 setColor:ccc3(255,0,0)];
		myKreis2.position = ccp(100, 0);
		[self addChild:myKreis2];
		*/
		
		_label = [CCLabelTTF labelWithString:roomName fontName:@"Helvetica" fontSize:30];
		_label.color = ccc3(254,254,254);
		_label.position = ccp(0, 10);
		_label.scale = 0.01;
		[self addChild:_label];
		
		
		_wattlabel = [CCLabelTTF labelWithString:@"0 W" fontName:@"Helvetica" fontSize:50];
		_wattlabel.color = ccc3(56,111,0);
		_wattlabel.position = ccp(0, -5);
		_wattlabel.scale = 0.01;
		[self addChild:_wattlabel];
		myEV_Locator = [EV_Locator instance];
		if (meter && [meter isKindOfClass:[DSMeter class]]) {
			[meter addObserver:self forKeyPath:@"powerConsumption" options:0 context:nil];
		}
		
	}
	
	return self;
}

-(void) initMe:(EVScene*)_EVScene withWorld:(b2World*)_world{
	world = _world;
	scene = _EVScene;
	myKreis.scale = 0.01f;
	
	// Create ball body and shape
	b2BodyDef raumBodyDef;
	raumBodyDef.type = b2_dynamicBody;
	
	
	
	raumBodyDef.position.Set((5+(float)(arc4random()%470))/PTM_RATIO, (5+(float)(arc4random()%310))/PTM_RATIO);
	//raumBodyDef.position.Set(240/PTM_RATIO, 160/PTM_RATIO);
	raumBodyDef.userData = self;
	_raumBody = _world->CreateBody(&raumBodyDef);
	
	
	circle3.m_radius = [self getRadius]/PTM_RATIO;
	
	
	raumShapeDef.shape = &circle3;
	raumShapeDef.density = 1.0f;
	raumShapeDef.friction = 0.5f;
	raumShapeDef.restitution = 0.6f;
	raumFixture = _raumBody->CreateFixture(&raumShapeDef);
	//[self shrinkeMe];
	
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
	
	if (meter.powerConsumption) {
		[self updateWithWatt: [meter.powerConsumption intValue]];
	}
	else {
		[self updateWithWatt: 0];
	}
	//[self updateWithWatt:[meter.powerConsumption intValue]];
}

#pragma mark -
#pragma mark Observer responder

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	//if ([keyPath isEqualToString:[NSString stringWithFormat:@"%@%@", k_INFO_PREFIX_ROOM_CONSUMPTION, room.identifier]]) {
	if (![meter isFault]) {
		if ([keyPath isEqualToString:@"powerConsumption"]) {
			if (meter.powerConsumption) {
				[self updateWithWatt: [meter.powerConsumption intValue]];
			}
			else {
				[self updateWithWatt: 0];
			}

					
		}
	}
	
}

-(b2Fixture*)getFixture{
	return raumFixture;
}

-(void)updateWithWatt:(int)_value{
//	GSLog(@"raum update: %i", _value);
	//[_wattlabel setString:[NSString stringWithFormat:@"%iW",_value]];
	
	
	
	
	// Rechnung auf 1500 Watt max Anfang
	/*
	 GSLog(@"_value: %i", _value);
	 
	 // Soll von 10W bis 1500W darstellen
	 if (_value <= 10){
	 _value = 10;
	 }
	 if (_value >= 1500){
	 _value = 1500;
	 }
	 
	 float wert = (1.00/(1+(float)_value/1000));
	 */
	// Rechnung auf 1500 Watt max Ende
	
	
	
	// Rechnung auf 150 Watt max Anfang
	
	//GSLog(@"_value: %i", _value);
	
	// Soll von 10W bis 1500W darstellen
	int shouldValue = _value;
	
	if (_value <= 10){
		_value = 10;
	}
	if (_value >= 1000){
		_value = 1000;
	}
	watt = _value;
	wert = (1.00/(1+(float)_value/100));
	
	[scene rescaleAllNodesWithCallingNode:self];
	
	[_wattlabel setString:[NSString stringWithFormat:@"%iW",shouldValue]];
	
	/*
	float wert;
	
	if (room && [room isKindOfClass:[DSRoom class]]) {
		wert = (1.00/(1+(float)_value/100));
	}
	else {
		wert = (1.00/(1+(float)_value/1000));
	}
	
	// Rechnung auf 1500 Watt max Ende
	
	
	
	//GSLog(@"wert: %f", wert);
	CCFiniteTimeAction *scaleAction = [ResizeRaumNodeAnimation actionWithDuration:0.5f scale:1.2f-wert andUpdateTarget:self];// zoom in
	CCSequence *actions = [CCSequence actions:scaleAction, nil];// zoom in, then zoom out
	[myKreis runAction:actions];
	
	
	CCFiniteTimeAction *labelScaleAction = [ResizeRaumNodeAnimation actionWithDuration:0.5f scale:1.2f-wert andUpdateTarget:self];// zoom in
	CCSequence *labelActions = [CCSequence actions:labelScaleAction, nil];// zoom in, then zoom out
	[_label runAction:labelActions];
	
	CCFiniteTimeAction *label2ScaleAction = [ResizeRaumNodeAnimation actionWithDuration:0.5f scale:1.2f-wert andUpdateTarget:self];// zoom in
	CCSequence *label2Actions = [CCSequence actions:label2ScaleAction, nil];// zoom in, then zoom out
	[_wattlabel runAction:label2Actions];
	 */
	
	
}

-(void) rescaleMe:(int)_faktor {
//	GSLog(@"rescaleNode: %@ with _faktor: %f", roomName, (float)_faktor/100);
	CCFiniteTimeAction *scaleAction = [ResizeRaumNodeAnimation actionWithDuration:0.5f scale:(float)_faktor/100 andUpdateTarget:self];// zoom in
	CCSequence *actions = [CCSequence actions:scaleAction, nil];// zoom in, then zoom out
	[myKreis runAction:actions];
	
	
	CCFiniteTimeAction *labelScaleAction = [ResizeRaumNodeAnimation actionWithDuration:0.5f scale:(float)_faktor/100 andUpdateTarget:self];// zoom in
	CCSequence *labelActions = [CCSequence actions:labelScaleAction, nil];// zoom in, then zoom out
	[_label runAction:labelActions];
	
	CCFiniteTimeAction *label2ScaleAction = [ResizeRaumNodeAnimation actionWithDuration:0.5f scale:(float)_faktor/100 andUpdateTarget:self];// zoom in
	CCSequence *label2Actions = [CCSequence actions:label2ScaleAction, nil];// zoom in, then zoom out
	[_wattlabel runAction:label2Actions];
}


-(void) shrinkeMe{
	//cpCCSprite *cpCCSOwner = (cpCCSprite *) touchedShape->data;
	// Let's do 'pulse' special effect on whatever we just touched, just to give the user some feedback        
	
	CCFiniteTimeAction *shrinkAction = [ResizeRaumNodeAnimation actionWithDuration:0.5f scale:0.3f andUpdateTarget:self];// zoom in
	//GSLog(@"myKreis.contentSizeInPixels.width*_faktor: %f", myKreis.contentSizeInPixels.width*0.2);
	CCSequence *actions = [CCSequence actions:shrinkAction, nil];// zoom in, then zoom out
	[myKreis runAction:actions];
	//[self prescaleBodyFixture:0.2];
	
	//CCFiniteTimeAction *zoomAction = [ResizeRaumNodeAnimation actionWithDuration:5.0f scale:myKreis.scale * 0.2f andUpdateTarget:self];// zoom in
	//[myKreis runAction:zoomAction];
}
-(void) zoomMe{
	//cpCCSprite *cpCCSOwner = (cpCCSprite *) touchedShape->data;
	// Let's do 'pulse' special effect on whatever we just touched, just to give the user some feedback        
	
	CCFiniteTimeAction *shrinkAction = [ResizeRaumNodeAnimation actionWithDuration:0.5f scale:0.8f andUpdateTarget:self];// zoom in
	//GSLog(@"myKreis.contentSizeInPixels.width*_faktor: %f", myKreis.contentSizeInPixels.width*1.2);
	CCSequence *actions = [CCSequence actions:shrinkAction, nil];// zoom in, then zoom out
	[myKreis runAction:actions];
	//[self prescaleBodyFixture:1.2];
	
	//CCFiniteTimeAction *zoomAction = [ResizeRaumNodeAnimation actionWithDuration:5.0f scale:myKreis.scale * 0.2f andUpdateTarget:self];// zoom in
	//[myKreis runAction:zoomAction];
}

-(void) animateMe{
	if (myKreis.scale < 0.5){
		[self zoomMe];
	}else {
		[self shrinkeMe];
	}
}

-(void) prescaleBodyFixture:(double)_faktor {
	//_faktor = 0.2;
	//GSLog(@"radius: %f", 101.5*_faktor);
	circle3.m_radius = (101.5*_faktor)/PTM_RATIO;
	_raumBody->DestroyFixture(raumFixture);
	
	raumShapeDef.shape = &circle3;
	raumShapeDef.density = 1.0f;
	raumShapeDef.friction = 0.5f;
	raumShapeDef.restitution = 0.6f;
	
	raumFixture = _raumBody->CreateFixture(&raumShapeDef);
}

-(void) updateBodyFixture{
	
	circle3.m_radius = [self getRadius]/PTM_RATIO;
	//GSLog(@"radius: %f    scale: %f", circle3.m_radius,myKreis.scale);
	
	_raumBody->DestroyFixture(raumFixture);
	
	raumShapeDef.shape = &circle3;
	raumShapeDef.density = 1.0f;
	raumShapeDef.friction = 0.5f;
	raumShapeDef.restitution = 0.6f;
	
	raumFixture = _raumBody->CreateFixture(&raumShapeDef);
	
	// passe Labelgrößen an
	
	//float raumLabelSize = 50.0f*myKreis.scale;
	//GSLog(@"raumLabelSize: %f", raumLabelSize);
	
	
	//[_label setFont:[UIFont fontWithName:@"Helvetica" size:raumLabelSize]];
	//_label.frame = CGRectMake(0, 5, _label.frame.size.width, _label.frame.size.height);
	
	
	_label.position = ccp(0, 25*myKreis.scale);
	_wattlabel.position = ccp(0, -10*myKreis.scale);
}

-(float)getRadius {
	return 101.5f*myKreis.scale;
}



#pragma mark Touch Logik

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
	//GSLog(@"Sprite getappt");	
	CGPoint touchPoint = [touch locationInView:[touch view]];
	//touchPoint = [[CCDirector sharedDirector] convertToGL:touchPoint];
	
	touchPoint = [[CCDirector sharedDirector] convertToGL:touchPoint];
	b2Vec2 locationWorld = b2Vec2(touchPoint.x/PTM_RATIO, touchPoint.y/PTM_RATIO);
	
	//if([self isTouchOnSprite:touchPoint]){
	if ([self getFixture]->TestPoint(locationWorld)) {
		[myEV_Locator.evScene cancelOpenMenu];
		
		//GSLog(@"treffer");
		//whereTouch=ccpSub(self.position, touchPoint);
		
		b2MouseJointDef md;
		md.bodyA = self.groundBody;
		md.bodyB = self._raumBody;
		md.target = locationWorld;
		md.collideConnected = true;
		md.maxForce = 1000.0f * self._raumBody->GetMass();
		
		_mouseJoint = (b2MouseJoint *)self.world->CreateJoint(&md);
		self._raumBody->SetAwake(true);
		
		return YES;
	}
	
	return NO;
}

- (void)ccTouchMoved:(UITouch *)myTouch withEvent:(UIEvent *)event
{
	if (_mouseJoint == NULL) return;
	
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
	
    _mouseJoint->SetTarget(locationWorld);
	
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
	if (_mouseJoint) {
        self.world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }  
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	
    if (_mouseJoint) {
        self.world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
	
}

- (void) prepareDealloc {
	if (meter && [meter isKindOfClass:[DSMeter class]]) {
        @try {
            [meter removeObserver:self forKeyPath:@"powerConsumption"];
        }
        @catch (NSException *exception) {
            //just in case
        }

	}
	[[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
}

- (void) dealloc {
	
//	GSLog(@"room dealloc: %@", meter.name);
	[super dealloc];
}


@end
