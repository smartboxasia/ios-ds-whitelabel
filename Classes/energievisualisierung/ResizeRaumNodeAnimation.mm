//
//  ResizeRaumNodeAnimation.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "ResizeRaumNodeAnimation.h"


@implementation ResizeRaumNodeAnimation


+(id) actionWithDuration: (ccTime) t scale:(float) s andUpdateTarget:(CCNode*)_updateTarget
{
	
	return [[[self alloc] initWithDuration: t scale:s andUpdateTarget:_updateTarget] autorelease];
}

-(id) initWithDuration: (ccTime) t scale:(float) s
{
	if( (self=[super initWithDuration: t]) ) {
		endScaleX_ = s;
		endScaleY_ = s;
	}
	return self;
}

-(id) initWithDuration: (ccTime) t scale:(float) s andUpdateTarget:(CCNode*)_updateTarget
{
	if( (self=[super initWithDuration: t]) ) {
		updateTarget = _updateTarget;
		endScaleX_ = s;
		endScaleY_ = s;
	}
	return self;
}

+(id) actionWithDuration: (ccTime) t scaleX:(float)sx scaleY:(float)sy 
{
	return [[[self alloc] initWithDuration: t scaleX:sx scaleY:sy] autorelease];
}

-(id) initWithDuration: (ccTime) t scaleX:(float)sx scaleY:(float)sy
{
	if( (self=[super initWithDuration: t]) ) {	
		endScaleX_ = sx;
		endScaleY_ = sy;
	}
	return self;
}

-(id) copyWithZone: (NSZone*) zone
{
	CCAction *copy = [[[self class] allocWithZone: zone] initWithDuration: [self duration] scaleX:endScaleX_ scaleY:endScaleY_];
	return copy;
}

-(void) startWithTarget:(CCNode *)aTarget
{
	[super startWithTarget:aTarget];
	startScaleX_ = [target_ scaleX];
	startScaleY_ = [target_ scaleY];
	deltaX_ = endScaleX_ - startScaleX_;
	deltaY_ = endScaleY_ - startScaleY_;
}

-(void) update: (ccTime) t
{
	
	[target_ setScaleX: (startScaleX_ + deltaX_ * t ) ];
	[target_ setScaleY: (startScaleY_ + deltaY_ * t ) ];
	[updateTarget updateBodyFixture];
}



@end
