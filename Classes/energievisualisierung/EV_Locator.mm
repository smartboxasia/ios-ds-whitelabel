//
//  EV_Locator.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "EV_Locator.h"


@implementation EV_Locator

@synthesize ev_RootVC,ev_Overlay,evScene,rootViewController;

+ (EV_Locator *) instance {
	static EV_Locator *instance;

	@synchronized(self) {
		if (!instance) {
			instance = [[EV_Locator alloc] init];
		}
	}

	return instance;
}

- (id) init {
	if (self = [super init]) {
	}

	return self;
}
@end