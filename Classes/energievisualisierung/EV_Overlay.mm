//
//  EV_Overlay.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "EV_Overlay.h"
#import "EV_Locator.h"
#import "ModelLocator.h"
#import "EnergieAmpelVC.h"

@implementation EV_Overlay

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    GSLog("log");
    
    EV_Locator* ev_Locator = [EV_Locator instance];
	ev_Locator.ev_Overlay = self;

//    // iphone 5 support.
//    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        [labelBackground setFrame:CGRectMake(0, 0, 568, 49)];
//        [titelLabel setFrame:CGRectMake(182, 2, 316, 35)];
//        [wattAusgabe setFrame:CGRectMake(480, 2, 72, 35)];
//        [labelBackground setImage:[UIImage imageNamed:@"EV_leiste_oben_568"]];
//    }

    EnergieAmpelVC *energieAmpel = [EnergieAmpelVC new];
    energieAmpel.view.frame = CGRectMake(0, 0, energieAmpel.view.frame.size.width, energieAmpel.view.frame.size.height);

	[energieAmpel showStandBy];
	[energieAmpel startWabern];
	[self.view addSubview:energieAmpel.view];
	// [energieAmpel storeInML];

	if (![[ModelLocator instance] valueForKey:k_DSS_AVAILABLE]) {
		[energieAmpel showStandBy];
	}

	if ([[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] && [[[ModelLocator instance] valueForKey:k_DSS_AVAILABLE] isEqualToString:@"0"]) {
		[energieAmpel showStandBy];
	}

	[self.view sendSubviewToBack:energieAmpel.view];
	[self applyLabels];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self applyLabels];
	[[ModelLocator instance] addObserver:self forKeyPath:k_INFO_TOTAL_CONSUMPTION options:0 context:nil];
	[[ModelLocator instance] addObserver:self forKeyPath:k_DSS_NAME options:0 context:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
            [[ModelLocator instance] removeObserver:self forKeyPath:k_INFO_TOTAL_CONSUMPTION];
            [[ModelLocator instance] removeObserver:self forKeyPath:k_DSS_NAME];
    }
    @catch (NSException *exception) {
        //just in case
    }

}


- (void) applyLabels {
	if ([[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]) {
        NSNumber *consumption = [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION];
        if([consumption integerValue] > -1){
            wattAusgabe.text = [NSString stringWithFormat:@"%@ W", [[ModelLocator instance] valueForKey:k_INFO_TOTAL_CONSUMPTION]];
        }
        else{
            wattAusgabe.text = @"-- W";
        }
	}
	else {
		wattAusgabe.text = @"-- W";
	}


	titelLabel.text = [[ModelLocator instance] valueForKey:k_DSS_NAME];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:k_DSS_NAME] || [keyPath isEqualToString:k_INFO_TOTAL_CONSUMPTION]) {
		[self applyLabels];
	}
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

-(IBAction) onBackTouch:(id)_sender{
	EV_Locator* ev_Locator = [EV_Locator instance];
	//[ev_Locator.rootViewController showPortraitMode];
//	[ev_Locator.ev_RootVC toggleOverlay];
	[ev_Locator.ev_RootVC closeEVAnsicht];
}


@end
