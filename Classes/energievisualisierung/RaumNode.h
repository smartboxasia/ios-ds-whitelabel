//
//  RaumNode.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//


#import "cocos2d.h"
#import "Box2D.h"

@class EVScene;
@class DSMeter;
@class EV_Locator;

@interface RaumNode : CCNode {
	DSMeter *meter;
	NSString *roomName;
	
	CCSprite* myKreis;
	float mySize;
	 
	b2Body *_raumBody;
	b2Fixture *raumFixture;
	
	b2Body *groundBody;
	
	b2World *world;
	
	b2CircleShape circle3;
	b2FixtureDef raumShapeDef;
	
	CCLabelTTF* _label;
	CCLabelTTF* _wattlabel;
	
	b2MouseJoint *_mouseJoint;
	
	EV_Locator *myEV_Locator;
	
	EVScene *scene;
	
	float wert;
	float watt;
}

@property (nonatomic, assign) b2World *world;
@property (nonatomic, assign) b2Body *_raumBody;
@property (nonatomic, assign) b2Body *groundBody;
@property (nonatomic, assign) float wert;
@property (nonatomic, assign) float watt;

- (id) initWithRoom: (DSMeter*) _meter;

-(float)getRadius;
-(void) initMe:(EVScene*)_EVScene withWorld:(b2World*)_world;

-(void) updateBodyFixture;

-(b2Fixture*)getFixture;

-(void)updateWithWatt:(int)_value;
-(void) rescaleMe:(int)_faktor;
- (void) prepareDealloc;

@end
