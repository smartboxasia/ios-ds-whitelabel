//
//  EV_Overlay.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"


@interface EV_Overlay : UIViewController {
	IBOutlet UILabel *titelLabel;
	IBOutlet UILabel *wattAusgabe;
  IBOutlet UIImageView *labelBackground;
}

-(IBAction) onBackTouch:(id)_sender;

@end
