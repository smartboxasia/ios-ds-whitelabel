//
//  MainVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSRoom.h"
#import "DSDevice.h"
#import "GAI.h"


@interface MainVC : GAITrackedViewController {

	UIAlertView *alertUpdater;
	UIViewController *vcLoginWaiting;
	
    CGFloat sHeight;
    CGFloat sWidth;
  
	BOOL shouldShowWaitingView;
}

- (void) refreshOverview;
- (void) arrangeViewOrder;

- (void) removeSplashAndAddOverview;
- (void) addOverview;

- (void) showSettings;
- (void) hideSettings;

- (void) showServerList;
- (void) showAdjustmentScreen: (id) entity;
- (void) showShadeAdjustmentScreen: (id) entity;

- (void) animateFromOverviewToRoomScenes: (DSRoom*) room;
- (void) animateBackFromRoomScenes: (DSRoom*) room;

- (void) animateFromOverviewToRoomDevices: (DSRoom*) room;
- (void) animateBackFromRoomDevices: (DSRoom*) room;

- (void) animateFromScenesToRoomDevices: (DSRoom*) room withIndex:(int)index;
- (void) animateBackFromRoomDevicesToScenes: (DSRoom*) room withIndex:(int)index;

@end
