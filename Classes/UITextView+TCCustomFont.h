//
//  UITextView+TCCustomFont.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (TCCustomFont)

@property (nonatomic, copy) NSString* fontName;

@end
