//
//  DSDevice.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <CoreData/CoreData.h>

@class DSRoom;

@interface DSDevice :  NSManagedObject  
{
	NSMutableArray *allObserver;
	BOOL dirty;
}

@property (nonatomic, assign) BOOL dirty;

@property (nonatomic, retain) NSNumber * powerOn;
@property (nonatomic, retain) NSNumber * dimmFactor;
@property (nonatomic, retain) NSNumber * angleFactor;
@property (nonatomic, retain) NSNumber * consumption;
@property (nonatomic, retain) NSNumber * metering;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * group;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * outputMode;
@property (nonatomic, retain) DSRoom * room;
@property (nonatomic, retain) NSString * socket;
@property (nonatomic, retain) NSNumber * maxPower;
@property (nonatomic, retain) NSNumber * dimmingCurveId;
@property (nonatomic, retain) NSString * infoUrl;
@property (nonatomic, retain) NSString * serviceUrl;
@property (nonatomic, retain) NSString * gtin;
@property (nonatomic, retain) NSString * oemEanNumber;
@property (nonatomic, retain) NSString * old_dsid;

- (BOOL) getPowerStatus;
- (BOOL) isUsingDsuid;

@end



