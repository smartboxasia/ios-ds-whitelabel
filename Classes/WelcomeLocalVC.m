//
//  WelcomeLocalVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "WelcomeLocalVC.h"
#import "WelcomeLocalItemCell.h"
#import "ModelLocator.h"
#import "StartupController.h"
#import "DataController.h"
#import "DSServer.h"

#import <QuartzCore/QuartzCore.h>

@interface WelcomeLocalVC ()

@end

@implementation WelcomeLocalVC

@synthesize collectionView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	[[ModelLocator instance] setValue:self forKey:k_VIEW_WELCOME];
	
    self.welcomeLabel.text = NSLocalizedString(@"key_welcome", nil);
    self.selectLabel.text = NSLocalizedString(@"key_welcome_select_server", nil);
    [self.skipButton setTitle:NSLocalizedString(@"key_skip", nil) forState:UIControlStateNormal];
    
	//[[ModelLocator instance] addObserver:self forKeyPath:@"localServerAddresses" options:0 context:nil];
	[self.collectionView registerNib:[UINib nibWithNibName:@"WelcomeLocalItemCell" bundle:nil] forCellWithReuseIdentifier:@"WelcomeLocalItemCell"];
	
	self.collectionView.layer.masksToBounds = NO;
	self.collectionView.layer.cornerRadius = 4; // if you like rounded corners
	self.collectionView.layer.shadowOffset = CGSizeMake(0, 0);
	self.collectionView.layer.shadowRadius = 3;
	self.collectionView.layer.shadowOpacity = 0.3;
	self.collectionView.clearsContextBeforeDrawing = YES;
	self.collectionView.clipsToBounds = YES;

//  // iphone 5 support.
//	CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
//	if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//		self.imageBackground.image = [UIImage imageNamed:@"SplashBG-568h.png"];
//	}

    // iphone 4 support.
	CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
	if ([UIScreen mainScreen].scale == 2.f && screenHeight == 480.0f) {
        self.imageLogo.hidden = YES;
    } else {
        self.imageLogo.hidden = NO;
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(autoDetectedDssListChanged) name:kNotificationDssListChanged object:Nil];
    
    self.screenName = @"Local Connections";
}

- (IBAction) actionSkip:(id)sender {
	NSArray *allDss = [[DataController instance] fetchAllAutoDetectedDss];
	
	for (DSServer *dss in allDss) {
		[[DataController instance] deleteDsServer:dss];
	}
	
	[[StartupController instance] performDssConnect:@""];
}

#pragma mark -
#pragma mark Observer for serverlist

- (void) autoDetectedDssListChanged {
	[self.collectionView reloadData];
}

#pragma mark -
#pragma mark UICollectionView delegates stuff

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	//return 20;
	return [[ModelLocator instance].localServerAddresses count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:

- (WelcomeLocalItemCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{	
    WelcomeLocalItemCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"WelcomeLocalItemCell" forIndexPath:indexPath];
	cell.labelName.text = [[ModelLocator instance].localServerAddresses objectAtIndex:indexPath.row];
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString *serverIP = [[ModelLocator instance].localServerAddresses objectAtIndex:indexPath.row];
	// NSString *serverIP = [[ModelLocator instance].localServerAddresses objectAtIndex:0];
	
	DSServer *dss = [[DataController instance] getDssForIpAddressInCoreData:serverIP];
	dss.detectedByBonjour = NO;
	[[DataController instance] saveContext];
	
	NSArray *allDss = [[DataController instance] fetchAllAutoDetectedDss];
	
	for (DSServer *dsServer in allDss) {
		[[DataController instance] deleteDsServer:dsServer];
	}
	
	[[StartupController instance] performDssConnectWithServer:dss];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_welcomeLabel release];
    [_selectLabel release];
    [_skipButton release];
    [super dealloc];
}
@end
