//
//  SettingsDeviceChooseRoomVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceChooseRoomVC.h"
#import "SettingsDeviceRoomsListItemVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "DataController.h"
#import "CustomLabel.h"
#import "SettingsScenesDetailVC.h"

@implementation SettingsDeviceChooseRoomVC

@synthesize scrollview;
@synthesize background;
@synthesize titelLabel;

- (id) initWithCurrentRoom: (DSRoom*)room andCurrentDevice: (DSDevice*)device{
    if (self = [super init]) {
        _currentRoom = room;
        _currentDevice = device;
	}
    
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[[ModelLocator instance] setValue:self forKey:k_VIEW_SETTINGS_CHOOSE_ROOM_DEVICE_LIST];

    // iphone 5 support
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        background.frame = CGRectMake(0, 0, 320, 513);
        [background setImage:[UIImage imageNamed:@"Einst_Card_Ribbon-568h"]];
    }
    
    titelLabel.text = NSLocalizedString(@"key_choose_room", @"");
	items = [NSMutableArray new];
	
	[self buildScrollView];
    
    self.screenName = @"Settings Devices Assign Room";
} 

- (void)buildScrollView {
	for (UIViewController *item in items) {
		[item.view removeFromSuperview];
		[item release];
	}
	[items removeAllObjects];
    
    NSInteger yOffset = 0;
    
	NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
	NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sorted = [[DataController instance].rooms sortedArrayUsingDescriptors:[NSArray arrayWithObjects:orderDescriptor, nameDescriptor, nil]];
    [orderDescriptor release];
    [nameDescriptor release];
	
	for (DSRoom* room in sorted) {
		
		SettingsDeviceRoomsListItemVC *item = [[SettingsDeviceRoomsListItemVC alloc] initWithRoom:room andOffset:yOffset andMaximum:[sorted count] andDevice:_currentDevice];
		item.view.frame = CGRectMake(0, yOffset, item.view.frame.size.width, item.view.frame.size.height);
        if (room == _currentRoom) {
            [item.buttonStepInto.imageView setImage: [UIImage imageNamed:@"Ausw_btn_speichern"]];
        }
        else{
            [item.buttonStepInto.imageView setImage: nil];
        }

		[self.scrollview addSubview:item.view];
		
		yOffset += item.view.frame.size.height;
		[items addObject:item];
	}
    
	self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, yOffset);
}


#pragma mark -
#pragma mark IB Outlets
- (IBAction) actionBack {
	
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings goBack];
	
	[[ModelLocator instance] setValue:nil forKey:k_VIEW_SETTINGS_CHOOSE_ROOM_DEVICE_LIST];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
	
	for (UIViewController *child in items) {
		[child.view removeFromSuperview];
		[child release];
	}
	[items removeAllObjects];
	
    [super dealloc];
}


@end
