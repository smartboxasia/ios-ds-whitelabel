//
//  SettingsDevicesListItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDevicesListItemVC.h"
#import "DSDevice.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "SettingsDeviceDetailVC.h"

@implementation SettingsDevicesListItemVC

@synthesize imageBackground, labelName, buttonStepInto, imageGroupIcon;

- (id) initWithDevice: (DSDevice*) theDevice andDarkBg: (BOOL) isDark {
	if(self = [super init]) {
		device = theDevice;
		hasDarkBackground = isDark;
	}
	
	return self;
}

- (void)setNameLabel {
    //#8467 Do a right justify of the dSID if the name is not available
    if(device.name && [device name].length > 0) {
        self.labelName.text = device.name;
        self.labelName.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    else {
        self.labelName.text = device.identifier;
        self.labelName.lineBreakMode = NSLineBreakByTruncatingHead;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	if(device) {
        [self setNameLabel];
		
		if(hasDarkBackground) {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Gerate_Liste_dunkel.png"];		
		}
		else {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Gerate_Liste_hell.png"];
		}
        
        if([device.group isEqualToString:@"1"]){
            self.imageGroupIcon.image = [UIImage imageNamed:@"lightbulb.png"];	
        }
        else if ([device.group isEqualToString:@"2"]){
            self.imageGroupIcon.image = [UIImage imageNamed:@"shade.png"];	
        }
        else if ([device.group isEqualToString:@"3"]){
            self.imageGroupIcon.image = [UIImage imageNamed:@"blue_on.png"];	
        }
        else if ([device.group isEqualToString:@"4"]){
            self.imageGroupIcon.image = [UIImage imageNamed:@"music.png"];
        }
        else if ([device.group isEqualToString:@"5"]){
            self.imageGroupIcon.image = [UIImage imageNamed:@"television.png"];
        }


	}
	
	
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //#7115 Set the name label when view will appear to show changes which are done on the detail screen
    [self setNameLabel];
}

- (IBAction) actionShowDetail {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[[SettingsDeviceDetailVC alloc] initWithDevice:device]];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
