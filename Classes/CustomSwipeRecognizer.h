//
//  CustomButton.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UISwipeGestureRecognizer.h>

typedef void (^ActionBlock)();

@interface CustomSwipeRecognizer : UISwipeGestureRecognizer {
	ActionBlock _actionBlock;
}

//-(void) handleControlEvent:(UIControlEvents)event withBlock:(ActionBlock) action;

@end
