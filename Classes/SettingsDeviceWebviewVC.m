//
//  SettingsDeviceWebviewVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceWebviewVC.h"
#import "ModelLocator.h"

@interface SettingsDeviceWebviewVC ()

@end

@implementation SettingsDeviceWebviewVC

@synthesize navigationTitleBar;
@synthesize webview;

- (id)initWithTitle: (NSString*) theTitle andTargetUrl: (NSString*) theTargetUrl {
    self = [super init];
    if (self) {
		title = theTitle;
		targetUrl = theTargetUrl;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    // iphone 5 support.
//    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
//        self.webview.frame = CGRectMake(self.webview.frame.origin.x, self.webview.frame.origin.y, 320, 524);
//    }
//    else{
//         self.webview.frame = CGRectMake(self.webview.frame.origin.x, self.webview.frame.origin.y, 320, 436);
//    }

    CGFloat sHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat sWidth = [UIScreen mainScreen].bounds.size.width;
    self.webview.frame = CGRectMake(self.webview.frame.origin.x, self.webview.frame.origin.y, sWidth, sHeight-44);

	self.navigationTitleBar.topItem.title = title;

	NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString: targetUrl] cachePolicy: NSURLRequestReloadIgnoringCacheData timeoutInterval: 60];
    
    [self.webview setAutoresizingMask:(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth )];
	[self.webview loadRequest: request];
    
    self.screenName = @"Settings Device Webview";
}

- (IBAction) actionBack:(id)sender {
	UIViewController *mainVc = [[ModelLocator instance] valueForKey:k_VIEW_MAIN];
	
	[UIView transitionFromView:self.view
                        toView:mainVc.view
                      duration:0.6
                       options: UIViewAnimationOptionTransitionFlipFromRight
                    completion:^(BOOL finished) {
						if (finished) {
							
						}
					}
	 ];
}

- (IBAction) actionOpenInSafari:(id)sender {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString: targetUrl]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
