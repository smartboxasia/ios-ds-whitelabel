//
//  RoomDeviceListItemVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSDevice.h"

@class DSDevice;

@interface RoomDeviceListItemVC : UIViewController {
	UIButton *buttonIcon;
	UIButton *buttonLabel;
    int colorCode;
	BOOL doAnimation;
	DSDevice *device;
	UIImageView *imageBackground;
    UIImageView *imageColorBar;
}

@property (nonatomic, retain) IBOutlet UIButton *buttonIcon;
@property (nonatomic, retain) IBOutlet UIButton *buttonLabel;
@property (nonatomic, retain) IBOutlet UIImageView *imageBackground;
@property (nonatomic, retain) IBOutlet UIImageView *imageColorBar;
@property (assign) int colorCode;
@property (assign) BOOL doAnimation;

- (id)initWithDevice: (DSDevice*) theDevice colorCode:(int) theColor doAnimation:(BOOL) animation;
- (IBAction) actionButtonLabel: (id) sender;
- (IBAction) actionButtonIcon: (id) sender;
- (void) drawBackground;

@end
