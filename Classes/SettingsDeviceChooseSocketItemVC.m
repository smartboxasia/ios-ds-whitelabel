//
//  SettingsDeviceChooseSocketItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceChooseSocketItemVC.h"
#import "DSRoom.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "SettingsRoomsRoomDetailVC.h"
#import "DataController.h"
#import "SettingsRoomsListVC.h"
#import "SettingsScenesListVC.h"

@interface SettingsDeviceChooseSocketItemVC ()

@end

@implementation SettingsDeviceChooseSocketItemVC

@synthesize imageBackground, labelName, buttonStepInto, buttonBig,activityIndicator;

- (id) initWithSocket: (NSString*)socket andDevice: (DSDevice*)device {
	if(self = [super init]) {
		_socket = socket;
        _device = device;
	}
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    activityIndicator.hidden = YES;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(_device) {
        if (_socket) {
			self.labelName.text = _socket;
		}
        else{
            self.labelName.text = @"-";
        }
		
        self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_hell.png"];
		
		if ([_device.socket isEqualToString:_socket]) {
			[self.buttonStepInto.imageView setImage: [UIImage imageNamed:@"Ausw_btn_speichern"]];
		}
		else{
			[self.buttonStepInto.imageView setImage: nil];
		}
	}
	
}


- (IBAction) actionSetSocket {
    if(_device){
        if ([_device.socket isEqualToString:_socket]) {
            //choose the same, just go back
            
        } else {
            activityIndicator.hidden = NO;
            [activityIndicator startAnimating];
            
            if (_socket){
                [[DataController instance] cloudServiceSetSocket:_socket forDevice:_device];
                _device.socket = _socket;
            }else{
                [[DataController instance] cloudServiceDeleteSocketTypeForDevice:_device];
                _device.socket = @"-";
            }
        }
		
		SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
		[settings goBack];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
    [super dealloc];
}

@end
