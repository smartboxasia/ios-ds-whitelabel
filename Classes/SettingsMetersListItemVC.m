//
//  SettingsMetersListItemVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsMetersListItemVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "SettingsMetersDetailVC.h"


@implementation SettingsMetersListItemVC

@synthesize buttonStepInto, imageBackground, labelName;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

- (id) initWithMeter: (DSMeter*) theMeter withDarkBg: (BOOL) isDark andIndex: (NSInteger) _index {
	if(self = [super init]) {
		meter = theMeter;
		hasDarkBackground = isDark;
		index = _index;
		editModeOn = NO;
	}
	
	return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	if(meter) {
		self.labelName.text = [NSString stringWithFormat:@"%@", meter.name];
		
		if(hasDarkBackground) {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_dunkel.png"];		
		}
		else {
			self.imageBackground.image = [UIImage imageNamed:@"Einst_Liste_hell.png"];
		}
	}
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.labelName.text = meter.name;
    [meter addObserver:self forKeyPath:@"name" options:0 context:nil];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [meter removeObserver:self forKeyPath:@"name"];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"name"]) {
		self.labelName.text = meter.name;
	}
}

- (IBAction) actionButtonClicked {
	// Aktivität anzeigen
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[[SettingsMetersDetailVC alloc] initWithMeter:meter]];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

    [super dealloc];
}


@end
