// 
//  DSScene.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "DSScene.h"

@implementation DSScene 

@synthesize dirty;

@dynamic favourit;
@dynamic mainList;
@dynamic name;
@dynamic sceneNo;
@dynamic devices;
@dynamic room;
@dynamic sortOrder;
@dynamic dss;
@dynamic group;

- (id) init {
	self = [super init];
	
	if (self) {
		allObserver = [NSMutableArray new];
	}
	
	return self;
}


- (void) addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context {
	if (!allObserver) {
		allObserver = [NSMutableArray new];
	}
	[super addObserver:observer forKeyPath:keyPath options:options context:context];
	
	NSMutableDictionary *d = [NSMutableDictionary new];
	[d setObject:observer forKey:keyPath];
	[allObserver addObject:d];
}

- (void) removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath {
	for (NSDictionary *dict in allObserver) {
		for (NSString *myKeypath in [dict allKeys]) {
			id theObserver = [dict objectForKey:myKeypath];
			
			if (observer == theObserver && [keyPath isEqualToString:myKeypath]) {
				[allObserver removeObject:dict];
                @try {
                    [super removeObserver:observer forKeyPath:keyPath];
                }
                @catch (NSException *exception) {
                    //just in case
                }
				return;
			}
		}
	}
	
    @try {
        [super removeObserver:observer forKeyPath:keyPath];
    }
    @catch (NSException *exception) {
        //just in case
    }
}

- (void)prepareForDeletion {
	while ([allObserver count] > 0) {
		NSDictionary *dict = [allObserver objectAtIndex:0];
		NSString *keypath = [[dict allKeys] objectAtIndex:0];
		id observer = [dict objectForKey:keypath];
		
		//if ([observer respondsToSelector:@selector(releaseObservation)]) {
		[self removeObserver:observer forKeyPath:keypath];
			//[observer releaseObservation];
		//}
	}
	
	[super prepareForDeletion];
}

@end
