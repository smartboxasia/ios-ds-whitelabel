// 
//  DSMeter.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "DSMeter.h"


@implementation DSMeter 

@synthesize dirty;

@synthesize meteringEnabled;

@dynamic powerConsumption;
@dynamic energyMeterValue;
@dynamic identifier;
@dynamic name;
@dynamic apiVersion;
@dynamic dss;


- (id) init {
	self = [super init];
	
	if (self) {
		allObserver = [NSMutableArray new];
        meteringEnabled = YES;
	}
	
	return self;
}



//new dSUID is 34 chars long, the old dSID is 24 chars long
- (BOOL) isUsingDsuid{
    if (self.identifier.length > 25) {
        return YES;
    }
    else
        return NO;
}


- (void) addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context {
	if (!allObserver) {
		allObserver = [NSMutableArray new];
	}
	[super addObserver:observer forKeyPath:keyPath options:options context:context];
	
	NSMutableDictionary *d = [NSMutableDictionary new];
	[d setObject:observer forKey:keyPath];
	[allObserver addObject:d];
}

- (void) removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath {
	for (NSDictionary *dict in allObserver) {
		for (NSString *myKeypath in [dict allKeys]) {
			id theObserver = [dict objectForKey:myKeypath];
			
			if (observer == theObserver && [keyPath isEqualToString:myKeypath]) {
				[allObserver removeObject:dict];
                @try {
                    [super removeObserver:observer forKeyPath:keyPath];
                }
                @catch (NSException *exception) {
                    //just in case
                }
				return;
			}
		}
	}
	
    @try {
        [super removeObserver:observer forKeyPath:keyPath];
    }
    @catch (NSException *exception) {
        //just in case
    }
}

- (void)prepareForDeletion {
	while ([allObserver count] > 0) {
		NSDictionary *dict = [allObserver objectAtIndex:0];
		NSString *keypath = [[dict allKeys] objectAtIndex:0];
		id observer = [dict objectForKey:keypath];
		
		//if ([observer respondsToSelector:@selector(releaseObservation)]) {
		[self removeObserver:observer forKeyPath:keypath];
			//[observer releaseObservation];
		//}
	}
	
	[super prepareForDeletion];
}

@end
