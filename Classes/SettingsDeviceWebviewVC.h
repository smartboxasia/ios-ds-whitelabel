//
//  SettingsDeviceWebviewVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface SettingsDeviceWebviewVC : GAITrackedViewController {
	NSString *title;
	NSString *targetUrl;
}

@property (nonatomic, retain) IBOutlet UINavigationBar *navigationTitleBar;
@property (nonatomic, retain) IBOutlet UIWebView *webview;

- (id)initWithTitle: (NSString*) theTitle andTargetUrl: (NSString*) theTargetUrl;
- (IBAction) actionBack:(id)sender;
- (IBAction) actionOpenInSafari:(id)sender;

@end
