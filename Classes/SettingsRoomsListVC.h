//
//  SettingsRoomsListVC.h
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@class CustomLabel;

@interface SettingsRoomsListVC : GAITrackedViewController {
	UIScrollView *scrollview;
	NSMutableArray *items;
	
	bool editmodeOn;
    IBOutlet CustomLabel *titleLabel;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (void) buildScrollview;

- (IBAction) actionBack;
- (void) toggleEditmode;
- (void) displayEditButton;
- (void) hideEditButton;

- (void) arrangeItems;
- (void) magneticPointHasChanged: (int) from toIndex: (int) to;

@end
