//
//  InfoBarTemperatureVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "InfoBarTemperatureVC.h"
#import "ModelLocator.h"

@implementation InfoBarTemperatureVC

@synthesize labelTemperature,labelCity;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([[ModelLocator instance] valueForKey:k_STANDARD_OUTSIDE_TEMP]) {
		self.labelTemperature.text = [NSString stringWithFormat:@"%@° C", [[ModelLocator instance] valueForKey:k_STANDARD_OUTSIDE_TEMP]];
	}
	else {
		self.labelTemperature.text = @"--° C";
	}
    
    if ([[ModelLocator instance] valueForKey:k_STANDARD_CITY]) {
		self.labelCity.text = [[ModelLocator instance] valueForKey:k_STANDARD_CITY];
	}
	else {
		self.labelCity.text = @"--";
	}
    
	if ([[ModelLocator instance] valueForKey:k_STANDARD_CONDITION_TEMP]) {
		conditionImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[ModelLocator instance] valueForKey:k_STANDARD_CONDITION_TEMP]]];
	}
	else {
		conditionImageView.image = [UIImage imageNamed:@"Mostly Cloudy.png"];
	}
    [[ModelLocator instance] addObserver:self forKeyPath:k_STANDARD_OUTSIDE_TEMP options:0 context:nil];
	[[ModelLocator instance] addObserver:self forKeyPath:k_STANDARD_CITY options:0 context:nil];
    [[ModelLocator instance] addObserver:self forKeyPath:k_STANDARD_CONDITION_TEMP options:0 context:nil];

}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    @try {
            [[ModelLocator instance] removeObserver:self forKeyPath:k_STANDARD_OUTSIDE_TEMP];
	[[ModelLocator instance] removeObserver:self forKeyPath:k_STANDARD_CONDITION_TEMP];
    [[ModelLocator instance] removeObserver:self forKeyPath:k_STANDARD_CITY];
    }
    @catch (NSException *exception) {
        //just in case
    }

}

#pragma mark -
#pragma mark Observer responder

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([keyPath isEqualToString:k_STANDARD_OUTSIDE_TEMP]) {
            self.labelTemperature.text = [NSString stringWithFormat:@"%@° C", [[ModelLocator instance] valueForKey:k_STANDARD_OUTSIDE_TEMP]];
        }
        
        if ([keyPath isEqualToString:k_STANDARD_CONDITION_TEMP]) {
            conditionImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[ModelLocator instance] valueForKey:k_STANDARD_CONDITION_TEMP]]];
        }
        if ([keyPath isEqualToString:k_STANDARD_CITY]) {
            self.labelCity.text = [[ModelLocator instance] valueForKey:k_STANDARD_CITY];
        }
    });
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) prepareDealloc {

}

- (void)dealloc {
    [super dealloc];
}


@end
