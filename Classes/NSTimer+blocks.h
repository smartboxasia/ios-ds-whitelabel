//
//  NSTimer+blocks.h
//  touch-n-switch
//
//  Created by mathias on 22.11.10.
//  Copyright 2010 Granny&Smith Technikagentur GmbH & Co. KG. All rights reserved.
//

#import <Foundation/NSTimer.h>


@interface NSTimer (PSYBlockTimer)

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats usingBlock:(void (^)(NSTimer *timer))fireBlock;
+ (NSTimer *)timerWithTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats usingBlock:(void (^)(NSTimer *timer))fireBlock;

@end
