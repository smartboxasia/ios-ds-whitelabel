//
//  SettingsDeviceDetailPrivacyButtonVC.m
//  SMARTBOX iOS
//
//  Created by zynick on 30/5/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

#import "SettingsDeviceDetailPrivacyButtonVC.h"
#import "SettingsVC.h"
#import "ModelLocator.h"
#import "SettingsDeviceDetailPrivacyFacebookVC.h"

@interface SettingsDeviceDetailPrivacyButtonVC ()

@end

@implementation SettingsDeviceDetailPrivacyButtonVC

- (id)init {
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction) actionButtonClicked:(id)sender {
	SettingsVC *settings = (SettingsVC*)[[ModelLocator instance] valueForKey:k_VIEW_SETTINGS];
	[settings showSubGroup:[[SettingsDeviceDetailPrivacyFacebookVC alloc] init]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
